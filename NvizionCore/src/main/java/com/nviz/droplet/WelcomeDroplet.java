package com.nviz.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class WelcomeDroplet extends DynamoServlet {
	public static final String OPARAM_OUTPUT = "output";
	public static final String OPARAM_MESSAGE = "greeting";
	
	private String mGreetings;
	
	public String getGreetings() {
		return mGreetings;
	}

	public void setGreetings(String greetings) {
		mGreetings = greetings;
	}



	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		    throws ServletException, IOException {
		pRequest.setParameter(OPARAM_MESSAGE, getGreetings());
		pRequest.serviceLocalParameter(OPARAM_OUTPUT, pRequest, pResponse);
	}
}
