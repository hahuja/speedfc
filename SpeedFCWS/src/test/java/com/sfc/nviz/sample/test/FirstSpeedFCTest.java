package com.sfc.nviz.sample.test;

import java.io.IOException;

import javax.servlet.ServletException;

import junit.framework.TestCase;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import atg.nucleus.Nucleus;
import atg.nucleus.NucleusTestUtils;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;

public class FirstSpeedFCTest extends TestCase {

	Logger mLogger = Logger.getLogger(this.getClass());
	Nucleus mNucleus = null;

	// ------------------------------------
	/***
	 * Starts Nucleus. Fails the test if there is a problem starting Nucleus.
	 */
	@Override
	public void setUp() {
		mLogger.log(Level.INFO, "Start Nucleus.");
		try {
			System.setProperty("derby.locks.deadlockTrace", "true");
			System.setProperty("-Datg.dynamo.home", "C:/NVIZ/ATG/ATG10.1/home");
			mNucleus = NucleusTestUtils.startNucleusWithModules(new String[] {
					"DAF.Deployment", "DPS", "NvizionReferenceStore.SpeedFCDust", "NvizionReferenceStore.Integration"  }, this.getClass(), this.getClass()
					.getName(), "/atg/deployment/DeploymentRepository");
		} catch (ServletException e) {
			fail(e.getMessage());
		}

	}

	// ------------------------------------
	/***
	 * If there is a running Nucleus, this method shuts it down. The test will
	 * fail if there is an error while shutting down Nucleus.
	 */
	@Override
	public void tearDown() {
		mLogger.log(Level.INFO, "Stop Nucleus");
		if (mNucleus != null) {
			try {
				NucleusTestUtils.shutdownNucleus(mNucleus);
			} catch (ServiceException e) {
				fail(e.getMessage());
			} catch (IOException e) {
				fail(e.getMessage());
			}
		}
	}

	// ----------------------------------------
	/***
	 * Resolves a component that is defined within the ATG platform and is not
	 * specifically part of this test's configpath. Confirms that Nucleus can
	 * start given a set of modules and a properly set DYNAMO_HOME environment
	 * variable. (ex: DYNAMO_HOME=/home/user/ATG/ATG9.0/home)
	 */
	public void testResolveComponentWithNucleus() {
		assertNotNull(mNucleus);
		MutableRepository catalog = (MutableRepository) mNucleus.resolveName("/atg/deployment/DeploymentRepository");
		assertNotNull("DeploymentRepository should not be null.", catalog);
		MutableRepository profile = (MutableRepository) mNucleus.resolveName("/atg/userprofiling/ProfileAdapterRepository");
		// Good enough for this test.
		// Don't want to disturb any data that might be in this repository.
	}
}
