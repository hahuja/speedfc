<%-- 
  Handles search queries when ATG Search is not installed, by performing a Repository Search.
  
  Required Parameters:
    None.
  
  Optional Parameters:
    None.
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>
  <dsp:importbean bean="/atg/dynamo/servlet/RequestLocale" var="requestLocale"/>
  <dsp:importbean bean="/atg/dynamo/droplet/multisite/CartSharingSitesDroplet" />
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
  <dsp:importbean bean="/atg/multisite/Site"/>

  <dsp:getvalueof var="contextRoot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>
  <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="Site.defaultPageSize"/>
  <dsp:getvalueof var="errorURL" param="redirectURLOnEmptySearch"/>
  
  <fmt:message var="searchInput" key="common.search.input"/>
  
  <%-- 
    Search form displayed at the top of each top level page when ATG Search is not installed
  --%>
  <dsp:form method="post" id="simpleSearch" formid="simplesearchform">
    
    <%-- Set Form inputs --%>
    <dsp:input bean="ProductSearch.errorURL" type="hidden" value="${errorURL}"/>
    <dsp:input bean="ProductSearch.successURL" type="hidden" value="${contextRoot}/search/searchResults.jsp"/>    
    <dsp:input bean="ProductSearch.currentPage" type="hidden" value="1"/>
    <dsp:input bean="ProductSearch.resultsPerPage" type="hidden" value="${pageSize}"/>    
    <dsp:input bean="ProductSearch.dummySearchText" type="hidden" value="${searchInput}"/>
    <dsp:input bean="ProductSearch.searchPerformedThisRequest" type="hidden" value="${true}"/>
  
    <dsp:input iclass="atg_store_searchInput enterSubmit" bean="ProductSearch.searchInput" 
               type="text" name="atg_store_searchInput" value="${searchInput}" title="${searchInput}" 
               id="atg_store_searchInput" autocomplete="off"/>
    
    <%--
      Check if the current site has a shared shopping, other sites will be editable check boxes. If site
      doesn't have a shareable, then search within the current site context.
    --%>
	<%--
      CartSharingSitesDroplet returns a collection of sites that share the shopping
	  cart shareable (atg.ShoppingCart) with the current site.
	  You may optionally exclude the current site from the result.

      Input Parameters:
        excludeInputSite - Should the returned sites include the current
   
      Open Parameters:
        output - This parameter is rendered once, if a collection of sites
                 is found.
   
      Output Parameters:
        sites - The list of sharing sites.
    --%>
    <div id="atg_store_searchStoreSelect">
      <dsp:droplet name="CartSharingSitesDroplet">
        <dsp:param name="excludeInputSite" value="true"/>
        
        <dsp:oparam name="output">
          <dsp:getvalueof var="sites" param="sites"/>
          
          <dsp:droplet name="ForEach">
          <dsp:param name="array" param="sites"/>
          <dsp:setvalue param="site" paramvalue="element"/>
          
            <%-- current site, selected and disabled --%>      
            <dsp:oparam name="outputStart">
    
              <dsp:input bean="ProductSearch.siteIds" type="hidden" beanvalue="Site.id"/>
            </dsp:oparam>

            <%-- other sites --%>
            <dsp:oparam name="output">
              <div>
                <dsp:input bean="ProductSearch.siteIds" type="checkbox" paramvalue="site.id" id="otherStore" checked="false"/>
                <label for="otherStore">
                  <fmt:message key="search.otherStoresLabel">
                    <fmt:param>
                      <dsp:valueof param="site.name"/>
                    </fmt:param>
                  </fmt:message>
                </label>  
              </div>    
            </dsp:oparam>
          </dsp:droplet>
        </dsp:oparam>
        
        <dsp:oparam name="empty">
          <dsp:input bean="ProductSearch.siteIds" type="hidden" beanvalue="Site.id"/>
        </dsp:oparam>
        
      </dsp:droplet>
    </div>
    
    <%-- Submit button --%>
    <fmt:message var="submitText" key="search_simpleSearch.submit"/>
      <span class="atg_store_smallButton">
        <dsp:input bean="ProductSearch.search" type="submit" id="atg_store_searchSubmit" value="${submitText}" title="${submitText}"/>
      </span>
    <dsp:input bean="ProductSearch.search" type="hidden" value="search"/>
    
  </dsp:form>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/search/gadgets/simpleSearch.jsp#1 $$Change: 683854 $ --%>
