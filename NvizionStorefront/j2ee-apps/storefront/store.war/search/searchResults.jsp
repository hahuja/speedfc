<%-- 
  This page lays out the elements that make up the search results page.
    
  Required Parameters:
    q
      The search question.  (Required when doing a query search)
   
  Optional Parameters:
    p
      The page number
    pageSize
      The number of results to display per page.
    sId
      The ID of a site whose content should also be included in the search.  There may be multiple.
    viewAll 
      Indicates if all products should be displayed on this page as opposed to pageSize products.
--%>
<dsp:page>
  <dsp:getvalueof var="question" param="q" />
  <dsp:getvalueof var="p" param="p" />
  <dsp:getvalueof var="pageSize" param="pageSize" />
  <dsp:getvalueof var="viewAll" param="viewAll" />
  
  <crs:pageContainer divId="atg_store_searchResultsIntro" index="false" follow="false"
                     bodyClass="category atg_store_searchResults atg_store_leftCol">
    <jsp:body>
      <%-- Title --%>
      <div id="atg_store_contentHeader">
        <h2 class="title">
          <fmt:message key="search_searchResults.title"/>
        </h2>
      </div>
          
      <%-- Main Content --%>
      <div class="atg_store_main">
        <div id="ajaxContainer">
          <div divId="ajaxRefreshableContent">
            <%-- Render the search results. --%>
            <dsp:include page="/search/searchResultsRenderer.jsp">
              <dsp:param name="q" value="${question}"/>
              <dsp:param name="p" value="${p}"/>
              <dsp:param name="pageSize" value="${pageSize}"/>
              <dsp:param name="viewAll" value="${viewAll}"/>
            </dsp:include>
          </div>
              
          <div name="transparentLayer" id="transparentLayer"></div>
       
          <div name="ajaxSpinner" id="ajaxSpinner"></div>
        </div>
      </div>
          
      <%-- Left side bar links --%>
      <div class="aside">
        <%-- Rendering promotions--%>
        <dsp:include page="/global/gadgets/targetingRandom.jsp">
          <dsp:param name="targeter" bean="/atg/registry/Slots/CategoryPromotionContent1"/>
          <dsp:param name="renderer" value="/promo/gadgets/promotionalContentTemplateRenderer.jsp"/>
          <dsp:param name="elementName" value="promotionalContent"/>
        </dsp:include>
        <dsp:include page="/global/gadgets/targetingRandom.jsp">
          <dsp:param name="targeter" bean="/atg/registry/Slots/CategoryPromotionContent2"/>
          <dsp:param name="renderer" value="/promo/gadgets/promotionalContentTemplateRenderer.jsp"/>
          <dsp:param name="elementName" value="promotionalContent"/>
        </dsp:include>
      </div>

      <%-- Browser back button suport --%>
      <script type="text/javascript">
        dojo.require("dojo.back");
        dojo.back.init();
        dojo.back.setInitialState(new HistoryState(""));
       
      </script>
        
    </jsp:body>
  </crs:pageContainer>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/search/searchResults.jsp#1 $$Change: 683854 $--%>