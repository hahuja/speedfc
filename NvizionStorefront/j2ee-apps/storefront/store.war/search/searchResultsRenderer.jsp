<%-- 
  This JSP executes and renders the search results
  
  Required Parameters:
    q
      The search question.
   
  Optional Parameters:
    p
      The page number
      
    pageSize
      The number of results to display per page.
    
    sid
      The ID of a site whose content should also be included in the search. There may be multiple.
    viewAll
      Indicates all search results should be displayed on this page
--%>
<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1" %>
<dsp:page>

  <dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>
  <dsp:importbean bean="/atg/multisite/Site"/>

  <dsp:getvalueof var="question" param="q" />
  <dsp:getvalueof var="p" param="p" />
  <dsp:getvalueof var="pageSize" param="pageSize" />
  <dsp:getvalueof var="viewAll" param="viewAll" />
  <dsp:getvalueof var="siteIds" value="${paramValues.sid}"/>
  <dsp:getvalueof var="searchPerformedThisRequest" bean="ProductSearch.searchPerformedThisRequest"/>

  <%-- Execute the search if we have a search query --%>
  <div id="atg_store_mainHeader">
    <c:if test="${not empty question}">
    
      <%-- Execute a search if it has not been executed in this request (e.g load from a bookmark) --%>
      <c:if test="${not searchPerformedThisRequest}">
        <c:choose>
          <c:when test="${viewAll eq 'true'}">
            <dsp:setvalue bean="ProductSearch.resultsPerPage" value="-1"/>
            <dsp:setvalue bean="ProductSearch.currentPage" value="-1"/>
          </c:when>
          <c:otherwise>
            <%-- Use the pageSize param, if it's empty use the site property, if it's still empty, default to 12 --%>
            <c:if test="${empty pageSize}">
              <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="Site.defaultPageSize"/>

              <c:if test="${empty pageSize}">
                <c:set var="pageSize" value="12"/>
              </c:if>
            </c:if>

            <dsp:setvalue bean="ProductSearch.resultsPerPage" value="${pageSize}"/>
            <dsp:setvalue bean="ProductSearch.currentPage" value="${empty p ? '1' : p}"/>
          </c:otherwise>
        </c:choose>
    
        <dsp:setvalue bean="ProductSearch.successURL" value=""/>
        <dsp:setvalue bean="ProductSearch.dummySearchText" value="Search"/>
        <dsp:setvalue bean="ProductSearch.searchInput" value="${question}"/>
        <dsp:setvalue bean="ProductSearch.siteIds" value="${siteIds}"/>
        <dsp:setvalue bean="ProductSearch.search" value=""/>
      </c:if>
      
      <%-- Check to see if there are any results --%>
      <dsp:getvalueof var="resultArray" bean="ProductSearch.searchResults"/>
      <dsp:getvalueof var="searchResultsSize" bean="ProductSearch.searchResultsSize"/>
      
      <div class="atg_store_searchResultsCount">
        <c:choose>
          <c:when test="${searchResultsSize == 1}">
            <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.oneResultFor"/>
          </c:when>
          <c:otherwise>
            <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.resultFor"/>
          </c:otherwise>
        </c:choose>
        <c:out value="${searchResultsSize}"/>&nbsp;<c:out value="${resultsMessage}"/>&nbsp;<span class="searchTerm"><dsp:valueof value="${question}" /></span>
      </div>
    </c:if>
  </div>

  <%-- Display the results --%>
  <c:choose>
    <c:when test="${not empty resultArray}">
      <div id="atg_store_catSubProdList">
        <dsp:include page="/global/gadgets/productListRange.jsp">
          <dsp:param name="productArray" value="${resultArray}"/>
          <dsp:param name="productDivName" value="atg_store_prodList"/>
          <dsp:param name="isSimpleSearchResults" value="true"/>
          <dsp:param name="p" value="${p}"/>
          <dsp:param name="searchResultsSize" value="${searchResultsSize}"/>
          <dsp:param name="viewAll" value="${viewAll}"/>
          <dsp:param name="displaySiteIndicator" value="true"/>
          <dsp:param name="mode" value="name"/>
        </dsp:include>
      </div>
    </c:when>
    <c:otherwise>
      <crs:messageContainer optionalClass="atg_store_noMatchingItem"
        titleKey="facet_facetSearchResults.noMatchingItem"/>
    </c:otherwise>
  </c:choose>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/search/searchResultsRenderer.jsp#1 $$Change: 683854 $--%>
