<%--
  Renders a facet removal link when javascript is disabled.
  
  Required Parameters:
    currentFacetHolder
      A FacetValue object which represents the currently selected facet
  
  Optional Parameters:
    categoryId
      The current categoryId
--%>
<dsp:page>

  <dsp:importbean bean="/atg/commerce/search/refinement/RefinementValueDroplet"/>

  <dsp:getvalueof var="facetValueUrl" vartype="java.lang.String" bean="/OriginatingRequest.requestURI"/>
  <dsp:getvalueof var="categoryId" param="categoryId"/>

  <div class="atg_store_facetsGroup" id="facet_${currentFacetHolder.facet.id}">
    
    <%-- Refinement name (e.g Price) --%>
    <h5>
      <fmt:message key="${currentFacetHolder.facet.label}"/>
    </h5>

    <dsp:getvalueof var="facetingProperty" vartype="java.lang.String"
                    param="currentFacetHolder.facet.refinementElement.property"/>

    <dsp:getvalueof var="facetingProperty" vartype="java.lang.String"
                    param="currentFacetHolder.facet.refinementElement.property"/>

    <%--
      Used to get human readable strings from a facet. (e.g 50-100 or Red)
           
      Input Parameters:
        refinementValue - The facet
              
        refinementId - Facet id
              
        locale - The desired locale
            
        Open Parameters:
          output - Rendered when there are no errors.
              
        Output Parameters:
          displayValue - The facet refinements display value
    --%>
    <dsp:droplet name="RefinementValueDroplet">
      <dsp:param name="refinementValue" value="${currentFacetHolder.value}"/>
      <dsp:param name="refinementId" value="${currentFacetHolder.facet.id}"/>
      <dsp:param name="locale" bean="/atg/userprofiling/Profile.PriceList.locale"/>
            
      <dsp:oparam name="output">
        <dsp:getvalueof var="displayValue" param="displayValue"/>
      </dsp:oparam>
    </dsp:droplet>  

    <%--
      Generates a FacetTrail bean based on the input parameters
   
      Input Parameters:
       trail - The facet trail string to be converted to a bean
       refineConfig - the RefineConfig to use
    
      Open Parameters:
        output - Serviced when no errors occur
    
      Output Parameters:
        facetTrail - The generated FacetTrail bean.  
    --%>
    <dsp:droplet name="CommerceFacetTrailDroplet">
      <dsp:param name="trail" bean="FacetSearchTools.facetTrail"/>
      <dsp:param name="removeFacet" value="${currentFacetHolder}"/>
          
      <dsp:oparam name="output">
        <dsp:getvalueof var="facetStr" param="facetTrail.trailString"/>
      </dsp:oparam>
    </dsp:droplet>

    <dsp:getvalueof var="originatingRequestURL" 
                    vartype="java.lang.String" bean="/OriginatingRequest.requestURL"/>
    <c:set var="url" value="${originatingRequestURL}"/>
    <c:set var="facetTrailVar" value="${facetStr}"/>
    <%@include file="/navigation/gadgets/navLinkHelper.jspf" %>

    <div id="facetoptions_${currentFacetHolder.facet.id}">
      <ul>
        <li class="remove">
          <fmt:message var="toolTip" key="facet_facetTrailSimple.removeFacetTitle"/>
          <a href="${url}" title="${toolTip}"><c:out value="${displayValue}"/></a>
        </li>
      </ul>
  </div>
</div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/facet/gadgets/facetObject.jspf#1 $$Change: 683854 $--%>
