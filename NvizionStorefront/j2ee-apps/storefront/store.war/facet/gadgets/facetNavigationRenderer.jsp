<%--
  Renders facet links when Javascript is disabled
  
  Required Parameters:
    q
      The search question.    
  
  Optional Parameters:
    addFacet
      Specifies a new facet which should be applied, and added to the facet trail.
    facetOrder
      Specifies the order of facets
--%>
<dsp:page>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  <dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet"/>
  <dsp:importbean bean="/atg/store/droplet/FacetDisplayDroplet"/>

  <dsp:getvalueof var="question" param="question" />
  <dsp:getvalueof var="facetValueUrl" vartype="java.lang.String" bean="/OriginatingRequest.requestURI"/>
  <dsp:getvalueof var="facetHolders" bean="FacetSearchTools.facets"/>
  <dsp:getvalueof var="addFacet" param="addFacet"/>

  <dsp:getvalueof var="FSTfacetTrail" bean="FacetSearchTools.facetTrail"/>
  <c:if test="${empty FSTfacetTrail && not empty addFacet}">
    <dsp:setvalue bean="FacetSearchTools.facetTrail" value="${addFacet}"/>
  </c:if>

  <script type="text/javascript">
    //dojo.require("dijit.layout.ContentPane");
    var moreLable = "<fmt:message key='facet.panel.more'/>";
    var lessLable = "<fmt:message key='facet.panel.less'/>";
    var removeFacetLabel = "<fmt:message key='facet_facetTrailSimple.removeFacetTitle'/>";
    var filterByLabel = "<fmt:message key='facet_facetRenderer.title.filterBy'/>";
  </script>
  
  <div dojoType="dijit.layout.ContentPane" id="facetOptions" class="atg_store_facetsGroup_options">
    
    <%--
      Generates a FacetTrail bean based on the input parameters
   
      Input Parameters:
        trail - The facet trail string to be converted to a bean
        refineConfig - the RefineConfig to use
    
      Open Parameters:
        output - Serviced when no errors occur
    
      Output Parameters:
        facetTrail - The generated FacetTrail bean.  
    --%>
    <dsp:droplet name="CommerceFacetTrailDroplet">
      <dsp:param name="trail" bean="FacetSearchTools.facetTrail"/>
      <dsp:param name="refineConfig" param="catRC"/>
      <dsp:param name="addFacet" param="addFacet"/>
      
      <dsp:oparam name="error">
        <fmt:message key="common.facetSrchErrorMessage"/>
      </dsp:oparam>

      <dsp:oparam name="output">
        <dsp:getvalueof var="currentTrail" param="facetTrail"/>
      </dsp:oparam>
    </dsp:droplet>
    
    <%--
      Use the FacetDisplayDroplet to order facets by their priority.
    
      Input Parameters:
        appliedFacets - The currently applied facets i.e facetTrail
    
      Open Parameters:
        output - Rendered if there are no errors
    
      Output Parameters:
        facetOptions - A List of FacetHolders and FacetValues
    --%>
    <dsp:droplet name="FacetDisplayDroplet">
      <dsp:param name="appliedFacets" value="${currentTrail}" />
      <dsp:oparam name="output">
        <dsp:getvalueof var="facetOptions" param="facetOptions" />
        <dsp:getvalueof var="appliedFacetCount" param="appliedFacetCount" />
      </dsp:oparam>
    </dsp:droplet> 
    
    <%-- Render facet links using the output from FacetDisplayDroplet --%>
    <c:forEach var="currentFacetHolder" items="${facetOptions}" varStatus="status">
      <c:choose>
        <%-- UNSELECTED FACET --%> 
        <c:when test="${currentFacetHolder.class.name == 'atg.repository.search.refinement.FacetHolder'}">
          <%@include file="facetsRenderer.jspf" %>  
        </c:when>
        
        <%-- CURRENTLY SELECTED FACET (instanceof FacetValue)--%>          
        <c:otherwise>
          <%-- If there is only 1 facet selected dont include the q_facetTrail parameter  --%>
          <c:set var="doNotIncludeFacetTrailParam" value="${appliedFacetCount == 1}"/>
          <%@ include file="facetObject.jspf" %>
          <c:set var="doNotIncludeFacetTrailParam" value="${false}"/>
        </c:otherwise>  
      </c:choose>
    </c:forEach> 
    
  </div>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/facet/gadgets/facetNavigationRenderer.jsp#1 $$Change: 683854 $--%>