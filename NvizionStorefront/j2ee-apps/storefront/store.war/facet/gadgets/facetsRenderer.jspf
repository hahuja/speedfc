<%--
  Renders a facet apply link when javascript is disabled.
  
  Required Parameters:
    currentFacetHolder
      A FacetHolder object which represents the unapplied facet
  
  Optional Parameters:
    categoryId
      The current categoryId
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/commerce/search/refinement/RefinementValueDroplet"/>

  <div class="atg_store_facetsGroup" id="facet_${currentFacetHolder.facet.id}">
    
    <%-- Refinement name (e.g Price) --%>
    <h5>
      <fmt:message key="${currentFacetHolder.facet.label}"/>
    </h5>

    <c:if test="${not empty currentFacetHolder.facetValueNodes}">
      <div id="facetoptions_${currentFacetHolder.facet.id}">
        <ul>
          <c:forEach var="currentFacetValueNode" items="${currentFacetHolder.facetValueNodes}">

            <%--
              Retrieve the current category repository item from the categoryId string.
  
              Input Parameters:
                id - The categoryId string
  
              Open Parameters:
                output - Rendered when there are no errors
    
              Output Parameters:
                element - The category repository item
            --%>
            <dsp:droplet name="CategoryLookup">
              <dsp:param name="id" param="categoryId"/>
              <dsp:oparam name="error">
                <dsp:valueof param="errorMsg"/>
              </dsp:oparam>
              <dsp:oparam name="output">
                <dsp:setvalue param="catRC" paramvalue="element.refineConfig"/>
                <dsp:getvalueof var="catRC" param="element.refineConfig"/>
              </dsp:oparam>
            </dsp:droplet>

            <%--
              Generates a FacetTrail bean based on the input parameters
   
              Input Parameters:
                trail - The facet trail string to be converted to a bean
                refineConfig - the RefineConfig to use
    
              Open Parameters:
                output - Serviced when no errors occur
    
              Output Parameters:
                facetTrail - The generated FacetTrail bean.  
            --%>
            <dsp:droplet name="CommerceFacetTrailDroplet">
              <dsp:param name="trail" bean="FacetSearchTools.facetTrail"/>
              <dsp:param name="addFacet" value="${currentFacetValueNode.facetValue}"/>
              <dsp:param name="refineConfig" param="catRC"/>
              <dsp:oparam name="output">
                <dsp:getvalueof var="facetUrl" param="facetTrail.trailString"/>
              </dsp:oparam>
            </dsp:droplet>

            <c:set var="qty" value="${currentFacetValueNode.facetValue.matchingDocsCount}"/>
            <dsp:getvalueof var="trailsize" value="${fn:length(relatedCategories)}"/>
            <dsp:getvalueof var="facetingProperty" vartype="java.lang.String"
                            param="currentFacetValueNode.facetValue.facet.refinementElement.property"/>

            <%--
              Used to get human readable strings from a facet. (e.g 50-100 or Red)
           
              Input Parameters:
                refinementValue - The facet
              
                refinementId - Facet id
              
                locale - The desired locale
            
              Open Parameters:
                output - Rendered when there are no errors.
              
              Output Parameters:
                displayValue - The facet refinements display value
            --%>
            <dsp:droplet name="RefinementValueDroplet">
              <dsp:param name="refinementValue" value="${currentFacetValueNode.facetValue.value}"/>
              <dsp:param name="refinementId" value="${currentFacetHolder.facet.id}"/>
              <dsp:param name="locale" bean="/atg/userprofiling/Profile.PriceList.locale"/>
            
              <dsp:oparam name="output">
                <dsp:getvalueof var="displayValue" param="displayValue"/>
              
                <li>
                  <dsp:getvalueof var="originatingRequestURL" vartype="java.lang.String" bean="/OriginatingRequest.requestURL"/>
                  <c:set var="url" value="${originatingRequestURL}"/>                
                  <c:set var="facetTrailVar" value="${facetUrl}"/>
                  <%@include file="/navigation/gadgets/navLinkHelper.jspf" %>
    
                  <fmt:message var="filterBy" key="facet_facetRenderer.title.filterBy"/>
                  <fmt:message var="facetLabel" key="${currentFacetHolder.facet.label}"/>
                  <a href="${url}" title="${filterBy}&nbsp;${facetLabel}">${displayValue} (${qty})</a>
                </li>    
              </dsp:oparam>
            </dsp:droplet>
          
          </c:forEach>
        </ul>

      </div>
    </c:if>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/facet/gadgets/facetsRenderer.jspf#1 $$Change: 683854 $--%>


