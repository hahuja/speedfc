<%--
  Renders product details content and hidden forms
  
  Page includes:
    /mobile/browse/productDetailsContainer.jsp
    /mobile/browse/addToCartForm.jsp
    /mobile/browse/updateCartForm.jsp
    /mobile/browse/gadgets/emailMeForm.jsp
  
  Required parameters:
    product
      the product to display
    picker
      the path to the JSP that will render buttons for this product
    
  Optional parameters:
    selectedSku
      The currently selected SKU. If present, signifies that this is a single-SKU product
    selectedQty
      The currently selected quantity
    selectedSize
      The currently selected size
    selectedColor
      The currently selected color
    availableSizes
      Available sizes for this product
    availableColors
      Available colors for this product
    oneSize
      If true, this product only has one size
    oneColor
      If true, this product only has one color
    ciId 
      the id of commerce item being edited. This parameter is only present when editing the cart.
--%>

<dsp:page>
  <fmt:message var="titleString" key="mobile.productDetails.pageTitle"/>
  <crs:mobilePageContainer titleString="${titleString}">
    <jsp:attribute name="modalContent">
      <dsp:include page="gadgets/emailMeForm.jsp">
        <dsp:param name="productId" param="product.repositoryId"/>
        <dsp:param name="selectedSkuId" param="selectedSku.repositoryId"/>
      </dsp:include>
    </jsp:attribute>
    <jsp:body>
      <dsp:importbean bean="/atg/commerce/catalog/ProductBrowsed"/>

      <dsp:getvalueof var="missingProductId" vartype="java.lang.String" bean="/atg/commerce/order/processor/SetProductRefs.substituteDeletedProductId"/>
      <c:choose>
        <c:when test="${missingProductId != param.product.repositoryId}">
          <%--
                  Notify anyone who cares that the current product has been viewed.
                  This droplet sends a special 'ViewItemEvent' JMS message.
                  This event is required in order for the productId to be automatically
                  placed in the page body by the auto-tagging code for recommendations.
                  The ProductTrackingCodeProcessor takes care of including the productId
                  in the page body.

                  Input parameters:
                    eventobject
                      Specifies a product browsed.

                  Output parameters:
                    None.

                  Open parameters:
                    None.
                --%>
          <dsp:droplet name="ProductBrowsed">
            <dsp:param name="eventobject" param="product"/>
          </dsp:droplet>
      
          <%-- Quantity defaults to 1 --%>
          <c:if test="${empty param.selectedQty}">
            <dsp:setvalue param="selectedQty" value="1"/>
          </c:if>
      
          <dsp:getvalueof var="ciId" param="ciId"/>

          <dsp:include page="json/getSkuJSON.jsp">
            <dsp:param name="product" param="product"/>
          </dsp:include>

          <div id="mobile_store_productDetailsContainer">
            <dsp:include page="productDetailsContainer.jsp">
              <dsp:param name="product" param="product"/>
              <dsp:param name="picker" param="picker"/>
              <dsp:param name="selectedQty" param="selectedQty"/>
              <dsp:param name="selectedSize" param="selectedSize"/>
              <dsp:param name="selectedColor" param="selectedColor"/>
              <dsp:param name="selectedSku" param="selectedSku"/>
              <dsp:param name="availableSizes" param="availableSizes"/>
              <dsp:param name="availableColors" param="availableColors"/>
              <dsp:param name="oneSize" param="oneSize"/>
              <dsp:param name="oneColor" param="oneColor"/>
              <dsp:param name="isUpdateCart" value="${!empty ciId}"/>
            </dsp:include>
          </div>

          <c:choose>
            <c:when test="${empty ciId}">
              <%-- Add to cart regularly --%>
              <dsp:include page="gadgets/addToCartForm.jsp">
                <dsp:param name="productId" param="product.repositoryId"/>
                <dsp:param name="selectedQty" param="selectedQty"/>
                <dsp:param name="selectedSkuId" param="selectedSku.repositoryId"/>
              </dsp:include>
            </c:when>
            <c:otherwise>
              <%-- Update cart --%>
              <dsp:include page="gadgets/updateCartForm.jsp">
                <dsp:param name="productId" param="product.repositoryId"/>
                <dsp:param name="selectedQty" param="selectedQty"/>
                <dsp:param name="selectedSku" param="selectedSku"/>
                <dsp:param name="ciId" value="${ciId}"/>
              </dsp:include>
            </c:otherwise>
          </c:choose>

          <%-- Initialize the related items slider --%>
          <script>
            $(document).ready(function(){
              initRelatedItemsSlider();
            });
          </script>
      
        </c:when>
        <c:otherwise>
          <%-- this is the missing product --%>
        </c:otherwise>
      </c:choose>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/productDetail.jsp#3 $$Change: 692002 $--%>
