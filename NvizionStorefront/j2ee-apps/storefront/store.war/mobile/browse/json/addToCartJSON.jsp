<%--
  Renders appropriate JSON for adding an item to the cart.
  
  Optional Parameters:
    errorAddToCart
      if present, signifies that an error occurred
--%>
<%@page contentType="application/json"%>
<%@page trimDirectiveWhitespaces="true"%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/store/order/purchase/CartFormHandler"/>

  <json:object>
    <c:choose>
      <c:when test="${param.errorAddToCart}">
        <dsp:getvalueof var="formExceptions" bean="CartFormHandler.formExceptions"/>
        <json:property name="addToCartError" value="${formExceptions}"/>
      </c:when>
      <c:otherwise>
        <%-- Include the updated cart item count --%>
        <%@include file="/mobile/cart/gadgets/cartItemCount.jspf" %>
        <%-- Output the cart item count as a hidden input --%>
          <json:property name="cartItemCount" value="${itemsQuantity}"/>
          <json:property name="cartUrl">
            <c:url value="${contextPrefix}/cart/cart.jsp"/>
          </json:property>
      </c:otherwise>
    </c:choose>
  </json:object>
</dsp:page>