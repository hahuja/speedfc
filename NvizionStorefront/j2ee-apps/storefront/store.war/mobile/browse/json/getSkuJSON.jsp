<%--
  Renders appropriate JSON for a product's SKUs
  
  Required Parameters:
    product
      the product whose SKUs to iterate
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/droplet/SkuAvailabilityLookup"/>
  <dsp:importbean bean="/atg/store/droplet/CatalogItemFilterDroplet"/>
  <dsp:importbean bean="/atg/commerce/pricing/priceLists/PriceDroplet"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <%-- Sort SKUs into maps based on availability --%>
  <jsp:useBean id="availableSkuMap" class="java.util.HashMap"/>
  <jsp:useBean id="preorderSkuMap" class="java.util.HashMap"/>
  <jsp:useBean id="backorderSkuMap" class="java.util.HashMap"/>
  <jsp:useBean id="unavailableSkuMap" class="java.util.HashMap"/>

  <dsp:droplet name="CatalogItemFilterDroplet">
    <dsp:param name="collection" param="product.childSKUs"/>
    <dsp:oparam name="output">
      <dsp:getvalueof var="skus" param="filteredCollection"/>
      <c:forEach items="${skus}" var="currentSku">
      
        <%-- Construct the key that will be used to identify this SKU 
             by enumerating the known SKU types.
             Order is important for keys based on multiple properties;
             these keys will be derived in by 'checkForSelectedSku' in product.js,
             so if they are not specified in the same order everywhere then
             picker selection will not work properly.
        --%>
        <c:choose>
          <c:when test="${currentSku.type == 'clothing-sku'}">
            <c:set var="mapKey" value="${currentSku.color}:${currentSku.size}"/>
          </c:when>
          <c:when test="${currentSku.type == 'furniture-sku'}">
            <c:set var="mapKey" value="${currentSku.woodFinish}"/>
          </c:when>
          <c:otherwise> <%-- Default to SKU id --%>
            <c:set var="mapKey" value="${currentSku.repositoryId}"/>
          </c:otherwise>
        </c:choose>
        
        <%-- Get the SKU price(s) --%>
        <dsp:droplet name="PriceDroplet">
          <dsp:param name="product" param="product"/>
          <dsp:param name="sku" value="${currentSku}"/>
          <dsp:oparam name="output">
            <dsp:setvalue param="theListPrice" paramvalue="price"/>
            <dsp:getvalueof var="profileSalePriceList" bean="Profile.salePriceList"/>
            <c:choose>
              <c:when test="${not empty profileSalePriceList}">
                <dsp:droplet name="PriceDroplet">
                  <dsp:param name="priceList" bean="Profile.salePriceList"/>
                  <dsp:oparam name="output">
                    <dsp:getvalueof var="listPrice" vartype="java.lang.Double" param="price.listPrice"/>
                    <dsp:getvalueof var="price" vartype="java.lang.Double" param="theListPrice.listPrice"/>
                    <c:set var="productPrice">
                      <dsp:include page="/global/gadgets/formattedPrice.jsp">
                        <dsp:param name="price" value="${listPrice}" />
                      </dsp:include>
                    </c:set>
                    <c:set var="salePrice">
                      <dsp:include page="/global/gadgets/formattedPrice.jsp">
                        <dsp:param name="price" value="${price}" />
                      </dsp:include>
                    </c:set>
                  </dsp:oparam>
                  <dsp:oparam name="empty">
                    <dsp:getvalueof var="price" vartype="java.lang.Double" param="theListPrice.listPrice"/>
                    <c:set var="productPrice">
                      <dsp:include page="/global/gadgets/formattedPrice.jsp">
                        <dsp:param name="price" value="${price}" />
                      </dsp:include>
                    </c:set>
                  </dsp:oparam>
                </dsp:droplet><%-- End price droplet on sale price --%>
              </c:when>
              <c:otherwise>
                <c:set var="productPrice">
                  <dsp:include page="/global/gadgets/formattedPrice.jsp">
                    <dsp:param name="price" value="${price}" />
                  </dsp:include>
                </c:set>
              </c:otherwise>
            </c:choose><%-- End Is Empty Check --%>
          </dsp:oparam>
        </dsp:droplet>
        
        <%-- Construct the SKU JSON object --%>
        <c:set var="skuJSON">
          <json:object>
            <json:property name="salePrice" value="${salePrice}"/>
            <json:property name="productPrice" value="${productPrice}"/>
            <json:property name="skuId" value="${currentSku.repositoryId}"/>
          </json:object>
        </c:set>
        
        <%-- Put the JSON in the correct map --%>
        <dsp:droplet name="SkuAvailabilityLookup">
          <dsp:param name="product" param="product"/>
          <dsp:param name="skuId" value="${currentSku.repositoryId}"/>
          <dsp:oparam name="available">
            <c:set target="${availableSkuMap}" property="${mapKey}" value="${skuJSON}"/>
          </dsp:oparam>
          <dsp:oparam name="preorderable">
            <c:set target="${preorderSkuMap}" property="${mapKey}" value="${skuJSON}"/>
          </dsp:oparam>
          <dsp:oparam name="backorderable">
            <c:set target="${backorderSkuMap}" property="${mapKey}" value="${skuJSON}"/>
          </dsp:oparam>
          <dsp:oparam name="unavailable">
            <c:set target="${unavailableSkuMap}" property="${mapKey}" value="${skuJSON}"/>
          </dsp:oparam>
        </dsp:droplet>
      </c:forEach>
    </dsp:oparam>
  </dsp:droplet>
  
  <script type="text/javascript">
    var productSKUs;
    $(document).ready(function(){
      productSKUs = {
        <c:forEach var="entry" items="${availableSkuMap}">
          "<c:out value='${entry.key}' escapeXml='false'/>":skuFunctions.available(<c:out value='${entry.value}' escapeXml='false'/>),
        </c:forEach>
        <c:forEach var="entry" items="${preorderSkuMap}">
          "<c:out value='${entry.key}' escapeXml='false'/>":skuFunctions.preorder(<c:out value='${entry.value}' escapeXml='false'/>),
        </c:forEach>
        <c:forEach var="entry" items="${backorderSkuMap}">
          "<c:out value='${entry.key}' escapeXml='false'/>":skuFunctions.backorder(<c:out value='${entry.value}' escapeXml='false'/>),
        </c:forEach>
        <c:forEach var="entry" items="${unavailableSkuMap}">
          "<c:out value='${entry.key}' escapeXml='false'/>":skuFunctions.outOfStock(<c:out value='${entry.value}' escapeXml='false'/>),
        </c:forEach>
      };
    });
  </script>
  
</dsp:page>