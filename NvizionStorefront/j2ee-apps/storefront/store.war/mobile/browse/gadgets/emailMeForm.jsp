<%--
  This page renders form for email notification

  Required parameters:
    selectedSkuId
      the id of the SKU that has been selected
	  productId
	    the id of the product that has been selected

  Optional parameters:
    None
--%>
<dsp:page>

  <dsp:importbean bean="/atg/store/inventory/BackInStockFormHandler"/>
  <dsp:importbean bean="/atg/store/profile/SessionBean"/>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>

  <dsp:getvalueof var="selectedSku" param="selectedSku"/> 
  <div id="mobile_store_emailMePopup">
	  <dsp:form id="mobile_store_emailMeForm" formid="mobile_store_emailMeForm" method="post" name="mobile_store_emailMeForm"
	    action="${originatingRequest.contextPath}/browse/gadgets/emailMeFormErrors.jsp" onsubmit="skuFunctions.emailMeSubmit(event);">
      <%-- Form properties, SKU and products that are unavailable. --%>
			<dsp:input id="mobile_store_emailMeSkuId" bean="BackInStockFormHandler.catalogRefId" type="hidden" paramvalue="selectedSkuId"/>
			<dsp:input bean="BackInStockFormHandler.productId" type="hidden" paramvalue="productId"/>

			<%-- Set the standard URLs --%>
			<dsp:input bean="BackInStockFormHandler.successURL" type="hidden" value=""/>
      <dsp:input bean="BackInStockFormHandler.errorURL" type="hidden" value=""/>

      <table id="mobile_store_emailMe" class="mobile_store_mobileCRSRedirectTable" summary="" role="presentation" datatable="0" onclick="event.stopPropagation();"> 
        <tr>
	        <td><fmt:message key="mobile.notifyMe_whenInStock"/></td>
	      </tr>
	      <tr id="mobile_store_emailAddressRow">
		      <td>
            <fmt:message key="common.email" var="hintText"/>
            <dsp:input id="mobile_store_rememberMeEmailAddress" bean="BackInStockFormHandler.emailAddress" type="email" 
              beanvalue="/atg/userprofiling/Profile.email" onclick="event.stopPropagation();"
              placeholder="${hintText}" autocapitalize="off" autocorrect="off"/>
            <div><p><fmt:message key="mobile.form.validation.invalid"/></p></div>
          </td>
	      </tr>
	      <tr>
		      <td>
			<fmt:message var="privacyTitle" key="mobile.company_privacyAndTerms.pageTitle"/>
            <dsp:a page="/company/terms.jsp" title="${privacyTitle}" class="icon-help email-me">
            </dsp:a>
			      <div class="checkBox">
				        <dsp:input name="rememberEmail" type="hidden" bean="SessionBean.values.rememberedEmail" value=""/>
                <input id="mobile_store_rememberCheckbox" type="checkbox" name="rememberCheckbox"  checked="checked"/>
                <label for="mobile_store_rememberCheckbox" onclick="">                  
                  <fmt:message key="mobile.notifyMe_rememberForCheckout"/>
                </label>
            </div>
			    </td>
		    </tr>
      </table>
      <fmt:message key="common.button.submitText" var="submitText"/>
      <dsp:input type="hidden" bean="BackInStockFormHandler.notifyMe" value="${submitText}"/>
	  </dsp:form>
  </div>

  <div id="mobile_store_emailMe_confirm">
	  <table class="mobile_store_mobileCRSRedirectTable" summary="" role="presentation" datatable="0">
		  <tr>
			  <td><fmt:message key="mobile.notifyMe_title"/></td>
			</tr>
			<tr>
				<td><p><fmt:message key="mobile.notifyMe_thankYouMsg"/></td>
			</tr>
		</table> 
	</div>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/gadgets/emailMeForm.jsp#4 $$Change: 692002 $ --%>
