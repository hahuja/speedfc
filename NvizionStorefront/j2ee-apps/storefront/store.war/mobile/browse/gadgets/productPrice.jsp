<%--
  This page will display product price or out of stock message if selected SKU is out of stock 
  
  Page includes:
    /mobile/browse/gadgets/priceLookup.jsp
    /mobile/browse/gadgets/priceRange.jsp
  
  Required parameters:
    product – the current product
  
  Optional parameters:
    selectedSku – the currently selected sku
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/droplet/SkuAvailabilityLookup"/>

  <dsp:getvalueof var="selectedSku" param="selectedSku"/>
  
  <div id="mobile_store_price">
    <c:choose>
      <c:when test="${not empty selectedSku}">
        <dsp:include page="priceLookup.jsp">
          <dsp:param name="sku" param="selectedSku"/>
          <dsp:param name="product" param="element"/>
        </dsp:include>
      </c:when>
      <c:otherwise>
        <dsp:include page="priceRange.jsp">
          <dsp:param name="product" param="element"/>
        </dsp:include>
      </c:otherwise>
    </c:choose>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/gadgets/productPrice.jsp#3 $$Change: 692002 $--%>
