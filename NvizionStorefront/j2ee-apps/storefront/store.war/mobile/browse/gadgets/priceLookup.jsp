<%--
	This page, given a product and sku, and using the Profile bean, looks for that sku's price
	in the sale priceList (if it exists) and the regular price list.

  Required parameters:
    product
      the product repository item whose price we wish to display
    sku
      the sku repository item for the product whose price we wishto display

  Optional parameters:
    None
--%>

<dsp:page>

  <dsp:importbean bean="/atg/commerce/pricing/priceLists/PriceDroplet"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>


  <%-- The first call to price droplet is going to get the price from the profile's list price or
       the default price list --%>

  <dsp:droplet name="PriceDroplet">
    <dsp:param name="product" param="product"/>
    <dsp:param name="sku" param="sku"/>

    <dsp:oparam name="output">
      <dsp:setvalue param="theListPrice" paramvalue="price"/>

      <%-- is there a sale price? --%>
      <dsp:getvalueof var="profileSalePriceList" bean="Profile.salePriceList"/>

      <c:choose>
        <c:when test="${not empty profileSalePriceList}">
          <dsp:droplet name="PriceDroplet">
            <dsp:param name="priceList" bean="Profile.salePriceList"/>
            <dsp:oparam name="output">
              <dsp:getvalueof var="listPrice" vartype="java.lang.Double" param="price.listPrice"/>
              <dsp:getvalueof var="price" vartype="java.lang.Double" param="theListPrice.listPrice"/>
              <p>
                <strong>
                  <dsp:include page="/mobile/global/gadgets/formattedPrice.jsp">
                    <dsp:param name="price" value="${listPrice}"/>
                  </dsp:include>
                </strong>
                <br/><fmt:message key="common.price.old"/>&nbsp;
                <span>
                  <dsp:include page="/mobile/global/gadgets/formattedPrice.jsp">
                    <dsp:param name="price" value="${price}"/>
                  </dsp:include>
                </span>
              </p>
            </dsp:oparam>
            <dsp:oparam name="empty">
              <dsp:getvalueof var="price" vartype="java.lang.Double" param="theListPrice.listPrice"/>
              <p>
                <strong>
                  <dsp:include page="/mobile/global/gadgets/formattedPrice.jsp">
                    <dsp:param name="price" value="${price}"/>
                  </dsp:include>
                </strong>
              </p>
            </dsp:oparam>
          </dsp:droplet><%-- End price droplet on sale price --%>
        </c:when>
        <c:otherwise>
          <p>
            <strong>
              <dsp:include page="/mobile/global/gadgets/formattedPrice.jsp">
                <dsp:param name="price" value="${price}"/>
              </dsp:include>
            </strong>
          </p>
        </c:otherwise>
      </c:choose><%-- End Is Empty Check --%>
    </dsp:oparam>
  </dsp:droplet>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/gadgets/priceLookup.jsp#3 $$Change: 692002 $--%>
