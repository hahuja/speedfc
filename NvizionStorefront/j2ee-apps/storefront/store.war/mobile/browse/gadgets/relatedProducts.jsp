<%-- 
  Product details: Related items

  Page includes:
    /global/gadgets/productLinkGenerator.jsp product link generator

  Required parameters:
    relatedProducts
      the filtered collection of related items we want to display

  Optional parameters:
    None
--%>
<dsp:page>
  <div id="mobile_store_relatedItemsContainer">
    <dsp:getvalueof var="relatedProducts" param="relatedProducts"/>
    <c:forEach var="relatedProduct" items="${relatedProducts}">
      <div class="cell">
        <dsp:param name="relatedProduct" value="${relatedProduct}"/>
        <dsp:getvalueof var="relatedImageUrl" param="relatedProduct.smallImage.url"/>
        <dsp:getvalueof var="relatedProductId" param="relatedProduct.repositoryId"/>
        <dsp:getvalueof var="relatedProductName" param="relatedProduct.displayName"/>
        <%--
            Generates a URL for the product, the URL is stored in the productUrl request scoped variable
        --%>
        <dsp:tomap var="mapRelatedProduct" param="relatedProduct"/>
        <dsp:include page="/global/gadgets/productLinkGenerator.jsp">
          <dsp:param name="product" param="relatedProduct"/>
          <dsp:param name="siteId" param="${mapRelatedProduct.auxiliaryData.siteId}"/>
        </dsp:include>

		<c:if test="${empty relatedImageUrl}">        
        	<c:set var="relatedImageUrl" value="/nrsdocroot/content/images/products/small/MissingProduct_small.jpg"/>
        </c:if>
        <a href="${fn:escapeXml(productUrl)}">
          <img src="${relatedImageUrl}" alt="${relatedProductName}"/>
        </a>
      </div>
    </c:forEach>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/gadgets/relatedProducts.jsp#3 $$Change: 692002 $--%>
