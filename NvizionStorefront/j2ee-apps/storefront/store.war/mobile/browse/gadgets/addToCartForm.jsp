<%--
  Hidden form for adding items to the cart
  
  Required Parameters:
    productId
      the id of the product to add to the cart
    
  Optional Parameters:
    selectedSkuId
      SKU being added to shopping cart
    selectedQty
      number of items being added
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/store/order/purchase/CartFormHandler"/>
  
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:form id="mobile_store_addToCart" formid="mobile_store_addToCart"
            action="${originatingRequest.requestURI}" method="post"
            name="mobile_store_addToCart">
    <dsp:input bean="CartFormHandler.addItemToOrderErrorURL" type="hidden"
               value="${contextPrefix}/browse/json/addToCartJSON.jsp?errorAddToCart=true"/>
    <dsp:input bean="CartFormHandler.addItemCount" value="1" type="hidden"/>
    <dsp:input bean="CartFormHandler.addItemToOrderSuccessURL" type="hidden"
               value="${contextPrefix}/browse/json/addToCartJSON.jsp"/>
               
    <dsp:input id="addToCart_skuId" bean="CartFormHandler.items[0].catalogRefId" paramvalue="selectedSkuId"
               type="hidden"/>
    <dsp:input id="addToCart_qty" bean="CartFormHandler.items[0].quantity" paramvalue="selectedQty" type="hidden"/>
    
    <dsp:input bean="CartFormHandler.items[0].productId" paramvalue="productId" type="hidden"/>
    <dsp:input bean="CartFormHandler.addItemToOrder" type="hidden" value="Add to cart"/>
  </dsp:form>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/gadgets/addToCartForm.jsp#3 $$Change: 692002 $--%>
