<%--
  This page renders form for back to/update shopping cart

  Required parameters:
    productId
      the id of the product to add to the cart
    selectedSku
      SKU being added to shopping cart
  	selectedQty
  	  number of items being added
  	ciId
  	  the id of commerce item being edited

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:importbean bean="/atg/store/order/purchase/CartFormHandler"/>
  <dsp:getvalueof var="errorURL" vartype="java.lang.String"
                  value=""/>
  <dsp:form id="mobile_store_updateCart" formid="mobile_store_updateCart"
            action="${originatingRequest.requestURI}" method="post"
            name="mobile_store_addToCart">
    <dsp:input bean="CartFormHandler.addItemToOrderErrorURL" type="hidden" value="${errorURL}"/>
    <dsp:input bean="CartFormHandler.addItemCount" value="1" type="hidden"/>
    <dsp:input bean="CartFormHandler.removeAndAddItemToOrderSuccessURL" type="hidden"
               value="${contextPrefix}/cart/cart.jsp"/>
    <dsp:input id="addToCart_skuId" bean="CartFormHandler.items[0].catalogRefId" paramvalue="selectedSku.repositoryId" type="hidden"/>
    <dsp:input bean="CartFormHandler.items[0].productId" paramvalue="productId" type="hidden"/>
    <dsp:input id="addToCart_qty" bean="CartFormHandler.items[0].quantity" paramvalue="selectedQty" type="hidden"/>
    <dsp:input bean="CartFormHandler.removalCommerceIds" paramvalue="ciId" type="hidden"/>
    <dsp:input bean="CartFormHandler.removeAndAddItemToOrder" type="hidden" value="Update Cart"/>
  </dsp:form>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/gadgets/updateCartForm.jsp#1 $$Change: 683854 $--%>
