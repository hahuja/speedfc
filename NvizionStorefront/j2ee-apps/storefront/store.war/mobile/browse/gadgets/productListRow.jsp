<%--
  This JSP renders a row in product search results pages.

  Page includes:
    /global/gadgets/productLinkGenerator.jsp product link generator

  Required parameters:
    product
      the product repository item whose details are displayed

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:include page="/global/gadgets/productLinkGenerator.jsp">
    <dsp:param name="product" param="product"/>
  </dsp:include>
  <dsp:a href="${fn:escapeXml(productUrl)}">
    <div class="searchResult">
      <dsp:getvalueof var="productName" param="product.displayName"/>
      <dsp:getvalueof var="productImageUrl" param="product.thumbnailImage.url"/>
      
      <c:if test="${empty productImageUrl}">
      	<c:set var="productImageUrl" value="/nrsdocroot/content/images/products/thumb/MissingProduct_thumb.jpg"/>
      </c:if>
      
      <%-- 'notLoaded' class will be removed once the image has loaded --%>
      <img class="notLoaded" onload="imgLoad(this);" src="${productImageUrl}" alt="${productName}"/>

      <p class="searchResultProduct">
        <span class="product_name"><c:out value="${productName}"/></span>
        <dsp:importbean bean="/atg/commerce/pricing/priceLists/PriceDroplet"/>
        <dsp:importbean bean="/atg/userprofiling/Profile"/>


        <%-- The first call to price droplet is going to get the price from the profile's list price or
                 the default price list --%>

        <dsp:droplet name="PriceDroplet">
          <dsp:param name="product" param="product"/>
          <dsp:param name="sku" param="product.childSKUs[0]"/>

          <dsp:oparam name="output">
            <dsp:setvalue param="theListPrice" paramvalue="price"/>

            <%-- is there a sale price? --%>
            <dsp:getvalueof var="profileSalePriceList" bean="Profile.salePriceList"/>

            <c:choose>
              <c:when test="${not empty profileSalePriceList}">
                <dsp:droplet name="PriceDroplet">
                  <dsp:param name="priceList" bean="Profile.salePriceList"/>
                  <dsp:oparam name="output">
                    <dsp:getvalueof var="listPrice" vartype="java.lang.Double" param="price.listPrice"/>
                    <dsp:getvalueof var="price" vartype="java.lang.Double" param="theListPrice.listPrice"/>
                    <span class="mobile_store_productPrice">
                      <dsp:include page="/mobile/global/gadgets/formattedPrice.jsp">
                        <dsp:param name="price" value="${listPrice}"/>
                      </dsp:include>
                    </span>
                    <span class="mobile_store_salePrice">
                      <fmt:message key="common.price.old"/>
                      <dsp:include page="/mobile/global/gadgets/formattedPrice.jsp">
                        <dsp:param name="price" value="${price}"/>
                      </dsp:include>
                    </span>
                  </dsp:oparam>
                  <dsp:oparam name="empty">
                    <dsp:getvalueof var="price" vartype="java.lang.Double" param="theListPrice.listPrice"/>
                    <span class="mobile_store_productPrice">
                      <dsp:include page="/mobile/global/gadgets/formattedPrice.jsp">
                        <dsp:param name="price" value="${price}"/>
                      </dsp:include>
                    </span>
                  </dsp:oparam>
                </dsp:droplet><%-- End price droplet on sale price --%>
              </c:when>
              <c:otherwise>
                <c:out value="otherwise"/>
                <span class="mobile_store_productPrice">
                  <dsp:include page="/mobile/global/gadgets/formattedPrice.jsp">
                    <dsp:param name="price" value="${price}"/>
                  </dsp:include>
                </span>
              </c:otherwise>
            </c:choose><%-- End Is Empty Check --%>
          </dsp:oparam>
        </dsp:droplet>
      </p>
    </div>
  </dsp:a>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/gadgets/productListRow.jsp#3 $$Change: 692002 $--%>
