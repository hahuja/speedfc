<%--
  Renders pickers for a furniture product

  Page includes:
    /mobile/browse/gadgets/pickerList.jsp

  Required parameters:
    availableColors
      Available colors for this product
  
  Optional parameters:
    selectedColor
      The currently selected color
    oneColor
      If true, this product only has one color
  
  Unused parameters:
    product
    selectedSku
    availableSizes
    selectedSize
    oneSize
--%>
<dsp:page>
  <li>
    <fmt:message key="common.woodFinish" var="finishLabel"/>
    <dsp:include page="pickerList.jsp">
      <dsp:param name="id" value="mobile_store_woodFinishSelect"/>
      <dsp:param name="collection" param="availableColors"/>
      <dsp:param name="type" value="woodFinish"/>
      <dsp:param name="callback" value="skuFunctions.woodFinishPickerSelect"/>
      <dsp:param name="selectedValue" param="selectedColor"/>
      <dsp:param name="disabled" param="oneColor"/>
      <dsp:param name="defaultLabel" value="${finishLabel}"/>
      <dsp:param name="valueProperty" value="name"/>
    </dsp:include>
  </li>
</dsp:page>