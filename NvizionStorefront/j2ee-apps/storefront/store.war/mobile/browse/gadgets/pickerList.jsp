<%--
Renders a list of SKU picker selections

  Required parameters:
    collection
      the collection of values to render
    valueProperty
      the name of the property used for the item's value
    type
      the 'type' of value being selected. This must correspond exactly to the one of the
      values passed to 'pickerSelectFunction' in product.js
    callback
      the function to be called when the picker value is changed
    id 
      the value for the 'id' attribute of the rendered picker
  
  Optional parameters:
    selectedValue
      the currently selected value
    disabled
      if present, causes the picker to be disabled
    defaultLabel
      the label to display when there is no selection
    labelProperty
      the name of the property used for the item's label. Defaults to valueProperty
--%>
<dsp:getvalueof var="collection" param="collection"/>
<dsp:getvalueof var="type" param="type"/>
<dsp:getvalueof var="callback" param="callback"/>
<dsp:getvalueof var="selectedValue" param="selectedValue"/>
<dsp:getvalueof var="defaultLabel" param="defaultLabel"/>
<dsp:getvalueof var="disabled" param="disabled"/>
<dsp:getvalueof var="id" param="id"/>
<dsp:getvalueof var="valueProperty" param="valueProperty"/>
<dsp:getvalueof var="labelProperty" param="labelProperty"/>

<%-- Label included for accessibility --%>
<label for="${id}" class="noText">
  <select title="${empty selectedValue ? "" : defaultLabel}" id="${id}" name="${type}" onchange="${callback}(event);"
          onclick="event.stopPropagation(); expandPickers(true);"
          ontouchend="event.stopPropagation(); expandPickers(true);"
          class="${(empty selectedValue) ? '' : 'selected'}"
          ${(!empty disabled && disabled) ? 'disabled' : ''}>
    <c:if test="${!empty defaultLabel}">
      <%-- Default option is always disabled --%>
      <option value="" label="${defaultLabel}" disabled />
    </c:if>
    <c:forEach var="item" items="${collection}">
      <c:set var="itemValue" value="${item[valueProperty]}"/>
      <c:set var="itemLabel" value="${(empty labelProperty) ? itemValue : item[labelProperty]}"/>
      <option value="${itemValue}" label="${itemLabel}"
              ${(itemValue == selectedValue) ? 'selected' : ''}/>
    </c:forEach>
  </select>
</label>
