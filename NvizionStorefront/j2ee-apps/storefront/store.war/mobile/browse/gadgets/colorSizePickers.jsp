<%--
  Renders pickers for a color/size SKU

  Page includes:
    /mobile/browse/gadgets/pickerList.jsp

  Required parameters:
    availableSizes
      Available sizes for this product
    availableColors
      Available colors for this product

  Optional parameters:
    selectedSize
      The currently selected size
    selectedColor
      The currently selected color
    oneSize
      If true, this product only has one size
    oneColor
      If true, this product only has one color
    
  Unused parameters:
    product
    selectedSku
--%>
<dsp:page>
  <dsp:getvalueof var="availableColors" param="availableColors"/>
  <dsp:getvalueof var="availableSizes" param="availableSizes"/>
  
  <c:if test="${fn:length(availableColors) > 0}">
    <li>
      <fmt:message key="common.color" var="colorLabel"/>
      <dsp:include page="pickerList.jsp">
        <dsp:param name="id" value="mobile_store_colorSelect"/>
        <dsp:param name="collection" param="availableColors"/>
        <dsp:param name="type" value="color"/>
        <dsp:param name="callback" value="skuFunctions.colorSizePickerSelect"/>
        <dsp:param name="selectedValue" param="selectedColor"/>
        <dsp:param name="disabled" param="oneColor"/>
        <dsp:param name="defaultLabel" value="${colorLabel}"/>
        <dsp:param name="valueProperty" value="name"/>
      </dsp:include>
    </li>
  </c:if>
  <c:if test="${fn:length(availableSizes) > 0}">
    <li>
      <fmt:message key="common.size" var="sizeLabel"/>
      <dsp:include page="pickerList.jsp">
        <dsp:param name="id" value="mobile_store_sizeSelect"/>
        <dsp:param name="collection" param="availableSizes"/>
        <dsp:param name="type" value="size"/>
        <dsp:param name="callback" value="skuFunctions.colorSizePickerSelect"/>
        <dsp:param name="selectedValue" param="selectedSize"/>
        <dsp:param name="disabled" param="oneSize"/>
        <dsp:param name="defaultLabel" value="${sizeLabel}"/>
        <dsp:param name="valueProperty" value="name"/>
      </dsp:include>
    </li>
  </c:if>
</dsp:page>