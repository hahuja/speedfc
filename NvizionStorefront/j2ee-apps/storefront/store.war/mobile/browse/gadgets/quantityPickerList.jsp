<%--
  Renders a list of quantity selections. Quantity "1" is selected by default.

  Required parameters:
    id
      the value for the 'id' attribute of the rendered picker

  Optional parameters:
    selectedValue
      the currently selected value
    disabled
      if present, causes the picker to be disabled
    max
      the maximum quantity to display in list. Defaults to 10.
--%>
<dsp:page>
  <dsp:getvalueof var="id" param="id"/>
  <dsp:getvalueof var="selectedValue" param="selectedValue"/>
  <dsp:getvalueof var="disabled" param="disabled"/>
  <dsp:getvalueof var="max" param="max"/>
  
  <c:if test="${empty selectedValue}">
    <c:set var="selectedValue" value="1"/>
  </c:if>
  
  <label for="${id}" onclick="" class="noText">
    <span class="qtyLabelText"><fmt:message key="common.quantity"/><fmt:message key="common.labelSeparator"/></span>
    <select id="${id}" name="qty" onchange="skuFunctions.quantitySelect(event);"
            onclick="event.stopPropagation(); expandPickers(true);" class="selected">
      <c:forEach var="itemValue" begin="1" end="${(empty max) ? 10 : max}" step="1">
            <option value="${itemValue}" label="${itemValue}"
                    ${(itemValue == selectedValue) ? 'selected' : ''}/>
      </c:forEach>
    </select>
  </label>
</dsp:page>