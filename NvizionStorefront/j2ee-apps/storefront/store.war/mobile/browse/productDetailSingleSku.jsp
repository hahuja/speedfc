<%--
  This page is a template for single-SKU products

  Page includes:
    /mobile/browse/productDetail.jsp

  Required parameters:
    productId
      the product id whose details being displayed

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
  <dsp:droplet name="ProductLookup">
    <dsp:param name="id" param="productId"/>
    <dsp:oparam name="output">
      <dsp:include page="productDetail.jsp">
        <dsp:param name="product" param="element"/>
        <dsp:param name="selectedQty" param="selectedQty"/>
        <dsp:param name="selectedSku" param="element.childSKUs[0]"/>
        <dsp:param name="ciId" param="ciId"/>    
      </dsp:include>
    </dsp:oparam>
  </dsp:droplet>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/productDetailSingleSku.jsp#1 $$Change: 683854 $--%>
