<%--
  Renderer of credit card info

  Page includes:
    None

  Required parameters:
    creditCard
      credit card to be displayed

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/util/PlaceLookup" />
  <dsp:importbean bean="/atg/commerce/util/MasterCountryList" />
  <dsp:importbean bean="/atg/store/droplet/CountryListDroplet" />
  <dsp:importbean bean="/atg/store/order/purchase/CommitOrderFormHandler"/>

      <dsp:getvalueof var="contextroot" bean="/OriginatingRequest.contextPath"/>
      <dsp:getvalueof var="showFullInfo" param="showFullInfo"/>

      <div class="vcard">

        <div class="card_type">
          <%-- display only last 4 digits --%>
          <dsp:getvalueof var="creditCardNumber" param="creditCard.creditCardNumber" />
          <c:set var="creditCardNumberLength" value="${fn:length(creditCardNumber)}"/>
          <dsp:getvalueof var="expirationYear" param="creditCard.expirationYear" />
          <c:set var="expirationYearLength" value="${fn:length(expirationYear)}" />

          <span><dsp:valueof param="creditCard.creditCardType"/></span>
          <span class="cardNumber"><fmt:message key="mobile.common.ellipsis"/><c:out value="${fn:substring(creditCardNumber,creditCardNumberLength-4,creditCardNumberLength)}"/></span>
          <fmt:message key="common.expirationDate" var="expAbbrExpansion"/>
          <span class="cardExpiration"><abbr title="${expAbbrExpansion}"><fmt:message key="checkout_creditCards.expiration" /></abbr>: <dsp:valueof param="creditCard.expirationMonth" />/<c:out value="${fn:substring(expirationYear,expirationYearLength-2,expirationYearLength)}"/></span>
        </div>
        <dsp:getvalueof var="billingAddress" param="creditCard.billingAddress"/>
        <c:choose>
          <c:when test="${billingAddress != null}">
            <div class="fn">
              <dsp:valueof param="creditCard.billingAddress.firstName"/>
              <dsp:valueof param="creditCard.billingAddress.lastName"/>
            </div>
            <div  class="adr">
              <div><dsp:valueof param="creditCard.billingAddress.address1" /><fmt:message key="common.comma"/></div>
              <%-- Now display state. --%>
              <span class="locality"><dsp:valueof param="creditCard.billingAddress.city"/><fmt:message key="common.comma"/></span>
              <dsp:getvalueof var="state" param="creditCard.billingAddress.state"/>
              <c:if test="${not empty state}">
                <span class="region"><dsp:valueof param="creditCard.billingAddress.state"/></span>
              </c:if>

              <%-- ZIP-code, Country name and Phone --%>
              <span class="postal-code"><dsp:valueof param="creditCard.billingAddress.postalCode"/></span>

              <c:if test="${showFullInfo == 'true'}">
              <br/>
              <dsp:droplet name="/atg/store/droplet/CountryListDroplet">
                <dsp:param name="userLocale" bean="/atg/dynamo/servlet/RequestLocale.locale" />
                <dsp:param name="countryCode" param="creditCard.billingAddress.country"/>
                   <dsp:oparam name="false">
                   <dsp:valueof param="countryDetail.displayName" />
                </dsp:oparam>
              </dsp:droplet>

              <br/>
              <dsp:valueof param="creditCard.billingAddress.phoneNumber"/>
              </c:if>
            </div>
          </c:when>
          <c:otherwise>
            <div class="fn">
              <fmt:message key="mobile.myaccount_billingAddress.empty"/>
            </div>
          </c:otherwise>
        </c:choose>
      </div>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/creditCardRenderer.jsp#3 $$Change: 692002 $--%>
