<%--
  Order confirmation for anonymous user

  Page includes:
    /mobile/global/gadgets/registration.jsp registration form

  Required parameters:
    None

  Optional parameters:
    registrationErrors
      indicator if there are registration errors
--%>
<dsp:page>

  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/userprofiling/Profile" var="profilebean"/>

  <dsp:getvalueof var="registrationErrors" param="registrationErrors"/>

  <div align="center" class="mobile_store_actionConfirmation">
	  <c:choose>
	    <c:when test="${!empty profilebean.email}">
	      <fmt:message key="mobile.checkout_confirmResponse.emailText" />
	      <br/>
	      <strong><c:out value="${profilebean.email}"/></strong>
	    </c:when>
	    <c:otherwise>
	      <fmt:message key="mobile.checkout_confirmResponse.omsOrderId" />
		  <br/>
		  <strong><dsp:valueof bean="ShoppingCart.last.id"/></strong>
	    </c:otherwise>
	  </c:choose>
  </div>
  <br/>
  <ul class="mobile_store_list">
		
    <c:choose>
      <c:when test="${registrationErrors == 'true'}">
	    <c:set var="clicked" value="imageClicked"/>
	  </c:when>
  	  <c:otherwise>
	    <c:set var="hidden" value="hidden"/>
  	  </c:otherwise>
    </c:choose>
		
	<li class="${clicked}" onclick="loginPageClick('regRow', this);">
	  <span class="parentSpan">
	     <span class="title">
	       <fmt:message key="checkout_confirmResponse.registerTitle"/>
	     </span>
	     <span class="rightContentContainer">
	       <div class="arrow"></div>
	     </span>
	   </span>
	</li>
    <li id="regRow" class="${hidden} expandable">
      <dsp:include page="/mobile/global/gadgets/registration.jsp"/>
    </li>
  </ul>
  <br/>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/confirmResponseAnonymous.jsp#3 $$Change: 692002 $ --%>