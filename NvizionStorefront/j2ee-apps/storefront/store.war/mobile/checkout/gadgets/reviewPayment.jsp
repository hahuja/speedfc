<%-- 
  Render Payment Method and Billing Address box.

  Page includes:
    /mobile/checkout/gadgets/creditCardRenderer.jsp renderer of credit card info

  Required parameters:
    order
      order which payment data need to be displayed

  Optional parameters:
    isCheckout
      indicator if this order is about to checkout or has been already placed
      Possible values: true - is about to be checkout; false - placed order
--%>

<dsp:page>
  <%-- Get the payment group for billing address and payment method --%>
  <dsp:getvalueof var="paymentGroupRelationships" vartype="java.lang.Object" param="order.paymentGroupRelationships"/>
  <c:forEach var="paymentGroupRelationship" items="${paymentGroupRelationships}">
    <dsp:param name="rel" value="${paymentGroupRelationship}"/>
    <dsp:setvalue param="checkPaymentGroup" paramvalue="rel.paymentGroup"/>
    <dsp:getvalueof var="paymentGroupClassType" param="checkPaymentGroup.paymentGroupClassType"/>
    <c:if test="${paymentGroupClassType == 'creditCard'}">
      <dsp:setvalue param="paymentGroup" paramvalue="checkPaymentGroup"/>
    </c:if>
  </c:forEach>

  <dsp:getvalueof var="isCheckout" param="isCheckout" />
  <fmt:message var="title" key="mobile.checkout_confirmPaymentOptions.payment"/>
  <div class="mobile_store_payment">
    <h2>
      <c:choose>
        <c:when test="${isCheckout}">
          <c:set var="linkId" value="editBilling"/>
          <dsp:a id="${linkId}" page="../../checkout/billing.jsp">${title}</dsp:a>
        </c:when>
        <c:otherwise>
          ${title}
        </c:otherwise>
      </c:choose>
    </h2>

    <div class="mobile_store_detail" onclick="editReviewOrderBlock('${linkId}');">
      <dsp:include page="creditCardRenderer.jsp">
        <dsp:param name="creditCard" param="paymentGroup"/>
        <dsp:param name="showFullInfo" value="true"/>
      </dsp:include>
      <c:if test="${isCheckout}">
      	<dsp:a class="withArrow" page="../../checkout/billing.jsp" title="${title}"></dsp:a>
      </c:if>
    </div>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/reviewPayment.jsp#3 $$Change: 692002 $--%>
