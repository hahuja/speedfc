<%-- 
  Render order items with order summary box.

  Page includes:
    /checkout/gadgets/checkoutErrorMessages.jsp checkout error messages output
    /mobile/global/gadgets/orderItemsRenderer.jsp order items renderer
    /mobile/checkout/gadgets/checkoutOrderSummary.jsp checkout order summary

  Required parameters:
    order
      order which items should be displayed

  Optional parameters:
    isCheckout
      indicator if this order is about to checkout or has been already placed
      Possible values: true - is about to be checkout; false - placed order
--%>

<dsp:page>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>
  <dsp:getvalueof var="isCheckout" param="isCheckout" />
  <div class="mobile_store_cart">
    <dsp:getvalueof var="commerceItems" vartype="java.lang.Object" param="order.commerceItems"/> 
    <c:if test="${not empty commerceItems}">
    <c:choose>
      <c:when test="${isCheckout}"><%-- if isCheckout true, wrap the blocks with a link tag --%>
        <div id="mobile_store_cartContent" class="mobile_store_main">
          <dsp:include page="/checkout/gadgets/checkoutErrorMessages.jsp">
            <dsp:param name="formHandler" bean="CouponFormHandler"/>
            <dsp:param name="divid" value="mobile_store_formValidationError"/>
          </dsp:include>
          <c:forEach var="currentItem" items="${commerceItems}" varStatus="status">
            <dsp:include page="../../global/gadgets/orderItemsRenderer.jsp" flush="true">
              <dsp:param name="currentItem" value="${currentItem}" />
              <dsp:param name="isCheckout" param="isCheckout" />
            </dsp:include>
          </c:forEach>
        </div>
      </c:when>
      <c:otherwise>
          <div id="mobile_store_cartContent" class="mobile_store_main">
            <c:forEach var="currentItem" items="${commerceItems}" varStatus="status">
              <dsp:include page="../../global/gadgets/orderItemsRenderer.jsp" flush="true">
                <dsp:param name="currentItem" value="${currentItem}" />
                <dsp:param name="isCheckout" param="isCheckout" />
              </dsp:include>
            </c:forEach>
          </div>
      </c:otherwise>
    </c:choose>
    </c:if>
    <dsp:include page="../../checkout/gadgets/checkoutOrderSummary.jsp" flush="true">
      <dsp:param name="order" param="order"/>
      <dsp:param name="isCheckout" param="isCheckout" />
    </dsp:include>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/reviewOrderItems.jsp#3 $$Change: 692002 $--%>
