<%-- 
  Renders order's shipping method info on review page.

  Page includes:
    /mobile/global/gadgets/formattedPrice.jsp price formatter

  Required parameters:
    order
      order which payment data need to be displayed

  Optional parameters:
    isCheckout
      indicator if this order is about to checkout or has been already placed
      Possible values: true - is about to be checkout; false - placed order
--%>

<dsp:page>
  <div class="mobile_store_shipping_method">
  
    <dsp:getvalueof var="isCheckout" param="isCheckout" />
    <fmt:message var="title" key="common.defaultShippingMethod"/>
    <h2>
      <c:choose>
        <c:when test="${isCheckout}">
          <c:set var="linkId" value="editShippingMethod"/>
          <dsp:a id="${linkId}" page="../../checkout/shippingMethod.jsp">${title}</dsp:a>
        </c:when>
        <c:otherwise>
          ${title}
        </c:otherwise>
      </c:choose>
    </h2>

    <div class="mobile_store_detail" onclick="editReviewOrderBlock('${linkId}');">
      <dsp:getvalueof var="shippingMethod" param="hardgoodShippingGroup.shippingMethod"/>
      <span class="shipping_method_name"><fmt:message key="common.delivery${fn:replace(shippingMethod, ' ', '')}"/><fmt:message key="common.labelSeparator"/></span>
      <span class="shipping_method_value">
        <dsp:include page="../../global/gadgets/formattedPrice.jsp">
          <dsp:param name="price" param="order.priceInfo.shipping"/>
        </dsp:include>
      </span>
      <c:if test="${isCheckout}">
      	<dsp:a class="withArrow" page="../../checkout/shippingMethod.jsp" title="${title}"></dsp:a>
      </c:if>
    </div>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/reviewShippingMethod.jsp#3 $$Change: 692002 $--%>
