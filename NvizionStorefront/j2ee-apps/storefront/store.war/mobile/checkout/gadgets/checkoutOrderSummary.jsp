<%--
  This page renders the block with order total information including the applied promotions.

  Form Condition:
    This gadget must be contained inside of a form.
    CartFormHandler must be invoked from a submit button in this form for fields in this page to be processed

  Page includes:
    /mobile/cart/gadgets/promotionCode.jsp promotion code box
    /global/gadgets/formattedPrice.jsp price formatter

  Required parameters:
    order
      the order to render information of
    isCheckout
      an checkout process indicator

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:getvalueof var="commerceItems" vartype="java.lang.Object" param="order.commerceItems"/>
  <dsp:getvalueof var="order" param="order"/>
  <dsp:getvalueof var="isCheckout" param="isCheckout" />
  

  <div class="mobile_store_orderSummary">
    <c:if test="${not empty commerceItems}">

      <div class="mobile_store_orderDiscounts">
        <div class="mobile_store_appliedOrderDiscounts">
          <c:if test="${fn:length(order.priceInfo.adjustments) gt 1}">
          <ul>
            <%--
              Display all available pricing adjustments except the first one.
              The first adjustment is always order's raw subtotal, and hence doesn't contain a discount.
            --%>
            <c:forEach var="priceAdjustment" varStatus="status" items="${order.priceInfo.adjustments}" begin="1">
              <dsp:tomap var="pricingModel" value="${priceAdjustment.pricingModel}"/>
              <li class="mobile_store_appliedDiscount">
                <c:out value="${pricingModel.description}" escapeXml="false"/>
              </li>
            </c:forEach>
          </ul>
           </c:if>
        </div>

        <%-- Coupon code gadget for Review your order' checkout step --%>
        <c:if test="${isCheckout}">
          <dsp:include page="/mobile/cart/gadgets/promotionCode.jsp"/>
        </c:if>
        
      </div>

      <div class="mobile_store_orderInfo">
        <dl class="mobile_store_orderSubTotals">
          <%-- Display Subtotal --%>
          <dt class="subtotal"><fmt:message key="common.items"/><fmt:message key="common.labelSeparator"/></dt>
          <dd class="subtotal">
            <dsp:include page="/global/gadgets/formattedPrice.jsp">
              <dsp:param name="price" param="order.priceInfo.rawSubtotal"/>
            </dsp:include>
          </dd>

          <%-- Display Discount --%>
          <dsp:getvalueof var="discountAmount" param="order.priceInfo.discountAmount"/>
          <c:if test="${discountAmount gt 0}">
            <dt class="discount"><fmt:message key="common.discount"/><fmt:message key="common.labelSeparator"/></dt>
            <dd class="discount">
              &minus;
              <dsp:include page="/global/gadgets/formattedPrice.jsp">
                <dsp:param name="price" param="order.priceInfo.discountAmount"/>
              </dsp:include>
            </dd>
          </c:if>

          <%-- Display Shipping --%>
          <dt class="shipping"><fmt:message key="common.shipping"/><fmt:message key="common.labelSeparator"/></dt>
          <dd class="shipping">
            <dsp:include page="/global/gadgets/formattedPrice.jsp">
              <dsp:param name="price" param="order.priceInfo.shipping"/>
            </dsp:include>
          </dd>

          <%-- Display Tax --%>
          <dt class="tax"><fmt:message key="common.tax"/><fmt:message key="common.labelSeparator"/></dt>
          <dd class="tax">
            <dsp:include page="/global/gadgets/formattedPrice.jsp">
              <dsp:param name="price" param="order.priceInfo.tax"/>
            </dsp:include>
          </dd>
        </dl>

        <dl class="mobile_store_orderTotal">
          <%-- Display Total --%>
          <dt class="total"><fmt:message key="common.total"/><fmt:message key="common.labelSeparator"/></dt>
          <dd class="total">
            <dsp:include page="/global/gadgets/formattedPrice.jsp">
              <dsp:param name="price" param="order.priceInfo.total"/>
            </dsp:include>
          </dd>
        </dl>
      </div>
    </c:if>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/checkoutOrderSummary.jsp#3 $$Change: 692002 $--%>