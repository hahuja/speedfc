<%--
  Regular order confirmation for a user who is logged in

  Page includes:
    /mobile/myaccount/orderDetail.jsp order details renderer
    /mobile/myaccount/myOrders.jsp list of placed orders

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

	<div align="center" class="mobile_store_actionConfirmation">
	  <fmt:message key="mobile.checkout_confirmResponse.emailText" />
	  <br/>
	  <dsp:valueof bean="Profile.email"/>
	</div>
	<br/>
	<div class="mobile_store_displayList">
		<ul class="mobile_store_mobileList">
		  <li class="withArrow">
			<div class="content">
			  <dsp:a page="/mobile/myaccount/orderDetail.jsp" class="block">
				<dsp:param name="orderId" bean="ShoppingCart.last.id"/>
				<fmt:message key="mobile.myaccount_checkout_confirmation_viewOrder" />
			  </dsp:a>
			</div>
		  </li>
		  <li class="withArrow">
			<div class="content">
			  <dsp:a page="/mobile/myaccount/myOrders.jsp" class="block">
				<fmt:message key="mobile.myaccount_checkout_confirmation_viewOrders" />
			  </dsp:a>
			</div>
		  </li>
		</ul>
	</div>
	<br/>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/confirmResponse.jsp#3 $$Change: 692002 $ --%>