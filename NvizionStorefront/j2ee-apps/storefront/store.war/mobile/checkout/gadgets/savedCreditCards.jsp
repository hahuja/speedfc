<%--
  This gadget lists the user's saved payment methods

  Page includes:
    /mobile/checkout/gadgets/creditCardRenderer.jsp credit card data renderer

  Required parameters:
    areaPath
      area path to form correct edit/create links
    selectable
      indicates if rendered items can be selected by user

  Optional parameters:
    displayDefaultLabeled
      indicates if default credit card is marked with 'Default' label
      Note: can be used only if 'selectable' parameter is false
    selectProperty
      sets formHandler property that should store selected card key
      Note: makes sense only if 'selectable' parameter is true
      Default value: MobileBillingFormHandler.storedCreditCardName
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Compare"/>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <dsp:getvalueof var="areaPath" param="areaPath"/>

  <dsp:getvalueof var="selectableMode" param="selectable"/>
  <dsp:getvalueof var="displayDefaultLabeled" param="displayDefaultLabeled"/>
  <dsp:getvalueof var="selectProperty" param="selectProperty"/>

  <c:if test="${not empty selectableMode and empty selectProperty}">
    <c:set var="selectProperty" value="MobileBillingFormHandler.storedCreditCardName"/>
  </c:if>

  <dsp:getvalueof var="defaultCardId" bean="Profile.defaultCreditCard.repositoryId"/>

  <ul id="mobile_store_creditCardList" class="mobile_store_mobileList
                                             <c:if test="${displayDefaultLabeled == 'true'}"> withCheckBoxes</c:if>">

    <%-- Check if the profile has credit cards and show them. --%>
    <dsp:getvalueof var="creditCards" vartype="java.lang.Object" bean="Profile.creditCards"/>
    <c:if test="${not empty creditCards}">
      <%-- Get all saved credit cards, display default card first, then sort other cards by title. --%>
      <dsp:droplet name="MapToArrayDefaultFirst">
        <dsp:param name="map" bean="Profile.creditCards"/>
        <dsp:param name="defaultId" value="${defaultCardId}"/>
        <dsp:param name="sortByKeys" value="true"/>

        <dsp:oparam name="output">
          <dsp:getvalueof var="sortedArray" vartype="java.lang.Object" param="sortedArray"/>

          <c:if test="${not empty sortedArray}">
          <%-- Display all saved credit cards. --%>
          <c:forEach var="userCard" items="${sortedArray}" varStatus="status">
            <dsp:param name="userCard" value="${userCard}"/>
            <c:if test="${not empty userCard }">
              <dsp:getvalueof var="billingAddress" param="userCard.value.billingAddress"/>

              <c:if test="${displayDefaultLabeled == 'true'}">
                <c:set var="isDefaultCard" value="${status.index eq 0 && not empty defaultCardId}"/>
              </c:if>

              <li class="withBlueArrowBig <c:if test='${empty billingAddress}'>mobile_store_empty_address</c:if>
                                          <c:if test='${not empty errorCard && errorCard == userCard.value.id}'> mobile_store_error_state</c:if>
                                          <c:if test='${isDefaultCard}'> checked</c:if>">
                <div class="content">
                  <c:if test="${selectableMode != 'false'}">
                    <dsp:input type="radio" value="${userCard.key}" name="creditcard"
                               id="mobile_store_savedCreditCard${status.count}" checked="false"
                               bean="${selectProperty}"/>
                  </c:if>

                  <%-- Display Credit Card key --%>
                  <label for="mobile_store_savedCreditCard${status.count}" onclick="">
                      <dsp:include page="creditCardRenderer.jsp">
                        <dsp:param name="creditCard" param="userCard.value"/>
                      </dsp:include>
                      <c:if test="${isDefaultCard}">
                        <span class="defaultLabel"><fmt:message key="common.default"/></span>
                      </c:if>
                  </label>

                  <%-- Display edit link --%>
                  <fmt:message var="editLinkTitle" key="common.button.editCardText"/>
                  <dsp:a bean="MobileB2CProfileFormHandler.editCard"
                         page="${areaPath}/editCreditCard.jsp" paramvalue="userCard.key" title="${editLinkTitle}"/>
                </div>
                <c:if test='${not empty errorCard && errorCard == userCard.value.id}'>
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.emptyBillingAddress"/>
                  </span>
                </c:if>
              </li>
            </c:if>
          </c:forEach>
          </c:if>
        </dsp:oparam>
      </dsp:droplet>

    </c:if>

    <li class="mobile_store_formActions">
      <div class="content withArrow">
        <dsp:a page="${areaPath}/newCreditCard.jsp" iclass="listLink">
          <fmt:message key="myaccount_newCreditCard.title"/>
        </dsp:a>
      </div>
    </li>
  </ul>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/savedCreditCards.jsp#3 $$Change: 692002 $--%>
