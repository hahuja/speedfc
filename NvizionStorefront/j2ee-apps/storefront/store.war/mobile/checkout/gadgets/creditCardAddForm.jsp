<%--
  This page for rendering the logic as well as presentation for the account specific payment information details .

  Page includes:
    None

  Required parameters:
    formHandler
      Full path to form handler that will handle creating request
    cardParamsMap
      Form handler's map property that contains credit card properties

  Optional parameters:
    nicknameProperty
      Indicates where credit card Nickname property is saved
    saveCard
      Flag to show "Save Card to Profile" option
    defaultCard
      Flag to show "Make Default Card" option
--%>
<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <dsp:getvalueof var="formHandler" param="formHandler"/>
  <dsp:getvalueof var="cardParamsMap" param="cardParamsMap"/>
  <dsp:getvalueof var="nicknameProperty" param="nicknameProperty"/>
  <dsp:getvalueof var="saveCardOption" param="saveCard"/>
  <dsp:getvalueof var="defaultCardOption" param="defaultCard"/>

  <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="${formHandler}.formExceptions"/>
  <dsp:getvalueof var="transient" bean="Profile.transient"/>

  <jsp:useBean id="errorMap" class="java.util.HashMap"/>

  <c:if test="${not empty formExceptions}">
    <c:forEach var="formException" items="${formExceptions}">
      <dsp:param name="formException" value="${formException}"/>
      <%-- Check the error message code to see what we should do --%>
      <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
      <c:choose>
        <c:when test="${'missingRequiredValue' eq errorCode}">
          <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
          <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
        </c:when>
        <c:when test="${'errorInvalidCreditCard' eq errorCode}">
          <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
          <c:set target="${errorMap}" property="${propertyName}" value="invalid"/>
        </c:when>
        <c:when test="${'errorDuplicateCCNickname' eq errorCode}">
          <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
          <c:set target="${errorMap}" property="${propertyName}" value="duplicate"/>
        </c:when>
      </c:choose>
    </c:forEach>
  </c:if>

  <div class="mobile_store_register">
    <ul class="mobile_store_mobileList mobile_store_basicForm">
      <li <c:if test="${not empty errorMap['creditCardNickname']}">class="mobile_store_error_state"</c:if>>
        <div class="content">
          <fmt:message var="nicknameLabel" key="common.nickName"/>
          <c:choose>
            <c:when test="${nicknameProperty == 'map'}">
              <c:set var="nicknameTarget" value="${cardParamsMap}"/>
            </c:when>
            <c:otherwise>
              <c:set var="nicknameTarget" value="${formHandler}"/>
            </c:otherwise>
          </c:choose>
          <label for="mobile_store_newCardNickname" /><%-- This makes VoiceOver to pausing before select element reading --%>
          <dsp:input bean="${nicknameTarget}.creditCardNickname"
                     iclass="text" type="text" maxlength="42"
                     id="mobile_store_newCardNickname" required="true">
            <dsp:tagAttribute name="placeholder" value="${nicknameLabel}"/>
          </dsp:input>	  
        </div>
        <c:if test="${not empty errorMap['creditCardNickname']}">
          <span class="errorMessage">
            <fmt:message key="mobile.form.validation.${errorMap['creditCardNickname']}"/>
          </span>
        </c:if>
      </li>
      <dsp:getvalueof var="type" bean="${cardParamsMap}.creditCardType"/>
      <li class="mobile_arrowed <c:if test="${not empty errorMap['creditCardType']}">mobile_store_error_state</c:if>">
        <div class="content">
          <label for="mobile_store_paymentInfoAddNewCardCardType"></label><%-- This makes VoiceOver to pausing before select element reading --%>
          <dsp:select id="mobile_store_paymentInfoAddNewCardCardType"
                      bean="${cardParamsMap}.creditCardType"
                      required="true" iclass="mobile_store_selectForm ${empty type ? 'default' : ''}" onchange="changeDropdown(event)">
            <dsp:option value="" iclass="default">
              <fmt:message key="common.cardType"/>
            </dsp:option>
            <dsp:option value="Visa">
              <fmt:message key="common.visa"/>
            </dsp:option>
            <dsp:option value="MasterCard">
              <fmt:message key="common.masterCard"/>
            </dsp:option>
            <dsp:option value="AmericanExpress">
              <fmt:message key="common.americanExpress"/>
            </dsp:option>
            <dsp:option value="Discover">
              <fmt:message key="common.discover"/>
            </dsp:option>
          </dsp:select>          
        </div>
        <c:if test="${not empty errorMap['creditCardType']}">
          <span class="errorMessage">
            <fmt:message key="mobile.form.validation.${errorMap['creditCardType']}"/>
          </span>
        </c:if>
      </li>

      <li <c:if test="${not empty errorMap['creditCardNumber']}">class="mobile_store_error_state"</c:if>>
        <div class="content mobile_store_numberField">
          <fmt:message var="cardNumberLabel" key="common.cardNumber"/>
          <dsp:input type="tel" id="mobile_store_cardNumber" bean="${cardParamsMap}.creditCardNumber"
                     autocomplete="off" required="true" maxlength="19">
            <dsp:tagAttribute name="placeholder" value="${cardNumberLabel}"/>
          </dsp:input>
        </div>
        <c:if test="${not empty errorMap['creditCardNumber']}">
          <span class="errorMessage">
            <fmt:message key="mobile.form.validation.${errorMap['creditCardNumber']}"/>
          </span>
        </c:if>
      </li>

      <dsp:getvalueof var="month" bean="${cardParamsMap}.expirationMonth"/>
      <dsp:getvalueof var="year" bean="${cardParamsMap}.expirationYear"/>
      <li class="mobile_arrowed <c:if test="${not empty errorMap['expirationMonth'] || not empty errorMap['expirationYear']}">mobile_store_error_state bottom</c:if>">
        <div class="content cell">
          <label class="hint expires">
            <fmt:message key="common.expirationDate"/>
          </label>

          <%-- Check for errors in expiration date --%>
          <c:if test="${not empty errorMap['expirationMonth'] || not empty errorMap['expirationYear']}">
            <c:set var="divClassExp" value="mobile_store_error_state"/>
          </c:if>
          <%-- Add bottom border --%>
          <c:if test="${transient}">
            <c:set var="divClassExpBottom" value="bottom"/>
          </c:if>
          <div class="mobile_store_expire_date ${divClassExp} ${divClassExpBottom}">
            <div class="mobile_store_month">
              <dsp:select id="mobile_store_cardExpirationDateMonthSelect"
                          bean="${cardParamsMap}.expirationMonth"
                          required="true" iclass="mobile_store_selectForm ${empty month ? 'default' : ''}"
                          onchange="changeDropdown(event)">
                <dsp:option value="">
                  <fmt:message key="common.month"/>
                </dsp:option>
                <%-- Display months --%>
                <c:forEach var="count" begin="1" end="12" step="1" varStatus="status">
                  <dsp:option value="${count}">
                    <fmt:message key="common.month${count}"/>
                  </dsp:option>
                </c:forEach>
              </dsp:select>              
              <label for="mobile_store_cardExpirationDateMonthSelect" /><%-- This makes VoiceOver to pausing before select element reading --%>
            </div>

            <div class="mobile_store_date_delimiter">
              <label class="hint">/&nbsp;</label>
            </div>

            <div class="mobile_store_year">
              <crs:yearList numberOfYears="16"
                            bean="${cardParamsMap}.expirationYear"
                            id="mobile_store_cardExpirationDateYearSelect"
                            selectRequired="true" yearString="true" onchange="changeDropdown(event)"
                            iclass="mobile_store_selectForm ${empty year ? 'default' : ''}"/>
              <label for="mobile_store_cardExpirationDateYearSelect" /><%-- This makes VoiceOver to pausing before select element reading --%>
            </div>
          </div>
        </div>
        <c:choose>
          <c:when test="${not empty errorMap['expirationMonth']}">
            <span class="errorMessage"><fmt:message key="mobile.form.validation.${errorMap['expirationMonth']}"/></span>
          </c:when>
          <c:when test="${not empty errorMap['expirationYear']}">
            <span class="errorMessage"><fmt:message key="mobile.form.validation.${errorMap['expirationYear']}"/></span>
          </c:when>
        </c:choose>
      </li>

      <%-- If the shopper is transient (a guest shopper) we don't offer to save the card --%>
      <c:if test="${not transient}">
        <%-- Display save created credit card to profile --%>
        <c:if test="${saveCardOption == 'true'}">
          <li class="clear">
            <div class="content">
              <dsp:input type="checkbox" bean="${formHandler}.saveCreditCard"
                         checked="true" id="mobile_store_saveCreditCard"/>
              <label for="mobile_store_saveCreditCard" onclick="">
                <fmt:message key="checkout_billing.savePaymentInfor"/>
              </label>
            </div>
          </li>
        </c:if>
        <c:if test="${defaultCardOption == 'true'}">
          <li class="clear">
            <div class="content">
              <dsp:input type="checkbox" bean="${cardParamsMap}.newCreditCard"
                         checked="" id="mobile_store_defaultCreditCard"/>
              <label for="mobile_store_defaultCreditCard" onclick="">
                <fmt:message key="myaccount_paymentInfoCardAddEdit.defaultCard"/>
              </label>
            </div>
          </li>
        </c:if>
      </c:if>
    </ul>

    <div class="mobile_store_formActions">
      <span class="mobile_store_basicButton">
        <fmt:message var="submitLabel" key="common.button.continueText"/>
        <dsp:input bean="${formHandler}.storeNewCreditCardDataWithoutAddress" type="submit" iclass="mainActionBtn"
                   alt="${submitLabel}" value="${submitLabel}" id="mobile_store_createCreditCard"/>
      </span>
    </div>
  </div>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/creditCardAddForm.jsp#4 $$Change: 692002 $--%>
