<%--
  This container page contains hidden input param for billing info form

  Page includes:
    None

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/store/droplet/ProfileSecurityStatus"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>

  <%--
    Check if user is anonymous or the session has expired. If so set the sessionExpirationURL

    Output parameters:
      anonymous
        user is not logged in
      default
        user has been recognized
  --%>
  <dsp:droplet name="ProfileSecurityStatus">
    <dsp:oparam name="anonymous">
      <dsp:input type="hidden" name="sessionExpirationURL" bean="MobileB2CProfileFormHandler.sessionExpirationURL" value="${pageContext.request.contextPath}/index.jsp"/>
    </dsp:oparam>
    <dsp:oparam name="default">
      <dsp:input type="hidden" name="sessionExpirationURL" bean="MobileB2CProfileFormHandler.sessionExpirationURL" value="${pageContext.request.contextPath}/checkout/login.jsp"/>
    </dsp:oparam>
  </dsp:droplet>

  <dsp:input type="hidden" name="moveToConfirmSuccessURL" bean="MobileB2CProfileFormHandler.moveToConfirmSuccessURL" value="confirm.jsp"/>
  <dsp:input type="hidden" name="moveToConfirmErrorURL" bean="MobileB2CProfileFormHandler.moveToConfirmErrorURL" value="billing.jsp?preFillValues=true"/>
  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/billingFormParams.jsp#3 $$Change: 692002 $--%>
