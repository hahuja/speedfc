<%--
  Display priceBean details including quantity, discounted price, promotion applied and old price.

  Page includes:
    formattedPrice
      /mobile/global/gadgets/formattedPrice.jsp price formatter

  Required parameters:
    quantity
      items quantity
    price
      item's price
    oldPrice
      list price for item (before applying discounts)

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:getvalueof var="quantity" param="quantity"/>
  <dsp:getvalueof var="price" param="price"/>
  <dsp:getvalueof var="oldPrice" param="oldPrice"/>
  <dsp:include page="../../global/gadgets/formattedPrice.jsp">
    <dsp:param name="price" value="${price}"/>
    <dsp:param name="saveFormattedPrice" value="true"/>
  </dsp:include>
  <span class="${fn:length(formattedPrice) > 7 ? 'longPrice' : ''}">
    <c:if test="${not empty quantity}">
      <span class="mobile_store_quantity">
        <fmt:formatNumber value="${quantity}" type="number"/>
      </span>
    </c:if>
    <span class="mobile_store_atRateOf">
      <fmt:message key="common.atRateOf"/>
    </span>
    <c:if test="${not empty price}">
      <span class="mobile_store_newPrice">
        ${formattedPrice}
      </span>
    </c:if>
    <c:if test="${not empty oldPrice}">
      <br/>
      <span class="mobile_store_oldPrice">
        <fmt:message key="common.price.old"/>
        <del>
          <dsp:include page="../../global/gadgets/formattedPrice.jsp">
            <dsp:param name="price" value="${oldPrice}"/>
          </dsp:include>
        </del>
      </span>
    </c:if>
  </span>
  <br/>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/confirmItemPrice.jsp#3 $$Change: 692002 $--%>
