<%--
  Renders billing page during checkout process where user can select saved credit card or create new one

  Page includes:
    /mobile/checkout/selectCreditCard.jsp list of saved credit cards
    /mobile/checkout/newCreditCard new credit card page

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupDroplet"/>
  <dsp:importbean bean="/atg/store/droplet/EnsureCreditCard"/>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet"/>

  <%-- If there are errors during form submition (we tried to select CC w/o billing address) the current order is invalidated and doesn't contain priceInfo.
     Will reprice whole order in this case --%>
  <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="MobileBillingFormHandler.formExceptions"/>
  <c:if test="${not empty formExceptions}">
    <dsp:droplet name="RepriceOrderDroplet">
      <dsp:param name="pricingOp" value="ORDER_TOTAL"/>
    </dsp:droplet>
  </c:if>

  <%-- Commenting code related to store credits --%>
  <%-- Apply available store credits to the order  --%>
  <%--
  <dsp:setvalue bean="MobileBillingFormHandler.applyStoreCreditsToOrder" value=""/>

  <dsp:getvalueof var="order" vartype="atg.commerce.Order" bean="ShoppingCart.current"/>
  <c:choose>
    <c:when test="${order.priceInfo.total > order.storeCreditsAppliedTotal}">
      <%-- Store credits are not enough. --%>

      <%-- INITALIZE COMMERCE SHIPPING OBJECTS --%>
      <dsp:droplet name="ShippingGroupDroplet">
        <dsp:param name="createOneInfoPerUnit" value="true"/>
        <dsp:param name="clearShippingInfos" value="true"/>
        <dsp:param name="shippingGroupTypes" value="hardgoodShippingGroup"/>
        <dsp:param name="initShippingGroups" value="true"/>
        <dsp:param name="initBasedOnOrder" value="true"/>
        <dsp:oparam name="output"/>
      </dsp:droplet>

      <%-- Each order should possess one Credit Card payment group. Check this and create group, if needed. --%>
      <dsp:droplet name="EnsureCreditCard">
        <dsp:param name="order" bean="ShoppingCart.current"/>
      </dsp:droplet>

      <dsp:getvalueof var="creditCards" vartype="java.lang.Object" bean="Profile.creditCards"/>
      <c:choose>
        <c:when test="${not empty creditCards}">
          <%-- Display list of credit cards --%>
          <dsp:include page="selectCreditCard.jsp"/>
        </c:when>
        <c:otherwise>
          <%-- Display new credit card form --%>
          <dsp:include page="newCreditCard.jsp"/>
        </c:otherwise>
      </c:choose>
  <%--
    </c:when>
    <c:otherwise>
      <%-- Order is paid by store credits, display its total and 'Continue' button. --%>
      <%--
      <dsp:include page="paidByStoreCredits.jsp"/>
    </c:otherwise>
  </c:choose>
--%>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/billing.jsp#3 $$Change: 692002 $--%>