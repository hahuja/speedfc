<%--
  This page fragment renders user's payment methods for selections

  Page includes:
    None

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet"/>

  <%-- If there are errors during form submition (we entered invalid CVV) the current order is invalidated and doesn't contain priceInfo.
       Will reprice whole order in this case --%>
  <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="MobileBillingFormHandler.formExceptions"/>
  <jsp:useBean id="errorMap" class="java.util.HashMap"/>
  <c:if test="${not empty formExceptions}">
    <c:forEach var="formException" items="${formExceptions}">
      <dsp:param name="formException" value="${formException}"/>
      <%-- Check the error message code to see what we should do --%>
      <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
      <c:choose>
        <c:when test="${'missingRequiredValue' eq errorCode or 'invalidCreditCardVerificationNumber' eq errorCode}">
          <c:set target="${errorMap}" property="mobile_store_cvvCodeField" value="invalid"/>
        </c:when>
      </c:choose>
    </c:forEach>

    <dsp:droplet name="RepriceOrderDroplet">
      <dsp:param name="pricingOp" value="ORDER_TOTAL"/>
    </dsp:droplet>
  </c:if>

  <fmt:message key="checkout_billing.billingInformation" var="pageTitle"/>
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div class="mobile_store_checkout" id="mobile_store_selectPaymentMethod">
        <h2>
          <dsp:getvalueof var="creditCardType" bean="MobileBillingFormHandler.creditCard.creditCardType"/>
          <c:out value="${creditCardType}"/>
          <fmt:message key="mobile.common.ellipsis"/>
          <dsp:getvalueof var="creditCardNumber" bean="MobileBillingFormHandler.creditCard.creditCardNumber"/>
          <c:set var="creditCardNumberLength" value="${fn:length(creditCardNumber)}"/>
          <c:out value="${fn:substring(creditCardNumber,creditCardNumberLength-4,creditCardNumberLength)}"/>
        </h2>

        <dsp:form id="atg_store_checkoutBilling" formid="atg_store_checkoutBilling" action="#" method="post">
          <div class="mobile_store_cvvCode">
            <div class="mobile_store_creditCard_type${creditCardType}">
              <%-- Include hidden form params --%>
              <dsp:input type="hidden" value="confirm.jsp" bean="MobileBillingFormHandler.moveToConfirmSuccessURL"/>
              <dsp:input type="hidden" value="billingCVV.jsp" bean="MobileBillingFormHandler.moveToConfirmErrorURL"/>
              <dsp:input type="hidden" value="login.jsp" bean="MobileBillingFormHandler.sessionExpirationURL"/>
              <dsp:input type="hidden" value="billingWithSavedCard" id="atg_store_continueButton" bean="MobileBillingFormHandler.moveToConfirm" name="submit" priority="-10"/>

              <%-- Coupon code --%>
              <dsp:getvalueof var="couponCode" bean="CouponFormHandler.couponCode"/>
              <dsp:input bean="CouponFormHandler.couponCode" priority="10" type="hidden" id="atg_store_promotionCodeInput" value="${couponCode}"/>

              <fmt:message key="checkout_billing.securityCode" var="securityCodeText"/>
              <dsp:input id="mobile_store_cvvCodeField" type="tel" value="" bean="MobileBillingFormHandler.newCreditCardVerificationNumber" maxlength="4">
                <dsp:tagAttribute name="placeholder" value="${securityCodeText}"/>
              </dsp:input>
              <c:if test="${not empty errorMap['mobile_store_cvvCodeField']}">
                <span id="mobile_store_formValidationError">
                  <fmt:message key="mobile.form.validation.${errorMap['mobile_store_cvvCodeField']}"/>
                </span>
              </c:if>
              <p>
                <c:choose>
                  <c:when test="${creditCardType eq 'Visa'}">
                    <fmt:message key="checkout_whatsThisPopup.instructions.visa"/>
                  </c:when>
                  <c:otherwise>
                    <fmt:message key="checkout_whatsThisPopup.instructions.americanExpress"/>
                  </c:otherwise>
                </c:choose>
              </p>

              <%-- "Submit" button --%>
              <br/>
              <div class="mobile_store_formActions">
                <span class="mobile_store_basicButton">
                  <fmt:message var="submitButtonText" key="common.button.continueText"/>
                  <dsp:input bean="MobileBillingFormHandler.moveToConfirm" type="submit"
                             id="mobile_store_submitButton" iclass="mainActionBtn" value="${submitButtonText}"/>
                </span>
              </div>
            </div>
          </div>
        </dsp:form>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/billingCVV.jsp#3 $$Change: 692002 $--%>
