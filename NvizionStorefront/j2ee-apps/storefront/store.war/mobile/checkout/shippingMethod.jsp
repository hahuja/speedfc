<%--
  This page includes the gadgets for the select shipping method page for a single shipping group.
  (That is, all items will be shipped to the same shipping address)

  Page includes:
    /checkout/gadgets/checkoutErrorMessages.jsp error messages output
    /mobile/checkout/shippingFormParams.jsp shipping form hidden parameters
    /global/gadgets/formattedPrice.jsp price formatter

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>
  <dsp:importbean bean="/atg/store/mobile/droplet/ValidateShippingMethod"/>
  <dsp:importbean bean="/atg/commerce/pricing/AvailableShippingMethods"/>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <fmt:message key="common.foot.shipping" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div class="mobile_store_displayList" id="mobile_store_shippingMethod">
        <h2><fmt:message key="common.defaultShippingMethod"/></h2>

        <dsp:include page="/checkout/gadgets/checkoutErrorMessages.jsp">
          <dsp:param name="formhandler" bean="ShippingGroupFormHandler"/>
          <dsp:param name="divid" value="mobile_store_formValidationError"/>
        </dsp:include>

          <%-- Check if there are any not gift shipping groups --%>
        <dsp:getvalueof var="anyHardgoodShippingGroups" vartype="java.lang.Boolean"
                        bean="ShippingGroupFormHandler.anyHardgoodShippingGroups"/>

        <c:if test='${anyHardgoodShippingGroups}'>

          <dsp:form id="mobile_store_checkoutShippingMethod" iclass="mobile_store_checkoutOption"
                    formid="mobile_store_checkoutShippingAddress"
                    action="${pageContext.request.requestURI}" method="post">


            <%-- Include hidden form params --%>
            
            <%-- Coupon code --%>
            <dsp:getvalueof var="couponCode" bean="CouponFormHandler.currentCouponCode"/>
            <dsp:input bean="CouponFormHandler.couponCode" priority="10" type="hidden" id="atg_store_promotionCodeInput" value="${couponCode}"/>
            
            <dsp:include page="shippingFormParams.jsp" flush="true"/>

            <ul id="mobile_store_shippingMethods" class="mobile_store_mobileList">

                <%-- If shipping group is not passed, get first non-gift shipping group with relationships
           or first gift shipping group --%>
              <c:if test="${empty shippingGroup}">
                <dsp:getvalueof var="shippingGroup"
                                bean="ShippingGroupFormHandler.firstNonGiftHardgoodShippingGroupWithRels"/>
              </c:if>
              <c:if test="${empty shippingGroup}">
                <dsp:getvalueof var="giftShippingGroups" vartype="java.lang.Object"
                                bean="ShippingGroupFormHandler.giftShippingGroups"/>
                <c:if test="${not empty giftShippingGroups}">
                  <dsp:getvalueof var="shippingGroup" value="${giftShippingGroups[0]}"/>
                </c:if>
              </c:if>

                <%-- Get current shipping method defined in the shipping group --%>
              <dsp:getvalueof value="${shippingGroup.shippingMethod}" var="currentMethod"/>

                <%-- Display available methods --%>
              <dsp:droplet name="AvailableShippingMethods">
                <dsp:param name="shippingGroup" value="${shippingGroup}"/>
                <dsp:oparam name="output">
                  <dsp:getvalueof var="availableShippingMethods" vartype="java.lang.Object"
                                  param="availableShippingMethods"/>

                    <%-- Check if current shipping method defined in the shipping group is
              the one from available shipping methods --%>
                  <c:if test="${not empty currentMethod}">
                    <c:set var="isCurrentInAvailableMethods" value="false"/>
                    <c:forEach var="method" items="${availableShippingMethods}" varStatus="status">
                      <c:if test="${currentMethod eq method}">
                        <c:set var="isCurrentInAvailableMethods" value="true"/>
                      </c:if>
                    </c:forEach>
                  </c:if>
                  <c:if test="${empty currentMethod or not isCurrentInAvailableMethods}">
                      <%-- Current method in shipping group is either not defined or is not
                    available for this destination. Get default shipping method from
                    user profile. --%>
                    <dsp:getvalueof bean="Profile.defaultCarrier" var="currentMethod"/>
                  </c:if>
                  <c:forEach var="method" items="${availableShippingMethods}" varStatus="status">
                    <dsp:droplet name="ValidateShippingMethod">
                      <dsp:param name="shippingMethod" value="${method}"/>
                      <dsp:param name="shippingGroup" value="${shippingGroup}"/>
                      <dsp:oparam name="valid">
                        <dsp:param name="method" value="${method}"/>

                          <%-- Determine shipping price for the current shipping method --%>
                        <dsp:droplet name="/atg/store/pricing/PriceShippingMethod">
                          <dsp:param name="shippingGroup" value="${shippingGroup}"/>
                          <dsp:param name="shippingMethod" param="method"/>
                          <dsp:oparam name="output">
                            <dsp:getvalueof var="shippingPrice" param="shippingPrice"/>
                          </dsp:oparam>
                        </dsp:droplet>
                        <c:set var="shippingMethod" value="${fn:replace(method, ' ', '')}"/>
                        <c:set var="shippingMethodResourceKey" value="checkout_shipping.delivery${shippingMethod}"/>
                        <c:set var="shippingMethodContentResourceKey" value="${shippingMethodResourceKey}Content"/>
                        <li>
                          <div class="content withArrow">
                            <dsp:input type="radio" iclass="radio"
                                       bean="ShippingGroupFormHandler.shippingMethod" paramvalue="method"
                                       id="mobile_store_shipping${shippingMethod}"/>

                            <label for="mobile_store_shipping${shippingMethod}" onclick="">
                                <%-- shipping method name --%>
                              <span class="mobile_store_shippingMethodTitle"><fmt:message
                                      key="${shippingMethodResourceKey}"/><fmt:message
                                      key="common.labelSeparator"/></span>
                          <span class="mobile_store_shippingMethodPrice">
                            <dsp:include page="/global/gadgets/formattedPrice.jsp">
                              <dsp:param name="price" value="${shippingPrice}"/>
                            </dsp:include>
                          </span>
                            </label>
                          </div>
                        </li>
                      </dsp:oparam>
                    </dsp:droplet>
                  </c:forEach>
                </dsp:oparam>
              </dsp:droplet><%-- End Available Shipping Methods Droplet --%>
            </ul>

            <dsp:input type="hidden" bean="ShippingGroupFormHandler.updateShippingMethod" value="updateShippingMethod"/>

          </dsp:form>
          <script type="text/javascript">
            $(document).ready(function() {
              $("#mobile_store_shippingMethods").delayedSubmit();
            });
          </script>
        </c:if>

      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/shippingMethod.jsp#3 $$Change: 692002 $--%>
