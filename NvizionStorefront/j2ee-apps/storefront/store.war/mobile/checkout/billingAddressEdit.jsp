<%--
  This page fragment renders edit billing address page for user if he creates new credit card

  Page includes:
    /mobile/myaccount/gadgets/addressAddEdit.jsp renderer of address form

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>

  <dsp:setvalue bean="ShippingGroupFormHandler.editShippingAddressNickName" paramvalue="nickName"/>
  <dsp:setvalue bean="ShippingGroupFormHandler.shippingAddressNewNickName" paramvalue="nickName"/>
  <dsp:setvalue bean="ShippingGroupFormHandler.initEditAddressForm" value=""/>

  <fmt:message key="mobile.common.editBillingAddress" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>

      <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="ShippingGroupFormHandler.formExceptions"/>

      <%-- Handle form exceptions --%>
      <jsp:useBean id="errorMap" class="java.util.HashMap"/>
      <c:if test="${not empty formExceptions}">
        <c:forEach var="formException" items="${formExceptions}">
          <dsp:param name="formException" value="${formException}"/>
          <%-- Check the error message code to see what we should do --%>
          <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>

          <c:choose>
            <c:when test="${'duplicateNickname' eq errorCode}"><c:set target="${errorMap}" property="nickName" value="inUse"/></c:when>
            <c:when test="${'stateIsIncorrect' eq errorCode}"><c:set target="${errorMap}" property="state" value="invalid"/><c:set target="${errorMap}" property="country" value="invalid"/></c:when>
            <c:when test="${'missingRequiredValue' eq errorCode}">
              <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
              <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
            </c:when>
          </c:choose>
        </c:forEach>
      </c:if>

      <div class="mobile_store_displayList" id="mobile_store_newBillingAddress">
        <h2>
          <span><fmt:message key="mobile.common.editBillingAddress"/></span>
        </h2>

        <div class="mobile_store_register">
          <dsp:form id="mobile_store_paymentInfoAddNewBillingAddressForm" formid="mobile_store_paymentInfoAddNewBillingAddressForm"
                    action="${originatingRequest.requestURI}" method="post">

            <dsp:input type="hidden" bean="ShippingGroupFormHandler.editShippingAddressSuccessURL" value="creditCardAddressSelect.jsp"/>
			<dsp:getvalueof param="nickName" var="nickname"/>
            <dsp:input type="hidden" bean="ShippingGroupFormHandler.editShippingAddressErrorURL" value="billingAddressEdit.jsp?preFillValues=true&nickName=${nickname}"/>
            
            <%-- Coupon code --%>
            <dsp:getvalueof var="couponCode" bean="CouponFormHandler.currentCouponCode"/>
            <dsp:input bean="CouponFormHandler.couponCode" priority="10" type="hidden" id="atg_store_promotionCodeInput" value="${couponCode}"/>

            <ul class="mobile_store_mobileList">
              <%-- Check for errors in nickName --%>
              <c:if test="${not empty errorMap['nickName']}">
                <c:set var="liClassNick" value="mobile_store_error_state"/>
              </c:if>
              <li class="${liClassNick}">
                <div class="content">
                  <dsp:input type="hidden" bean="ShippingGroupFormHandler.editShippingAddressNickName"/>
                  <dsp:input type="text" id="mobile_store_nickNameInput" maxlength="42"
                             bean="ShippingGroupFormHandler.shippingAddressNewNickName">
                    <fmt:message var="nickPlace" key="common.nickName"/>
                    <dsp:tagAttribute name="placeholder" value="${nickPlace}"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['nickName']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['nickName']}"/>
                  </span>
                </c:if>
              </li>

                <%-- Start inclusion of addressAddEdit for rendering parameters of the form --%>
              <dsp:include page="/mobile/myaccount/gadgets/addressAddEdit.jsp">
                <dsp:param name="formHandlerComponent" value="/atg/commerce/order/purchase/ShippingGroupFormHandler.editAddress"/>
                <dsp:param name="restrictionDroplet" value="/atg/store/droplet/ShippingRestrictionsDroplet"/>
                <dsp:param name="errorMap" value="${errorMap}"/>
              </dsp:include>

            </ul>

            <div class="mobile_store_formActions">
              <span class="mobile_store_basicButton">
                <fmt:message var="shipToButtonText" key="mobile.common.done"/>
                <dsp:input id="mobile_store_createShippingAddress" iclass="mainActionBtn" type="submit" bean="ShippingGroupFormHandler.editShippingAddress" value="${shipToButtonText}"/>
              </span>
            </div>

          </dsp:form>
        </div>

      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/billingAddressEdit.jsp#3 $$Change: 692002 $--%>
