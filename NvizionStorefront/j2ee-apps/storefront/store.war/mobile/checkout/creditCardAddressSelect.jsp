<%--
  This page fragment renders all stored addresses for a registered user

  Page includes:
    /mobile/checkout/gadgets/billingAddressesList.jsp list of saved addresses to use as billing one

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>

  <fmt:message key="myAccount.checkoutDefaults.addCreditCard" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div class="mobile_store_displayList" id="mobile_store_shippingAddress">
        <h2><fmt:message key="myaccount_paymentInfoCardAddEdit.billingAddress"/></h2>

        <dsp:form id="mobile_store_checkoutShippingAddress" formid="mobile_store_checkoutShippingAddress"
          action="${pageContext.request.requestURI}" method="post">

          <div class="mobile_store_chooseShippingAddresses">
            <dsp:input type="hidden" bean="MobileBillingFormHandler.selectBillingAddressSuccessURL" value="billingCVV.jsp"/>
            <dsp:input type="hidden" bean="MobileBillingFormHandler.selectBillingAddressErrorURL" value="creditCardAddressSelect.jsp?preFillValues=true"/>

            <%-- Coupon code --%>
            <dsp:getvalueof var="couponCode" bean="CouponFormHandler.currentCouponCode"/>
            <dsp:input bean="CouponFormHandler.couponCode" priority="10" type="hidden" id="atg_store_promotionCodeInput" value="${couponCode}"/>

            <dsp:include page="gadgets/billingAddressesList.jsp" flush="false"/>

            <dsp:input type="hidden" bean="MobileBillingFormHandler.selectBillingAddress" value="selectBillingAddress" priority="-10"/>
          </div>
        </dsp:form>
        <script type="text/javascript">
          $(document).ready(function() {
            $("#mobile_store_savedBillingAddresses").delayedSubmit();
          });
        </script>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/creditCardAddressSelect.jsp#3 $$Change: 692002 $--%>
