<%--
  This page fragment renders page to edit existing card

  Page includes:
    /mobile/myaccount/gadgets/creditCardEditForm.jsp edit credit card form

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>

  <dsp:getvalueof var="mode" param="mode"/>

  <c:if test="${mode == 'restore'}">
    <dsp:setvalue bean="MobileB2CProfileFormHandler.restoreCardDataFromSession" value=""/>
  </c:if>

  <fmt:message key="myaccount_myAccountMenu.paymentInfo" var="paymentInfoTitle"/>
  <fmt:message key="mobile.myaccount_payment_subHeader.editCard" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:attribute name="modalContent">
      <dsp:getvalueof var="cardNickname" bean="MobileB2CProfileFormHandler.editValue.nickname"/>
      <div class="mobile_store_moveDialog" id="removeItemDialog">
        <div class="mobile_store_moveItems">
          <ul>
            <li class="remove">
              <fmt:message key="mobile.button.removeText" var="removeText"/>
              <fmt:message key="mobile.myaccount_storedCreditCards.removeCard" var="removeCard"/>
              <dsp:a title="${removeCard}" bean="MobileB2CProfileFormHandler.removeCard" value="${cardNickname}" iclass="mobile_store_removeLink" href="selectCreditCard.jsp">${removeText}</dsp:a>
            </li>
          </ul>
        </div>
      </div>
      <div class="mobile_store_changeDialog" id="changeBillingAddressDialog">
        <div class="mobile_store_content">
          <ul>
            <li class="change">
              <a href="javascript:void(0);" onclick="">Overwrite Current Billing Address</a>
            </li>
          </ul>
        </div>
      </div>
    </jsp:attribute>
    <jsp:body>
	  <div class="mobile_store_checkout">
      <h2>
        <span><fmt:message key="myaccount_accountCardEdit.title"/></span>
      </h2>

      <div class="mobile_store_register">
        <dsp:form id="mobile_store_paymentInfoAddNewCardForm" formid="mobile_store_paymentInfoAddNewCardForm"
                  action="${originatingRequest.requestURI}" method="post">

          <dsp:getvalueof var="originatingRequestURL"
                    vartype="java.lang.String" bean="/OriginatingRequest.requestURL"/>

          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.updateCardSuccessURL" value="billing.jsp"/>
          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.updateCardErrorURL" value="${originatingRequestURL}?preFillValues=true"/>
          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.updateAddressSuccessURL"
                     value="editCreditCardAddressEdit.jsp?editAddress="/>
          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.newAddressSuccessURL"
                     value="editCreditCardAddressAdd.jsp"/>

          <dsp:include page="../myaccount/gadgets/creditCardEditForm.jsp" flush="false">
          </dsp:include>
        </dsp:form>
      </div>

    </div>
	</jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/editCreditCard.jsp#3 $$Change: 692002 $--%>
