<%--
  This page renders the form that allow the shopper to add a new shipping address.

  Page includes:
    /mobile/checkout/shippingFormParams.jsp shipping form parameters
    /mobile/myaccount/gadgets/addressAddEdit.jsp renderer of address form

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <fmt:message key="common.foot.shipping" var="pageTitle"/>
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <%-- Handle form exceptions --%>
      <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="ShippingGroupFormHandler.formExceptions"/>
      <jsp:useBean id="errorMap" class="java.util.HashMap"/>
      <c:if test="${not empty formExceptions}">
        <c:forEach var="formException" items="${formExceptions}">
          <dsp:param name="formException" value="${formException}"/>
          <%-- Check the error message code to see what we should do --%>
          <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
          <dsp:getvalueof var="propertyPath" vartype="java.lang.String" param="formException.propertyPath"/>
          <c:choose>
            <c:when test="${'duplicateNickname' eq errorCode}">
              <c:set target="${errorMap}" property="nickName" value="inUse"/>
            </c:when>
            <c:when test="${'stateIsIncorrect' eq propertyPath}">
              <c:set target="${errorMap}" property="state" value="invalid"/>
              <c:set target="${errorMap}" property="country" value="invalid"/>
            </c:when>
            <c:when test="${'missingRequiredValue' eq errorCode}">
              <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
              <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
            </c:when>
          </c:choose>
        </c:forEach>
      </c:if>

      <div class="mobile_store_displayList" id="mobile_store_shippingAddresCreate">
        <h2><fmt:message key="mobile.checkout_shippingAddressCreate.title"/></h2>
        <dsp:form action="${pageContext.request.requestURI}" method="post" formid="shippingaddresscreateform">
          <div class="mobile_store_createNewShippingAddress">
            <%-- Include hidden form params --%>
            <dsp:include page="shippingFormParams.jsp" flush="true"/>
            <dsp:input type="hidden" bean="ShippingGroupFormHandler.address.ownerId" beanvalue="Profile.id"/>
            <ul class="mobile_store_mobileList">
              <%-- Check for errors in "Nickname this Address" --%>
              <c:if test="${not empty errorMap['nickName'] or not empty errorMap['newShipToAddressName']}">
                <c:set var="liClassNick" value="mobile_store_error_state"/>
              </c:if>
              <li class="${liClassNick}">
                <div class="content">
                  <dsp:input type="text" bean="ShippingGroupFormHandler.newShipToAddressName"
                             iclass="mobile_store_textBoxForm" maxlength="42" required="true" id="mobile_store_addressNameInput">
                    <fmt:message var="nickPlace" key="common.addressNickname"/>
                    <dsp:tagAttribute name="placeholder" value="${nickPlace}"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['nickName']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['nickName']}"/>
                  </span>
                </c:if>
                <c:if test="${not empty errorMap['newShipToAddressName']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['newShipToAddressName']}"/>
                  </span>
                </c:if>
              </li>

              <%-- Inclusion of addressAddEdit for rendering parameters of the form --%>
              <dsp:include page="/mobile/myaccount/gadgets/addressAddEdit.jsp">
                <dsp:param name="formHandlerComponent" value="/atg/commerce/order/purchase/ShippingGroupFormHandler.address"/>
                <dsp:param name="restrictionDroplet" value="/atg/store/droplet/ShippingRestrictionsDroplet"/>
                <dsp:param name="errorMap" value="${errorMap}"/>
              </dsp:include>
            </ul>

            <%-- "Submit" button --%>
            <div class="mobile_store_formActions">
              <span class="mobile_store_basicButton">
                <fmt:message var="shipToButtonText" key="mobile.common.done"/>
                <dsp:input id="mobile_store_createShippingAddress" iclass="mainActionBtn" type="submit"
                           bean="ShippingGroupFormHandler.shipToNewAddress" value="${shipToButtonText}"/>
              </span>
            </div>
          </div>
        </dsp:form>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/shippingAddressAdd.jsp#3 $$Change: 692002 $--%>
