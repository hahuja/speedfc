<%--
  Draft page to pay by store credits

  Page includes:
    None

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/order/purchase/BillingFormHandler" />

  <fmt:message key="mobile.checkout_payment.title" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div class="mobile_store_displayList" id="mobile_store_selectPaymentMethod">
        <h2>
          <span><fmt:message key="mobile.checkout_payment.title"/></span>
        </h2>

        <div class="mobile_store_paidByStoreCredits">

          <dsp:input bean="BillingFormHandler.moveToConfirmSuccessURL" type="hidden" value="confirm.jsp"/>
          <p><fmt:message key="common.paidStoreCredit"/></p>
          <%-- Go to Confirm button. --%>
          <span class="mobile_store_basicButton">
            <fmt:message var="caption" key="common.button.continueText"/>
            <dsp:input bean="BillingFormHandler.billingWithStoreCredit" type="submit" value="${caption}"/>
          </span>
        </div>

      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/paidByStoreCredits.jsp#3 $$Change: 692002 $--%>
