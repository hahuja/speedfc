<%-- 
  This is the last checkout step page. It displays all cart contents, shipping and billing information.
  It also displays button to confirm current order.

  Page includes:
    /mobile/global/gadgets/obtainHardgoodShippingGroup.jsp return hardgoodShippingGroups counter,
                                                                  hardgoodShippingGroup object,
                                                                  giftWrapCommerceItem
    /mobile/myaccount/myOrders.jsp list of placed orders
    /mobile/global/gadgets/orderSummary.jsp renderer of order summary

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>

  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/store/droplet/ProfileSecurityStatus"/>
  <dsp:importbean bean="/atg/store/order/purchase/CommitOrderFormHandler"/>
  <dsp:param name="order" bean="ShoppingCart.current"/>
  <dsp:include page="/mobile/global/gadgets/obtainHardgoodShippingGroup.jsp">
    <dsp:param name="order" param="order"/>
  </dsp:include>
  
  
    <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="CommitOrderFormHandler.formExceptions"/>
   <%-- Handle form exceptions --%>
    <jsp:useBean id="errorMap" class="java.util.HashMap"/>
    <c:if test="${not empty formExceptions}">
      <c:forEach var="formException" items="${formExceptions}">
        <dsp:param name="formException" value="${formException}"/>
<!--         Check the error message code to see what we should do -->
        <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
        <c:choose>
          <c:when test="${'invalidConfirmEmailAddress' eq errorCode}">
            <c:set target="${errorMap}" property="email" value="invalid"/>
          </c:when>
          <c:when test="${'confirmEmailAddressAlreadyExists' eq errorCode}">
            <c:set target="${errorMap}" property="email" value="exists"/>
          </c:when>
        </c:choose>
      </c:forEach>
    </c:if>
  
  
  
  <c:choose>
    <c:when test="${(requestScope.hardgoodShippingGroups != 1) or not empty requestScope.giftWrapCommerceItem}">
      <dsp:include page="/mobile/myaccount/myOrders.jsp">
        <dsp:param name="redirectOrderId" param="order.id"/>
        <dsp:param name="hideOrderList" value="true" />
        <dsp:param name="redirectUrl" value="/checkout/confirm.jsp"/>
      </dsp:include>
    </c:when>
    <c:otherwise>
      <fmt:message key="mobile.checkout_review_your_order.title" var="reviewYourOrder"/>
      <crs:mobilePageContainer titleString="${reviewYourOrder}">
        <jsp:body>
            <div id="mobile_store_orderDetailContainer" class="mobile_store_confirmDetails">
              <h2><c:out value="${reviewYourOrder}"/></h2>
              
              <dsp:form formid="confirmgadgetsform" action="${originatingRequest.requestURI}"  method="post">
              
              <dsp:include page="../global/gadgets/orderSummary.jsp">
                <dsp:param name="order" param="order"/>
                <dsp:param name="isCheckout" value="true"/>
                <dsp:param name="hardgoodShippingGroup" value="${requestScope.hardgoodShippingGroup}" />
              </dsp:include>

              <%-- Place My Order button --%>                
                <fmt:message var="placeOrderButtonText" key="checkout_confirmPlaceOrder.button.placeOrderText"/>
                <%-- Success/Error URLs. --%>
                <dsp:input bean="CommitOrderFormHandler.commitOrderSuccessURL" type="hidden" value="../checkout/confirmResponse.jsp"/>
                <dsp:input bean="CommitOrderFormHandler.commitOrderErrorURL" type="hidden" value="../checkout/confirm.jsp"/>
                <dsp:droplet name="ProfileSecurityStatus">
                  <dsp:oparam name="anonymous">
                    <div class="mobile_store_confirmationEmail">
	                  <h2><fmt:message key="mobile.checkout_confirmResponse.emailTitle"/></h2>
	                  	<c:choose>
		                  <c:when test="${not empty errorMap['email']}">
	              			<c:set var="divClass" value="mobile_store_detail mobile_store_error_state_rounded"/>
	              		  </c:when>
	              		  <c:otherwise>
	              			<c:set var="divClass" value="mobile_store_detail"/>
	              		  </c:otherwise>
              		    </c:choose>
                      <div class="${divClass}">
	                    <fmt:message key="mobile.checkout_confirmResponse.emailPlaceholder" var="emailHint"/>
                        <dsp:input id="mobile_store_checkoutConfirmEmail" iclass="mobile_store_textBoxForm" type="email"
                          bean="CommitOrderFormHandler.confirmEmailAddress" name="email"
                          placeholder="${emailHint}" autocapitalize="off"/>
                        <fmt:message var="privacyTitle" key="mobile.company_privacyAndTerms.pageTitle"/>
                        <c:choose>
                        <c:when test="${empty errorMap['email']}">
                        	<dsp:a page="/company/terms.jsp" title="${privacyTitle}" class="icon-help">
			            	</dsp:a>
			             </c:when>
			             <c:otherwise>
			              <c:if test="${errorMap['email'] eq 'invalid'}">
		                  	<fmt:message var="emailError" key="mobile.form.validation.invalid"/>
		                  	<span class="roundedErrorMessage">${emailError}</span>
				          </c:if>
                           <c:if test="${errorMap['email'] eq 'exists'}">
		                  	<fmt:message var="emailError" key="mobile.form.validation.inUse"/>
		                  	<span class="roundedErrorMessage">${emailError}</span>
				          </c:if>
				         </c:otherwise>
				         </c:choose>
                      </div>
                    </div>
                  </dsp:oparam>
                </dsp:droplet>
                <div class="mobile_store_confirmControls">
                  <div class="mobile_store_basicButton">
                  <dsp:input bean="CommitOrderFormHandler.commitOrder"
                             type="submit"
                             id="mobile_store_placeMyOrderButton"
                             iclass="mainActionBtn"
                             value="${placeOrderButtonText}"/>
                </div>
                </div>
                
              </dsp:form>
            </div>
        </jsp:body>
      </crs:mobilePageContainer>
    </c:otherwise>
  </c:choose>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/confirm.jsp#3 $$Change: 692002 $--%>
