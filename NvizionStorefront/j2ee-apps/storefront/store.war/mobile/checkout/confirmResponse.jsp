<%--
  This page is displayed after the user successfully places an order.
    
  Page includes:
    /mobile/checkout/gadgets/confirmResponseAnonymous.jsp confirmation page for anonymous user
    /mobile/checkout/gadgets/confirmResponse.jsp confirmation page for registered user

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/dynamo/droplet/Compare"/>
  <dsp:importbean bean="/atg/userprofiling/PropertyManager"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  
  <fmt:message var="title" key="mobile.checkout_confirm.title" />
  <crs:mobilePageContainer titleString="${title}">
    <div class="mobile_store_pageHeader">
      <fmt:message key="checkout_title.orderPlaced" />
    </div>
    
		<dsp:droplet name="Compare">
	    <dsp:param bean="Profile.securityStatus" name="obj1"/>
	    <dsp:param bean="PropertyManager.securityStatusAnonymous" name="obj2"/>
	    <dsp:oparam name="equal">
	      <dsp:include page="gadgets/confirmResponseAnonymous.jsp"/>
	    </dsp:oparam>
	    <dsp:oparam name="default">
	      <dsp:include page="gadgets/confirmResponse.jsp"/>
	    </dsp:oparam>
	  </dsp:droplet>
	
  </crs:mobilePageContainer>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/confirmResponse.jsp#3 $$Change: 692002 $ --%>