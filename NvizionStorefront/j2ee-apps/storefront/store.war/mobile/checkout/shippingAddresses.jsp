<%--
  This page renders the user's saved shipping addresses for selection

  Page includes:
    /mobile/checkout/shippingFormParams.jsp shipping form parameters
    /mobile/global/util/displayAddress.jsp renderer of address info

  Required parameters:
    permittedAddresses
      list of shipping permitted addresses

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst"/>
  <dsp:importbean bean="/atg/store/droplet/ShippingRestrictionsDroplet"/>
  <dsp:importbean bean="/atg/store/profile/ProfileCheckoutPreferences"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>

  <fmt:message key="common.foot.shipping" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div class="mobile_store_displayList" id="mobile_store_shippingAddress">
        <h2><fmt:message key="checkout_shipping.chooseShippingAddress"/></h2>
        <dsp:include page="/checkout/gadgets/checkoutErrorMessages.jsp">
          <dsp:param name="formhandler" bean="ShippingGroupFormHandler"/>
          <dsp:param name="divid" value="mobile_store_formValidationError"/>
        </dsp:include>

        <dsp:form id="mobile_store_checkoutShippingAddress" formid="shippingaddressform"
                  action="${pageContext.request.requestURI}" method="post">
          <%-- Include hidden form params --%>
          <dsp:include page="shippingFormParams.jsp" flush="true"/>
          
          <%-- Coupon code --%>
          <dsp:getvalueof var="couponCode" bean="CouponFormHandler.currentCouponCode"/>
          <dsp:input bean="CouponFormHandler.couponCode" priority="10" type="hidden" id="atg_store_promotionCodeInput" value="${couponCode}"/>
          

          <ul id="mobile_store_savedAddresses" class="mobile_store_mobileList">

            <%-- loop through profile addresses --%>
            <c:forEach var="shippingGroupEntry" items="${permittedAddresses}">
              <li class="mobile_store_address withBlueArrowBig">
                <dsp:getvalueof var="shippingAddress" value="${shippingGroupEntry.value.shippingAddress}"/>
                <dsp:getvalueof var="shippingAddressKey" value="${shippingGroupEntry.key}"/>
                <div class="content">
                  <dsp:input type="radio" name="address" value="${shippingAddressKey}"
                             id="${shippingAddress.repositoryItem.repositoryId}" checked="false"
                             bean="ShippingGroupFormHandler.shipToAddressName"/>
                  <label for="${shippingAddress.repositoryItem.repositoryId}" onclick="">
                    <dsp:include page="/mobile/global/util/displayAddress.jsp" flush="false">
                      <dsp:param name="address" value="${shippingAddress}"/>
                      <dsp:param name="private" value="false"/>
                    </dsp:include>
                  </label>
                  <%-- If this shipping group is gift shipping group? --%>
                  <c:set var="description" value="${shippingGroupEntry.value.description}"/>
                  <dsp:getvalueof var="giftPrefix" bean="/atg/commerce/gifts/GiftlistManager.giftShippingGroupDescriptionPrefix"/>
                  <c:if test="${!(fn:startsWith(description, giftPrefix))}">

                    <%-- Display Edit Link --%>
                      <dsp:getvalueof var="shipToAddressName" bean="ShippingGroupFormHandler.shipToAddressName"/>
                      <c:url var="addresspage" value="/mobile/checkout/shippingAddressEdit.jsp">
                        <c:param name="nickName" value="${shippingAddressKey}"/>
                        <c:param name="selectedAddress" value="${shipToAddressName}"/>
                      </c:url>
                      <fmt:message var="editText" key="checkout_shipping.edit"/>
                      <dsp:a href="${fn:escapeXml(addresspage)}" title="${editText}"></dsp:a>
                  </c:if>
                </div>
              </li>
            </c:forEach><%-- End loop through addresses --%>

            <%-- Create new shipping address page --%>
            <li class="mobile_store_formActions">
              <div class="content withArrow">
                <dsp:a page="/mobile/checkout/shippingAddressAdd.jsp" iclass="listLink">
                  <fmt:message key="checkout_shippingAddresses.createShippingAddress"/>
                </dsp:a>
              </div>
            </li>

          </ul>

          <%-- Submit input --%>
          <dsp:input type="hidden" bean="ShippingGroupFormHandler.shipToExistingAddress" value="moveToBilling"/>

        </dsp:form>
        <script type="text/javascript">
          $(document).ready(function() {
            $("#mobile_store_savedAddresses").delayedSubmit();
          });
        </script>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/shippingAddresses.jsp#3 $$Change: 692002 $--%>
