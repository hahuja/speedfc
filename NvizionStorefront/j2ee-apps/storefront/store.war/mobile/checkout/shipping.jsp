<%--
  This page renders the user's saved shipping addresses for selection

  Page includes:
    /mobile/checkout/shippingInitialize.jsp shipping data initialization
    /mobile/checkout/shippingAddresses.jsp list of saved addresses
    /mobile/checkout/shippingAddressAdd.jsp add address form

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/store/order/purchase/CheckoutOptionSelections"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <%-- if there's a checkout option param, remember it in the CheckoutOptionSelections for use on later pages --%>
  <dsp:getvalueof var="checkoutoption" param="checkoutoption"/>
  <c:if test="${not empty checkoutoption}">
    <dsp:setvalue bean="CheckoutOptionSelections.checkoutOption" paramvalue="checkoutoption"/>
  </c:if>
  <%-- Reprice before any page rendering occurs --%>
  <dsp:droplet name="RepriceOrderDroplet">
    <dsp:param name="pricingOp" value="ITEMS"/>
  </dsp:droplet>

  <%-- If the order has any hard goods shipping groups, figure out which hard goods page
       to use. --%>
  <dsp:getvalueof var="anyHardgoodShippingGroups" vartype="java.lang.Boolean"
                  bean="ShippingGroupFormHandler.anyHardgoodShippingGroups"/>

  <c:if test="${anyHardgoodShippingGroups}">

    <dsp:include page="shippingInitialize.jsp" flush="true">
      <dsp:param name="init" value="true"/>
    </dsp:include>

    <c:choose>
      <c:when test="${not empty permittedAddresses}">
        <dsp:include page="shippingAddresses.jsp">
          <dsp:param name="permittedAddresses" value="${permittedAddresses}"/>
        </dsp:include>
      </c:when>
      <c:otherwise>
        <dsp:include page="shippingAddressAdd.jsp"/>
      </c:otherwise>
    </c:choose>

  </c:if>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/shipping.jsp#3 $$Change: 692002 $--%>
