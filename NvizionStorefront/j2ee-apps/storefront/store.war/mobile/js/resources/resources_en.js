/* Provides format strings for localization of page text.

Strings should be added to the RESOURCES object in the format:

  '<resource name>':'<resource content>',

NOTE: <resource content> must be HTML-escaped to avoid parsing issues

@version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/resources/resources_en.js#3 $$Change: 692002 $ --%>
*/
var RESOURCES = {
  'mobile.search.facets.selected':'{0} selected',
  'mobile.search.refine.popup':'Refining...',
  'mobile.search.refine.cancel':'Cancel',
  'mobile.search.refine.done':'Done',
  'mobile.search.loading':'Loading...',
  'common.button.addToCartText':'Add to Cart',
  'mobile.productDetails.updatecart':'Update Cart',
  'mobile.button.preorderLabel':'Preorder',
  'mobile.button.preorderText':'Available Soon',
  'mobile.button.backorderText':'Backordered',
  'mobile.button.emailMeText':'Email Me',
  'navigation_shoppingCart.viewCart':'View Cart',
  'mobile.notifyMe_whenInStock':'When it\'s in stock, please let me know',
  'mobile.passwordReset.email':'email address',
  'mobile.notifyMe_title':'Thanks for your interest',
  'mobile.notifyMe_thankYouMsg':'We\'ll send you an email as soon as the item becomes available.',
  'common.temporarilyOutOfStock':'Out of Stock',
  'common.price.old':'was',
};