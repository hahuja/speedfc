/* Provides format strings for localization of page text.

Strings should be added to the RESOURCES object in the format:

  '<resource name>':'<resource content>',

NOTE: <resource content> must be HTML-escaped to avoid parsing issues

@version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/resources/resources_es.js#2 $$Change: 684277 $ --%>
*/
var RESOURCES = {
  'mobile.search.facets.selected':'{0} seleccionado',
  'mobile.search.refine.popup':'Restringir...',
  'mobile.search.refine.cancel':'Cancelar',
  'mobile.search.refine.done':'Listo',
  'mobile.search.loading':'Cargando...',
  'common.button.addToCartText':'Agregar al carro',
  'mobile.productDetails.updatecart':'Actualizar carro',
  'mobile.button.preorderLabel':'Orden por adelantado',
  'mobile.button.preorderText':'Encargar',
  'mobile.button.backorderText':'Orden no satisfecha',
  'mobile.button.emailMeText':'Enviar mensaje',
  'navigation_shoppingCart.viewCart':'Ver carro',
  'mobile.notifyMe_whenInStock':'Cuando se encuentre en stock, comun&iacute;quemelo',
  'mobile.passwordReset.email':'direcci&oacute;n de correo electr&oacute;nico',
  'mobile.notifyMe_title':'Gracias por su inter&eacute;s',
  'mobile.notifyMe_thankYouMsg':'Le enviaremos un mensaje tan pronto como el art&iacute;culo est&eacute; disponible.',
  'common.temporarilyOutOfStock':'Fuera de stock',
  'common.price.old':'antes',
};