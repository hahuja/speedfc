/**
 * Global Javascript functions
 */

/**
 * Chain together multiple animations.
 * 
 * Usage: chainAnimations([function, duration,]...);
 */
function chainAnimations(){
  var args = Array.prototype.slice.call(arguments);
  var f = args.shift();
  var delay = args.shift();
  var tail = args;
  var callee = arguments.callee;
  if (f){
    f();
    if (delay) setTimeout(function (){ callee.apply(this, tail);}, delay);
  }
}

// String formatting in one pass
String.prototype.format = function format(){
  var formatted = ""; // this will be our formatted string
  var split = this.split('');
  var inParam = false; // state variable
  var paramNumber = '';
  for (var i = 0; i < split.length; i++){
    var current = split[i];
    switch (current){
      case '{': // begin specifying a parameter
        inParam = true;
        break;
      case '}': // done specifying parameter
        inParam = false;
        // insert the parameter or, if it doesn't exist, leave it as-is
        var param = arguments[parseInt(paramNumber, 10)] || "{"+paramNumber+"}";
        formatted += param; // insert the parameter into the formatted string
        paramNumber = ""; // make sure to reset the paramNumber
        break;
      default:
        if (inParam){
          paramNumber += current;
        }
        else{
          formatted += current;
        }
        break;
    }
  }
  return formatted;
};

/**
 * Refreshes badge count on shopping cart
 * 
 * @param newCount
 *            new count
 */
function refreshCartBadge(newCount){
  if (newCount > 0) {
    var $cartBadge = $("#mobile_store_cartBadge");
    $cartBadge.show(); // in case it's currently hidden
    chainAnimations(function(){ $cartBadge.toggleClass('highlight').toggleClass('textFade'); }, 500,
                    function(){ $cartBadge.text(newCount).toggleClass('textFade');}, 900,
                    function(){ $cartBadge.toggleClass('highlight')});
  }
}

/**
 * Toggle contact block
 */
function toggleContact(){
  var $contactInfo = $("#mobile_store_contactInfo").show();
  toggleModal(true);
  return false;
}

/**
 * Toggle rows (li's) on login/registration pages
 * 
 * @param li
 *            li ID
 */
function loginPageToggle(li){
  $('#'+li).prev('li').toggleClass('imageClicked');
  $('#'+li).toggleClass('hidden');
  return false;
}

/**
 * Used to toggle rows on the login screen.
 * 
 * @param li
 *            li ID
 */
function loginPageClick(li){
  if (li === 'loginRow'){
    var regRow = $('#regRow');
    if (! regRow.hasClass("hidden"))
      loginPageToggle('regRow');
  } else {
    var loginRow = $('#loginRow');
    if (! loginRow.hasClass("hidden"))
      loginPageToggle('loginRow');
  }
    loginPageToggle(li);
    
  return false;
}

/**
 * One-pass cookie parser
 * 
 * @param name
 *            cookie name
 */
function getCookieByName(name){
  var cookiesSplit = document.cookie.split('');

  // State variables
  var inName = true;
  var isSkip = true;
  var currentName = '';
  var currentValue = '';

  // Check the cookie string
  for (var i = 0; i < cookiesSplit.length; i++){
    var ch = cookiesSplit[i];
    switch (ch){
      case ';':
        inName = true;
        if (isSkip){ // not the cookie we want
          currentName = '';
          currentValue = '';
        }
        else return decodeURI(currentValue);
        break;
      case '=':
        inName = false;
        if (currentName === name) isSkip = false; // this is the right cookie
        break;
      case ' ':
        break;
      default:
        if (inName) currentName += ch;
        else currentValue += ch;
        break;
    }
  }
  if (!isSkip) return decodeURI(currentValue);
  else return null;
}

/**
 * Shows or hides modal
 * 
 * @param showOrHide
 *            false for hiding
 */
function toggleModal(showOrHide){
  $("#mobile_store_modalOverlay").toggle(showOrHide);
  // if hiding the modal overlay, also hide all direct children
  if (showOrHide == false){
    $("#mobile_store_modalOverlay > :not(div.shadow)").hide();
  }
}

/**
 * Adds/removes 'searchFocus' class to element with 'mobile_store_header'-id
 * 
 * @param isFocus boolean
 */
function searchFocus(isFocus){
  $("#mobile_store_header").toggleClass('searchFocus', isFocus);
}

$(window).one("load",function () {
  window.scrollTo(0, 1);
});