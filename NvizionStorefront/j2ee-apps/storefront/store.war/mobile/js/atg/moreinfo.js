/* More Info Javascript functions 
Version: $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/atg/moreinfo.js#4 $ $Change: 692002 $
*/

/**
 * Toggles action buttons for store (map, phone, email)
 * 
 * @param caller
 *            store info row
 */
function storeLocationClick(caller){

  var $this = $(caller);

  var toggleLocationControls = function ($controlPanel) {
    $controlPanel.children('img').toggleClass('clicked');
    $controlPanel.next('div').children('div').toggleClass('hidden');
    return false;
  }

  if ($this.next('div').css('display') == 'none') {
	$this.find('.leftArrow')[0].className="downArrow";
    $this.next('div').toggle();
    window.setTimeout(toggleLocationControls, 4, $this);
  } else {
    toggleLocationControls($this);
    $this.find('.downArrow')[0].className="leftArrow";
    window.setTimeout(function($controlPanel) {
      $controlPanel.next('div').toggle();
    }, 200, $this); //give a bit more time to browser for animation and then completely hide controls.
  }
}

/**
 * Updates page with new locale
 * 
 * @param e
 *            input which URL contains locale attribute
 */
function chooseLanguage(e){
  var href = e.currentTarget.value;
  setTimeout(function(){ window.location.href = href;}, 500);
  return true;
}