/**
 * Oracle ATG modified the touchslider.js code from
 * https://github.com/zgrossbart/jstouchslide in the creation of this file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */

var sliders = {
  /**
   * This function just binds the touch functions for the grid.
   * It is very important to stop the default, stop the
   * propagation, and return false.  If we don't then the touch
   * events might cause the regular browser behavior for touches
   * and the screen will start sliding around.
   */
  makeTouchable: function(/*string*/ gridid, /*int*/ touchSensitivity,/*int*/ listWidth, /*int*/ colWidth,/*int*/ parentWidth, /*function*/ postTouchMove, /*function*/ postTouchEnd) {

    var sliderObject = this;
    var postTouchMoveApply = null;
    var postTouchEndApply = null;
    this.ParentContainerWidth = parentWidth;
    this.width = listWidth;
    this.colWidth = colWidth;
    
    if(postTouchMove !== null){
      postTouchMoveApply = function(){
        postTouchMove.apply(sliderObject,arguments);
      };
    }
    if(postTouchEnd !== null){
      postTouchEndApply = function(){
        postTouchEnd.apply(sliderObject,arguments);
      };
    }

    $(gridid).each( function() {

      sliderObject.cells = $(this).children(".cell");

      this.ontouchstart = function(e) {
        sliderObject.touchStart($(this), e,touchSensitivity);
        return true;
      };
      this.ontouchend = function(e) {
        e.preventDefault();
        e.stopPropagation();
        if (sliderObject.sliding) {
          sliderObject.sliding = false;
          sliderObject.touchEnd($(this), e,touchSensitivity,postTouchEndApply);
          return false;
        } else {
          /*
           We never slid so we can just return true
           and perform the default touch end
           */
          return true;
        }
      };
      this.ontouchmove = function(e) {
        return sliderObject.touchMove($(this), e,postTouchMoveApply);
      };
    });
  },
  /**
   * A little helper to parse off the 'px' at the end of the left
   * CSS attribute and parse it as a number.
   */
  getLeft: function(/*JQuery*/ elem) {
    return parseInt(elem.css('left').substring(0, elem.css('left').length - 2), 10);
  },
  /**
   * When the touch starts we add our sliding class a record a few
   * variables about where the touch started.  We also record the
   * start time so we can do momentum.
   */
  touchStart: function(/*JQuery*/ elem, /*event*/ e) {
    
    $.each(this.cells, function(){$(this).show();});
    
    elem.css({
      '-webkit-transition-duration': '0'
    });

    this.startX = e.targetTouches[0].clientX;
    this.startY = e.targetTouches[0].clientY;
    this.startLeft = this.getLeft(elem);
    this.touchStartTime = new Date().getTime();
    this.touching = true;
  },
  /**
   * When the touch ends we need to adjust the grid for momentum
   * and to snap to the grid.  We also need to make sure they
   * didn't drag farther than the end of the list in either
   * direction.
   */
  touchEnd: function(/*JQuery*/ elem, /*event*/ e,/*int*/ touchSensitivity,/*function*/ callBack) {
    if (this.getLeft(elem) > 0) {
      /*
       * This means they dragged to the right past the first item
       */
      this.doSlide(elem, 0, '2s', false);

      elem.parent().removeClass('sliding');
      this.startX = null;
      this.startY = null;
      this.touching = false;
    } else if ((Math.abs(this.getLeft(elem)) + elem.parent().width()) > this.width) {
      /*
       * This means they dragged to the left past the last item
       */
	   if((this.width - elem.parent().width()) > 0){
		this.doSlide(elem, '-' + (this.width - elem.parent().width()), '2s', false);
	   } else {
	    this.doSlide(elem, 0, '2s',false);	
	   }
	  elem.parent().removeClass('sliding');
      this.startX = null;
      this.startY = null;
      this.touching = false;
    } else {
      /*
       This means they were just dragging within the bounds of the grid
       and we just need to handle the momentum and snap to the grid.
       */
      this.slideMomentum(elem, e, touchSensitivity,callBack);
    }
  },
  /**
   * If the user drags their finger really fast we want to push
   * the slider a little farther since they were pushing a large
   * amount.
   */
  slideMomentum: function(/*jQuery*/ elem, /*event*/ e, /*int*/ touchSensitivity,/*function*/ callBack) {
    var slideAdjust = (new Date().getTime() - this.touchStartTime) * 10;
    var left = this.getLeft(elem);
    var abs = Math.abs;

    /*
     We calculate the momentum by taking the amount of time they were sliding
     and comparing it to the distance they slide.  If they slide a small distance
     quickly or a large distance slowly then they have almost no momentum.
     If they slide a long distance fast then they have a lot of momentum.
     */

    var changeX = touchSensitivity * (abs(this.startLeft) - abs(left));

    slideAdjust = Math.round(changeX / slideAdjust);

    var newLeft = slideAdjust + left;

    /*
     * We need to calculate the closest column so we can figure out
     * where to snap the grid to.
     */
    var t = newLeft % this.colWidth;

    if ((abs(t)) > ((this.colWidth / 2))) {
      /*
       * Show the next cell
       */
      newLeft -= (this.colWidth - abs(t));
    } else {
      /*
       * Stay on the current cell
       */
      newLeft -= t;
    }

    var newLeftValue = 0;

    if (this.slidingLeft) {
      var maxLeft = parseInt('-' + (this.width - this.ParentContainerWidth), 10);
      /*
       * Sliding to the left
       */
      newLeftValue = Math.max(maxLeft, newLeft);
      this.doSlide(elem, newLeftValue, '0.5s',true,callBack);
    } else {
      /*
       * Sliding to the right
       */
      newLeftValue = Math.min(0, newLeft);
      this.doSlide(elem, newLeftValue, '0.5s', true,callBack);
    }

    elem.parent().removeClass('sliding');
    this.startX = null;
    this.startY = null;
    this.touching = false;
  },
  /**
   * Slides the elem to that left position over a certian duration
   */
  doSlide: function(/*jQuery*/ elem, /*int*/ x, /*string*/ duration, /* boolean */ showNext, /*function*/ callBack) {

    elem.css({
      left: x + 'px',
      '-webkit-transition-property': 'left',
      '-webkit-transition-duration': duration
    });

    if(showNext && callBack !== null) {
      callBack(x,duration);
    }
  },
  /**
   * While they are actively dragging we just need to adjust the
   * position of the grid using the place they started and the
   * amount they've moved.
   */
  touchMove: function(/*JQuery*/ elem, /*event*/ e, /*function*/ callBack) {
		var dy = this.startY - e.targetTouches[0].clientY;
	    var dx = this.startX - e.targetTouches[0].clientX;
	    //This is faster than calling Math.abs();
	    var absDY = dy < 0 ? -dy : dy;
	    var absDX = dx < 0 ? -dx : dx;
	    //We need to guess so to whether the user is trying to go up and down or left or right.
	    if(absDX > 0 && absDY < absDX) {

	      if (!this.sliding) {
	        elem.parent().addClass('sliding');
	      }

	      this.sliding = true;
	      //We thing that you are sliding left or right so we need to disable the default touch actions, so the page doesn't move
	      e.preventDefault();
	      e.stopPropagation();

	      var left = 0;

	      if (this.startX > e.targetTouches[0].clientX) {
	        /*
	         * Sliding to the left
	         */
	        left =  (dx - this.startLeft);
	        elem.css('left', '-' + left + 'px');
	        this.slidingLeft = true;
	        //We only want to attempt to re-highlight the center cell if we have moved enough that it has changed
	        if(callBack !== null && absDX >= this.colWidth)
	          callBack(-1*left);
	      } else {
	        /*
	         * Sliding to the right
	         */
	        left = (e.targetTouches[0].clientX - this.startX + this.startLeft);
	        elem.css('left', left + 'px');
	        this.slidingLeft = false;
	        if(callBack !== null && absDX >= this.colWidth)
	          callBack(left);
	      }
	      return false;
	    }
	    return true;
	  }
	};

var productItemSlider = {
  /**
   * We start by creating the sliding grid out of the specified
   * element.  We'll look for each child with a class of cell when
   * we create the slide panel.
   */
  createSlidePanel: {
    value: function(/*string*/ gridid, /*string*/ cellPrefix, /*int*/ numberOfCellsToDisplay) {
      var x = 0;

      //Since this is a promotional slider, we only want one item cell on the page,
      //so we set each cell to its parent's width
      var parent = $(gridid).parent();
      var parentContainerWidth = parent.width();
      this.numberOfCellsToDisplay = numberOfCellsToDisplay;
      var cellWidth =  parentContainerWidth / numberOfCellsToDisplay;

      var pageOffSet = 0;
      this.cellDetailsClass = cellPrefix + "Details";
      this.cellImageClass = cellPrefix + "Image";
      this.productInfoDisplayDiv = $("#mobile_store_homeBottomSlotProductDetails");

      var thisObject = this;

      $(gridid).each( function() {
        $(this).css({
          'position': 'relative'
        });

        $(this).parent().css('overflow', 'hidden');

        var children = $(this).children(".cell");

        children.each( function(index) {
          $(this).css({
            width: cellWidth + 'px',
            height: '90%',
            position: 'absolute',
            left: x + 'px'
          });
          //We need ot keep track of the cell id so we add an id and
          //its index for each cell, its image, and its description
          $(this).attr('id', cellPrefix + "Id" + index);

          x += cellWidth;
        });

          if(x > parentContainerWidth) {
            pageOffSet = -1 * ((x - parentContainerWidth) / 2);
          }
          
        $(this).css({
          'left': pageOffSet
        });

        //In the case where there are only enough cells to fit on the page, we
        //set the index of the center cell to center and subtract 1 to get start the index at 0
        var currentCellIndex = Math.ceil(numberOfCellsToDisplay/2) - 1;
        thisObject.centerCellDefault = currentCellIndex;
        var cellsOffPage = 0;
        if(pageOffSet < 0) {
          //We find out how many cells are off the page, and add that to the center cell index
          cellsOffPage = Math.round(Math.abs(pageOffSet/cellWidth));
          currentCellIndex += cellsOffPage;
        }

        var end = cellsOffPage+numberOfCellsToDisplay;
        $.each(children, function(index) {
          var $child = $(this);
          if(index < cellsOffPage || index >= end) {
            $child.hide();
          }
        });
        thisObject.currentCenterCell = currentCellIndex;
        thisObject.productInfoDisplayDiv.html($(children[thisObject.currentCenterCell]).find("." + thisObject.cellDetailsClass).html());
        $(children[thisObject.currentCenterCell]).find("." + thisObject.cellImageClass).toggleClass("highlightProduct");

        try {
          document.createEvent('TouchEvent');
          /*
           Now that we've finished the layout we'll make our panel respond
           to all of the touch events.
           */
          thisObject.makeTouchable(gridid, 3000,x,cellWidth,parentContainerWidth,thisObject.postTouchMove,thisObject.postTouchMoveAndEnd);
        } catch (e) {
        }
      });
    }
  },

  postTouchMoveAndEnd: {
    value: function(/*int*/ left, /*string*/ duration) {
      this.postTouchMove(left);
      var object = this;
      
      var strippedDuration = duration.substring(0,duration.length-1);
      
      setTimeout(function(){object.postTouchEnd(left);},strippedDuration);
      }
  },
  /**
   * This will calculate the middle cell based on the leftposition and do the appropriate action
   */
  postTouchMove: {
    value: function(/* int */ left) {
      if(left <= 0) {
        left = left < 0 ? -left : left;
        var currentCell = Math.round(left/this.colWidth) + this.centerCellDefault;
        if(this.currentCenterCell !== currentCell) {         
          var $newCell = $(this.cells[currentCell]);
          var $currentCell = $(this.cells[this.currentCenterCell]);   
          this.productInfoDisplayDiv.html($newCell.find("." + this.cellDetailsClass).html());
          $currentCell.find("." + this.cellImageClass).toggleClass("highlightProduct");
          $newCell.find("." + this.cellImageClass).toggleClass("highlightProduct");
          this.currentCenterCell = currentCell;
        }
      }
    }
  },

  /**
   * This function will calculate which items are on the screen, and set their display to hidden
   */
  postTouchEnd: {
    value: function(/*int*/ left) {
      if(!this.touching && left <= 0) {
        var cellsOffPage = Math.abs(left)/this.colWidth;
        var end = cellsOffPage+this.numberOfCellsToDisplay;
        $.each(this.cells, function(index) {
          var $cell = $(this);
          if(index < cellsOffPage || index >= end) {
            $cell.hide();
          } else {
            $cell.show();
          }
        });
      }
    }
  }
};

var promotionalItemSlider = {
  /**
   * We start by creating the sliding grid out of the specified
   * element.  We'll look for each child with a class of cell when
   * we create the slide panel.
   */
  createSlidePanel: {
    value: function(/*string*/ gridid) {
      var x = 0;

      //Since this is a promotional slider, we only want one item cell on the page,
      //so we set each cell to its parent's width
      var parent = $(gridid).parent();
      var parentContainerWidth = parent.width();
      var cellWidth =  parentContainerWidth;
      var thisObject = this;

      $(gridid).each( function() {
        $(this).css({
          'position': 'relative',
          'left': '0'
        });

        $(this).parent().css('overflow', 'hidden');
        var children = $(this).children(".cell");

        children.each( function(index) {
          $(this).css({
            width: cellWidth + 'px',
            height: '90%',
            position: 'absolute',
            left: x + 'px'
          });

          x += cellWidth;
        });

        thisObject.currentCenterCell = 0;

        children.slice(1).hide();
        try {
          document.createEvent('TouchEvent');
          /*
           Now that we've finished the layout we'll make our panel respond
           to all of the touch events.
           */
          thisObject.makeTouchable(gridid, 6000,x,cellWidth,parentContainerWidth,thisObject.postTouchMove,thisObject.postTouchMoveAndEnd);
        } catch (e) {
        }
      });
    }
  },
  /**
   * This will calculate the middle cell based on the leftposition and do the appropriate action
   */
  postTouchMove: {
    value: function(/* int */ left) {
      if(left <= 0) {
        left = left < 0 ? -left : left;
        var currentCell = Math.round(left/this.colWidth);
        if(this.currentCenterCell !== currentCell) {
          displayProductsFromIndex(currentCell);
          setCircleStatus(this.currentCenterCell ,false);
          setCircleStatus(currentCell,true);
          this.currentCenterCell = currentCell;
        }
      }
    }
  },
    /**
   * This function will calculate which items are on the screen, and set their display to hidden
   */
  postTouchEnd: {
    value: function(/*int*/ left) {
      if(!this.touching && left <= 0) {
        left = left < 0 ? -left : left;
        var cellsOffPage = Math.round(left/this.colWidth);
        var end = cellsOffPage+1;
        $.each(this.cells, function(index) {
          var $cell = $(this);
          if(index < cellsOffPage || index >= end) {
            $cell.hide();
          } else {
            $cell.show();
          }
        });
      }
    }
  },
    postTouchMoveAndEnd: {
    value: function(/*int*/ left, /*string*/ duration) {
      this.postTouchMove(left);
      var object = this;
      
      var strippedDuration = duration.substring(0,duration.length-1) * 1000;
      
      setTimeout(function(){object.postTouchEnd(left);},strippedDuration);
      }
  }
};