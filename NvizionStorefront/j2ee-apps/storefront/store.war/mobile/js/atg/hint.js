(function ($) {
	  /**
		* Converts list to dropdown one.
		*/
  $.fn.dropdownList = function() {
    return this.each(function() {
      var $ul = $(this);

      function selectItemHandler(event) {
        var thisObj = event.currentTarget;

        if ($('#currentBillingAddress').length != 0 && $('#currentBillingAddress')[0] != thisObj) {
          $('#currentBillingAddress').next().remove();
          $('#currentBillingAddress').remove();
        }

        $("input[type=radio][name$='selectedBillingAddress']", $(thisObj)).attr("checked", "checked").change();
        var radios = $("input[type=radio][name$='selectedBillingAddress']", $ul);

        $(radios).parents("li").unbind("click");
		    setTimeout(function(thisObj) {
          hideUnselectedAddresses(thisObj);
        }, 500, thisObj);
      }

      function selectedItemClickHandler(event) {
        event.preventDefault();

        var radios = $("input[type=radio][name$='selectedBillingAddress']", $ul);
        $(radios).not(":checked").parents("li").css("display", "");
        $("li.mobile_store_formActions, li.mobile_store_border", $ul).css("display", "");
        $(this).unbind("click");

        if ($('#currentBillingAddress').length != 0) {
          var unmatchedRadios = $("li[id!='currentBillingAddress'] input[id!='" + $('#matchedSecondaryAddressId').val() + "'][type=radio][name$='selectedBillingAddress']", $ul);
          var matchedRadios = $("li#currentBillingAddress input[type=radio][name$='selectedBillingAddress'], li input[id='" + $('#matchedSecondaryAddressId').val() + "'][type=radio][name$='selectedBillingAddress']", $ul);

          $(unmatchedRadios).parents('li').click(
            function(event) {
              changeBillingAddressDialog(event, selectItemHandler);
            }
          );
          $(matchedRadios).parents('li').click(selectItemHandler);
        } else {
          $(radios).parents("li").click(selectItemHandler);
        }

        $(this).removeClass("mobile_store_hideSelectedBAMark");
      }

      function hideUnselectedAddresses(li) {
        if ($('li:first-child', $ul)[0] != $(li)[0]) {
            var borderLi = $(li).next().detach();
            $(li).detach();
            $('li:first-child', $ul).before($(borderLi));
            $(borderLi).before($(li));
        }

        var uncheckedLis = $("input[type=radio][name$='selectedBillingAddress']", $ul).not(':checked').parents('li');
        $(uncheckedLis).css('display', 'none');
        $(uncheckedLis).removeClass('mobile_store_selectedBillingAddress');
        $('li.mobile_store_formActions, li.mobile_store_border', $ul).css('display', 'none');

        var checkedLi = $("input[type=radio][name$='selectedBillingAddress']:checked", $ul).parents('li');
        $(checkedLi).click(selectedItemClickHandler);
        $(checkedLi).addClass('mobile_store_selectedBillingAddress mobile_store_hideSelectedBAMark');
      }

      var li = $("input[type=radio][name$='selectedBillingAddress']:checked", $ul).parents('li');
      hideUnselectedAddresses(li);
      li.click(selectedItemClickHandler);
    });
  };

  /**
	 * Adds delayed submit to the list form. Called when user clicks on any
	 * list item except border & action ones.
	 * 
	 * @param delay
	 *            timeout before submitting form (default - 200ms)
	 */
  $.fn.delayedSubmit = function(delay) {
    delay = (typeof delay === "undefined") ? 200 : delay;

    return this.each(function() {
      var $container = $(this);

      $("li", $container).not(".mobile_store_formActions, .mobile_store_border").click(function (event) {
        var target = event.target || event.srcElement;
        if (target.localName === "a") return;
        
        var radio = $("input:radio", $(this));
        radio.attr("checked", "checked");
        radio.change();

        setTimeout(function () {
          $container.parents("form").submit();
        }, delay);
      });
    });
  };
})(jQuery);
