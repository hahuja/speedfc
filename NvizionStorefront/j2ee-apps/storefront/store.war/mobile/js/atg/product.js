/**
 * Product javascript
 */

/**
 * Initialize the related items slider
 */
function initRelatedItemsSlider(){
  var relatedSlider = Object.create(sliders, relatedProductsSlider);
  relatedSlider.createSlidePanel("#mobile_store_relatedItemsContainer", "related_product", 4);
}

/**
 * This function toggles product's view between normal and enlarged
 */
function toggleProductView() {
  var detailsContainer = $(".mobile_store_pickers");
  detailsContainer.toggleClass("mobile_store_productEnlarged");
}

var relatedProductsSlider = {
  /**
   * We start by creating the sliding grid out of the specified
   * element.  We'll look for each child with a class of cell when
   * we create the slide panel.
   */
  createSlidePanel: {
    value: function(/*string*/ gridid, /*string*/ cellPrefix, /*int*/ numberOfCellsToDisplay) {
      var x = 0;

      var parentContainerWidth = $(gridid).parent().width();
      var cellWidth = parentContainerWidth / numberOfCellsToDisplay;
      var thisObject = this;

      $(gridid).each(function() {
        $(this).css({
          'position': 'relative',
          'left': '0'
        });

        $(this).parent().css('overflow', 'hidden');
        $(this).children('.cell').each(function(index) {
          $(this).css({
            width: cellWidth + 'px',
            height: '90%',
            position: 'absolute',
            left: x + 'px'
          });

          x += cellWidth;
        });

        try {
          document.createEvent('TouchEvent');
          /**
			 * Now that we've finished the layout
			 * we'll make our panel respond to all
			 * of the touch events.
			 */
          thisObject.makeTouchable(gridid, 3000, x, cellWidth, parentContainerWidth, null, null);
        } catch (e) {
          /**
           * Then we aren't on a device that supports touch
           */
          $(this).css({
            'height': '70px',
            'overflow': 'auto'
          });
        }
      });
    }
  }
};

/**
 * Run email me functionality
 * 
 * @param e
 *            click event
 */
function emailMeSubmit(e) {
  e.preventDefault();
  var $form = $(e.currentTarget);
  var $input = $("td:has(input[type='email'])", $form).removeClass("error");
  var $checkbox = $("input#mobile_store_rememberCheckbox")[0];
  if ($checkbox.checked) {
    $("input[name='rememberEmail']")[0].value = $("input", $input)[0].value;
  }
  $.post($form.attr("action"),
          $form.serialize(),
          function(errors) {
            if (errors.length > 0) {
              $input.closest("tr").addClass('error');
            }
            else {
              var $confirm = $("#mobile_store_emailMePopup_confirm");
              $(".mobile_store_modalDialog").hide();
              $confirm.show();
            }
          },
          'json');
}

/**
 * New SKU pickers
 */
function expandPickers(showOrHide){
  $("li.mobile_store_pickers").toggleClass("expanded", showOrHide);
}

/**
 * skuFunctions is declared on load, but initialized on document ready. This is
 * because the functions within require that specific DOM elements are present.
 * This cannot be guaranteed until the entire document has loaded.
 */
var skuFunctions;
$(document).ready(function(){
  // skuFunctions contains all functions relevant to selecting SKUs and adding to cart
  // It uses the module pattern to abstract common variables and functions as well
  // as avoid polluting the global namespace.
  skuFunctions = function() {
    var $addToCartForm = $("#mobile_store_addToCart");
    var isUpdateCart = ($addToCartForm.length == 0);
    // see mobile/browse/productDetail.jsp — the update cart form will only be present
    // when the add to cart form is not.
    var $updateCartForm = $("#mobile_store_updateCart");
    var $skuIdField = $("input#addToCart_skuId");
    var $actionsContainer = $("#mobile_store_addToCartButton");
    var $addToCartButton = $("button", $actionsContainer);
    var $buttonText = $("span#mobile_store_buttonText", $actionsContainer);
    
    var $emailMeContainer = $("div#mobile_store_emailMePopup");
    var $emailMeConfirm =$("div#mobile_store_emailMe_confirm");
    var $emailMeForm = $("form", $emailMeContainer);
    var $emailMeSkuField = $("#mobile_store_emailMeSkuId", $emailMeForm);
    var $emailMeAddress = $("#mobile_store_rememberMeEmailAddress", $emailMeForm);
    var $emailMeAddressRow = $("#mobile_store_emailAddressRow",$emailMeForm);
    var $rememberCheckbox = $("input#mobile_store_rememberCheckbox", $emailMeForm)[0];
    var $rememberInput = $("input[name='rememberEmail']", $emailMeForm);
    
    var $priceContainer = $("#mobile_store_price");
    var $initialPriceDisplay = $priceContainer.html();
    
    // Text on submit button ("Add To Cart", "PreOrder" etc.) at last successfull submit
    var prevButtonText = null;

    
    /**
     * Update the product price. This is done dynamically because there
     * are multiple ways the price can be displayed.
    */
    var updatePrice = function(salePrice, oldPrice) {
      var price = "<p><strong>" + salePrice + "</strong>";
      if (oldPrice) {
        price = price + "<br>" + RESOURCES['common.price.old'] + "&nbsp;<span>" + oldPrice + "</span>";
      }
      price = price + "</p>";
      $priceContainer.html(price);
    };

    /**
      * This function checks addToCart response for errors.
      * responseData - JSON response returned when add to cart form was submitted
      *  Contains new cart item count, cart url, and form errors (if any)
     */
    var checkAddToCartError = function(responseData) {
      var error = responseData.addToCartError;
      if (error) {
        // Handle errors
        console.log(error);
      } else {
        var cartUrl = responseData.cartUrl;
        prevButtonText = $addToCartButton.text();
        $addToCartButton.text(RESOURCES['navigation_shoppingCart.viewCart'])
                        .unbind("click")
                        .removeAttr("onclick")
                        .click(function(event){
                          event.stopPropagation();
                          // redirect to cart page
                          document.location.href = cartUrl;
                        });
        refreshCartBadge(responseData.cartItemCount);
      }
    };
  
    // This function handles clicks on the cart button
    var cartLinkClick;
    if (isUpdateCart){ // when updating the cart, just submit the form normally
      cartLinkClick = function(event) {
        event.stopPropagation();
        $updateCartForm.submit();
      };
    }
    else{ // when adding to cart, submit the form via AJAX and then check the response
      cartLinkClick = function(event) {
        event.stopPropagation();
        $.post($addToCartForm.attr("action"),
               $addToCartForm.serialize(),
               checkAddToCartError);
      };
    }

    /**
	 * For SKUs that can be added to the cart: Updates
	 * product price, sale price, and the "Add To Cart"
	 * button. Returns a function that closes over
	 * button label, button text, and SKU fields so that
	 * no unnecessary operations are performed as the
	 * user selects SKUs
	 */
    var addToCartFunction = function(buttonLabel, buttonText, skuFields){
      return function(){
        updatePrice(skuFields.productPrice, skuFields.salePrice);
        $skuIdField.val(skuFields.skuId);
        $addToCartButton.text(buttonLabel)
                        .removeAttr("onclick") // jQuery 'unbind' doesn't remove click events 
                        .unbind('click')       // bound in the markup, so we also use 'removeAttr'
                        .click(cartLinkClick)
                        .removeAttr("disabled");
        $buttonText.text(buttonText);
      }
    };
    
    var emailMeClick = function(event){
      $emailMeContainer.show();
      $emailMeConfirm.hide();
      toggleModal(true);
    }
    
    var emailMeSubmit = function(event){
      event.preventDefault();
      if ($rememberCheckbox.checked) {
        $rememberInput.val($emailMeAddress.val());
      }
      $.post($emailMeForm.attr("action"),
             $emailMeForm.serialize(),
             function(errors) {
               if (errors.length > 0) {
                 $emailMeAddressRow.toggleClass('error',true);
               }
               else {
                $emailMeAddressRow.toggleClass('error',false);
                 $emailMeContainer.hide();
                 $emailMeConfirm.show();
               }
             },
             'json');
    }
    

	    /**
		 * For SKUs that cannot be added to the cart:
		 * Updates product price, sale price, and changes
		 * the "Add to Cart" button to "Email Me". Returns a
		 * function that closes over SKU fields so that no
		 * unnecessary operations are performed as the user
		 * selects SKUs
		 */
    var outOfStockFunction = function(skuFields){
      return function(){
        updatePrice(skuFields.productPrice, skuFields.salePrice);
        $emailMeSkuField.val(skuFields.skuId);
        $addToCartButton.text(RESOURCES['mobile.button.emailMeText'])
                        .removeAttr("onclick") // jQuery 'unbind' doesn't remove click events 
                        .unbind('click')       // bound in the markup, so we also use 'removeAttr'
                        .removeAttr("disabled")
                        .click(emailMeClick);
        $buttonText.text(RESOURCES['common.temporarilyOutOfStock']);
      }
    };

    // curried addToCartFunction, fixing button text
    var updateCartFunction = function(skuFields){
        return addToCartFunction(RESOURCES['mobile.productDetails.updatecart'], "", skuFields);
    };

    var toggleRedirect = function(){
      $(".mobile_store_modalDialog").hide();
      toggleModal(true);
      $emailMeForm.show();
    };

    /**
	 * Executed every time a picker value is changed to
	 * determine which SKU (if any) has been selected.
	 * 
	 * @param properties
	 *            – an array of names of SKU
	 *            properties. The function will search
	 *            for 'select' elements on the page with
	 *            the corresponding names Order is
	 *            important for constructing the key!
	 */ 
    function checkForSelectedSku(properties){
      var key = "";
      for (var i = 0; i < properties.length; i++){
        var prop = properties[i];
        var $input = $("select[name='"+prop+"']");
        if ($input.length > 0){
          key += $input.val()+":";
        }
      }
      var skuFunction = productSKUs[key.slice(0,-1)]; // remove the trailing colon
      if (skuFunction){ // if this is not a valid SKU selection, skuFunction will be null
        skuFunction();
      }
      else{
        // disable the button if this is not a valid selection
        $addToCartButton.attr("disabled","disabled");
      }
    }
    

    /**
	 * Called when a picker's value has changed. Accepts
	 * an indeterminate number of arguments representing
	 * the names of sku properties Rather than declared
	 * explicitly, these arguments are parsed from the
	 * 'arguments' object
	 */
    var pickerSelectFunction = function(){
      // "slice" the arguments object to make sure we only iterate over the function arguments,
      // not any of the other fields of the object
      var properties = Array.prototype.slice.call(arguments);
      return function(event){
        var $this = $(event.currentTarget);
        // we need to set "title" attribute for select to make VoiceOver read it as "Name - Value"
        var defaultText = $("option:disabled", $this).attr("label");
        var noValue = ($this.val() == "");
        if (noValue){
          // revert to initial display if there is no valid selection
          $priceContainer.html($initialPriceDisplay);
        }
         // we need to set the appropriate title for the current selection
        $this.attr("title", noValue ? "" : defaultText);
        // if no value has been selected, toggle "selected" off. Otherwise, toggle it on.
        $(event.currentTarget).toggleClass('selected', !noValue);
        checkForSelectedSku(properties); // check if this is a complete SKU selection
      }
    }

    // Called when the quantity picker's value has changed.
    var quantitySelect = function(event) {
      var $qtyField = $("input#addToCart_qty");
      var selectedQty = $("select[name='qty']").val();
      $qtyField.val(selectedQty);
      if (isUpdateCart){
        $addToCartButton.text(RESOURCES['mobile.productDetails.updatecart']);
      }
      else{
    	  var currText = $addToCartButton.text();
    	  if (currText == RESOURCES['navigation_shoppingCart.viewCart']){
    		  $addToCartButton.unbind("click")
    		  	.removeAttr("onclick")
    		  	.click(cartLinkClick)
    		  	.text(prevButtonText);
    	  }
      }
    }
    
    // Create the 'interface' for the skuFunctions module,
    // exposing only the functions that need to be called directly
    var returnObj = {
      cartLinkClick: cartLinkClick,
      toggleRedirect: toggleRedirect,
      emailMe: emailMeClick,
      emailMeSubmit: emailMeSubmit,
      outOfStock: outOfStockFunction,
      colorSizePickerSelect: pickerSelectFunction("color","size"),
      multiSkuPickerSelect: pickerSelectFunction("feature"),
      woodFinishPickerSelect: pickerSelectFunction("woodFinish"),
      quantitySelect: quantitySelect,
      available: function(skuFields){
        // curried addToCartFunction, fixing button text
        return addToCartFunction(RESOURCES['common.button.addToCartText'], "", skuFields);
      },
      preorder: function(skuFields){
        // curried addToCartFunction, fixing button text
        return addToCartFunction(RESOURCES['mobile.button.preorderLabel'],
                                 RESOURCES['mobile.button.preorderText'],
                                 skuFields);
      },
      backorder: function(skuFields){
        // curried addToCartFunction, fixing button text
        return addToCartFunction(RESOURCES['common.button.addToCartText'],
                                 RESOURCES['mobile.button.backorderText'],
                                 skuFields);
      },
    };
    
    // If we are updating the cart, then all 'add to cart' functions do the same thing
    if (isUpdateCart){
      returnObj.available = updateCartFunction;
      returnObj.preorder = updateCartFunction;
      returnObj.backorder = updateCartFunction;
    }
    
    return returnObj;
  }();
});