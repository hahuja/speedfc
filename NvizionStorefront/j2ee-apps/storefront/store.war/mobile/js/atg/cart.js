/**
 * This function makes area scrollable
 * 
 * @param element
 *            a DOM element which will be scrollable
 */
function makeScrollable(element) {
  element.wrapAll("<div class=\"scroll\" />");
  var scrollingContainer = element.parent();
  var scrollingArea = element;
  var scrollingContainerHeight = scrollingContainer.height();
  var scrollingAreaHeight = scrollingArea.height();
  var scrollingContainerTop = scrollingContainer.position().top;
  var scrollingContainerBottom = scrollingContainerTop + scrollingContainerHeight;
  var startY = 0;
  var currentMargin = 0;
  element.bind("touchmove", function(e) {
    e.preventDefault();
    e.stopPropagation();
    var currentY = e.originalEvent.changedTouches[0].clientY;
    if (currentY < scrollingContainerTop || currentY > scrollingContainerBottom) {
      return;
    }
    currentMargin = currentMargin + currentY - startY;
    if (currentMargin > 0) {
      currentMargin = 0;
    } else if (scrollingAreaHeight - scrollingContainerHeight + currentMargin < 0) {
      currentMargin = scrollingContainerHeight - scrollingAreaHeight;
    }
    scrollingArea.animate({"margin-top": currentMargin + "px"}, "fast");
    startY = e.originalEvent.changedTouches[0].clientY;

  });
  element.bind("touchstart", function(e) {
    startY = e.originalEvent.changedTouches[0].clientY;
  });
}

/**
 * This function shows the move dialog for the specified commerce item.
 * 
 * @param ciid
 *            commerce item id
 * @param x
 *            x coordinate of the left top corner
 * @param y
 *            y coordinate of the left top corner
 */
function showMoveDialog(ciid, x, y) {
  // Move dialog items box to the specified position
  $("div.mobile_store_moveDialog div.mobile_store_moveItems").css({top: y, right: x });

  // Remove the commerce item whose id is set into hidden input value
  $("div.mobile_store_moveDialog a.mobile_store_removeLink").click(function() {
    var name = $("#mobile_store_removeItemFromOrder").attr("name");
    var html = '<input type="hidden" name="' + name + '" value="' + ciid + '" />';
    var cartform = $(document.forms["cartform"]);
    cartform.append(html);

    cartform.submit();
  });

  // Hide dialog by clicking outside the dialog items box
  $("div.mobile_store_moveDialog").click(function() {
    $(this).hide();
  });

  // Show the dialog
  $("div.mobile_store_moveDialog").show();
  toggleModal(true);
}

/**
 * This function shows the share dialog for the specified commerce item.
 * 
 * @param ciid
 *            commerce item id
 * @param x
 *            x coordinate of the right top corner
 * @param y
 *            y coordinate of the right top corner
 */
function showShareDialog(ciid, x, y) {
  // Move dialog items box to the specified position
  $("div.mobile_store_shareDialog div.mobile_store_shareItems").css({top: y, right: x });

  // Hide dialog by clicking outside the dialog items box
  $("div.mobile_store_shareDialog").click(function() {
    $(this).hide();
  });

  // Show the dialog
  $("div.mobile_store_shareDialog").show();
  toggleModal(true);
}

/**
 * This function shows the editting box using values from the commerce item box
 * from the shopping cart.
 * 
 * @param cibox
 *            commerce item box from the shopping cart
 */
function createCommerceItemEditBox(cibox) {
  // Get commerce item id
  var ciid = $(cibox).attr("id").substring("mobile_store_cartItem_".length);

  // Create a copy of edit box template and insert it after the commerce item block
  var editBlock = $(".mobile_store_editCartBox:last").clone().insertAfter(cibox);

  // Set edit block values
  editBlock.children("div.image").html($(cibox).children("div.image").html());
  editBlock.children("p.name").html($("#productName", cibox).val());
  var propertiesHtml = $("div.item p.properties", cibox).html();
  // If the properties list is not empty, then add a comma at the end
  if (!/^\s*$/.test(propertiesHtml)) {
    propertiesHtml = propertiesHtml.replace(/(^\s+)|(\s+$)/g, "").concat(',');
  }
  $("a.mobile_store_productDetailPageLink", editBlock).prepend(propertiesHtml);
  $("a.mobile_store_productDetailPageLink", editBlock).attr("href", $("div.item p.name a", cibox).attr("href"));
  
  // Note, that productQty isn't defined for gift products. Set 1 for such case.  
  var productQty = $('[name='+ ciid+']').val();
  $("span.mobile_store_quantity", editBlock).html(productQty ? productQty : 1);

  // Hide and then remove the edit box on click
  editBlock.click(function() {
    $(this).hide();
    window.hiddenCommerceItemBox.show();
    $(this).remove();
  });

  $("a.mobile_store_moveLink", editBlock).click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var top = $(this).offset().top;
    var parenttop = $("#mobile_store_pageContainer").offset().top;
    // This will get the right offset
    var right = $(document).width() - ($(this).offset().left + $(this).outerWidth());

    showMoveDialog(ciid, right, top - parenttop);
  });

  $("a.mobile_store_shareLink", editBlock).click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var top = $(this).offset().top;
    var parenttop = $("#mobile_store_pageContainer").offset().top;
    // This will get the right offset
    var right = $(document).width() - ($(this).offset().left + $(this).outerWidth());

    showShareDialog(ciid, right, top - parenttop);
  });

  // Remove already shown edit box
  if (window.editCommerceItemBox) {
    window.editCommerceItemBox.hide();
    window.editCommerceItemBox.remove();
  }

  // Show hidden box
  if (window.hiddenCommerceItemBox) {
    window.hiddenCommerceItemBox.show();
  }

  // Save edit box in global space
  window.editCommerceItemBox = editBlock;

  // Save hidden box in global space
  window.hiddenCommerceItemBox = $(cibox);

  $(cibox).hide();
  editBlock.show();

  // Truncate color
  var link = $("a.mobile_store_productDetailPageLink", editBlock);
  var linkLeft = link.offset().left;
  var linkWidth = link.outerWidth();
  var remLeft = $("a.mobile_store_moveLink", editBlock).offset().left;
  var toCut = linkLeft + linkWidth - remLeft + 15;

  if(toCut > 0) {
    var colWidth = $("span.property.color", editBlock).width();
    $("span.property.color", editBlock).truncate(colWidth - toCut);
  }
}

/**
 * This function makes commerce item box clickable. After clicking the commerce
 * item box the editing box will be created and shown in place of commerce item
 * box.
 * 
 * @param cibox
 *            commerce item box from the shopping cart to initialize
 */
function initCommerceItemBox(cibox) {
  // Make box clickable
  $(cibox).click(function() {
    createCommerceItemEditBox(this);
  });

  // Disable links inside of the box
  $("a", cibox).click(function(e) {
    e.preventDefault();
  });
}

/**
 * This function truncates the long text inside of the box to fit inside a
 * specified width.
 * 
 * @param width
 *            limit width
 */
(function ($) {
  $.fn.truncate = function (width) {
    return this.each(function () {
			var $element = $(this),
      token = '&hellip;',
      fontCSS = {
					'fontFamily': $element.css('fontFamily'),
					'fontSize': $element.css('fontSize'),
					'fontStyle': $element.css('fontStyle'),
					'fontWeight': $element.css('fontWeight'),
					'font-variant': $element.css('font-variant'),
					'text-indent': $element.css('text-indent'),
					'text-transform': $element.css('text-transform'),
					'letter-spacing': $element.css('letter-spacing'),
					'word-spacing': $element.css('word-spacing'),
					'display': 'none'
				},
				truncatedText = $element.text(),
				$helperSpan = $('<span/>').css(fontCSS).html(truncatedText).appendTo('body'),
				originalWidth = $helperSpan.width();

        if (originalWidth > width) {
          var truncatedTextLength = truncatedText.length;
          do {
            truncatedText = truncatedText.substr(0, truncatedTextLength - 1) + token;
            truncatedTextLength--;
            $helperSpan.html(truncatedText);
          } while ($helperSpan.width() > width);
          $element.html(truncatedText);
        }

        $helperSpan.remove();
      });
  };
})(jQuery);

/**
 * Sends promotion coupon code to server
 */
function applyCoupon() {
  var name = $("#mobile_store_applyCoupon").attr("name");
  var value = $("#mobile_store_applyCoupon").attr("value");
  var html = '<input type="hidden" name="' + name + '" value="' + value + '" />';
  $("#mobile_store_promotionCodeInput").after(html);

  $("#mobile_store_promotionCodeInput").closest("form").submit();
}

/**
 * Apply coupon if the user press enter on the coupon field and the coupon value
 * has been changed.
 */
function applyNewCouponOnEnter() {
  if (event.keyCode=='13') {
    var enteredCode =  $('#mobile_store_promotionCodeInput').attr('value');
    var previousCode = $('#mobile_store_promotionPreviousCode').attr('value');
    if (enteredCode != previousCode) {
      applyCoupon();
    }
  }
}

var featuredItemsSlider = {
  /**
   * We start by creating the sliding grid out of the specified
   * element.  We'll look for each child with a class of cell when
   * we create the slide panel.
   */
  createSlidePanel: {
    value: function(/*string*/ gridid, /*string*/ cellPrefix, /*int*/ numberOfCellsToDisplay) {
      var x = 0;

      var parentContainerWidth = $(gridid).parent().width();
      var cellWidth = parentContainerWidth / numberOfCellsToDisplay;
      var thisObject = this;

      $(gridid).each(function() {
        $(this).css({
          'position': 'relative',
          'left': '0'
        });

        $(this).parent().css('overflow', 'hidden');
        $(this).children('.cell').each(function(index) {
          $(this).css({
            width: cellWidth + 'px',
            height: '90%',
            position: 'absolute',
            left: x + 'px'
          });

          x += cellWidth;
        });

        try {
          document.createEvent('TouchEvent');
            /**
			 * Now that we've finished the layout
			 * we'll make our panel respond to all
			 * of the touch events.
			 */
          thisObject.makeTouchable(gridid, 3000, x, cellWidth, parentContainerWidth, null, null);
        } catch (e) {
          /**
           * Then we aren't on a device that supports touch
           */
          $(this).css({
            'height': '70px',
            'overflow-x': 'auto',
            'overflow-y': 'hidden'
          });
        }
      });
    }
  }
};

/**
 * This function initializes the slide panel for the mobile store related items
 * container.
 */
function initFeaturedItemsSlidePanel() {
  var relatedSlider = Object.create(sliders, featuredItemsSlider);
  relatedSlider.createSlidePanel("#mobile_store_featuredItemsContainer", "", 4);
}

/**
 * Redirects user to appropriate page
 * 
 * @param linkId
 *            redirect link
 */
function editReviewOrderBlock(linkId) {
  if (linkId != '') {
    window.location.replace($('#'+linkId).attr('href'));
  }
}
