/**
 * This function makes area scrollable
 * 
 * @param element
 *            a DOM element which will be scrollable
 */
function makeScrollable(element) {
  element.wrapAll("<div class=\"scroll\" />");
  var scrollingContainer = element.parent();
  var scrollingArea = element;
  var scrollingContainerHeight = scrollingContainer.height();
  var scrollingAreaHeight = scrollingArea.height();
  var scrollingContainerTop = scrollingContainer.position().top;
  var scrollingContainerBottom = scrollingContainerTop + scrollingContainerHeight;
  var startY = 0;
  var currentMargin = 0;
  element.bind("touchmove", function(e) {
    e.preventDefault();
    e.stopPropagation();
    var currentY = e.originalEvent.changedTouches[0].clientY;
    if (currentY < scrollingContainerTop || currentY > scrollingContainerBottom) {
      return;
    }
    currentMargin = currentMargin + currentY - startY;
    if (currentMargin > 0) {
      currentMargin = 0;
    } else if (scrollingAreaHeight - scrollingContainerHeight + currentMargin < 0) {
      currentMargin = scrollingContainerHeight - scrollingAreaHeight;
    }
    scrollingArea.animate({"margin-top": currentMargin + "px"}, "fast");
    startY = e.originalEvent.changedTouches[0].clientY;

  });
  element.bind("touchstart", function(e) {
    startY = e.originalEvent.changedTouches[0].clientY;
  });
}

/**
 * This function shows the move dialog for the specified commerce item.
 * 
 * @param ciid
 *            commerce item id
 * @param x
 *            x coordinate of the left top corner
 * @param y
 *            y coordinate of the left top corner
 */
function showMoveDialog(ciid, x, y) {
  // Move dialog items box to the specified position
  $("div.mobile_store_moveDialog div.mobile_store_moveItems").css({top: y, right: x });

  // Remove the commerce item whose id is set into hidden input value
  $("div.mobile_store_moveDialog a.mobile_store_removeLink").click(function() {
    var name = $("#mobile_store_removeItemFromOrder").attr("name");
    var html = '<input type="hidden" name="' + name + '" value="' + ciid + '" />';
    var cartform = $(document.forms["cartform"]);
    cartform.append(html);

    cartform.submit();
  });

  // Hide dialog by clicking outside the dialog items box
  $("div.mobile_store_moveDialog").click(function() {
    $(this).hide();
  });

  // Show the dialog
  $("div.mobile_store_moveDialog").show();
  toggleModal(true);
}

/**
 * This function shows the share dialog for the specified commerce item.
 * 
 * @param ciid
 *            commerce item id
 * @param x
 *            x coordinate of the right top corner
 * @param y
 *            y coordinate of the right top corner
 */
function showShareDialog(ciid, x, y) {
  // Move dialog items box to the specified position
  $("div.mobile_store_shareDialog div.mobile_store_shareItems").css({top: y, right: x });

  // Hide dialog by clicking outside the dialog items box
  $("div.mobile_store_shareDialog").click(function() {
    $(this).hide();
  });

  // Show the dialog
  $("div.mobile_store_shareDialog").show();
  toggleModal(true);
}

/**
 * This function shows the editting box using values from the commerce item box
 * from the shopping cart.
 * 
 * @param cibox
 *            commerce item box from the shopping cart
 */
function createCommerceItemEditBox(cibox) {
  // Get commerce item id
  var ciid = $(cibox).attr("id").substring("mobile_store_cartItem_".length);

  // Create a copy of edit box template and insert it after the commerce item block
  var editBlock = $(".mobile_store_editCartBox:last").clone().insertAfter(cibox);

  // Set edit block values
  editBlock.children("div.image").html($(cibox).children("div.image").html());
  editBlock.children("p.name").html($("#productName", cibox).val());
  var propertiesHtml = $("div.item p.properties", cibox).html();
  // If the properties list is not empty, then add a comma at the end
  if (!/^\s*$/.test(propertiesHtml)) {
    propertiesHtml = propertiesHtml.replace(/(^\s+)|(\s+$)/g, "").concat(',');
  }
  $("a.mobile_store_productDetailPageLink", editBlock).prepend(propertiesHtml);
  $("a.mobile_store_productDetailPageLink", editBlock).attr("href", $("div.item p.name a", cibox).attr("href"));
  
  // Note, that productQty isn't defined for gift products. Set 1 for such case.  
  var productQty = $('[name='+ ciid+']').val();
  $("span.mobile_store_quantity", editBlock).html(productQty ? productQty : 1);

  // Hide and then remove the edit box on click
  editBlock.click(function() {
    $(this).hide();
    window.hiddenCommerceItemBox.show();
    $(this).remove();
  });

  $("a.mobile_store_moveLink", editBlock).click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var top = $(this).offset().top;
    var parenttop = $("#mobile_store_pageContainer").offset().top;
    // This will get the right offset
    var right = $(document).width() - ($(this).offset().left + $(this).outerWidth());

    showMoveDialog(ciid, right, top - parenttop);
  });

  $("a.mobile_store_shareLink", editBlock).click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var top = $(this).offset().top;
    var parenttop = $("#mobile_store_pageContainer").offset().top;
    // This will get the right offset
    var right = $(document).width() - ($(this).offset().left + $(this).outerWidth());

    showShareDialog(ciid, right, top - parenttop);
  });

  // Remove already shown edit box
  if (window.editCommerceItemBox) {
    window.editCommerceItemBox.hide();
    window.editCommerceItemBox.remove();
  }

  // Show hidden box
  if (window.hiddenCommerceItemBox) {
    window.hiddenCommerceItemBox.show();
  }

  // Save edit box in global space
  window.editCommerceItemBox = editBlock;

  // Save hidden box in global space
  window.hiddenCommerceItemBox = $(cibox);

  $(cibox).hide();
  editBlock.show();

  // Truncate color
  var link = $("a.mobile_store_productDetailPageLink", editBlock);
  var linkLeft = link.offset().left;
  var linkWidth = link.outerWidth();
  var remLeft = $("a.mobile_store_moveLink", editBlock).offset().left;
  var toCut = linkLeft + linkWidth - remLeft + 15;

  if(toCut > 0) {
    var colWidth = $("span.property.color", editBlock).width();
    $("span.property.color", editBlock).truncate(colWidth - toCut);
  }
}

/**
 * This function makes commerce item box clickable. After clicking the commerce
 * item box the editing box will be created and shown in place of commerce item
 * box.
 * 
 * @param cibox
 *            commerce item box from the shopping cart to initialize
 */
function initCommerceItemBox(cibox) {
  // Make box clickable
  $(cibox).click(function() {
    createCommerceItemEditBox(this);
  });

  // Disable links inside of the box
  $("a", cibox).click(function(e) {
    e.preventDefault();
  });
}

/**
 * This function truncates the long text inside of the box to fit inside a
 * specified width.
 * 
 * @param width
 *            limit width
 */
(function ($) {
  $.fn.truncate = function (width) {
    return this.each(function () {
			var $element = $(this),
      token = '&hellip;',
      fontCSS = {
					'fontFamily': $element.css('fontFamily'),
					'fontSize': $element.css('fontSize'),
					'fontStyle': $element.css('fontStyle'),
					'fontWeight': $element.css('fontWeight'),
					'font-variant': $element.css('font-variant'),
					'text-indent': $element.css('text-indent'),
					'text-transform': $element.css('text-transform'),
					'letter-spacing': $element.css('letter-spacing'),
					'word-spacing': $element.css('word-spacing'),
					'display': 'none'
				},
				truncatedText = $element.text(),
				$helperSpan = $('<span/>').css(fontCSS).html(truncatedText).appendTo('body'),
				originalWidth = $helperSpan.width();

        if (originalWidth > width) {
          var truncatedTextLength = truncatedText.length;
          do {
            truncatedText = truncatedText.substr(0, truncatedTextLength - 1) + token;
            truncatedTextLength--;
            $helperSpan.html(truncatedText);
          } while ($helperSpan.width() > width);
          $element.html(truncatedText);
        }

        $helperSpan.remove();
      });
  };
})(jQuery);

/**
 * Sends promotion coupon code to server
 */
function applyCoupon() {
  var name = $("#mobile_store_applyCoupon").attr("name");
  var value = $("#mobile_store_applyCoupon").attr("value");
  var html = '<input type="hidden" name="' + name + '" value="' + value + '" />';
  $("#mobile_store_promotionCodeInput").after(html);

  $("#mobile_store_promotionCodeInput").closest("form").submit();
}

/**
 * Apply coupon if the user press enter on the coupon field and the coupon value
 * has been changed.
 */
function applyNewCouponOnEnter() {
  if (event.keyCode=='13') {
    var enteredCode =  $('#mobile_store_promotionCodeInput').attr('value');
    var previousCode = $('#mobile_store_promotionPreviousCode').attr('value');
    if (enteredCode != previousCode) {
      applyCoupon();
    }
  }
}

var featuredItemsSlider = {
  /**
   * We start by creating the sliding grid out of the specified
   * element.  We'll look for each child with a class of cell when
   * we create the slide panel.
   */
  createSlidePanel: {
    value: function(/*string*/ gridid, /*string*/ cellPrefix, /*int*/ numberOfCellsToDisplay) {
      var x = 0;

      var parentContainerWidth = $(gridid).parent().width();
      var cellWidth = parentContainerWidth / numberOfCellsToDisplay;
      var thisObject = this;

      $(gridid).each(function() {
        $(this).css({
          'position': 'relative',
          'left': '0'
        });

        $(this).parent().css('overflow', 'hidden');
        $(this).children('.cell').each(function(index) {
          $(this).css({
            width: cellWidth + 'px',
            height: '90%',
            position: 'absolute',
            left: x + 'px'
          });

          x += cellWidth;
        });

        try {
          document.createEvent('TouchEvent');
            /**
			 * Now that we've finished the layout
			 * we'll make our panel respond to all
			 * of the touch events.
			 */
          thisObject.makeTouchable(gridid, 3000, x, cellWidth, parentContainerWidth, null, null);
        } catch (e) {
          /**
           * Then we aren't on a device that supports touch
           */
          $(this).css({
            'height': '70px',
            'overflow-x': 'auto',
            'overflow-y': 'hidden'
          });
        }
      });
    }
  }
};

/**
 * This function initializes the slide panel for the mobile store related items
 * container.
 */
function initFeaturedItemsSlidePanel() {
  var relatedSlider = Object.create(sliders, featuredItemsSlider);
  relatedSlider.createSlidePanel("#mobile_store_featuredItemsContainer", "", 4);
}

/**
 * Redirects user to appropriate page
 * 
 * @param linkId
 *            redirect link
 */
function editReviewOrderBlock(linkId) {
  if (linkId != '') {
    window.location.replace($('#'+linkId).attr('href'));
  }
}
/**
 * Global Javascript functions
 */

/**
 * Chain together multiple animations.
 * 
 * Usage: chainAnimations([function, duration,]...);
 */
function chainAnimations(){
  var args = Array.prototype.slice.call(arguments);
  var f = args.shift();
  var delay = args.shift();
  var tail = args;
  var callee = arguments.callee;
  if (f){
    f();
    if (delay) setTimeout(function (){ callee.apply(this, tail);}, delay);
  }
}

// String formatting in one pass
String.prototype.format = function format(){
  var formatted = ""; // this will be our formatted string
  var split = this.split('');
  var inParam = false; // state variable
  var paramNumber = '';
  for (var i = 0; i < split.length; i++){
    var current = split[i];
    switch (current){
      case '{': // begin specifying a parameter
        inParam = true;
        break;
      case '}': // done specifying parameter
        inParam = false;
        // insert the parameter or, if it doesn't exist, leave it as-is
        var param = arguments[parseInt(paramNumber, 10)] || "{"+paramNumber+"}";
        formatted += param; // insert the parameter into the formatted string
        paramNumber = ""; // make sure to reset the paramNumber
        break;
      default:
        if (inParam){
          paramNumber += current;
        }
        else{
          formatted += current;
        }
        break;
    }
  }
  return formatted;
};

/**
 * Refreshes badge count on shopping cart
 * 
 * @param newCount
 *            new count
 */
function refreshCartBadge(newCount){
  if (newCount > 0) {
    var $cartBadge = $("#mobile_store_cartBadge");
    $cartBadge.show(); // in case it's currently hidden
    chainAnimations(function(){ $cartBadge.toggleClass('highlight').toggleClass('textFade'); }, 500,
                    function(){ $cartBadge.text(newCount).toggleClass('textFade');}, 900,
                    function(){ $cartBadge.toggleClass('highlight')});
  }
}

/**
 * Toggle contact block
 */
function toggleContact(){
  var $contactInfo = $("#mobile_store_contactInfo").show();
  toggleModal(true);
  return false;
}

/**
 * Toggle rows (li's) on login/registration pages
 * 
 * @param li
 *            li ID
 */
function loginPageToggle(li){
  $('#'+li).prev('li').toggleClass('imageClicked');
  $('#'+li).toggleClass('hidden');
  return false;
}

/**
 * Used to toggle rows on the login screen.
 * 
 * @param li
 *            li ID
 */
function loginPageClick(li){
  if (li === 'loginRow'){
    var regRow = $('#regRow');
    if (! regRow.hasClass("hidden"))
      loginPageToggle('regRow');
  } else {
    var loginRow = $('#loginRow');
    if (! loginRow.hasClass("hidden"))
      loginPageToggle('loginRow');
  }
    loginPageToggle(li);
    
  return false;
}

/**
 * One-pass cookie parser
 * 
 * @param name
 *            cookie name
 */
function getCookieByName(name){
  var cookiesSplit = document.cookie.split('');

  // State variables
  var inName = true;
  var isSkip = true;
  var currentName = '';
  var currentValue = '';

  // Check the cookie string
  for (var i = 0; i < cookiesSplit.length; i++){
    var ch = cookiesSplit[i];
    switch (ch){
      case ';':
        inName = true;
        if (isSkip){ // not the cookie we want
          currentName = '';
          currentValue = '';
        }
        else return decodeURI(currentValue);
        break;
      case '=':
        inName = false;
        if (currentName === name) isSkip = false; // this is the right cookie
        break;
      case ' ':
        break;
      default:
        if (inName) currentName += ch;
        else currentValue += ch;
        break;
    }
  }
  if (!isSkip) return decodeURI(currentValue);
  else return null;
}

/**
 * Shows or hides modal
 * 
 * @param showOrHide
 *            false for hiding
 */
function toggleModal(showOrHide){
  $("#mobile_store_modalOverlay").toggle(showOrHide);
  // if hiding the modal overlay, also hide all direct children
  if (showOrHide == false){
    $("#mobile_store_modalOverlay > :not(div.shadow)").hide();
  }
}

/**
 * Adds/removes 'searchFocus' class to element with 'mobile_store_header'-id
 * 
 * @param isFocus boolean
 */
function searchFocus(isFocus){
  $("#mobile_store_header").toggleClass('searchFocus', isFocus);
}

$(window).one("load",function () {
  window.scrollTo(0, 1);
});(function ($) {
	  /**
		* Converts list to dropdown one.
		*/
  $.fn.dropdownList = function() {
    return this.each(function() {
      var $ul = $(this);

      function selectItemHandler(event) {
        var thisObj = event.currentTarget;

        if ($('#currentBillingAddress').length != 0 && $('#currentBillingAddress')[0] != thisObj) {
          $('#currentBillingAddress').next().remove();
          $('#currentBillingAddress').remove();
        }

        $("input[type=radio][name$='selectedBillingAddress']", $(thisObj)).attr("checked", "checked").change();
        var radios = $("input[type=radio][name$='selectedBillingAddress']", $ul);

        $(radios).parents("li").unbind("click");
		    setTimeout(function(thisObj) {
          hideUnselectedAddresses(thisObj);
        }, 500, thisObj);
      }

      function selectedItemClickHandler(event) {
        event.preventDefault();

        var radios = $("input[type=radio][name$='selectedBillingAddress']", $ul);
        $(radios).not(":checked").parents("li").css("display", "");
        $("li.mobile_store_formActions, li.mobile_store_border", $ul).css("display", "");
        $(this).unbind("click");

        if ($('#currentBillingAddress').length != 0) {
          var unmatchedRadios = $("li[id!='currentBillingAddress'] input[id!='" + $('#matchedSecondaryAddressId').val() + "'][type=radio][name$='selectedBillingAddress']", $ul);
          var matchedRadios = $("li#currentBillingAddress input[type=radio][name$='selectedBillingAddress'], li input[id='" + $('#matchedSecondaryAddressId').val() + "'][type=radio][name$='selectedBillingAddress']", $ul);

          $(unmatchedRadios).parents('li').click(
            function(event) {
              changeBillingAddressDialog(event, selectItemHandler);
            }
          );
          $(matchedRadios).parents('li').click(selectItemHandler);
        } else {
          $(radios).parents("li").click(selectItemHandler);
        }

        $(this).removeClass("mobile_store_hideSelectedBAMark");
      }

      function hideUnselectedAddresses(li) {
        if ($('li:first-child', $ul)[0] != $(li)[0]) {
            var borderLi = $(li).next().detach();
            $(li).detach();
            $('li:first-child', $ul).before($(borderLi));
            $(borderLi).before($(li));
        }

        var uncheckedLis = $("input[type=radio][name$='selectedBillingAddress']", $ul).not(':checked').parents('li');
        $(uncheckedLis).css('display', 'none');
        $(uncheckedLis).removeClass('mobile_store_selectedBillingAddress');
        $('li.mobile_store_formActions, li.mobile_store_border', $ul).css('display', 'none');

        var checkedLi = $("input[type=radio][name$='selectedBillingAddress']:checked", $ul).parents('li');
        $(checkedLi).click(selectedItemClickHandler);
        $(checkedLi).addClass('mobile_store_selectedBillingAddress mobile_store_hideSelectedBAMark');
      }

      var li = $("input[type=radio][name$='selectedBillingAddress']:checked", $ul).parents('li');
      hideUnselectedAddresses(li);
      li.click(selectedItemClickHandler);
    });
  };

  /**
	 * Adds delayed submit to the list form. Called when user clicks on any
	 * list item except border & action ones.
	 * 
	 * @param delay
	 *            timeout before submitting form (default - 200ms)
	 */
  $.fn.delayedSubmit = function(delay) {
    delay = (typeof delay === "undefined") ? 200 : delay;

    return this.each(function() {
      var $container = $(this);

      $("li", $container).not(".mobile_store_formActions, .mobile_store_border").click(function (event) {
        var target = event.target || event.srcElement;
        if (target.localName === "a") return;
        
        var radio = $("input:radio", $(this));
        radio.attr("checked", "checked");
        radio.change();

        setTimeout(function () {
          $container.parents("form").submit();
        }, delay);
      });
    });
  };
})(jQuery);
/* Landing Page Javascript
   Version: $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/atg/home.js#3 $ $Change: 692002 $ */

/**
 * Removes items from sessionStorage
 */
function clearSessionStorage(){
  sessionStorage.removeItem("targeters");
  sessionStorage.removeItem("loggedin");
  sessionStorage.removeItem("userid");
}

/**
 * Changes the class on the circle id to signify whether its on or off
 * 
 * @param gridid
 *            circle id
 * @param status
 *            indicator if circle is on
 */
function setCircleStatus(/*id*/ gridid,/*boolean*/ status){
  if(status){
    $("#pageCircle_" + gridid).removeClass('BLANK').addClass('ON');
  } else {
    $("#pageCircle_" + gridid).removeClass('ON').addClass('BLANK');
  }
}

/**
 * Creates the promotional item slider
 */
function createPromotionalItemSlider(){
  var parentSlider = Object.create(sliders,promotionalItemSlider);
  parentSlider.createSlidePanel("#mobile_store_homeTopSlotContent");
}

/**
 * Saves the promotional content items and creates the slider.
 */
function storeAndCreatePromotionalItemSlider(){
  var html = $("#mobile_store_homeTopSlotContent").html();
  sessionStorage.removeItem("promotionalContent");
  sessionStorage.setObject("promotionalContent",html);
  createPromotionalItemSlider();
}

/**
 * Displays promotional content item from session storage
 */
function displayPromotionalContentItems(){
  var html = sessionStorage.getObject("promotionalContent");
  $("#mobile_store_homeTopSlotContent").empty().html(html);
  createPromotionalItemSlider();
}

/**
 * Displays the product results based on the index from sessionStorage
 * 
 * @param index
 *            targeter index
 */
function displayProductsFromIndex(/*int*/ index){  
  var targeters = sessionStorage.getObject("targeters");
  
  if(targeters){
	  displayProducts(targeters[index]);
  }
}

/**
 * Displays the product results based on the targeter path from sessionStorage
 * 
 * @param targeter
 *            targeter which products should be displayed
 */
function displayProducts(/*string*/ targeter){  
  var html = sessionStorage.getObject(targeter);
  
  if(html)
    createProductItemSlider(html);
}

/**
 * This displays the first set of results from the targeter results
 */
function displayInitialPromotionalContentProducts(){
  var targeters = sessionStorage.getObject("targeters");
  var firstTargeter = targeters[0];
  
  displayProducts(firstTargeter);
}

/**
 * This attempts to display the product information from the sessionStorage, or
 * retrieves it if it doesn't exist
 * 
 * @param retalerId
 *            retailer id
 * @param siteBaseURL
 *            URL of the site
 * @param loggedIn
 *            indicator if user is logged in
 * @param currencySymbol
 *            currency symbol
 */
function displayPromotionalContentProducts(/*string*/ retailerId,/*string*/ siteBaseURL, /*boolean*/ loggedIn, /*string*/ currencySymbol){
  
  var load = false;
  
  var currentMarker = makePromoContentMarker(siteBaseURL, loggedIn),
      storedMarker = sessionStorage.getItem("promoContentMarker");
  
  //If nothing to show or marker changed
  if (!sessionStorage.getObject("targeters") || storedMarker !== currentMarker) {
	  load = true;
  }
	
  if(!load){
    displayInitialPromotionalContentProducts();  
  } else {
    retrievePromotionalContentProducts(retailerId,siteBaseURL,loggedIn,currencySymbol); 
  }
}

/**
 * This adds blank cells around the html content passed in
 * 
 * @param html
 *            products info in html format
 * @param doubleRearPadding
 *            flag to apply doubled padding
 * @return product info with paddings in html format
 */
function addPaddingCells(/*string*/ html, /*boolean*/ doubleRearPadding){
  
  var returnString = "<div class='cell'></div>" + html + "<div class='cell'></div>";
  if(doubleRearPadding){
    returnString = returnString + "<div class='cell'></div>";
  }
  
  return returnString;
}

/**
 * This allows us to get the results of a template operations
 * 
 * @param htmlContent
 *            html content like div
 * @return HTML Content as String
 */
function returnHTMLObjectAsString(/* HTML Object like a div */ htmlContent) {
  return $("<div />").append(htmlContent).html();
}

/**
 * Check if the number is even
 * 
 * @param number
 *            number to check
 */
function isEven(/*int*/ number){
  return number % 2 === 0;
}

/**
 * This creates the product item slider from the html passed in
 * 
 * @param htmlContent
 *            product's info content in html format
 */
function createProductItemSlider(/*string*/ htmlContent){
  var productSlider = Object.create(sliders,productItemSlider);
  $("#mobile_store_homeBottomSlotContent").empty().html(htmlContent);
  productSlider.createSlidePanel('#mobile_store_homeBottomSlotContent ',"cell", 3);
}

/**
 * This function initializes the home page.
 * 
 * First, stores the promotional content items in case we need to re-render them
 * based on orientation changes.
 * 
 * Then, we register the templates for product info json. Then we just to
 * display the product info from session storage, if present, or we down the
 * product json, store it, and display it
 * 
 * @param retailerId
 *            retailer Id
 * @param siteBaseURL
 *            URL of the site
 * @param loggedIn
 *            indicator if the user
 * @param currencySymbol
 *            currency symbol
 */
function loadHomePage(/* string */ retailerId,/* string */siteBaseURL, /*boolean*/ loggedIn, /*string*/ currencySymbol) {
  
  storeAndCreatePromotionalItemSlider();
  
  //We construct the template for each product cell
  var productInfoTemplateString = '\
  <div class="cell">\
    <a href="${linkUrl}">\
      <img alt="${name}" class="cellImage" src="${imageUrl}"/>\
      <p class="cellDetails">\
        <span>${name}</span>\
        {{if (prices.listPrice && prices.salePrice) }}<span class="saleListPrice">${prices.listPrice}</span><span class="salePrice"><B> ${prices.salePrice}</B></span>\
        {{else prices.listPrice}}<span class="listPrice"> ${prices.listPrice}</span>{{/if}}\
      </p>\
    </a>\
  </div>';
  $.template( 'productInfoTemplate', productInfoTemplateString);
  
  var productInfoRecTempateString = '\
  <div class="cell">\
    <a href="${$item.processURL(url)}">\
      <img alt="${name}" class="cellImage" src="${thumb_image_link}"/>\
      <p class="cellDetails">\
        <span>${name}</span>\
        <span class="listPrice">${$item.processPrice(price)}</span>\
      </p>\
    </a>\
  </div>';
  $.template( 'productInfoRecsTemplate', productInfoRecTempateString);

  var orientationSupport = "onorientationchange" in window, orientationEvent = orientationSupport ? "orientationchange" : "resize";
  
  //We add this resize event to re-render the parents/child so that things are centered correctly
  window.addEventListener(orientationEvent, function() {
    displayPromotionalContentItems();
    displayPromotionalContentProducts();
  }, false);

  displayPromotionalContentProducts(retailerId, siteBaseURL, loggedIn, currencySymbol);
}

/**
 * This attempts to get the rec cookies to use in our rec request
 * @return cookies object
 */
function getRecCookies(){
  var cookies = {};
  
  cookies.visitorId = getCookieByName("atgRecVisitorId");
  cookies.sessionId = getCookieByName("atgRecSessionId");
  
  return cookies;
}

/**
 * This function retrieves the product results from the targeters
 * 
 * @param retailerId
 *            retailer Id
 * @param siteBaseURL
 *            URL of the site
 * @param loggedIn
 *            indicator if the user
 * @param currencySymbol
 *            currency symbol
 */
function retrievePromotionalContentProducts(/* string */ retailerId,/*string*/siteBaseURL, /*boolean*/ loggedIn, /*string*/ currencySymbol){
  
    clearSessionStorage();
  
    $.getJSON(makeFullURL(siteBaseURL, 'promo/gadgets/homePromotionalProductsJSON.jsp'), function(data){
      
      var targeters = [];
      var emptyTargeter;
      
      $.each(data.targeterResults, function(index,collection){
        //store the targeter names to use as a key to retrieving them for storage
        targeters[index] = collection.targeter;
        
        if(collection.products.length === 0){
          //this assumes that if there are no results, that means that we should fill it with recs
          emptyTargeter = collection.targeter;
        } else{
          var padding = false;
          if(isEven(collection.products.length))
             padding = true;
          
          var html = addPaddingCells(returnHTMLObjectAsString($.tmpl('productInfoTemplate',collection.products)),padding);
          sessionStorage.setObject(collection.targeter,html);
        }
      });
      
      //If we have a targerer that needs to show recs results, lets get them now
      if(emptyTargeter){
        if(retailerId !== "" && retailerId !== null && retailerId !== undefined){
      	  var cookies = getRecCookies();
          if(cookies){
             var recUrl ="http://recs.atgsvcs.com/pr/recommendations/3.0/json/" + retailerId + "/"
             if(cookies.visitorId)
               recUrl = recUrl + cookies.visitorId;
             
             recUrl = recUrl + "?";
             
             if(cookies.sessionId){
            	 recUrl = recUrl + "sessionId=" + cookies.sessionId + "&"
             }
             
             //We want the thumbnail image, so we add slots.homeChildren.dataItems=thumb_image_link to URL
             recUrl = recUrl + "slots.homeChildren.numRecs=7&slots.homeChildren.dataItems=thumb_image_link&channel=mobile";
             
            $.ajax({
                  type: 'GET',
                  dataType: 'jsonp',
                  url: recUrl,
                  success: function(data) {
                    var padding = false;
                    if(isEven(data.slots.homeChildren.recs.length))
                       padding = true;
                    
                    var optionsRecs = {
                        processPrice: function(item){
                      if(currencySymbol == 'USD')
                        return "$" + item.toFixed(2);
                      if(currencySymbol == 'EUR') 
                        return item.toFixed(2) + '�';  
                      },
                        processURL: function(item){
                          return siteBaseURL + item;
                        }
                    };
                    
                    var html = addPaddingCells(returnHTMLObjectAsString($.tmpl('productInfoRecsTemplate', data.slots.homeChildren.recs,optionsRecs)),padding);
                    sessionStorage.setObject(emptyTargeter,html);
                  }});
          }
        } else{ // In this case, recs isn't installed, so for now lets just fill it in with data from CRS
        	$.getJSON(makeFullURL(siteBaseURL, 'promo/gadgets/homeNoRecsProductsJSON.jsp'), function(data){
        		$.each(data.targeterResults, function(index,collection){
              var padding = false;
              if(isEven(collection.products.length))
                 padding = true;
        	    var html = addPaddingCells(returnHTMLObjectAsString($.tmpl('productInfoTemplate',collection.products)),padding);
        	    sessionStorage.setObject(emptyTargeter,html);
        	  });
        	});
        }
      }
      
      sessionStorage.setItem("promoContentMarker", makePromoContentMarker(siteBaseURL, loggedIn));
      sessionStorage.setObject("targeters", targeters);
      displayInitialPromotionalContentProducts();
  });
}


/**
 * Function makes marker value Use to unique identify promotional content
 * 
 * @param siteBaseURL
 *            baseURL
 * @param loggedIn
 *            boolean value
 * @param userId
 *            user's ID
 * 
 * @return marker
 */
function makePromoContentMarker(/*string*/siteBaseURL, /*boolean*/ loggedIn) {
	var userId = getCookieByName("DYN_USER_ID");
	return marker = siteBaseURL + "|" + loggedIn + "|" + userId;
}

/**
 * Function makes URL for JSON excluding "undefined"-text
 * 
 * @param siteBaseURL
 *            baseURL
 * @param additionalPath
 *            additional path parameters
 * 
 * @return full URL-address
 */
function makeFullURL(/*string*/siteBaseURL, /*string*/additionalPath) {
	if (siteBaseURL!=undefined) {
		var index = siteBaseURL.indexOf(";jsessionid");
		if (index == -1) {
			return siteBaseURL + additionalPath;
		}
		else {
			var head = siteBaseURL.substr(0, index);
			var tail = siteBaseURL.substr(index);
			 
			return head + additionalPath + tail;
		}
	} else {
		return additionalPath;
	}
}/* More Info Javascript functions 
Version: $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/atg/moreinfo.js#4 $ $Change: 692002 $
*/

/**
 * Toggles action buttons for store (map, phone, email)
 * 
 * @param caller
 *            store info row
 */
function storeLocationClick(caller){

  var $this = $(caller);

  var toggleLocationControls = function ($controlPanel) {
    $controlPanel.children('img').toggleClass('clicked');
    $controlPanel.next('div').children('div').toggleClass('hidden');
    return false;
  }

  if ($this.next('div').css('display') == 'none') {
	$this.find('.leftArrow')[0].className="downArrow";
    $this.next('div').toggle();
    window.setTimeout(toggleLocationControls, 4, $this);
  } else {
    toggleLocationControls($this);
    $this.find('.downArrow')[0].className="leftArrow";
    window.setTimeout(function($controlPanel) {
      $controlPanel.next('div').toggle();
    }, 200, $this); //give a bit more time to browser for animation and then completely hide controls.
  }
}

/**
 * Updates page with new locale
 * 
 * @param e
 *            input which URL contains locale attribute
 */
function chooseLanguage(e){
  var href = e.currentTarget.value;
  setTimeout(function(){ window.location.href = href;}, 500);
  return true;
}/**
 * Apply 'default' css style to select element if selected option is default one
 * 
 * @param e
 *            select element
 */
function changeDropdown(e){
  // we have to copy over the class from the 'option' to the 'select'
  // because Webkit will not honor styles applied to an option element
  var $this = $(e.currentTarget);
  $this.toggleClass("default", $("option:selected", $this).is('option:first-child'));
}

/**
 * Make nearest input radio checked
 * 
 * @param event
 *            click event
 */
function checkAddressRadio(event) {
  $("input", event.currentTarget.parentElement).attr("checked", true);
}

/**
 * Starts edit address procedure on 'Edit Credit Card' page
 * 
 * @param event
 *            change event
 * 
 */
function creditCardSelectAddressToEdit(event) {
	event.stopPropagation();
	$(event.currentTarget).parents("form").submit();
}

/**
 * Starts address creation procedure on 'Edit Credit Card' page
 */
function creditCardCreateNewAddress() {
	$("#createNewAddress").val(true);
	window.event.cancelBubble = true;
	$("#createNewAddress").parents("form").submit();
}

/**
 * Submits a form to the order details page to show order with id = orderId
 * 
 * @param orderId
 *            ID of order
 */
function loadOrderDetails(orderId) {
  $("#orderId").attr("value", orderId);
  $("#loadOrderDetailForm").submit();
}

/**
 * Shows modal dialog to confirm delete operation
 * 
 * @param offsetContainer
 *            a container that contains delete link, clicking on it shows modal
 *            dialog. Dialog position calculates from this container element.
 */
function removeItemDialog(offsetContainer) {
  var $offsetContainer = $(offsetContainer);
  var top = $offsetContainer.offset().top - $("#mobile_store_pageContainer").offset().top;
  var right = $(document).width() - $offsetContainer.offset().left - $offsetContainer.outerWidth() - 1; // -1px for the left border

  $("div.mobile_store_moveDialog div.mobile_store_moveItems").css({top: top, right: right });

  // Hide dialog by clicking outside the dialog items box
  $("div.mobile_store_moveDialog").click(function() {
    $(this).hide();
  });

  $("div.mobile_store_moveDialog").show();
  toggleModal(true);
}

/**
 * Shows modal dialog to confirm changing billing address
 * 
 * @param event
 *            change event
 * @param aCallback
 *            callback function
 */
function changeBillingAddressDialog(event, aCallback) {
  event.preventDefault();
  var $link = $(event.currentTarget);
  //var top = $link.offset().top - $("#mobile_store_pageContainer").offset().top - $link.outerHeight();
  var top = $link.offset().top + $link.outerHeight()/2 - 25;
  var right = $(document).width() - $link.offset().left - $link.outerWidth();

  $("div#changeBillingAddressDialog div.mobile_store_content").css({top: top, right: right });

  // Hide dialog by clicking outside the dialog items box
  $("div#changeBillingAddressDialog").click(function() {
    $(this).hide();
  });

  $('div#changeBillingAddressDialog li a').click(function() {aCallback(event);});

  $("div#changeBillingAddressDialog").show();
  toggleModal(true);
}

/**
 * Displays the full CRS modal redirect dialog. The link to the full CRS in the
 * dialog will contain the orderId parameter.
 * 
 * @param orderId
 *            an 'orderId' parameter value to add to the link to the full CRS
 *            site
 */
function toggleRedirectWithOrderId(orderId){
  var $modalDialog = $("#mobile_store_modalMessageBox");
  var link = $("a", $modalDialog);
  link.attr("href", addURLParam("orderId", orderId, link.attr("href")));
  $modalDialog.show();
  toggleModal(true);
  window.scroll(0,1);
}

/**
 * Displays the full CRS modal redirect dialog if address is used in gift list.
 */
function toggleCantDeleteAddressDialog() {
  $("#mobile_store_modalMessageBox").show();
  toggleModal(true);
  window.scroll(0,1);
}

/**
 * Attaches value from email input to the url value (email parameter)
 * 
 * @param emailFieldId
 *            id of email input tag
 * @param urlId
 *            id of url hidden tag
 */
function copyEmailToUrl(emailFieldId, urlId) {
  $('#' + urlId).val($('#' + urlId).val() + '&email=' + $('#' + emailFieldId).val());
}

/**
 * This function is a handler for country dropdown change event. It toggles
 * state dropdown and invokes standard handler
 * 
 * @param event 
 *            change event
 */
function selectCountry(event) {
  toggleState($(event.currentTarget).val());
  changeDropdown(event);
}

/**
 * This function shows states dropdown for selected country
 * 
 * @param coutry 
 *            a selected country
 * @param state
 *            [optional] - if specified then state will be preselected in the
 *            dropdown shown
 */
function toggleState(country, state) {
  var $currentState = $("select.state:visible");
  var $newState = $("select.state[data-country='"+country+"']");
  // important to add/remove the 'name' attribute from all selects;
  // otherwise, the URL becomes polluted with empty values
  var nameAttr = $currentState.attr("name");
  $("select.state").removeAttr("name");
  $currentState.val("").addClass("default").toggle();
  $newState.toggleClass("default", !(state && state != ""));
  $newState.val(state).attr('name', nameAttr).toggle();    
}

/**
 * This function is a handler for state dropdown change event. It toggles state
 * dropdown and populate appropriate country in country dropdown
 * 
 * @param event 
 *            change event
 */
function selectState(event) {
  var $countrySelect = $("#mobile_store_countrySelect");
  var state = $(event.currentTarget).val();
  var country = $("option:selected", event.currentTarget).attr("data-country");
  $("select.state:not(:visible)").removeAttr("name");
  if (country && country != "") {
    $countrySelect.val(country).removeClass("default");
    toggleState(country, state);
  }
  changeDropdown(event);
}
/**
 * Product javascript
 */

/**
 * Initialize the related items slider
 */
function initRelatedItemsSlider(){
  var relatedSlider = Object.create(sliders, relatedProductsSlider);
  relatedSlider.createSlidePanel("#mobile_store_relatedItemsContainer", "related_product", 4);
}

/**
 * This function toggles product's view between normal and enlarged
 */
function toggleProductView() {
  var detailsContainer = $(".mobile_store_pickers");
  detailsContainer.toggleClass("mobile_store_productEnlarged");
}

var relatedProductsSlider = {
  /**
   * We start by creating the sliding grid out of the specified
   * element.  We'll look for each child with a class of cell when
   * we create the slide panel.
   */
  createSlidePanel: {
    value: function(/*string*/ gridid, /*string*/ cellPrefix, /*int*/ numberOfCellsToDisplay) {
      var x = 0;

      var parentContainerWidth = $(gridid).parent().width();
      var cellWidth = parentContainerWidth / numberOfCellsToDisplay;
      var thisObject = this;

      $(gridid).each(function() {
        $(this).css({
          'position': 'relative',
          'left': '0'
        });

        $(this).parent().css('overflow', 'hidden');
        $(this).children('.cell').each(function(index) {
          $(this).css({
            width: cellWidth + 'px',
            height: '90%',
            position: 'absolute',
            left: x + 'px'
          });

          x += cellWidth;
        });

        try {
          document.createEvent('TouchEvent');
          /**
			 * Now that we've finished the layout
			 * we'll make our panel respond to all
			 * of the touch events.
			 */
          thisObject.makeTouchable(gridid, 3000, x, cellWidth, parentContainerWidth, null, null);
        } catch (e) {
          /**
           * Then we aren't on a device that supports touch
           */
          $(this).css({
            'height': '70px',
            'overflow': 'auto'
          });
        }
      });
    }
  }
};

/**
 * Run email me functionality
 * 
 * @param e
 *            click event
 */
function emailMeSubmit(e) {
  e.preventDefault();
  var $form = $(e.currentTarget);
  var $input = $("td:has(input[type='email'])", $form).removeClass("error");
  var $checkbox = $("input#mobile_store_rememberCheckbox")[0];
  if ($checkbox.checked) {
    $("input[name='rememberEmail']")[0].value = $("input", $input)[0].value;
  }
  $.post($form.attr("action"),
          $form.serialize(),
          function(errors) {
            if (errors.length > 0) {
              $input.closest("tr").addClass('error');
            }
            else {
              var $confirm = $("#mobile_store_emailMePopup_confirm");
              $(".mobile_store_modalDialog").hide();
              $confirm.show();
            }
          },
          'json');
}

/**
 * New SKU pickers
 */
function expandPickers(showOrHide){
  $("li.mobile_store_pickers").toggleClass("expanded", showOrHide);
}

/**
 * skuFunctions is declared on load, but initialized on document ready. This is
 * because the functions within require that specific DOM elements are present.
 * This cannot be guaranteed until the entire document has loaded.
 */
var skuFunctions;
$(document).ready(function(){
  // skuFunctions contains all functions relevant to selecting SKUs and adding to cart
  // It uses the module pattern to abstract common variables and functions as well
  // as avoid polluting the global namespace.
  skuFunctions = function() {
    var $addToCartForm = $("#mobile_store_addToCart");
    var isUpdateCart = ($addToCartForm.length == 0);
    // see mobile/browse/productDetail.jsp — the update cart form will only be present
    // when the add to cart form is not.
    var $updateCartForm = $("#mobile_store_updateCart");
    var $skuIdField = $("input#addToCart_skuId");
    var $actionsContainer = $("#mobile_store_addToCartButton");
    var $addToCartButton = $("button", $actionsContainer);
    var $buttonText = $("span#mobile_store_buttonText", $actionsContainer);
    
    var $emailMeContainer = $("div#mobile_store_emailMePopup");
    var $emailMeConfirm =$("div#mobile_store_emailMe_confirm");
    var $emailMeForm = $("form", $emailMeContainer);
    var $emailMeSkuField = $("#mobile_store_emailMeSkuId", $emailMeForm);
    var $emailMeAddress = $("#mobile_store_rememberMeEmailAddress", $emailMeForm);
    var $emailMeAddressRow = $("#mobile_store_emailAddressRow",$emailMeForm);
    var $rememberCheckbox = $("input#mobile_store_rememberCheckbox", $emailMeForm)[0];
    var $rememberInput = $("input[name='rememberEmail']", $emailMeForm);
    
    var $priceContainer = $("#mobile_store_price");
    var $initialPriceDisplay = $priceContainer.html();
    
    // Text on submit button ("Add To Cart", "PreOrder" etc.) at last successfull submit
    var prevButtonText = null;

    
    /**
     * Update the product price. This is done dynamically because there
     * are multiple ways the price can be displayed.
    */
    var updatePrice = function(salePrice, oldPrice) {
      var price = "<p><strong>" + salePrice + "</strong>";
      if (oldPrice) {
        price = price + "<br>" + RESOURCES['common.price.old'] + "&nbsp;<span>" + oldPrice + "</span>";
      }
      price = price + "</p>";
      $priceContainer.html(price);
    };

    /**
      * This function checks addToCart response for errors.
      * responseData - JSON response returned when add to cart form was submitted
      *  Contains new cart item count, cart url, and form errors (if any)
     */
    var checkAddToCartError = function(responseData) {
      var error = responseData.addToCartError;
      if (error) {
        // Handle errors
        console.log(error);
      } else {
        var cartUrl = responseData.cartUrl;
        prevButtonText = $addToCartButton.text();
        $addToCartButton.text(RESOURCES['navigation_shoppingCart.viewCart'])
                        .unbind("click")
                        .removeAttr("onclick")
                        .click(function(event){
                          event.stopPropagation();
                          // redirect to cart page
                          document.location.href = cartUrl;
                        });
        refreshCartBadge(responseData.cartItemCount);
      }
    };
  
    // This function handles clicks on the cart button
    var cartLinkClick;
    if (isUpdateCart){ // when updating the cart, just submit the form normally
      cartLinkClick = function(event) {
        event.stopPropagation();
        $updateCartForm.submit();
      };
    }
    else{ // when adding to cart, submit the form via AJAX and then check the response
      cartLinkClick = function(event) {
        event.stopPropagation();
        $.post($addToCartForm.attr("action"),
               $addToCartForm.serialize(),
               checkAddToCartError);
      };
    }

    /**
	 * For SKUs that can be added to the cart: Updates
	 * product price, sale price, and the "Add To Cart"
	 * button. Returns a function that closes over
	 * button label, button text, and SKU fields so that
	 * no unnecessary operations are performed as the
	 * user selects SKUs
	 */
    var addToCartFunction = function(buttonLabel, buttonText, skuFields){
      return function(){
        updatePrice(skuFields.productPrice, skuFields.salePrice);
        $skuIdField.val(skuFields.skuId);
        $addToCartButton.text(buttonLabel)
                        .removeAttr("onclick") // jQuery 'unbind' doesn't remove click events 
                        .unbind('click')       // bound in the markup, so we also use 'removeAttr'
                        .click(cartLinkClick)
                        .removeAttr("disabled");
        $buttonText.text(buttonText);
      }
    };
    
    var emailMeClick = function(event){
      $emailMeContainer.show();
      $emailMeConfirm.hide();
      toggleModal(true);
    }
    
    var emailMeSubmit = function(event){
      event.preventDefault();
      if ($rememberCheckbox.checked) {
        $rememberInput.val($emailMeAddress.val());
      }
      $.post($emailMeForm.attr("action"),
             $emailMeForm.serialize(),
             function(errors) {
               if (errors.length > 0) {
                 $emailMeAddressRow.toggleClass('error',true);
               }
               else {
                $emailMeAddressRow.toggleClass('error',false);
                 $emailMeContainer.hide();
                 $emailMeConfirm.show();
               }
             },
             'json');
    }
    

	    /**
		 * For SKUs that cannot be added to the cart:
		 * Updates product price, sale price, and changes
		 * the "Add to Cart" button to "Email Me". Returns a
		 * function that closes over SKU fields so that no
		 * unnecessary operations are performed as the user
		 * selects SKUs
		 */
    var outOfStockFunction = function(skuFields){
      return function(){
        updatePrice(skuFields.productPrice, skuFields.salePrice);
        $emailMeSkuField.val(skuFields.skuId);
        $addToCartButton.text(RESOURCES['mobile.button.emailMeText'])
                        .removeAttr("onclick") // jQuery 'unbind' doesn't remove click events 
                        .unbind('click')       // bound in the markup, so we also use 'removeAttr'
                        .removeAttr("disabled")
                        .click(emailMeClick);
        $buttonText.text(RESOURCES['common.temporarilyOutOfStock']);
      }
    };

    // curried addToCartFunction, fixing button text
    var updateCartFunction = function(skuFields){
        return addToCartFunction(RESOURCES['mobile.productDetails.updatecart'], "", skuFields);
    };

    var toggleRedirect = function(){
      $(".mobile_store_modalDialog").hide();
      toggleModal(true);
      $emailMeForm.show();
    };

    /**
	 * Executed every time a picker value is changed to
	 * determine which SKU (if any) has been selected.
	 * 
	 * @param properties
	 *            – an array of names of SKU
	 *            properties. The function will search
	 *            for 'select' elements on the page with
	 *            the corresponding names Order is
	 *            important for constructing the key!
	 */ 
    function checkForSelectedSku(properties){
      var key = "";
      for (var i = 0; i < properties.length; i++){
        var prop = properties[i];
        var $input = $("select[name='"+prop+"']");
        if ($input.length > 0){
          key += $input.val()+":";
        }
      }
      var skuFunction = productSKUs[key.slice(0,-1)]; // remove the trailing colon
      if (skuFunction){ // if this is not a valid SKU selection, skuFunction will be null
        skuFunction();
      }
      else{
        // disable the button if this is not a valid selection
        $addToCartButton.attr("disabled","disabled");
      }
    }
    

    /**
	 * Called when a picker's value has changed. Accepts
	 * an indeterminate number of arguments representing
	 * the names of sku properties Rather than declared
	 * explicitly, these arguments are parsed from the
	 * 'arguments' object
	 */
    var pickerSelectFunction = function(){
      // "slice" the arguments object to make sure we only iterate over the function arguments,
      // not any of the other fields of the object
      var properties = Array.prototype.slice.call(arguments);
      return function(event){
        var $this = $(event.currentTarget);
        // we need to set "title" attribute for select to make VoiceOver read it as "Name - Value"
        var defaultText = $("option:disabled", $this).attr("label");
        var noValue = ($this.val() == "");
        if (noValue){
          // revert to initial display if there is no valid selection
          $priceContainer.html($initialPriceDisplay);
        }
         // we need to set the appropriate title for the current selection
        $this.attr("title", noValue ? "" : defaultText);
        // if no value has been selected, toggle "selected" off. Otherwise, toggle it on.
        $(event.currentTarget).toggleClass('selected', !noValue);
        checkForSelectedSku(properties); // check if this is a complete SKU selection
      }
    }

    // Called when the quantity picker's value has changed.
    var quantitySelect = function(event) {
      var $qtyField = $("input#addToCart_qty");
      var selectedQty = $("select[name='qty']").val();
      $qtyField.val(selectedQty);
      if (isUpdateCart){
        $addToCartButton.text(RESOURCES['mobile.productDetails.updatecart']);
      }
      else{
    	  var currText = $addToCartButton.text();
    	  if (currText == RESOURCES['navigation_shoppingCart.viewCart']){
    		  $addToCartButton.unbind("click")
    		  	.removeAttr("onclick")
    		  	.click(cartLinkClick)
    		  	.text(prevButtonText);
    	  }
      }
    }
    
    // Create the 'interface' for the skuFunctions module,
    // exposing only the functions that need to be called directly
    var returnObj = {
      cartLinkClick: cartLinkClick,
      toggleRedirect: toggleRedirect,
      emailMe: emailMeClick,
      emailMeSubmit: emailMeSubmit,
      outOfStock: outOfStockFunction,
      colorSizePickerSelect: pickerSelectFunction("color","size"),
      multiSkuPickerSelect: pickerSelectFunction("feature"),
      woodFinishPickerSelect: pickerSelectFunction("woodFinish"),
      quantitySelect: quantitySelect,
      available: function(skuFields){
        // curried addToCartFunction, fixing button text
        return addToCartFunction(RESOURCES['common.button.addToCartText'], "", skuFields);
      },
      preorder: function(skuFields){
        // curried addToCartFunction, fixing button text
        return addToCartFunction(RESOURCES['mobile.button.preorderLabel'],
                                 RESOURCES['mobile.button.preorderText'],
                                 skuFields);
      },
      backorder: function(skuFields){
        // curried addToCartFunction, fixing button text
        return addToCartFunction(RESOURCES['common.button.addToCartText'],
                                 RESOURCES['mobile.button.backorderText'],
                                 skuFields);
      },
    };
    
    // If we are updating the cart, then all 'add to cart' functions do the same thing
    if (isUpdateCart){
      returnObj.available = updateCartFunction;
      returnObj.preorder = updateCartFunction;
      returnObj.backorder = updateCartFunction;
    }
    
    return returnObj;
  }();
});/* Search Javascript functions
Version: $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/atg/search.js#3 $ $Change: 692002 $
*/

/**
 * Parses the specified URL or current page URL if url parameter is undefined
 * and returns a dictionary Object that maps decoded parameter names to decoded
 * values.
 * 
 * @param url
 *            the specified url with params to map; if undefined the current
 *            location is used
 * 
 * @return An object with keys and values where keys are the parameter's names
 *         from the url
 */
function getURLParams(url){

  if (!url) {
    url = window.location.href;
  }

  var params = decodeURI(url);
  var indexOfQuestionMark = params.indexOf('?');
  if (indexOfQuestionMark !== -1) { //if this is whole url, not search part
    params = params.slice(indexOfQuestionMark);
  }

  // A state machine to parse the param string in one pass
  var state = false;
  var key, value;
  key = value = '';
  var paramsObject = {};
  // the first character will always be '?', so we skip it
  for (var i = 1; i < params.length; i++){
    var current = params.charAt(i);
    switch (current){
      case '=':
        state = !state;
        break;
      case '&':
        state = !state;
        paramsObject[key] = value;
			    key = value = '';
        break;
      default:
        if (state){
	          value += current;
        }
        else{
          key += current;
        }
        break;
    }
  }
  if (key) paramsObject[key] = value;// put the last pair on the paramsObject
  return paramsObject;
}

/**
 * Returns the specified URL with the specified parameter added or changed. If
 * the url not specified the current page URL is used.
 * 
 * @param key
 *            the name of the parameter to set (any existing entry for that
 *            parameter will be replaced)
 * @param value
 *            the desired value for the parameter
 * @param url
 *            the specified url to add the parameter, if undefined the current
 *            page URL is used
 * 
 * @return an encoded url string
 */
function addURLParam(key, val, url){
  if (!url) {
    url = window.location.href;
  }
  var params = getURLParams(url);
  params[key] = val;
  var loc = url.slice(0,url.indexOf('?'));
  return encodeURI(loc) + "?" + $.param(params); //JQuery.param makes it to be encoded like the encodeURIComponent function does.
}

/**
 * Load the next page of results using simple search
 * 
 * @param caller
 *            context
 * @param num
 *            page number
 */
function loadPageSimple(caller,num){
  $("span", caller).text(RESOURCES['mobile.search.loading']);
  $.ajax({
    type: "GET",
    url: "../search/gadgets/moreResults.jsp",
    data: {'page':num},
    context: caller,
    success: function(results){
      $(this).replaceWith(results);
    }
  });
  return false;
}

/**
 * Load the next page of results using full search
 * 
 * @param caller
 *            context
 * @param num
 *            page number
 * @param token
 *            token
 */
function loadPageFull(caller,num,token){
  $("span", caller).text(RESOURCES['mobile.search.loading']);
  $.ajax({
    type: "GET",
    url: "../atgsearch/gadgets/searchResultsRenderer.jsp",
    data: {'pageNum':num, 'token':token},
    context: caller,
    success: function(results){
      $(this).replaceWith(results);
    }
  });
  return false;
}

/**
 * Toggle the refinements display
 * 
 * @param show
 *            true if we're trying to show the refinements display
 */
function toggleRefinements(show){
  $("#mobile_store_refine").toggleClass('hidden', !show);
}

/**
 * Initialize facets, adding any pre-selected facets to the global FacetTrail
 */
function initFacets(){
  var facetString = unescape(getURLParams().facetTrail);
  
  if (facetString){
    // split the string into an array of entries of the form '<facet>:<value>[|<value ...]'
    var trailEntries = facetString.match(/[A-Za-z0-9_]+:[A-Za-z0-9_\-\|\.\/]+/gi);

    if (trailEntries){
      for (var i = 0; i < trailEntries.length; i++){
    	  // split up the entry into the facet and its value(s)
    	  var matches = trailEntries[i].match(/[A-Za-z0-9_\-\|\.\/]+/gi);
    	  var facet = matches[0];
    	  var values = matches[1];
    	  values = values.match(/[A-Za-z0-9_\-\.]+/gi);
    	  // Identify the corresponding 'select' element and select the appropriate values
    	  $("#mobile_store_facets select[name='"+facet+"']").val(values).toggleClass('visible',true);
	    }
    }
  }
}

/**
 * Remove 'notLoaded' css class from img element if image is loaded
 * 
 * @param caller
 *            the DOM element that called our function
 */
function imgLoad(caller, imgUrl, imgName){
  $(caller).removeClass('notLoaded').removeAttr('onload');
  return false;
}

/**
 * Executes on facet's selection at the Refine page 
 * 
 * @param event
 */
function facetSelect(event){
  var $select = $(event.currentTarget);
  if ($select.val()){ // if nothing is select, val() will be null
    $select.toggleClass('visible', true);
  }
  else{
    $select.toggleClass('visible', false);
  }
  // Switch the 'Cancel' button to a 'Done' button
  $("#mobile_store_refine .mobile_store_buttonContainer button[name='cancel']")
    .attr('name','done')
    .text(RESOURCES['mobile.search.refine.done'])
	  .one('click',submitRefinements);
}

/**
 * Submit a refinement request and load the results via AJAX
 * 
 * @param event
 *            change event
 */
function submitRefinements(event){
  var $this = $(event.currentTarget);
  $this.text(RESOURCES['mobile.search.refine.cancel']).attr('name',"cancel"); // switch the 'Done' button back to 'Cancel

  var facetString = "";

  $("#mobile_store_facets select").each(function(){
    var $select = $(this);
    var selectedValues = $select.val();
    if (selectedValues){
      var facetSubstring = $select.attr('name')+":";
      for (var i = 0; i < selectedValues.length; i++){
        facetSubstring += selectedValues[i] + "|";
      }
      facetString += facetSubstring.slice(0,-1) + ":";
    }
  });
  
  facetString = facetString.slice(0,-1); // remove the trailing colon

  if (facetString.length === 0){
	  // if the user de-selects all facets, just resubmit the original search
    $("#mobile_store_searchForm").submit();
    return;
  }

  // Add this facet selection to the page history, so that navigating 'back' from a later page
  // will bring the user to the refined results rather than the original search
  history.replaceState({'facetTrail':facetString}, "Faceted Search", addURLParam('facetTrail',facetString));

  var resultsContainer = $("ul.searchResults");
  var searchToken = $("#mobile_store_refine input[name='token']").attr('value');
  var requestData = {'token':searchToken, 'facetTrail':facetString};

  // Show the "Refining..." message in the overlay
  var messageBox = $("#mobile_store_modalMessageBox");
  messageBox.html("<img id='mobile_store_spinner' src='/nrsdocroot/content/mobile/images/spinner.png' alt='Loading'/>").show().addClass("refineOverlay");
  var overlay = $("#mobile_store_modalOverlay");
  window.scrollTo(0,1); // important to scroll back to the top of the page
  overlay.show();

  // Submit the refinement request
  $.ajax({
    type: "GET",
    url: "../atgsearch/gadgets/searchResultsRenderer.jsp",
    data: requestData,
    success: function(results){
	    var countContainer = $(results).first('input[name="count"]');
	    var count = countContainer.attr('value');
	    countContainer.remove(); // no need to keep it in the DOM
	    resultsContainer.html(results); // display new results on the page
	    $("#mobile_store_refine_header #mobile_store_toggleSearchResults span").text(count); // update the count
      messageBox.html('').hide().removeClass("refineOverlay"); // clean up the messageBox
	    overlay.hide();
    }
  });
}/**
 * Oracle ATG modified the touchslider.js code from
 * https://github.com/zgrossbart/jstouchslide in the creation of this file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */

var sliders = {
  /**
   * This function just binds the touch functions for the grid.
   * It is very important to stop the default, stop the
   * propagation, and return false.  If we don't then the touch
   * events might cause the regular browser behavior for touches
   * and the screen will start sliding around.
   */
  makeTouchable: function(/*string*/ gridid, /*int*/ touchSensitivity,/*int*/ listWidth, /*int*/ colWidth,/*int*/ parentWidth, /*function*/ postTouchMove, /*function*/ postTouchEnd) {

    var sliderObject = this;
    var postTouchMoveApply = null;
    var postTouchEndApply = null;
    this.ParentContainerWidth = parentWidth;
    this.width = listWidth;
    this.colWidth = colWidth;
    
    if(postTouchMove !== null){
      postTouchMoveApply = function(){
        postTouchMove.apply(sliderObject,arguments);
      };
    }
    if(postTouchEnd !== null){
      postTouchEndApply = function(){
        postTouchEnd.apply(sliderObject,arguments);
      };
    }

    $(gridid).each( function() {

      sliderObject.cells = $(this).children(".cell");

      this.ontouchstart = function(e) {
        sliderObject.touchStart($(this), e,touchSensitivity);
        return true;
      };
      this.ontouchend = function(e) {
        e.preventDefault();
        e.stopPropagation();
        if (sliderObject.sliding) {
          sliderObject.sliding = false;
          sliderObject.touchEnd($(this), e,touchSensitivity,postTouchEndApply);
          return false;
        } else {
          /*
           We never slid so we can just return true
           and perform the default touch end
           */
          return true;
        }
      };
      this.ontouchmove = function(e) {
        return sliderObject.touchMove($(this), e,postTouchMoveApply);
      };
    });
  },
  /**
   * A little helper to parse off the 'px' at the end of the left
   * CSS attribute and parse it as a number.
   */
  getLeft: function(/*JQuery*/ elem) {
    return parseInt(elem.css('left').substring(0, elem.css('left').length - 2), 10);
  },
  /**
   * When the touch starts we add our sliding class a record a few
   * variables about where the touch started.  We also record the
   * start time so we can do momentum.
   */
  touchStart: function(/*JQuery*/ elem, /*event*/ e) {
    
    $.each(this.cells, function(){$(this).show();});
    
    elem.css({
      '-webkit-transition-duration': '0'
    });

    this.startX = e.targetTouches[0].clientX;
    this.startY = e.targetTouches[0].clientY;
    this.startLeft = this.getLeft(elem);
    this.touchStartTime = new Date().getTime();
    this.touching = true;
  },
  /**
   * When the touch ends we need to adjust the grid for momentum
   * and to snap to the grid.  We also need to make sure they
   * didn't drag farther than the end of the list in either
   * direction.
   */
  touchEnd: function(/*JQuery*/ elem, /*event*/ e,/*int*/ touchSensitivity,/*function*/ callBack) {
    if (this.getLeft(elem) > 0) {
      /*
       * This means they dragged to the right past the first item
       */
      this.doSlide(elem, 0, '2s', false);

      elem.parent().removeClass('sliding');
      this.startX = null;
      this.startY = null;
      this.touching = false;
    } else if ((Math.abs(this.getLeft(elem)) + elem.parent().width()) > this.width) {
      /*
       * This means they dragged to the left past the last item
       */
	   if((this.width - elem.parent().width()) > 0){
		this.doSlide(elem, '-' + (this.width - elem.parent().width()), '2s', false);
	   } else {
	    this.doSlide(elem, 0, '2s',false);	
	   }
	  elem.parent().removeClass('sliding');
      this.startX = null;
      this.startY = null;
      this.touching = false;
    } else {
      /*
       This means they were just dragging within the bounds of the grid
       and we just need to handle the momentum and snap to the grid.
       */
      this.slideMomentum(elem, e, touchSensitivity,callBack);
    }
  },
  /**
   * If the user drags their finger really fast we want to push
   * the slider a little farther since they were pushing a large
   * amount.
   */
  slideMomentum: function(/*jQuery*/ elem, /*event*/ e, /*int*/ touchSensitivity,/*function*/ callBack) {
    var slideAdjust = (new Date().getTime() - this.touchStartTime) * 10;
    var left = this.getLeft(elem);
    var abs = Math.abs;

    /*
     We calculate the momentum by taking the amount of time they were sliding
     and comparing it to the distance they slide.  If they slide a small distance
     quickly or a large distance slowly then they have almost no momentum.
     If they slide a long distance fast then they have a lot of momentum.
     */

    var changeX = touchSensitivity * (abs(this.startLeft) - abs(left));

    slideAdjust = Math.round(changeX / slideAdjust);

    var newLeft = slideAdjust + left;

    /*
     * We need to calculate the closest column so we can figure out
     * where to snap the grid to.
     */
    var t = newLeft % this.colWidth;

    if ((abs(t)) > ((this.colWidth / 2))) {
      /*
       * Show the next cell
       */
      newLeft -= (this.colWidth - abs(t));
    } else {
      /*
       * Stay on the current cell
       */
      newLeft -= t;
    }

    var newLeftValue = 0;

    if (this.slidingLeft) {
      var maxLeft = parseInt('-' + (this.width - this.ParentContainerWidth), 10);
      /*
       * Sliding to the left
       */
      newLeftValue = Math.max(maxLeft, newLeft);
      this.doSlide(elem, newLeftValue, '0.5s',true,callBack);
    } else {
      /*
       * Sliding to the right
       */
      newLeftValue = Math.min(0, newLeft);
      this.doSlide(elem, newLeftValue, '0.5s', true,callBack);
    }

    elem.parent().removeClass('sliding');
    this.startX = null;
    this.startY = null;
    this.touching = false;
  },
  /**
   * Slides the elem to that left position over a certian duration
   */
  doSlide: function(/*jQuery*/ elem, /*int*/ x, /*string*/ duration, /* boolean */ showNext, /*function*/ callBack) {

    elem.css({
      left: x + 'px',
      '-webkit-transition-property': 'left',
      '-webkit-transition-duration': duration
    });

    if(showNext && callBack !== null) {
      callBack(x,duration);
    }
  },
  /**
   * While they are actively dragging we just need to adjust the
   * position of the grid using the place they started and the
   * amount they've moved.
   */
  touchMove: function(/*JQuery*/ elem, /*event*/ e, /*function*/ callBack) {
		var dy = this.startY - e.targetTouches[0].clientY;
	    var dx = this.startX - e.targetTouches[0].clientX;
	    //This is faster than calling Math.abs();
	    var absDY = dy < 0 ? -dy : dy;
	    var absDX = dx < 0 ? -dx : dx;
	    //We need to guess so to whether the user is trying to go up and down or left or right.
	    if(absDX > 0 && absDY < absDX) {

	      if (!this.sliding) {
	        elem.parent().addClass('sliding');
	      }

	      this.sliding = true;
	      //We thing that you are sliding left or right so we need to disable the default touch actions, so the page doesn't move
	      e.preventDefault();
	      e.stopPropagation();

	      var left = 0;

	      if (this.startX > e.targetTouches[0].clientX) {
	        /*
	         * Sliding to the left
	         */
	        left =  (dx - this.startLeft);
	        elem.css('left', '-' + left + 'px');
	        this.slidingLeft = true;
	        //We only want to attempt to re-highlight the center cell if we have moved enough that it has changed
	        if(callBack !== null && absDX >= this.colWidth)
	          callBack(-1*left);
	      } else {
	        /*
	         * Sliding to the right
	         */
	        left = (e.targetTouches[0].clientX - this.startX + this.startLeft);
	        elem.css('left', left + 'px');
	        this.slidingLeft = false;
	        if(callBack !== null && absDX >= this.colWidth)
	          callBack(left);
	      }
	      return false;
	    }
	    return true;
	  }
	};

var productItemSlider = {
  /**
   * We start by creating the sliding grid out of the specified
   * element.  We'll look for each child with a class of cell when
   * we create the slide panel.
   */
  createSlidePanel: {
    value: function(/*string*/ gridid, /*string*/ cellPrefix, /*int*/ numberOfCellsToDisplay) {
      var x = 0;

      //Since this is a promotional slider, we only want one item cell on the page,
      //so we set each cell to its parent's width
      var parent = $(gridid).parent();
      var parentContainerWidth = parent.width();
      this.numberOfCellsToDisplay = numberOfCellsToDisplay;
      var cellWidth =  parentContainerWidth / numberOfCellsToDisplay;

      var pageOffSet = 0;
      this.cellDetailsClass = cellPrefix + "Details";
      this.cellImageClass = cellPrefix + "Image";
      this.productInfoDisplayDiv = $("#mobile_store_homeBottomSlotProductDetails");

      var thisObject = this;

      $(gridid).each( function() {
        $(this).css({
          'position': 'relative'
        });

        $(this).parent().css('overflow', 'hidden');

        var children = $(this).children(".cell");

        children.each( function(index) {
          $(this).css({
            width: cellWidth + 'px',
            height: '90%',
            position: 'absolute',
            left: x + 'px'
          });
          //We need ot keep track of the cell id so we add an id and
          //its index for each cell, its image, and its description
          $(this).attr('id', cellPrefix + "Id" + index);

          x += cellWidth;
        });

          if(x > parentContainerWidth) {
            pageOffSet = -1 * ((x - parentContainerWidth) / 2);
          }
          
        $(this).css({
          'left': pageOffSet
        });

        //In the case where there are only enough cells to fit on the page, we
        //set the index of the center cell to center and subtract 1 to get start the index at 0
        var currentCellIndex = Math.ceil(numberOfCellsToDisplay/2) - 1;
        thisObject.centerCellDefault = currentCellIndex;
        var cellsOffPage = 0;
        if(pageOffSet < 0) {
          //We find out how many cells are off the page, and add that to the center cell index
          cellsOffPage = Math.round(Math.abs(pageOffSet/cellWidth));
          currentCellIndex += cellsOffPage;
        }

        var end = cellsOffPage+numberOfCellsToDisplay;
        $.each(children, function(index) {
          var $child = $(this);
          if(index < cellsOffPage || index >= end) {
            $child.hide();
          }
        });
        thisObject.currentCenterCell = currentCellIndex;
        thisObject.productInfoDisplayDiv.html($(children[thisObject.currentCenterCell]).find("." + thisObject.cellDetailsClass).html());
        $(children[thisObject.currentCenterCell]).find("." + thisObject.cellImageClass).toggleClass("highlightProduct");

        try {
          document.createEvent('TouchEvent');
          /*
           Now that we've finished the layout we'll make our panel respond
           to all of the touch events.
           */
          thisObject.makeTouchable(gridid, 3000,x,cellWidth,parentContainerWidth,thisObject.postTouchMove,thisObject.postTouchMoveAndEnd);
        } catch (e) {
        }
      });
    }
  },

  postTouchMoveAndEnd: {
    value: function(/*int*/ left, /*string*/ duration) {
      this.postTouchMove(left);
      var object = this;
      
      var strippedDuration = duration.substring(0,duration.length-1);
      
      setTimeout(function(){object.postTouchEnd(left);},strippedDuration);
      }
  },
  /**
   * This will calculate the middle cell based on the leftposition and do the appropriate action
   */
  postTouchMove: {
    value: function(/* int */ left) {
      if(left <= 0) {
        left = left < 0 ? -left : left;
        var currentCell = Math.round(left/this.colWidth) + this.centerCellDefault;
        if(this.currentCenterCell !== currentCell) {         
          var $newCell = $(this.cells[currentCell]);
          var $currentCell = $(this.cells[this.currentCenterCell]);   
          this.productInfoDisplayDiv.html($newCell.find("." + this.cellDetailsClass).html());
          $currentCell.find("." + this.cellImageClass).toggleClass("highlightProduct");
          $newCell.find("." + this.cellImageClass).toggleClass("highlightProduct");
          this.currentCenterCell = currentCell;
        }
      }
    }
  },

  /**
   * This function will calculate which items are on the screen, and set their display to hidden
   */
  postTouchEnd: {
    value: function(/*int*/ left) {
      if(!this.touching && left <= 0) {
        var cellsOffPage = Math.abs(left)/this.colWidth;
        var end = cellsOffPage+this.numberOfCellsToDisplay;
        $.each(this.cells, function(index) {
          var $cell = $(this);
          if(index < cellsOffPage || index >= end) {
            $cell.hide();
          } else {
            $cell.show();
          }
        });
      }
    }
  }
};

var promotionalItemSlider = {
  /**
   * We start by creating the sliding grid out of the specified
   * element.  We'll look for each child with a class of cell when
   * we create the slide panel.
   */
  createSlidePanel: {
    value: function(/*string*/ gridid) {
      var x = 0;

      //Since this is a promotional slider, we only want one item cell on the page,
      //so we set each cell to its parent's width
      var parent = $(gridid).parent();
      var parentContainerWidth = parent.width();
      var cellWidth =  parentContainerWidth;
      var thisObject = this;

      $(gridid).each( function() {
        $(this).css({
          'position': 'relative',
          'left': '0'
        });

        $(this).parent().css('overflow', 'hidden');
        var children = $(this).children(".cell");

        children.each( function(index) {
          $(this).css({
            width: cellWidth + 'px',
            height: '90%',
            position: 'absolute',
            left: x + 'px'
          });

          x += cellWidth;
        });

        thisObject.currentCenterCell = 0;

        children.slice(1).hide();
        try {
          document.createEvent('TouchEvent');
          /*
           Now that we've finished the layout we'll make our panel respond
           to all of the touch events.
           */
          thisObject.makeTouchable(gridid, 6000,x,cellWidth,parentContainerWidth,thisObject.postTouchMove,thisObject.postTouchMoveAndEnd);
        } catch (e) {
        }
      });
    }
  },
  /**
   * This will calculate the middle cell based on the leftposition and do the appropriate action
   */
  postTouchMove: {
    value: function(/* int */ left) {
      if(left <= 0) {
        left = left < 0 ? -left : left;
        var currentCell = Math.round(left/this.colWidth);
        if(this.currentCenterCell !== currentCell) {
          displayProductsFromIndex(currentCell);
          setCircleStatus(this.currentCenterCell ,false);
          setCircleStatus(currentCell,true);
          this.currentCenterCell = currentCell;
        }
      }
    }
  },
    /**
   * This function will calculate which items are on the screen, and set their display to hidden
   */
  postTouchEnd: {
    value: function(/*int*/ left) {
      if(!this.touching && left <= 0) {
        left = left < 0 ? -left : left;
        var cellsOffPage = Math.round(left/this.colWidth);
        var end = cellsOffPage+1;
        $.each(this.cells, function(index) {
          var $cell = $(this);
          if(index < cellsOffPage || index >= end) {
            $cell.hide();
          } else {
            $cell.show();
          }
        });
      }
    }
  },
    postTouchMoveAndEnd: {
    value: function(/*int*/ left, /*string*/ duration) {
      this.postTouchMove(left);
      var object = this;
      
      var strippedDuration = duration.substring(0,duration.length-1) * 1000;
      
      setTimeout(function(){object.postTouchEnd(left);},strippedDuration);
      }
  }
};/**
 * This adds a setObject method to the Storage interface so that we can add our
 * HTML fragments into local storage.
 * 
 * We should be able to do this without doing this, according to the
 * localstorage spec, but no browser has added support for it yet.
 */
Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

/**
 * This adds a getObject method to the Storage interface so that we can get our
 * HTML fragments out of ocal storage.
 * 
 * We should be able to do this without doing this, according to the
 * localstorage spec, but no browser has added support for it yet.
 */
Storage.prototype.getObject = function(key) {
    return this.getItem(key) && JSON.parse(this.getItem(key));
};