/**
 * Apply 'default' css style to select element if selected option is default one
 * 
 * @param e
 *            select element
 */
function changeDropdown(e){
  // we have to copy over the class from the 'option' to the 'select'
  // because Webkit will not honor styles applied to an option element
  var $this = $(e.currentTarget);
  $this.toggleClass("default", $("option:selected", $this).is('option:first-child'));
}

/**
 * Make nearest input radio checked
 * 
 * @param event
 *            click event
 */
function checkAddressRadio(event) {
  $("input", event.currentTarget.parentElement).attr("checked", true);
}

/**
 * Starts edit address procedure on 'Edit Credit Card' page
 * 
 * @param event
 *            change event
 * 
 */
function creditCardSelectAddressToEdit(event) {
	event.stopPropagation();
	$(event.currentTarget).parents("form").submit();
}

/**
 * Starts address creation procedure on 'Edit Credit Card' page
 */
function creditCardCreateNewAddress() {
	$("#createNewAddress").val(true);
	window.event.cancelBubble = true;
	$("#createNewAddress").parents("form").submit();
}

/**
 * Submits a form to the order details page to show order with id = orderId
 * 
 * @param orderId
 *            ID of order
 */
function loadOrderDetails(orderId) {
  $("#orderId").attr("value", orderId);
  $("#loadOrderDetailForm").submit();
}

/**
 * Shows modal dialog to confirm delete operation
 * 
 * @param offsetContainer
 *            a container that contains delete link, clicking on it shows modal
 *            dialog. Dialog position calculates from this container element.
 */
function removeItemDialog(offsetContainer) {
  var $offsetContainer = $(offsetContainer);
  var top = $offsetContainer.offset().top - $("#mobile_store_pageContainer").offset().top;
  var right = $(document).width() - $offsetContainer.offset().left - $offsetContainer.outerWidth() - 1; // -1px for the left border

  $("div.mobile_store_moveDialog div.mobile_store_moveItems").css({top: top, right: right });

  // Hide dialog by clicking outside the dialog items box
  $("div.mobile_store_moveDialog").click(function() {
    $(this).hide();
  });

  $("div.mobile_store_moveDialog").show();
  toggleModal(true);
}

/**
 * Shows modal dialog to confirm changing billing address
 * 
 * @param event
 *            change event
 * @param aCallback
 *            callback function
 */
function changeBillingAddressDialog(event, aCallback) {
  event.preventDefault();
  var $link = $(event.currentTarget);
  //var top = $link.offset().top - $("#mobile_store_pageContainer").offset().top - $link.outerHeight();
  var top = $link.offset().top + $link.outerHeight()/2 - 25;
  var right = $(document).width() - $link.offset().left - $link.outerWidth();

  $("div#changeBillingAddressDialog div.mobile_store_content").css({top: top, right: right });

  // Hide dialog by clicking outside the dialog items box
  $("div#changeBillingAddressDialog").click(function() {
    $(this).hide();
  });

  $('div#changeBillingAddressDialog li a').click(function() {aCallback(event);});

  $("div#changeBillingAddressDialog").show();
  toggleModal(true);
}

/**
 * Displays the full CRS modal redirect dialog. The link to the full CRS in the
 * dialog will contain the orderId parameter.
 * 
 * @param orderId
 *            an 'orderId' parameter value to add to the link to the full CRS
 *            site
 */
function toggleRedirectWithOrderId(orderId){
  var $modalDialog = $("#mobile_store_modalMessageBox");
  var link = $("a", $modalDialog);
  link.attr("href", addURLParam("orderId", orderId, link.attr("href")));
  $modalDialog.show();
  toggleModal(true);
  window.scroll(0,1);
}

/**
 * Displays the full CRS modal redirect dialog if address is used in gift list.
 */
function toggleCantDeleteAddressDialog() {
  $("#mobile_store_modalMessageBox").show();
  toggleModal(true);
  window.scroll(0,1);
}

/**
 * Attaches value from email input to the url value (email parameter)
 * 
 * @param emailFieldId
 *            id of email input tag
 * @param urlId
 *            id of url hidden tag
 */
function copyEmailToUrl(emailFieldId, urlId) {
  $('#' + urlId).val($('#' + urlId).val() + '&email=' + $('#' + emailFieldId).val());
}

/**
 * This function is a handler for country dropdown change event. It toggles
 * state dropdown and invokes standard handler
 * 
 * @param event 
 *            change event
 */
function selectCountry(event) {
  toggleState($(event.currentTarget).val());
  changeDropdown(event);
}

/**
 * This function shows states dropdown for selected country
 * 
 * @param coutry 
 *            a selected country
 * @param state
 *            [optional] - if specified then state will be preselected in the
 *            dropdown shown
 */
function toggleState(country, state) {
  var $currentState = $("select.state:visible");
  var $newState = $("select.state[data-country='"+country+"']");
  // important to add/remove the 'name' attribute from all selects;
  // otherwise, the URL becomes polluted with empty values
  var nameAttr = $currentState.attr("name");
  $("select.state").removeAttr("name");
  $currentState.val("").addClass("default").toggle();
  $newState.toggleClass("default", !(state && state != ""));
  $newState.val(state).attr('name', nameAttr).toggle();    
}

/**
 * This function is a handler for state dropdown change event. It toggles state
 * dropdown and populate appropriate country in country dropdown
 * 
 * @param event 
 *            change event
 */
function selectState(event) {
  var $countrySelect = $("#mobile_store_countrySelect");
  var state = $(event.currentTarget).val();
  var country = $("option:selected", event.currentTarget).attr("data-country");
  $("select.state:not(:visible)").removeAttr("name");
  if (country && country != "") {
    $countrySelect.val(country).removeClass("default");
    toggleState(country, state);
  }
  changeDropdown(event);
}
