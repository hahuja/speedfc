/* Landing Page Javascript
   Version: $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/atg/home.js#3 $ $Change: 692002 $ */

/**
 * Removes items from sessionStorage
 */
function clearSessionStorage(){
  sessionStorage.removeItem("targeters");
  sessionStorage.removeItem("loggedin");
  sessionStorage.removeItem("userid");
}

/**
 * Changes the class on the circle id to signify whether its on or off
 * 
 * @param gridid
 *            circle id
 * @param status
 *            indicator if circle is on
 */
function setCircleStatus(/*id*/ gridid,/*boolean*/ status){
  if(status){
    $("#pageCircle_" + gridid).removeClass('BLANK').addClass('ON');
  } else {
    $("#pageCircle_" + gridid).removeClass('ON').addClass('BLANK');
  }
}

/**
 * Creates the promotional item slider
 */
function createPromotionalItemSlider(){
  var parentSlider = Object.create(sliders,promotionalItemSlider);
  parentSlider.createSlidePanel("#mobile_store_homeTopSlotContent");
}

/**
 * Saves the promotional content items and creates the slider.
 */
function storeAndCreatePromotionalItemSlider(){
  var html = $("#mobile_store_homeTopSlotContent").html();
  sessionStorage.removeItem("promotionalContent");
  sessionStorage.setObject("promotionalContent",html);
  createPromotionalItemSlider();
}

/**
 * Displays promotional content item from session storage
 */
function displayPromotionalContentItems(){
  var html = sessionStorage.getObject("promotionalContent");
  $("#mobile_store_homeTopSlotContent").empty().html(html);
  createPromotionalItemSlider();
}

/**
 * Displays the product results based on the index from sessionStorage
 * 
 * @param index
 *            targeter index
 */
function displayProductsFromIndex(/*int*/ index){  
  var targeters = sessionStorage.getObject("targeters");
  
  if(targeters){
	  displayProducts(targeters[index]);
  }
}

/**
 * Displays the product results based on the targeter path from sessionStorage
 * 
 * @param targeter
 *            targeter which products should be displayed
 */
function displayProducts(/*string*/ targeter){  
  var html = sessionStorage.getObject(targeter);
  
  if(html)
    createProductItemSlider(html);
}

/**
 * This displays the first set of results from the targeter results
 */
function displayInitialPromotionalContentProducts(){
  var targeters = sessionStorage.getObject("targeters");
  var firstTargeter = targeters[0];
  
  displayProducts(firstTargeter);
}

/**
 * This attempts to display the product information from the sessionStorage, or
 * retrieves it if it doesn't exist
 * 
 * @param retalerId
 *            retailer id
 * @param siteBaseURL
 *            URL of the site
 * @param loggedIn
 *            indicator if user is logged in
 * @param currencySymbol
 *            currency symbol
 */
function displayPromotionalContentProducts(/*string*/ retailerId,/*string*/ siteBaseURL, /*boolean*/ loggedIn, /*string*/ currencySymbol){
  
  var load = false;
  
  var currentMarker = makePromoContentMarker(siteBaseURL, loggedIn),
      storedMarker = sessionStorage.getItem("promoContentMarker");
  
  //If nothing to show or marker changed
  if (!sessionStorage.getObject("targeters") || storedMarker !== currentMarker) {
	  load = true;
  }
	
  if(!load){
    displayInitialPromotionalContentProducts();  
  } else {
    retrievePromotionalContentProducts(retailerId,siteBaseURL,loggedIn,currencySymbol); 
  }
}

/**
 * This adds blank cells around the html content passed in
 * 
 * @param html
 *            products info in html format
 * @param doubleRearPadding
 *            flag to apply doubled padding
 * @return product info with paddings in html format
 */
function addPaddingCells(/*string*/ html, /*boolean*/ doubleRearPadding){
  
  var returnString = "<div class='cell'></div>" + html + "<div class='cell'></div>";
  if(doubleRearPadding){
    returnString = returnString + "<div class='cell'></div>";
  }
  
  return returnString;
}

/**
 * This allows us to get the results of a template operations
 * 
 * @param htmlContent
 *            html content like div
 * @return HTML Content as String
 */
function returnHTMLObjectAsString(/* HTML Object like a div */ htmlContent) {
  return $("<div />").append(htmlContent).html();
}

/**
 * Check if the number is even
 * 
 * @param number
 *            number to check
 */
function isEven(/*int*/ number){
  return number % 2 === 0;
}

/**
 * This creates the product item slider from the html passed in
 * 
 * @param htmlContent
 *            product's info content in html format
 */
function createProductItemSlider(/*string*/ htmlContent){
  var productSlider = Object.create(sliders,productItemSlider);
  $("#mobile_store_homeBottomSlotContent").empty().html(htmlContent);
  productSlider.createSlidePanel('#mobile_store_homeBottomSlotContent ',"cell", 3);
}

/**
 * This function initializes the home page.
 * 
 * First, stores the promotional content items in case we need to re-render them
 * based on orientation changes.
 * 
 * Then, we register the templates for product info json. Then we just to
 * display the product info from session storage, if present, or we down the
 * product json, store it, and display it
 * 
 * @param retailerId
 *            retailer Id
 * @param siteBaseURL
 *            URL of the site
 * @param loggedIn
 *            indicator if the user
 * @param currencySymbol
 *            currency symbol
 */
function loadHomePage(/* string */ retailerId,/* string */siteBaseURL, /*boolean*/ loggedIn, /*string*/ currencySymbol) {
  
  storeAndCreatePromotionalItemSlider();
  
  //We construct the template for each product cell
  var productInfoTemplateString = '\
  <div class="cell">\
    <a href="${linkUrl}">\
      <img alt="${name}" class="cellImage" src="${imageUrl}"/>\
      <p class="cellDetails">\
        <span>${name}</span>\
        {{if (prices.listPrice && prices.salePrice) }}<span class="saleListPrice">${prices.listPrice}</span><span class="salePrice"><B> ${prices.salePrice}</B></span>\
        {{else prices.listPrice}}<span class="listPrice"> ${prices.listPrice}</span>{{/if}}\
      </p>\
    </a>\
  </div>';
  $.template( 'productInfoTemplate', productInfoTemplateString);
  
  var productInfoRecTempateString = '\
  <div class="cell">\
    <a href="${$item.processURL(url)}">\
      <img alt="${name}" class="cellImage" src="${thumb_image_link}"/>\
      <p class="cellDetails">\
        <span>${name}</span>\
        <span class="listPrice">${$item.processPrice(price)}</span>\
      </p>\
    </a>\
  </div>';
  $.template( 'productInfoRecsTemplate', productInfoRecTempateString);

  var orientationSupport = "onorientationchange" in window, orientationEvent = orientationSupport ? "orientationchange" : "resize";
  
  //We add this resize event to re-render the parents/child so that things are centered correctly
  window.addEventListener(orientationEvent, function() {
    displayPromotionalContentItems();
    displayPromotionalContentProducts();
  }, false);

  displayPromotionalContentProducts(retailerId, siteBaseURL, loggedIn, currencySymbol);
}

/**
 * This attempts to get the rec cookies to use in our rec request
 * @return cookies object
 */
function getRecCookies(){
  var cookies = {};
  
  cookies.visitorId = getCookieByName("atgRecVisitorId");
  cookies.sessionId = getCookieByName("atgRecSessionId");
  
  return cookies;
}

/**
 * This function retrieves the product results from the targeters
 * 
 * @param retailerId
 *            retailer Id
 * @param siteBaseURL
 *            URL of the site
 * @param loggedIn
 *            indicator if the user
 * @param currencySymbol
 *            currency symbol
 */
function retrievePromotionalContentProducts(/* string */ retailerId,/*string*/siteBaseURL, /*boolean*/ loggedIn, /*string*/ currencySymbol){
  
    clearSessionStorage();
  
    $.getJSON(makeFullURL(siteBaseURL, 'promo/gadgets/homePromotionalProductsJSON.jsp'), function(data){
      
      var targeters = [];
      var emptyTargeter;
      
      $.each(data.targeterResults, function(index,collection){
        //store the targeter names to use as a key to retrieving them for storage
        targeters[index] = collection.targeter;
        
        if(collection.products.length === 0){
          //this assumes that if there are no results, that means that we should fill it with recs
          emptyTargeter = collection.targeter;
        } else{
          var padding = false;
          if(isEven(collection.products.length))
             padding = true;
          
          var html = addPaddingCells(returnHTMLObjectAsString($.tmpl('productInfoTemplate',collection.products)),padding);
          sessionStorage.setObject(collection.targeter,html);
        }
      });
      
      //If we have a targerer that needs to show recs results, lets get them now
      if(emptyTargeter){
        if(retailerId !== "" && retailerId !== null && retailerId !== undefined){
      	  var cookies = getRecCookies();
          if(cookies){
             var recUrl ="http://recs.atgsvcs.com/pr/recommendations/3.0/json/" + retailerId + "/"
             if(cookies.visitorId)
               recUrl = recUrl + cookies.visitorId;
             
             recUrl = recUrl + "?";
             
             if(cookies.sessionId){
            	 recUrl = recUrl + "sessionId=" + cookies.sessionId + "&"
             }
             
             //We want the thumbnail image, so we add slots.homeChildren.dataItems=thumb_image_link to URL
             recUrl = recUrl + "slots.homeChildren.numRecs=7&slots.homeChildren.dataItems=thumb_image_link&channel=mobile";
             
            $.ajax({
                  type: 'GET',
                  dataType: 'jsonp',
                  url: recUrl,
                  success: function(data) {
                    var padding = false;
                    if(isEven(data.slots.homeChildren.recs.length))
                       padding = true;
                    
                    var optionsRecs = {
                        processPrice: function(item){
                      if(currencySymbol == 'USD')
                        return "$" + item.toFixed(2);
                      if(currencySymbol == 'EUR') 
                        return item.toFixed(2) + '�';  
                      },
                        processURL: function(item){
                          return siteBaseURL + item;
                        }
                    };
                    
                    var html = addPaddingCells(returnHTMLObjectAsString($.tmpl('productInfoRecsTemplate', data.slots.homeChildren.recs,optionsRecs)),padding);
                    sessionStorage.setObject(emptyTargeter,html);
                  }});
          }
        } else{ // In this case, recs isn't installed, so for now lets just fill it in with data from CRS
        	$.getJSON(makeFullURL(siteBaseURL, 'promo/gadgets/homeNoRecsProductsJSON.jsp'), function(data){
        		$.each(data.targeterResults, function(index,collection){
              var padding = false;
              if(isEven(collection.products.length))
                 padding = true;
        	    var html = addPaddingCells(returnHTMLObjectAsString($.tmpl('productInfoTemplate',collection.products)),padding);
        	    sessionStorage.setObject(emptyTargeter,html);
        	  });
        	});
        }
      }
      
      sessionStorage.setItem("promoContentMarker", makePromoContentMarker(siteBaseURL, loggedIn));
      sessionStorage.setObject("targeters", targeters);
      displayInitialPromotionalContentProducts();
  });
}


/**
 * Function makes marker value Use to unique identify promotional content
 * 
 * @param siteBaseURL
 *            baseURL
 * @param loggedIn
 *            boolean value
 * @param userId
 *            user's ID
 * 
 * @return marker
 */
function makePromoContentMarker(/*string*/siteBaseURL, /*boolean*/ loggedIn) {
	var userId = getCookieByName("DYN_USER_ID");
	return marker = siteBaseURL + "|" + loggedIn + "|" + userId;
}

/**
 * Function makes URL for JSON excluding "undefined"-text
 * 
 * @param siteBaseURL
 *            baseURL
 * @param additionalPath
 *            additional path parameters
 * 
 * @return full URL-address
 */
function makeFullURL(/*string*/siteBaseURL, /*string*/additionalPath) {
	if (siteBaseURL!=undefined) {
		var index = siteBaseURL.indexOf(";jsessionid");
		if (index == -1) {
			return siteBaseURL + additionalPath;
		}
		else {
			var head = siteBaseURL.substr(0, index);
			var tail = siteBaseURL.substr(index);
			 
			return head + additionalPath + tail;
		}
	} else {
		return additionalPath;
	}
}