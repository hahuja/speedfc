/* Search Javascript functions
Version: $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/atg/search.js#3 $ $Change: 692002 $
*/

/**
 * Parses the specified URL or current page URL if url parameter is undefined
 * and returns a dictionary Object that maps decoded parameter names to decoded
 * values.
 * 
 * @param url
 *            the specified url with params to map; if undefined the current
 *            location is used
 * 
 * @return An object with keys and values where keys are the parameter's names
 *         from the url
 */
function getURLParams(url){

  if (!url) {
    url = window.location.href;
  }

  var params = decodeURI(url);
  var indexOfQuestionMark = params.indexOf('?');
  if (indexOfQuestionMark !== -1) { //if this is whole url, not search part
    params = params.slice(indexOfQuestionMark);
  }

  // A state machine to parse the param string in one pass
  var state = false;
  var key, value;
  key = value = '';
  var paramsObject = {};
  // the first character will always be '?', so we skip it
  for (var i = 1; i < params.length; i++){
    var current = params.charAt(i);
    switch (current){
      case '=':
        state = !state;
        break;
      case '&':
        state = !state;
        paramsObject[key] = value;
			    key = value = '';
        break;
      default:
        if (state){
	          value += current;
        }
        else{
          key += current;
        }
        break;
    }
  }
  if (key) paramsObject[key] = value;// put the last pair on the paramsObject
  return paramsObject;
}

/**
 * Returns the specified URL with the specified parameter added or changed. If
 * the url not specified the current page URL is used.
 * 
 * @param key
 *            the name of the parameter to set (any existing entry for that
 *            parameter will be replaced)
 * @param value
 *            the desired value for the parameter
 * @param url
 *            the specified url to add the parameter, if undefined the current
 *            page URL is used
 * 
 * @return an encoded url string
 */
function addURLParam(key, val, url){
  if (!url) {
    url = window.location.href;
  }
  var params = getURLParams(url);
  params[key] = val;
  var loc = url.slice(0,url.indexOf('?'));
  return encodeURI(loc) + "?" + $.param(params); //JQuery.param makes it to be encoded like the encodeURIComponent function does.
}

/**
 * Load the next page of results using simple search
 * 
 * @param caller
 *            context
 * @param num
 *            page number
 */
function loadPageSimple(caller,num){
  $("span", caller).text(RESOURCES['mobile.search.loading']);
  $.ajax({
    type: "GET",
    url: "../search/gadgets/moreResults.jsp",
    data: {'page':num},
    context: caller,
    success: function(results){
      $(this).replaceWith(results);
    }
  });
  return false;
}

/**
 * Load the next page of results using full search
 * 
 * @param caller
 *            context
 * @param num
 *            page number
 * @param token
 *            token
 */
function loadPageFull(caller,num,token){
  $("span", caller).text(RESOURCES['mobile.search.loading']);
  $.ajax({
    type: "GET",
    url: "../atgsearch/gadgets/searchResultsRenderer.jsp",
    data: {'pageNum':num, 'token':token},
    context: caller,
    success: function(results){
      $(this).replaceWith(results);
    }
  });
  return false;
}

/**
 * Toggle the refinements display
 * 
 * @param show
 *            true if we're trying to show the refinements display
 */
function toggleRefinements(show){
  $("#mobile_store_refine").toggleClass('hidden', !show);
}

/**
 * Initialize facets, adding any pre-selected facets to the global FacetTrail
 */
function initFacets(){
  var facetString = unescape(getURLParams().facetTrail);
  
  if (facetString){
    // split the string into an array of entries of the form '<facet>:<value>[|<value ...]'
    var trailEntries = facetString.match(/[A-Za-z0-9_]+:[A-Za-z0-9_\-\|\.\/]+/gi);

    if (trailEntries){
      for (var i = 0; i < trailEntries.length; i++){
    	  // split up the entry into the facet and its value(s)
    	  var matches = trailEntries[i].match(/[A-Za-z0-9_\-\|\.\/]+/gi);
    	  var facet = matches[0];
    	  var values = matches[1];
    	  values = values.match(/[A-Za-z0-9_\-\.]+/gi);
    	  // Identify the corresponding 'select' element and select the appropriate values
    	  $("#mobile_store_facets select[name='"+facet+"']").val(values).toggleClass('visible',true);
	    }
    }
  }
}

/**
 * Remove 'notLoaded' css class from img element if image is loaded
 * 
 * @param caller
 *            the DOM element that called our function
 */
function imgLoad(caller, imgUrl, imgName){
  $(caller).removeClass('notLoaded').removeAttr('onload');
  return false;
}

/**
 * Executes on facet's selection at the Refine page 
 * 
 * @param event
 */
function facetSelect(event){
  var $select = $(event.currentTarget);
  if ($select.val()){ // if nothing is select, val() will be null
    $select.toggleClass('visible', true);
  }
  else{
    $select.toggleClass('visible', false);
  }
  // Switch the 'Cancel' button to a 'Done' button
  $("#mobile_store_refine .mobile_store_buttonContainer button[name='cancel']")
    .attr('name','done')
    .text(RESOURCES['mobile.search.refine.done'])
	  .one('click',submitRefinements);
}

/**
 * Submit a refinement request and load the results via AJAX
 * 
 * @param event
 *            change event
 */
function submitRefinements(event){
  var $this = $(event.currentTarget);
  $this.text(RESOURCES['mobile.search.refine.cancel']).attr('name',"cancel"); // switch the 'Done' button back to 'Cancel

  var facetString = "";

  $("#mobile_store_facets select").each(function(){
    var $select = $(this);
    var selectedValues = $select.val();
    if (selectedValues){
      var facetSubstring = $select.attr('name')+":";
      for (var i = 0; i < selectedValues.length; i++){
        facetSubstring += selectedValues[i] + "|";
      }
      facetString += facetSubstring.slice(0,-1) + ":";
    }
  });
  
  facetString = facetString.slice(0,-1); // remove the trailing colon

  if (facetString.length === 0){
	  // if the user de-selects all facets, just resubmit the original search
    $("#mobile_store_searchForm").submit();
    return;
  }

  // Add this facet selection to the page history, so that navigating 'back' from a later page
  // will bring the user to the refined results rather than the original search
  history.replaceState({'facetTrail':facetString}, "Faceted Search", addURLParam('facetTrail',facetString));

  var resultsContainer = $("ul.searchResults");
  var searchToken = $("#mobile_store_refine input[name='token']").attr('value');
  var requestData = {'token':searchToken, 'facetTrail':facetString};

  // Show the "Refining..." message in the overlay
  var messageBox = $("#mobile_store_modalMessageBox");
  messageBox.html("<img id='mobile_store_spinner' src='/nrsdocroot/content/mobile/images/spinner.png' alt='Loading'/>").show().addClass("refineOverlay");
  var overlay = $("#mobile_store_modalOverlay");
  window.scrollTo(0,1); // important to scroll back to the top of the page
  overlay.show();

  // Submit the refinement request
  $.ajax({
    type: "GET",
    url: "../atgsearch/gadgets/searchResultsRenderer.jsp",
    data: requestData,
    success: function(results){
	    var countContainer = $(results).first('input[name="count"]');
	    var count = countContainer.attr('value');
	    countContainer.remove(); // no need to keep it in the DOM
	    resultsContainer.html(results); // display new results on the page
	    $("#mobile_store_refine_header #mobile_store_toggleSearchResults span").text(count); // update the count
      messageBox.html('').hide().removeClass("refineOverlay"); // clean up the messageBox
	    overlay.hide();
    }
  });
}