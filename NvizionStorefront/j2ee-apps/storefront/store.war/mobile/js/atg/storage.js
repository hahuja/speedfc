/**
 * This adds a setObject method to the Storage interface so that we can add our
 * HTML fragments into local storage.
 * 
 * We should be able to do this without doing this, according to the
 * localstorage spec, but no browser has added support for it yet.
 */
Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

/**
 * This adds a getObject method to the Storage interface so that we can get our
 * HTML fragments out of ocal storage.
 * 
 * We should be able to do this without doing this, according to the
 * localstorage spec, but no browser has added support for it yet.
 */
Storage.prototype.getObject = function(key) {
    return this.getItem(key) && JSON.parse(this.getItem(key));
};