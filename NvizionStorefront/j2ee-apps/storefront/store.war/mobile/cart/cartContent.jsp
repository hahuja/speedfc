<%--
  This page renders content of not empty shopping cart.

  Page includes:
    /mobile/checkout/gadgets/checkoutErrorMessages.jsp display checkout error messages
    /mobile/cart/gadgets/cartItems.jsp renderer of cart items
    /mobile/cart/gadgets/orderSummary.jsp renderer of order summary (payment info, shipping address, etc)

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/order/purchase/CartFormHandler"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>

  <div id="mobile_store_cartContainer">

    <dsp:form action="${pageContext.request.requestURI}" method="post" name="cartform" formid="cartform">
      <dsp:input type="hidden" bean="CartFormHandler.moveToPurchaseInfoSuccessURL"
                 value="${originatingRequest.contextPath}/mobile/checkout/shipping.jsp"/>
      <dsp:input type="hidden" bean="CartFormHandler.moveToPurchaseInfoErrorURL"
                 value="${originatingRequest.requestURI}"/>
      <dsp:input type="hidden" bean="CartFormHandler.updateSuccessURL"
                 value="${originatingRequest.requestURI}"/>
      <dsp:input type="hidden" bean="CartFormHandler.updateErrorURL"
                 value="${originatingRequest.requestURI}"/>
      <dsp:input bean="CartFormHandler.giftMessageUrl" type="hidden"
                 value="${originatingRequest.contextPath}/mobile/checkout/giftMessage.jsp"/>
      <dsp:input bean="CartFormHandler.shippingInfoURL" type="hidden"
                 value="${originatingRequest.contextPath}/mobile/checkout/shipping.jsp"/>
      <dsp:input bean="CartFormHandler.loginDuringCheckoutURL" type="hidden"
                 value="${originatingRequest.contextPath}/mobile/checkout/login.jsp"/>

      <div class="mobile_store_cart">
        <dsp:include page="/checkout/gadgets/checkoutErrorMessages.jsp">
          <dsp:param name="formHandler" bean="CartFormHandler"/>
          <dsp:param name="divid" value="mobile_store_formValidationError"/>
        </dsp:include>
        <dsp:include page="/checkout/gadgets/checkoutErrorMessages.jsp">
          <dsp:param name="formHandler" bean="CouponFormHandler"/>
          <dsp:param name="divid" value="mobile_store_formValidationError"/>
        </dsp:include>

          <%-- Shopping cart content --%>
        <div id="mobile_store_cartContent" class="mobile_store_main">
          <dsp:include page="gadgets/cartItems.jsp" flush="true">
            <dsp:param name="items" bean="ShoppingCart.current.commerceItems"/>
          </dsp:include>
        </div>

          <%-- Order summary --%>
        <dsp:include page="gadgets/orderSummary.jsp">
          <dsp:param name="order" bean="ShoppingCart.current"/>
        </dsp:include>

      </div>

      <%-- Action buttons --%>
      <div class="mobile_store_cartCheckout">
        <fmt:message var="checkoutText" key="common.button.checkoutText"/>
        <dsp:input id="mobile_store_cartCheckoutButton" class="mainActionBtn" type="submit" bean="CartFormHandler.checkout" value="${checkoutText}"/>
      </div>

    </dsp:form>

  </div>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/cart/cartContent.jsp#3 $$Change: 692002 $ --%>