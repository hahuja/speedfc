<%--
  Display price detail for each shopping cart item.

  Page includes:
    /global/gadgets/formattedPrice.jsp price formatter

  Required parameters:
    commerceItem
      the commerce item whose datailed price to show

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/pricing/UnitPriceDetailDroplet"/>

  <dsp:getvalueof var="listPrice" param="commerceItem.priceInfo.listPrice"/>

  <dsp:droplet name="UnitPriceDetailDroplet">
    <dsp:param name="item" param="commerceItem"/>
    <dsp:oparam name="output">

      <dsp:getvalueof var="unitPriceBeans" vartype="java.lang.Object" param="unitPriceBeans"/>
      <c:set var="priceBeansNumber" value="${fn:length(unitPriceBeans)}"/>

      <c:forEach var="unitPriceBean" items="${unitPriceBeans}" varStatus="unitPriceBeanStatus">

        <%-- Display commerce item quantity --%>

        <c:choose>

          <%-- First check to see if the item was discounted --%>
          <c:when test="${listPrice != unitPriceBean.unitPrice}">

            <%-- There's some discountin' going on --%>
            <dsp:include page="/global/gadgets/formattedPrice.jsp" flush="true">
              <dsp:param name="price" value="${unitPriceBean.unitPrice}"/>
              <dsp:param name="saveFormattedPrice" value="true"/>
            </dsp:include>            
            <span class="${fn:length(formattedPrice) > 6 ? 'longPrice' : ''}">
              <span class="mobile_store_quantity">
                <fmt:formatNumber value="${unitPriceBean.quantity}" type="number"/>
              </span>
              <span class="mobile_store_atRateOf">
                <fmt:message key="common.atRateOf"/>
              </span>
              <span class="mobile_store_newPrice">
                ${formattedPrice}
              </span>
              <br/>
              <span class="mobile_store_oldPrice">
                <fmt:message key="common.price.old"/>
                <del>
                  <dsp:include page="/global/gadgets/formattedPrice.jsp" flush="true">
                    <dsp:param name="price" value="${listPrice}"/>
                  </dsp:include>
                </del>
              </span>
            </span>
          </c:when>
          <c:otherwise>
          <%-- They match, no discounts --%>
          <dsp:include page="/global/gadgets/formattedPrice.jsp" flush="true">
            <dsp:param name="price" value="${listPrice}"/>
            <dsp:param name="saveFormattedPrice" value="true"/>
          </dsp:include>
          <span class="${fn:length(formattedPrice) > 7 ? 'longPrice' : ''}">
            <span class="mobile_store_quantity">
              <fmt:formatNumber value="${unitPriceBean.quantity}" type="number"/>
            </span>
            <span class="mobile_store_atRateOf">
              <fmt:message key="common.atRateOf"/>
            </span>
            <span class="mobile_store_newPrice">
              ${formattedPrice}
            </span>                          
          </span>
          </c:otherwise>
        </c:choose>
        <br/>
      </c:forEach>
    </dsp:oparam>
  </dsp:droplet><%-- End for unit price detail droplet --%>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/cart/gadgets/detailedItemPrice.jsp#3 $$Change: 692002 $--%>