<%--
  This page renders each item in the shopping cart.

  Form Condition:
    This gadget must be contained inside of a form.
    CartFormHandler must be invoked from a submit
    button in this form for fields in this page to be processed
    button in this form for fields in this page to be processed

  Page includes:
    /mobile/cart/gadgets/cartItemImg.jsp display product image
    /global/gadgets/productLinkGenerator.jsp product link generator
    /mobile/cart/gadgets/skuProperties.jsp renderer of sku properties
    /mobile/cart/gadgets/detailedItemPrice.jsp display price details

  Required parameters:
    currentItem
      the commerce item to render

  Optional parameters:
    None
--%>
<dsp:page>
  <div class="mobile_store_cartItem" id="mobile_store_cartItem_<dsp:valueof param='currentItem.id'/>">

    <div class="image">
      <dsp:include page="cartItemImg.jsp">
        <dsp:param name="commerceItem" param="currentItem"/>
      </dsp:include>
    </div>

    <div class="item">
      <p class="name">
        <%-- Link back to the product detail page --%>
        <dsp:include page="/global/gadgets/productLinkGenerator.jsp">
          <dsp:param name="product" param="currentItem.auxiliaryData.productRef"/>
          <dsp:param name="siteId" param="currentItem.auxiliaryData.siteId"/>
        </dsp:include>
        
        <dsp:getvalueof var="productName" param="currentItem.auxiliaryData.productRef.displayName"/>
        <input type="hidden" id="productName" value="${productName}"/>
        
        <c:choose>
          <c:when test="${not empty productUrl}">
            <%-- We need to readd selectedColor and selectedSize parameters as they will not be added by productLinkGenerator.jsp
            if search is not enabled. --%>
            <dsp:getvalueof var="skuType" vartype="java.lang.String" param="currentItem.auxiliaryData.catalogRef.type"/>
            <c:choose>
              <c:when test="${skuType == 'clothing-sku'}">
                <c:url var="pageurl" value="${productUrl}" context="/">
                  <c:param name="selectedSize">
                    <dsp:valueof param="currentItem.auxiliaryData.catalogRef.size"/>
                  </c:param>
                  <c:param name="selectedColor">
                    <dsp:valueof param="currentItem.auxiliaryData.catalogRef.color"/>
                  </c:param>
                </c:url>
              </c:when>
              <c:when test="${skuType == 'furniture-sku'}">
                <c:url var="pageurl" value="${productUrl}" context="/">
                  <c:param name="selectedColor">
                    <dsp:valueof param="currentItem.auxiliaryData.catalogRef.woodFinish"/>
                  </c:param>
                </c:url>
              </c:when>
              <c:otherwise>
                <%-- if none above, then single sku or multi-sku ? --%>
                <dsp:getvalueof var="childSKUs" param="currentItem.auxiliaryData.productRef.childSKUs"/>
                <c:choose>
                  <c:when test="${fn:length(childSKUs) > 1}">
                    <c:url var="pageurl" value="${productUrl}" context="/">
                      <c:param name="selectedFeature">
                        <%-- selected feature for multi-sku items is item's selected SKU itself --%>
                        <dsp:valueof param="currentItem.auxiliaryData.catalogRef.id"/>
                      </c:param>
                    </c:url>
                  </c:when>
                  <c:otherwise>
                    <c:url var="pageurl" value="${productUrl}" context="/"/>
                  </c:otherwise>
                </c:choose>
              </c:otherwise>
            </c:choose>

            <c:url var="productpageurl" value="${pageurl}" context="/">
              <c:param name="selectedQty">
                <dsp:valueof param="currentItem.quantity"/>
              </c:param>
              <c:param name="ciId">
                <dsp:valueof param="currentItem.id"/>
              </c:param>
            </c:url>

            <dsp:a href="${fn:escapeXml(productpageurl)}">
              <dsp:valueof param="currentItem.auxiliaryData.productRef.displayName">
                <fmt:message key="common.noDisplayName"/>
              </dsp:valueof>
            </dsp:a>
          </c:when>
          <c:otherwise>
            <dsp:valueof param="currentItem.auxiliaryData.productRef.displayName">
              <fmt:message key="common.noDisplayName"/>
            </dsp:valueof>
          </c:otherwise>
        </c:choose>
      </p>

      <p class="properties">
        <dsp:include page="skuProperties.jsp">
          <dsp:param name="sku" param="currentItem.auxiliaryData.catalogRef"/>
        </dsp:include>
      </p>
    </div>

    <div class="price">
      <div class="priceContent">
        <dsp:include page="detailedItemPrice.jsp">
          <dsp:param name="commerceItem" param="currentItem"/>
        </dsp:include>
      </div>
    </div>

  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/cart/gadgets/cartItem.jsp#4 $$Change: 692002 $--%>
