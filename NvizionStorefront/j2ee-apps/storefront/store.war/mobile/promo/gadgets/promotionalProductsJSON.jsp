<%--
  Renders promotional product details in JSON format

  Page includes:
    /global/gadgets/formattedPrice.jsp price formatter
    /global/gadgets/crossSiteLinkGenerator.jsp generator of cross-site links

  Required parameters:
    product
      product to be displayed
    productParams
      product parameters
--%>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/pricing/priceLists/PriceDroplet" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />

	<dsp:getvalueof var="myParams" param="productParams" />
	<dsp:getvalueof var="element" param="product" />

	<c:set var="product" value="${myParams.element}" />
	<json:object>
		<json:property name="name" value="${product.displayName}" />
		<dsp:getvalueof var="productImageUrl" value="${product.thumbnailImage.url}"/>	
			
		<c:choose>
			<c:when test="${not empty productImageUrl}">
				<json:property name="imageUrl" value="${productImageUrl}" />
			</c:when>
			<c:otherwise>
				<json:property name="imageUrl" value="/nrsdocroot/content/images/products/thumb/MissingProduct_thumb.jpg" />
			</c:otherwise>
		</c:choose>
		
		<dsp:include page="/global/gadgets/crossSiteLinkGenerator.jsp">
			<dsp:param name="product" param="element" />
		</dsp:include>
		<json:property name="linkUrl">
			<c:url value="${siteLinkUrl}" context="/">
				<c:param name="productId" value="${product.repositoryId}" />
			</c:url>
		</json:property>
		<dsp:droplet name="PriceDroplet">
			<dsp:param name="product" param="element" />
			<dsp:param name="sku" param="element.childSKUs[0]" />
			<dsp:oparam name="output">
				<dsp:setvalue param="theListPrice" paramvalue="price" />
				<dsp:getvalueof var="listPrice" vartype="java.lang.Double"
					param="theListPrice.listPrice" />
				<%-- Search for the sale price, if sale price list is defined for the current user. --%>
				<c:choose>
					<c:when
						test="${not empty nucleus['/atg/userprofiling/Profile'].salePriceList}">
						<%-- Lookup the sale price. --%>
						<dsp:droplet name="PriceDroplet">
							<dsp:param name="priceList" bean="Profile.salePriceList" />
							<dsp:oparam name="output">
								<%-- Sale price found, display both list and sale prices. --%>
								<dsp:getvalueof var="salePrice" vartype="java.lang.Double"
									param="price.listPrice" />
								<json:object name="prices">
									<json:property name="salePrice">
										<dsp:include page="/global/gadgets/formattedPrice.jsp">
											<dsp:param name="price" value="${salePrice}" />
										</dsp:include>
									</json:property>
									<json:property name="listPrice">
										<dsp:include page="/global/gadgets/formattedPrice.jsp">
											<dsp:param name="price" value="${listPrice}" />
										</dsp:include>
									</json:property>
								</json:object>
							</dsp:oparam>
							<dsp:oparam name="empty">
								<%-- Can't find sale price, display list price only. --%>
								<json:object name="prices">
									<json:property name="listPrice">
										<dsp:include page="/global/gadgets/formattedPrice.jsp">
											<dsp:param name="price" value="${listPrice}" />
										</dsp:include>
									</json:property>
								</json:object>
							</dsp:oparam>
						</dsp:droplet>
						<%-- End price droplet on sale price --%>
					</c:when>
					<c:otherwise>
						<%-- No sale price list defined for the current user, display list price only. --%>
						<json:object name="prices">
							<json:property name="listPrice">
								<dsp:include page="/global/gadgets/formattedPrice.jsp">
									<dsp:param name="price" value="${listPrice}" />
								</dsp:include>
							</json:property>
						</json:object>
					</c:otherwise>
				</c:choose>
				<%-- End Is Empty Check --%>
			</dsp:oparam>
		</dsp:droplet>
		<%-- End Price Droplet --%>
	</json:object>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/promo/gadgets/promotionalProductsJSON.jsp#3 $$Change: 692002 $--%>
