<%--
  Sets properties for specific targeter and render product results

  Page includes:
    /mobile/promo/gadgets/promotionalProductsJSON.jsp renderer of product details

  Required parameters:
    targeter
      used targeter

  Optional parametersL
    None
--%>
<dsp:page>
	<dsp:importbean bean="/atg/targeting/TargetingForEach" />
	<dsp:importbean bean="/atg/commerce/pricing/priceLists/PriceDroplet" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />

	<dsp:getvalueof var="mobileStorePrefix"
		bean="/atg/store/StoreConfiguration.mobileStorePrefix" />
	<dsp:getvalueof var="targeter" param="targeterPath" />
	<c:if test="${not empty targeter}">
		<json:object>
			<json:property name="targeter" value="${targeter}" />
			<json:array name="products">
				<dsp:droplet name="TargetingForEach" var="myParams">
					<dsp:param bean="${targeter}" name="targeter" />
					<dsp:param name="fireViewItemEvent" value="false" />
					<dsp:oparam name="output">
						<dsp:include
							page="${mobileStorePrefix}/promo/gadgets/promotionalProductsJSON.jsp">
							<dsp:param name="product" param="element" />
							<dsp:param name="productParams" converter="map"
								value="${myParams}" />
						</dsp:include>
					</dsp:oparam>
				</dsp:droplet>
			</json:array>
		</json:object>
	</c:if>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/promo/gadgets/promotionalTargeterJSON.jsp#3 $$Change: 692002 $--%>
