<%--
  This page is used for page ending together with pageStart.jsp.

  Required Parameters:
    None

  Optional Parameters:
    None
--%>
<%@ include file="/includes/taglibs.jspf" %>
<!-- page end -->
<dsp:page>
    </body>
  </html>
</dsp:page>
<!-- page end -->
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/includes/pageEnd.jsp#1 $ $Change: 683854 $--%>