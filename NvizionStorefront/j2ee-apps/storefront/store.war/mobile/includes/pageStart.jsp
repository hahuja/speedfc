<%--
  This page is used for other pages starting together with pageEnd.jsp.
  It is included by the mobile page container custom tag

  Page includes:
    /includes/taglibs.jspf connects required tag libs

  Required parameters:
    None

  Optional parameters:
    None
--%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- 
  JSP 2.1 parameter. With trimDirectiveWhitespaces enabled,
  template text containing only blank lines, or white space,
  is removed from the response output.
  
  trimDirectiveWhitespaces doesn't remove all white spaces in a
  HTML page, it is only supposed to remove the blank lines left behind
  by JSP directives (as described here 
  http://java.sun.com/developer/technicalArticles/J2EE/jsp_21/ ) 
  when the HTML is rendered. 
 --%>
<%@page trimDirectiveWhitespaces="true"%>

<%@ include file="/includes/taglibs.jspf" %>
<dsp:page>

  <dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath"/>
  <dsp:getvalueof var="mobileStorePrefix" bean="/atg/store/StoreConfiguration.mobileStorePrefix" scope="request"/>
  <dsp:getvalueof var="contextPrefix" value="${contextPath}${mobileStorePrefix}" scope="request"/>
  <dsp:getvalueof var="language" bean="/OriginatingRequest.requestLocale.locale.language"/>
  <c:if test="${empty language}">
    <dsp:getvalueof var="language" bean="/OriginatingRequest.locale.language"/>
  </c:if>
  
  <!DOCTYPE HTML>
  <html lang="${language}">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="format-detection" content="telephone=no"/>
      <link rel="stylesheet" href="${contextPrefix}/css/mobile.css" type="text/css" media="screen" title="no title"/>
      <link rel="icon" type="image/png" href="/nrsdocroot/content/favicon.ico"/>
      <link rel="apple-touch-icon" href="/nrsdocroot/content/mobile/images/apple-touch-icon.png"/>
      <script type="text/javascript" src="${contextPrefix}/js/jquery/jquery-1.5.1.min.js"></script>
      <script type="text/javascript" src="${contextPrefix}/js/jquery/jquery-templates-min.js"></script>
      <script type="text/javascript" src="${contextPrefix}/js/resources/resources_${language}.js"></script>
      <script type="text/javascript" src="${contextPrefix}/js/atg/mobile.js"></script>
      <title><crs:outMessage key="common.storeName"/><fmt:message key="common.textSeparator"/>&nbsp;<dsp:valueof param="titleString"/></title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    </head>
    <body>
</dsp:page>
<!-- page start-->
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/includes/pageStart.jsp#3 $ $Change: 692002 $--%>
