<%--
  This page renders the Checkout Defaults

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/store/profile/ProfileCheckoutPreferences"/>
  <dsp:importbean bean="/atg/store/profile/RegistrationFormHandler"/>

  <%-- Get localized shipping method name --%>
  <dsp:getvalueof var="defaultCarrier" vartype="java.lang.String" bean="Profile.defaultCarrier">
    <c:set var="defaultShippingMethod">
      <c:choose>
        <c:when test="${not empty defaultCarrier}">
          <fmt:message key="checkout_shipping.delivery${fn:replace(defaultCarrier, ' ', '')}"/>
        </c:when>
        <c:otherwise>
          <fmt:message key="common.notSpecified"/>
        </c:otherwise>
      </c:choose>
    </c:set>
  </dsp:getvalueof>
  
  <%-- Get shipping address --%>
  <dsp:getvalueof var="profileShippingAddress" bean="Profile.shippingAddress"/>
  <c:set var="defaultShippingAddress">
    <c:choose>
      <c:when test="${not empty profileShippingAddress}">
        <dsp:valueof bean="ProfileCheckoutPreferences.defaultShippingAddressNickname"/>
      </c:when>
      <c:otherwise>
        <fmt:message key="common.notSpecified"/>
      </c:otherwise>
    </c:choose>
  </c:set>
  
  <%-- Get default credit card data --%>
  <dsp:getvalueof var="profileDefaultCreditCard" bean="Profile.defaultCreditCard"/>
  <c:set var="defaultCreditCard">
    <c:choose>
      <c:when test="${not empty profileDefaultCreditCard}">
        <dsp:valueof bean="ProfileCheckoutPreferences.defaultCreditCardNickname"/>
        <fmt:message key="common.textSeparator"/>
        <dsp:getvalueof var="creditCardNumber" bean="Profile.defaultCreditCard.creditCardNumber" />
        <c:set var="creditCardNumberLength" value="${fn:length(creditCardNumber)}"/>
        <c:out value="${fn:substring(creditCardNumber, creditCardNumberLength-4, creditCardNumberLength)}"/>
      </c:when>
      <c:otherwise>
        <fmt:message key="common.notSpecified"/>
      </c:otherwise>
    </c:choose>
  </c:set>
  
  <fmt:message key="common.expressCheckoutPreferences" var="checkoutPreferencesText"/>
  <crs:mobilePageContainer titleString="${checkoutPreferencesText}">
		<jsp:body>
      <dsp:include page="./gadgets/subheaderAccounts.jsp" >
        <dsp:param name="centerText" value="${checkoutPreferencesText}" />
        <dsp:param name="highlight" value="center" />
      </dsp:include>
      <div class="mobile_store_checkoutDefaults">
        <ul class="mobile_store_mobileList checkout_defaults">
          <li>
            <div class="content">
              <dsp:a id="defaultShippingMethod" page="/mobile/myaccount/profilePickDefaultShippingMethod.jsp">
                <dl>
                  <dt><fmt:message key="common.defaultShippingMethod"/></dt>
                  <dd><c:out value="${defaultShippingMethod}"/></dd>
                </dl>
              </dsp:a>
            </div>
          </li>
          
          <li>
            <div class="content">
              <dsp:a id="defaultShippingAddress" page="/mobile/myaccount/profilePickDefaultShippingAddress.jsp">
                <dl>
                  <dt><fmt:message key="common.defaultShippingAddress"/></dt>
                  <dd><c:out value="${defaultShippingAddress}"/></dd>
                </dl>
              </dsp:a>
            </div>
          </li>
          
          <li>
            <div class="content">
              <dsp:a id="defaultCreditCard" page="/mobile/myaccount/profilePickDefaultCreditCard.jsp">
                <dl>
                  <dt><fmt:message key="common.defaultCreditCard"/></dt>
                  <dd><c:out value="${defaultCreditCard}"/></dd>
                </dl>
              </dsp:a>
            </div>
          </li>
        </ul>
      </div>
      
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/profileCheckOutPrefs.jsp#3 $$Change: 692002 $ --%>
