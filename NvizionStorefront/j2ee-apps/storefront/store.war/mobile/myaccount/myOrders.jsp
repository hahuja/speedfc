<%--
  This page renders the My Orders page.

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/global/gadgets/obtainHardgoodShippingGroup.jsp return hardgoodShippingGroups counter,
                                                                  hardgoodShippingGroup object,
                                                                  giftWrapCommerceItem
    /global/util/orderState.jsp display order state
    /mobile/includes/crsRedirect.jspf renderer of the redirect prompt to the full CRS site

  Required parameters:
    None

  Optional parameters:
    redirectOrderId
      when not empty the redirect to the full CRS site dialog appeared.
    hideOrderList
      when true, order list is hidden during the redirect dialog appeared. Used from confirm page.
    redirectUrl
      used in the modal dialog as redirection to the full CRS site link.
      If not defined, the redirection link points to the full CRS order detail page.
      Should not to contain the orderId parameter, use the redirectOrderId for that purpose.
--%>

<dsp:page>

  <dsp:importbean bean="/atg/commerce/order/OrderLookup"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/core/i18n/LocaleTools"/>


  <fmt:message key="mobile.myaccount_orders.title" var="pageTitle"/>
  <dsp:getvalueof param="redirectOrderId" var="redirectOrderId"/>

  <crs:mobilePageContainer titleString="${pageTitle}" displayModal="${empty redirectOrderId ? false : true}">
    <jsp:attribute name="modalContent">
      <%-- If redirectOrderId is not empty then this page accessed
           from the orderDetail.jsp or
           from the confirm.jsp pages
           with order that does not supported in the mobile CRS --%>
      <%-- Order with multishipping group or with gift items handling and dialog template --%>
      <%-- Redirect dialog for full CRS --%>
      <div id="mobile_store_modalMessageBox" style="${empty redirectOrderId ? '' : 'display: block'}">
        <div id="mobile_store_modalInclude">
          <dsp:getvalueof param="redirectUrl" var="successURL"/>
          <c:if test="${empty successURL}">
            <c:set var="successURL" value="/myaccount/orderDetail.jsp"/>
          </c:if>
          <fmt:message var="topString" key="mobile.checkout_multishipping.information"/>
          <fmt:message var="bottomString" key="mobile.checkout_multishipping.redirectText"/>
          <dsp:getvalueof var="orderId" param="orderId"/>
          <%@include file="/mobile/includes/crsRedirect.jspf" %>
        </div>
      </div>  
    </jsp:attribute>
    <jsp:body>

      <%-- header --%>
      <fmt:message var="myOrders" key="mobile.myaccount_orders.title"/>
      <dsp:include page="./gadgets/subheaderAccounts.jsp">
        <dsp:param name="centerText" value="${myOrders}"/>
        <dsp:param name="highlight" value="center"/>
      </dsp:include>

      <%-- body --%>
      <div class="mobile_store_displayList">
        <div class="my_orders">
          <form id="loadOrderDetailForm" action="orderDetail.jsp">

            <input type="hidden" name="orderId" id="orderId"/>

            <dsp:droplet name="OrderLookup">
              <dsp:param name="userId" bean="Profile.id"/>
              <dsp:param name="sortBy" value="submittedDate"/>
              <dsp:param name="state" value="closed"/>
              <dsp:param name="numOrders" value="-1"/>
              <dsp:oparam name="output">

                <dsp:getvalueof var="orders" param="result"/>
                <ul class="mobile_store_mobileList my_orders">
                  <c:forEach items="${orders}" var="order" varStatus="status">
                    <dsp:include page="/mobile/global/gadgets/obtainHardgoodShippingGroup.jsp">
                      <dsp:param name="order" value="${order}"/>
                    </dsp:include>
                    <c:choose>
                      <c:when test="${(requestScope.hardgoodShippingGroups != 1) or not empty requestScope.giftWrapCommerceItem}">
                        <c:set var="orderSelectFunction" value="toggleRedirectWithOrderId('${order.id}');"/>
                      </c:when>
                      <c:otherwise>
                        <c:set var="orderSelectFunction" value="loadOrderDetails('${order.id}');"/>
                      </c:otherwise>
                    </c:choose>
                    <li class="withArrow">
                      <a href="javascript:void(0)" onclick="${orderSelectFunction}">
                        <div class="content">
                          <ul>
                            <li>
                          <span class="orderId">
                            <c:choose>
                              <c:when test="${not empty order.omsOrderId}">
                                <c:out value="${order.omsOrderId}"/>
                              </c:when>
                              <c:otherwise>
                                <c:out value="${order.id}"/>
                              </c:otherwise>
                            </c:choose>
                          </span>
                            </li>
                            <li>
                              <span class="block mobile_store_formFieldMajorText"><fmt:message
                                      key="common.items"/></span>
                            </li>
                            <li>
                          <span class="block">
                            <c:choose>
                              <c:when test="${order.containsGiftWrap}">
                                <c:out value="${order.totalCommerceItemCount - 1}"/>
                              </c:when>
                              <c:otherwise>
                                <c:out value="${order.totalCommerceItemCount}"/>
                              </c:otherwise>
                            </c:choose>
                            <fmt:message key="common.items"/>
                          </span>
                            </li>
                            <li>
                              <span class="block mobile_store_formFieldMajorText"><fmt:message
                                      key="mobile.myaccount_orders.orderPlaced"/></span>
                            </li>
                            <li>
                          <span class="block">
                            <dsp:getvalueof var="submittedDate" vartype="java.util.Date" param="order.submittedDate"/>
                            <dsp:getvalueof var="dateFormat"
                                            bean="LocaleTools.userFormattingLocaleHelper.datePatterns.shortWith4DigitYear"/>

                            <fmt:formatDate value="${order.submittedDate}" pattern="${dateFormat}"/>
                          </span>
                            </li>
                            <li>
                              <span class="block mobile_store_formFieldMajorText"><fmt:message
                                      key="common.status"/></span>
                            </li>
                            <li>
                          <span class="block">
                            <dsp:include page="/global/util/orderState.jsp">
                              <dsp:param name="order" value="${order}"/>
                            </dsp:include>
                          </span>
                            </li>
                          </ul>
                        </div>
                      </a>
                    </li>
                  </c:forEach>
                </ul>
              </dsp:oparam>
              <dsp:oparam name="empty">
                <crs:messageContainer optionalClass="atg_store_myOrdersEmpty" titleKey="myaccount_myOrders.noOrders"/>
              </dsp:oparam>
            </dsp:droplet>
          </form>

        </div>
      </div>


      <%-- Hide order list when the hideOrderList is set. See also confirm.jsp page. --%>
      <dsp:getvalueof param="hideOrderList" var="hideOrderList"/>
      <c:if test="${hideOrderList}">
        <script type="text/javascript">
          $(document).ready(function() {
            $('div.my_orders').hide();
            $('#mobile_store_modalOverlay').bind('click', function() {
              $('div.my_orders').show();
            });
          });
        </script>
      </c:if>

    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/myOrders.jsp#3 $$Change: 692002 $--%>
