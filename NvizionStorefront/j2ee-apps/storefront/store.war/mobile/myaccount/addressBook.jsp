<%--
  This page renders a list of addresses saved to the user's profile
  and also provides an option to create a new address.

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/global/util/displayAddress.jsp renderer of address info

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst"/>
  <dsp:importbean bean="/atg/userprofiling/B2CProfileFormHandler"/>

  <dsp:importbean bean="/atg/commerce/collections/filter/droplet/GiftlistSiteFilterDroplet"/>
  <dsp:importbean bean="/atg/commerce/gifts/GiftlistLookupDroplet"/>
  <dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>

  <fmt:message var="title" key="myaccount_addressBook.title"/>
  <crs:mobilePageContainer titleString="${title}">

    <%-- 
      Here, we retrieve a list of all of the addresses used in gift lists.  We do this 
      because we must display a modal popup which links to the full CRS site when a user
      attempts to delete an address used in a gift list (desired functionality).  First,
      we create a list of gift-list addresses. Later in the jsp, we compare each of the
      user's addresses against the list to determine if it is used.  If it is, we set
      deletable to 'false' and pass it to accountAddressEdit.jsp.  Otherwise, we set deletable
      to 'true'. 
    --%>

    <%-- Start get all gift-list addresses --%>

    <jsp:useBean id="giftLists" class="java.util.HashMap"/>
    <%-- Get all gift-lists --%>
    <dsp:droplet name="GiftlistSiteFilterDroplet">
      <dsp:param name="collection" bean="/atg/userprofiling/Profile.giftlists"/>
      <dsp:oparam name="output">
        <dsp:getvalueof var="giftlists" param="filteredCollection"/>
        <c:forEach var="giftlist" items="${giftlists}" varStatus="giftlistStatus">
          <c:set var="giftlistId" value="${giftlist.repositoryId}"/>

          <%-- Use GiftlistLookupDroplet (ItemLookupDroplet) to retrieve each gift-list --%>
          <dsp:droplet name="GiftlistLookupDroplet">
            <dsp:param name="id" value="${giftlistId}"/>
            <dsp:oparam name="output">
              <%-- Set current giftlist in the GiftlistFormHandler --%>
              <dsp:setvalue bean="GiftlistFormHandler.giftlist" paramvalue="element"/>
            </dsp:oparam>
          </dsp:droplet>

          <%-- Use GiftlistFormHandler to provide shippingAddressId used in gift-list --%>
          <dsp:getvalueof var="currentGiftlistShippingId" bean="GiftlistFormHandler.shippingAddressId"/>
          <c:set target="${giftLists}" property="${currentGiftlistShippingId}" value="address"/>
        </c:forEach>
      </dsp:oparam>
    </dsp:droplet>
    <%-- End get all gift-list addresses --%>

    <%-- Display subheader --%>
    <fmt:message var="title" key="myaccount_addressBook.title"/>
    <dsp:include page="./gadgets/subheaderAccounts.jsp">
      <dsp:param name="centerText" value="${title}"/>
      <dsp:param name="highlight" value="center"/>
    </dsp:include>

    <div class="mobile_store_displayList">
      <ul class="mobile_store_mobileList withCheckBoxes" role="presentation">
        <%--
          Iterate through all this user's shipping addresses, sorting the array so that the
          default shipping address is first.

          Input parameters:
            defaultId
              repository Id of item that will be the first in the array
            map
              Map of repository items that will be converted into array
            sortByKeys
              returning array will be sorted by keys (address nicknames)

          Output parameters:
            sortedArray
              array of sorted profile addresses
        --%>

        <%-- Get Profile's default address --%>
        <dsp:getvalueof var="defaultAddress" bean="Profile.shippingAddress"/>

        <dsp:droplet name="MapToArrayDefaultFirst">
          <dsp:param name="defaultId" value="${defaultAddress.repositoryId}"/>
          <dsp:param name="map" bean="Profile.secondaryAddresses"/>
          <dsp:param name="sortByKeys" value="true"/>

          <dsp:oparam name="empty">
            <%-- Do nothing when empty --%>
          </dsp:oparam>

          <dsp:oparam name="output">
            <dsp:getvalueof var="sortedArray" vartype="java.lang.Object" param="sortedArray"/>

            <%-- Iterate through each address --%>
            <c:forEach var="shippingAddress" items="${sortedArray}" varStatus="status">

              <%-- Get the shipping address Id of each address --%>
              <c:set var="addressIdTrim" value="${shippingAddress.value.repositoryId}"/>

              <%-- Set deletable by checking if addressIdTrim is in our map of
             gift-list addresses--%>
              <c:choose>
                <c:when test="${empty giftLists[addressIdTrim]}">
                  <c:set var="deletable" value="true"/>
                </c:when>
                <c:otherwise>
                  <c:set var="deletable" value="false"/>
                </c:otherwise>
              </c:choose>

              <%-- Link to edit page and provide address information --%>
              <dsp:setvalue param="shippingAddress" value="${shippingAddress}"/>
              <c:url value="accountAddressEdit.jsp" var="editURL" scope="page">
                <c:param name="addEditMode" value="edit"/>
                <c:param name="deleteKey" value="${shippingAddress.key}"/>
                <c:param name="deletable" value="${deletable}"/>
              </c:url>
              <c:set var="isDefaultAddress" value="${status.index eq 0 && not empty defaultAddress}"/>
              <li class="mobile_store_borderRightRemove mobile_store_borderPaddingLeft withBlueArrowBig ${isDefaultAddress ? 'checked' : ''}">
                <div class="content">
                  <div class="mobile_store_addressName">
                    <dsp:valueof param="shippingAddress.key"/>
                    <c:if test="${isDefaultAddress}">
                      <span class="defaultLabel"><fmt:message key="common.default"/></span>
                    </c:if>
                  </div>
                  <dsp:include page="/mobile/global/util/displayAddress.jsp">
                    <dsp:param name="address" value="${shippingAddress.value}"/>
                    <dsp:param name="private" value="false"/>
                  </dsp:include>
                </div>
                <fmt:message var="addressEditTitle" key="myaccount_accountAddressEdit.title"/>
                <dsp:a href="${editURL}" bean="B2CProfileFormHandler.editAddress"
                       paramvalue="shippingAddress.key" title="${addressEditTitle}">
                </dsp:a>
              </li>
            </c:forEach>
          </dsp:oparam>
        </dsp:droplet>


        <%-- Display Create Address --%>
        <li class="mobile_store_borderPaddingLeft withArrow">
          <div class="content">
            <dsp:a page="/mobile/myaccount/accountAddressEdit.jsp" class="block">
              <dsp:param name="addEditMode" value="add"/>
              <fmt:message key="checkout_shippingAddresses.createShippingAddress"/>
            </dsp:a>
          </div>
        </li>
      </ul>
    </div>
    <br/>
    <br/>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/addressBook.jsp#3 $$Change: 692002 $--%>
