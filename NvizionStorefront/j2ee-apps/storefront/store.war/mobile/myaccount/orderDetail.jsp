<%--
  This page renders the Order details

  Page includes:
    /mobile/global/gadgets/obtainHardgoodShippingGroup.jsp return hardgoodShippingGroups counter,
                                                                  hardgoodShippingGroup object,
                                                                  giftWrapCommerceItem
    /mobile/myaccount/myOrders.jsp list of all orders (used to display message if order is multi-shipped)
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/global/gadgets/orderSummary.jsp renderer of order summary

  Required parameters:
    orderId
      order id
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/order/OrderLookup"/>

  <dsp:droplet name="OrderLookup">
    <dsp:param name="orderId" param="orderId"/>
    <dsp:oparam name="error"><%-- TODO: Error page --%>
      <div class="atg_store_main atg_store_myAccount atg_store_myOrderDetail">
        <dsp:valueof param="errorMsg"/>
      </div>
    </dsp:oparam>
    <dsp:oparam name="output">
      <%-- Retrieve the price lists's locale used for order. It will be used to format prices correctly. 
           We can't use Profile's price list here as already submitted order can be priced with price
           list different from current profile's price list.
      --%>
      <dsp:getvalueof var="commerceItems" vartype="java.lang.Object" param="result.commerceItems"/>     
      <c:if test="${not empty commerceItems}">
        <dsp:getvalueof var="priceListLocale" vartype="java.lang.String" param="result.commerceItems[0].priceInfo.priceList.locale"/>
      </c:if>

      <dsp:include page="/mobile/global/gadgets/obtainHardgoodShippingGroup.jsp" flush="false">
        <dsp:param name="order" param="result"/>
      </dsp:include>
      <c:choose>
        <c:when test="${(hardgoodShippingGroups != 1) or not empty giftWrapCommerceItem}">
          <dsp:include page="/mobile/myaccount/myOrders.jsp">
            <dsp:param name="redirectOrderId" param="orderId"/>
            <dsp:param name="priceListLocale" value="${priceListLocale}"/>
          </dsp:include>
        </c:when>
        <c:otherwise>
          <fmt:message key="mobile.myaccount_orders.title" var="myOrders"/>
          <crs:mobilePageContainer titleString="${myOrders}">
            <jsp:body>
              <c:set var="orderDetailText">
                <fmt:message key="common.order"/><fmt:message key="common.labelSeparator"/><dsp:valueof param="orderId"/>
              </c:set>
              <dsp:include page="/mobile/myaccount/gadgets/subheaderAccounts.jsp">
                <dsp:param name="centerText" value="${myOrders}"/>
                <dsp:param name="centerURL" value="/mobile/myaccount/myOrders.jsp"/>
                <dsp:param name="rightText" value="${orderDetailText}"/>
                <dsp:param name="highlight" value="right"/>
              </dsp:include>
              <div id="mobile_store_orderDetailContainer" class="mobile_store_orderDetails">
              <form action="#">
                <dsp:include page="../global/gadgets/orderSummary.jsp" flush="true">
                  <dsp:param name="order" param="result"/>
                  <dsp:param name="hardgoodShippingGroup" value="${requestScope.hardgoodShippingGroup}"/>
                  <dsp:param name="priceListLocale" value="${priceListLocale}"/>
                  <dsp:param name="isCheckout" value="false"/>
                </dsp:include>
              </form>
              </div>
            </jsp:body>
          </crs:mobilePageContainer>
        </c:otherwise>
      </c:choose>
    </dsp:oparam>
  </dsp:droplet>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/orderDetail.jsp#4 $$Change: 692002 $--%>
