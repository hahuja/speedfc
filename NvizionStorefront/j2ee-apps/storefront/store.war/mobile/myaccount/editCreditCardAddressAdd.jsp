<%--
  This page fragment renders create new billing address page for user when he edits existing credit card

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/myaccount/gadgets/addressAddEdit.jsp renderer of address form (except Nickname field)

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/B2CProfileFormHandler"/>

  <fmt:message key="myaccount_myAccountMenu.paymentInfo" var="paymentInfoTitle"/>
  <fmt:message key="mobile.myaccount_payment_subHeader.editCard" var="pageTitle"/>
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <dsp:include page="./gadgets/subheaderAccounts.jsp">
        <dsp:param name="centerText" value="${paymentInfoTitle}"/>
        <dsp:param name="centerURL" value="/mobile/myaccount/paymentInfo.jsp"/>
        <dsp:param name="rightText" value="${pageTitle}"/>
        <dsp:param name="highlight" value="right"/>
      </dsp:include>

      <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="B2CProfileFormHandler.formExceptions"/>

      <%-- Handle form exceptions --%>
      <jsp:useBean id="errorMap" class="java.util.HashMap"/>
      <c:if test="${not empty formExceptions}">
        <c:forEach var="formException" items="${formExceptions}">
          <dsp:param name="formException" value="${formException}"/>
          <%-- Check the error message code to see what we should do --%>
          <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>

          <c:choose>
            <c:when test="${'stateIsIncorrect' eq errorCode}">
              <c:set target="${errorMap}" property="state" value="invalid"/>
              <c:set target="${errorMap}" property="country" value="invalid"/>
            </c:when>
            <c:when test="${'missingRequiredValue' eq errorCode}">
              <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
              <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
            </c:when>
          </c:choose>
        </c:forEach>
      </c:if>

      <div class="mobile_store_displayList" id="mobile_store_newBillingAddress">
        <div class="mobile_store_register">
          <dsp:form id="mobile_store_paymentInfoAddNewBillingAddressForm" formid="mobile_store_paymentInfoAddNewBillingAddressForm"
                    action="${originatingRequest.requestURI}" method="post">

            <dsp:input type="hidden" bean="B2CProfileFormHandler.newAddressSuccessURL" value="editCreditCard.jsp?mode=restore"/>
            <dsp:input type="hidden" bean="B2CProfileFormHandler.newAddressErrorURL" value="editCreditCardAddressAdd.jsp?preFillValues=true"/>

            <ul class="mobile_store_mobileList">
              <%-- Start inclusion of addressAddEdit for rendering parameters of the form --%>
              <dsp:include page="/mobile/myaccount/gadgets/addressAddEdit.jsp">
                <dsp:param name="formHandlerComponent" value="/atg/userprofiling/B2CProfileFormHandler.editValue"/>
                <dsp:param name="restrictionDroplet" value="/atg/store/droplet/ShippingRestrictionsDroplet"/>
                <dsp:param name="errorMap" value="${errorMap}"/>
              </dsp:include>
            </ul>
            <div class="mobile_store_formActions">
              <span class="mobile_store_basicButton">
                <fmt:message var="shipToButtonText" key="mobile.common.done"/>
                <dsp:input id="mobile_store_createShippingAddress" bean="B2CProfileFormHandler.newAddress"
                           iclass="mainActionBtn" type="submit" value="${shipToButtonText}"/>
              </span>
            </div>
          </dsp:form>
        </div>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/editCreditCardAddressAdd.jsp#4 $$Change: 692002 $--%>
