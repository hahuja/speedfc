<%--
  This page fragment renders page to create new card

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/myaccount/gadgets/creditCardAddForm.jsp renderer of credit card add form

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>

  <fmt:message key="myaccount_myAccountMenu.paymentInfo" var="paymentInfoTitle"/>
  <fmt:message key="mobile.myaccount_payment_subHeader.addCard" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <dsp:include page="./gadgets/subheaderAccounts.jsp" >
        <dsp:param name="centerText" value="${paymentInfoTitle}" />
        <dsp:param name="centerURL" value="/mobile/myaccount/paymentInfo.jsp"/>

        <dsp:param name="rightText" value="${pageTitle}"/>

        <dsp:param name="highlight" value="right" />
      </dsp:include>

	  <div class="mobile_store_displayList">

      <div class="mobile_store_register">
        <dsp:form id="mobile_store_paymentInfoAddNewCardForm" formid="mobile_store_paymentInfoAddNewCardForm"
                  action="${originatingRequest.requestURI}" method="post">

          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.createCardSuccessURL" value="creditCardAddressSelect.jsp"/>
          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.createCardErrorURL" value="newCreditCard.jsp?preFillValues=true"/>

          <dsp:include page="../checkout/gadgets/creditCardAddForm.jsp" flush="false">
            <dsp:param name="formHandler" value="/atg/userprofiling/MobileB2CProfileFormHandler"/>
            <dsp:param name="cardParamsMap" value="/atg/userprofiling/MobileB2CProfileFormHandler.editValue"/>
            <dsp:param name="nicknameProperty" value="map"/>
            <dsp:param name="defaultCard" value="true"/>
          </dsp:include>
        </dsp:form>
      </div>	    

    </div>
	</jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/newCreditCard.jsp#3 $$Change: 692002 $--%>
