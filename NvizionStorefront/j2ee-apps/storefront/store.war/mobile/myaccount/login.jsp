<%--
  This page is called by the AccessControlServlet to redirect the user to the global login page.

  Page includes:
    /mobile/global/login.jsp login form

  Required parameters:
    none

  Optional Params:
    passwordSent
     if returning from the passwordReset.jsp, set this variable to true.
--%>

<dsp:page>
  <dsp:getvalueof var="passwordSent" param="passwordSent"/>
  <dsp:include page="../global/login.jsp">
    <c:if test="${passwordSent == 'true'}">
      <dsp:param name="passwordSent" value="${passwordSent}"/>
    </c:if>
  </dsp:include>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/login.jsp#1 $$Change: 683854 $--%>
