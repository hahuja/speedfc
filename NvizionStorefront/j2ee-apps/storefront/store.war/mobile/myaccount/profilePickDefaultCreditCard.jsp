<%--
  This page renders the Select Default Credit Card page of the Checkout Defaults

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/myaccount/gadgets/savedCreditCards.jsp list of saved credit cards

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>

  <fmt:message key="mobile.myaccount_payment.defaultCreditCard" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <%-- Subeader --%>
      <fmt:message var="defaultCreditCardText" key="common.defaultCreditCard"/>
      <fmt:message var="defaultsText" key="mobile.myaccount_checkoutDefaults.defaults"/>
      <dsp:include page="/mobile/myaccount/gadgets/subheaderAccounts.jsp">
        <dsp:param name="centerText" value="${defaultsText}" />
        <dsp:param name="centerURL" value="/mobile/myaccount/profileCheckOutPrefs.jsp" />

        <dsp:param name="rightText" value="${defaultCreditCardText}" />

        <dsp:param name="highlight" value="right" />
      </dsp:include>
    
      <div class="mobile_store_checkout">
        <dsp:form id="mobile_store_checkoutDefaultCreditCard" action="${pageContext.request.requestURI}" method="post" formid="mobile_store_checkoutDefaultCreditCard">
          <dsp:include page="gadgets/savedCreditCards.jsp" flush="false">
            <dsp:param name="selectable" value="true"/>
            <dsp:param name="checkout" value="false"/>
          </dsp:include>
          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.updateSuccessURL" value="profileCheckOutPrefs.jsp"/>
          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.updateErrorURL" beanvalue="/OriginatingRequest.requestURI"/>
          <dsp:input type="hidden" value="" bean="MobileB2CProfileFormHandler.checkoutDefaultCreditCard" priority="-10"/>
        </dsp:form>
        <script type="text/javascript">
          $(document).ready(function() {
            $("#mobile_store_creditCardList").delayedSubmit();
          });
        </script>
      </div>
   
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/profilePickDefaultCreditCard.jsp#3 $$Change: 692002 $--%>
