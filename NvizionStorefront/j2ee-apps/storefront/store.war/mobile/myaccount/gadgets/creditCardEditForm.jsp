<%--
  The page renders content of edit credit card form

  Page includes:
    /mobile/myaccount/gadgets/billingAddressesList.jsp list of available addresses

  Required parameters:
    None

  Optional parameters:
    None

--%>
<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="MobileB2CProfileFormHandler.formExceptions"/>

  <jsp:useBean id="errorMap" class="java.util.HashMap"/>
  <c:if test="${not empty formExceptions}">
    <c:forEach var="formException" items="${formExceptions}">
      <dsp:param name="formException" value="${formException}"/>
      <%-- Check the error message code to see what we should do --%>
      <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
      <c:choose>
        <c:when test="${'missingRequiredValue' eq errorCode}">
          <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
          <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
        </c:when>
        <c:when test="${'errorInvalidCreditCard' eq errorCode}">
          <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
          <c:set target="${errorMap}" property="${propertyName}" value="invalid"/>
        </c:when>
        <c:when test="${'errorDuplicateCCNickname' eq errorCode}">
          <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
          <c:set target="${errorMap}" property="${propertyName}" value="duplicate"/>
        </c:when>
      </c:choose>
    </c:forEach>
  </c:if>

  <c:if test="${not empty errorMap['newNickname'] || not empty errorMap['creditCardNickname']}">
    <c:set var="errorClassCardName" value="mobile_store_error_state"/>
  </c:if>
  <c:if test="${not empty errorMap['expirationMonth'] || not empty errorMap['expirationYear']}">
    <c:set var="errorClassExpTime" value="mobile_store_error_state"/>
  </c:if>

  <table class="mobile_store_mobileTable mobile_store_narrowLines" summary="" role="presentation" datatable="0">
    <col/>
    <col/>
    <col/>
    <tr>
      <td colspan="2" class="mobile_storeCardTitle ${errorClassCardName}">
        <div class="content">
        <fmt:message var="nicknameLabel" key="common.nickName"/>
        <dsp:input bean="MobileB2CProfileFormHandler.editValue.newNickname"
                   iclass="text" type="text" required="true" maxlength="42"
                   id="mobile_store_paymentInfoEditCardCreateNickname">
          <dsp:tagAttribute name="placeholder" value="${nicknameLabel}"/>
        </dsp:input>
        <label for="mobile_store_paymentInfoEditCardCreateNickname"/>
        <dsp:input bean="MobileB2CProfileFormHandler.editValue.nickname" type="hidden"/>
        </div>
      </td>
      <td class="mobile_store_cardTypeCell ${errorClassCardName}">      
        <table class="errorRow" summary="" role="presentation" datatable="0">
          <tr>
            <td>
              <dsp:getvalueof var="creditCardType" 
                              bean="MobileB2CProfileFormHandler.editValue.creditCardType" vartype="java.lang.String"/>
              <dsp:getvalueof var="creditCardNumber" bean="MobileB2CProfileFormHandler.editValue.creditCardNumber"/>
              <c:set var="creditCardNumberLength" value="${fn:length(creditCardNumber)}"/>
              <c:set var="creditCardLastNumbers"  value="${fn:substring(creditCardNumber,creditCardNumberLength-4,creditCardNumberLength)}"/>

              <label class="hint">
                <span>${creditCardType}</span>
                <span class="mobile_store_creditCardLastNumbers"><fmt:message key="mobile.common.ellipsis"/> ${creditCardLastNumbers}</span>
              </label>
            </td>
            <c:choose>
              <c:when test="${not empty errorMap['newNickname']}">
                <td class="noPadding"><span class="errorMessage"><fmt:message key="mobile.form.validation.${errorMap['newNickname']}"/></span></td>
              </c:when>
              <c:when test="${not empty errorMap['creditCardNickname']}">
                <td class="noPadding"><span class="errorMessage"><fmt:message key="mobile.form.validation.${errorMap['creditCardNickname']}"/></span></td>
              </c:when>
            </c:choose>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="mobile_store_expireCell ${errorClassExpTime}">
        <label class="hint">
          <fmt:message key="common.expirationDate"/>
        </label>
      </td>
      <td colspan="2" class="mobile_store_expireCellValue mobile_arrowed ${errorClassExpTime}">
        <table class="errorRow">
          <tr>
            <td>
              <div class="mobile_store_expire_date">
                <div class="content">
                  <div class="mobile_store_month">
                    <label for="mobile_store_cardExpirationDateMonthSelect"></label><%-- This makes VoiceOver to pausing before select element reading --%>
                    <dsp:getvalueof var="monthValue" bean="MobileB2CProfileFormHandler.editValue.expirationMonth"/>
                    <c:if test="${empty monthValue}">
                      <c:set var="defaultMonthClass" value="default"/>
                    </c:if>
                    <dsp:select id="mobile_store_cardExpirationDateMonthSelect"
                                bean="MobileB2CProfileFormHandler.editValue.expirationMonth"
                                required="true" iclass="mobile_store_selectForm ${defaultMonthClass}" onchange="changeDropdown(event)">
                      <dsp:option value="">
                        <fmt:message key="common.month"/>
                      </dsp:option>
                      <%-- Display months --%>
                      <c:forEach var="count" begin="1" end="12" step="1" varStatus="status">
                        <dsp:option value="${count}">
                          <fmt:message key="common.month${count}"/>
                        </dsp:option>
                      </c:forEach>
                    </dsp:select>
                  </div>
                  <div class="mobile_store_date_delimiter"><label class="hint">/&nbsp;</label></div>
                  <div class="mobile_store_year">
                    <dsp:getvalueof var="yearValue" bean="MobileB2CProfileFormHandler.editValue.expirationMonth"/>
                    <c:if test="${empty yearValue}">
                      <c:set var="defaultYearClass" value="default"/>
                    </c:if>
                    <crs:yearList numberOfYears="16"
                                  bean="/atg/userprofiling/MobileB2CProfileFormHandler.editValue.expirationYear"
                                  id="mobile_store_cardExpirationDateYearSelect"
                                  selectRequired="true" yearString="true" onchange="changeDropdown(event)"
                                  iclass="mobile_store_selectForm ${defaultYearClass}"/>
                  </div>
                </div>
              </div>
            </td>
            <c:choose>
              <c:when test="${not empty errorMap['expirationMonth']}">
                <td class="noPadding"><span class="errorMessage middleErr"><fmt:message key="mobile.form.validation.${errorMap['expirationMonth']}"/></span></td>
              </c:when>
              <c:when test="${not empty errorMap['expirationYear']}">
                <td class="noPadding"><span class="errorMessage middleErr"><fmt:message key="mobile.form.validation.${errorMap['expirationYear']}"/></span></td>
              </c:when>
            </c:choose>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="3" class="mobile_store_defaultCard">
        <div class="content">
          <dsp:getvalueof var="targetCardKey" vartype="java.lang.String" bean="MobileB2CProfileFormHandler.editCard"/>
          <dsp:getvalueof var="userCards" vartype="java.util.Map" bean="Profile.creditCards"/>
          <dsp:getvalueof var="defaultCardId" vartype="java.lang.String" bean="Profile.defaultCreditCard.id"/>
          <dsp:input type="checkbox" bean="MobileB2CProfileFormHandler.editValue.newCreditCard"
                     checked="${defaultCardId == userCards[targetCardKey].repositoryId}" id="mobile_store_defaultCreditCard"/>
          <label for="mobile_store_defaultCreditCard" onclick="">
            <fmt:message key="myaccount_paymentInfoCardAddEdit.defaultCard"/>
          </label>          
        </div>
      </td>
    </tr>
    <tr>
      <td class="mobile_store_billToCell">
        <label class="hint">
          <fmt:message key="emailtemplates_orderConfirmation.billTo"/>
        </label>
      </td>
      <td colspan="2" class="mobile_store_billingAddressesCell">
        <dsp:include page="billingAddressesList.jsp" flush="false">
          <dsp:param name="mode" value="internal"/>
          <dsp:param name="selectedBillingAddressId" bean="MobileB2CProfileFormHandler.editValue.billingAddress.repositoryId"/>
          <dsp:param name="currentBillingAddress" bean="MobileB2CProfileFormHandler.editValue.billingAddress"/>
        </dsp:include>
      </td>
    </tr>
    <tr>
      <td colspan="3">        
        <div class="deleteContainer deleteContainerOffset">
          <fmt:message key="mobile.myaccount_storedCreditCards.removeCard" var="deleteText"/>
          <div id="mobile_store_removeCardLink"
               onclick="removeItemDialog($(this).parent());" title="${deleteText}">
             ${deleteText}
          </div>
        </div>
      </td>
    </tr>
  </table>
  <div class="mobile_store_formActions">
    <span class="mobile_store_basicButton">
      <fmt:message var="submitLabel" key="common.button.saveText"/>
      <dsp:input bean="MobileB2CProfileFormHandler.updateCard" type="hidden" value="true"/>
      <input type="submit" class="mobile_store_blueSubmitButton" value="${submitLabel}" id="mobile_store_createCreditCard">
    </span>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/gadgets/creditCardEditForm.jsp#4 $$Change: 692002 $--%>
