<%--
  This page displays the subheader for the myaccounts pages.  This subheader specifies
  both the text and URL for the left and right cell.  Other than that, it passes the
  parameters it receives to subheader.jsp

  Page includes:
    /mobile/includes/subheader.jsp subheader core

  Required parameters:
    None

  Optional parameters:
    highlight
      Determines which part should be highlighted. Must be one of 'left','center','right'
    centerText
      Text specified in the center container.
    centerURL
      Url to link in the center cell.  If no url is specified, a link is not provided.
    rightText
      Text specified in the right container

NOTE: There is no option for a rightURL because it would not make sense to have one
      in the 'My Account' section
--%>

<dsp:page>
  <fmt:message var="myaccountTitle" key="myaccount_myAccountMenu.myAccount" />

  <dsp:include page="/mobile/includes/subheader.jsp" >
    <dsp:param name="leftText" value="${myaccountTitle}"/>
    <dsp:param name="leftURL" value="/mobile/myaccount/profile.jsp"/>

    <dsp:param name="centerText" param="centerText"/>
    <dsp:param name="centerURL" param="centerURL"/>
    
    <dsp:param name="rightText" param="rightText"/>
    <dsp:param name="rightURL" value=""/>

    <dsp:param name="highlight" param="highlight"/>
  </dsp:include>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/gadgets/subheaderAccounts.jsp#3 $$Change: 692002 $--%>
