<%--
  Renders content of address form without nickname

  Form Condition:
    This gadget must be contained inside of more than one forms.
    Following Formhandlers must be invoked from a submit
        button in one of the forms for fields in this page to be processed :
        - BillingFormHandler
        - ShippingGroupFormHandler
        - B2CProfileFormHandler

  Page includes:
    /mobile/global/util/countryStatePicker.jspf renders content of states select picker
    /mobile/global/util/countryListPicker.jspf renders content of countries select picker

  Required parameters:
    formHandlerComponent
      This needs to be a full component Path plus and sub objects.
        e.g. /atg/commerce/order/purchase/ShippingGroupFormHandler.address
    restrictionDroplet
      This checks for the various droplets used while choosing the country and state

  Optional parameters:
    errorMap
      contains errors found in the form.
--%>

<dsp:page>
<dsp:importbean bean="/atg/multisite/SiteContext"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>

<dsp:getvalueof var="formHandlerComponent" param="formHandlerComponent"/>
<dsp:getvalueof var="restrictionDroplet" param="restrictionDroplet"/>
<dsp:getvalueof var="errorMap" param="errorMap"/>

<%-- Check for errors in firstName --%>
<c:if test="${not empty errorMap['firstName']}">
  <c:set var="liClassFirst" value="mobile_store_error_state"/>
</c:if>
<li class="${liClassFirst}">
  <div class="content">
    <dsp:input type="text" bean="${formHandlerComponent}.firstName"
               maxlength="40" required="true"
               iclass="mobile_store_textBoxForm">
      <fmt:message var="firstPlace" key="common.firstName"/>
      <dsp:tagAttribute name="placeholder" value="${firstPlace}"/>
    </dsp:input>
  </div>
  <c:if test="${not empty errorMap['firstName']}">
    <span class="errorMessage">
      <fmt:message key="mobile.form.validation.${errorMap['firstName']}"/>
    </span>
  </c:if>
</li>
<%-- Check for errors in lastName --%>
<c:if test="${not empty errorMap['lastName']}">
  <c:set var="liClassLast" value="mobile_store_error_state"/>
</c:if>
<li class="${liClassLast}">
  <div class="content">
    <dsp:input type="text" bean="${formHandlerComponent}.lastName"
               iclass="mobile_store_textBoxForm"
               maxlength="40" required="true">
      <fmt:message var="lastPlace" key="common.lastName"/>
      <dsp:tagAttribute name="placeholder" value="${lastPlace}"/>
    </dsp:input>
  </div>
  <c:if test="${not empty errorMap['lastName']}">
    <span class="errorMessage">
      <fmt:message key="mobile.form.validation.${errorMap['lastName']}"/>
    </span>
  </c:if>
</li>
<%-- Check for errors in address --%>
<c:if test="${not empty errorMap['address1']}">
  <c:set var="liClassAddress" value="mobile_store_error_state"/>
</c:if>
<li class="${liClassAddress}">
  <div class="content">
    <dsp:input type="text" bean="${formHandlerComponent}.address1"
               iclass="mobile_store_textBoxForm"
               maxlength="40" required="true">
      <fmt:message var="streetPlace" key="mobile.street"/>
      <dsp:tagAttribute name="placeholder" value="${streetPlace}"/>
    </dsp:input>
  </div>
  <c:if test="${not empty errorMap['address1']}">
    <span class="errorMessage">
      <fmt:message key="mobile.form.validation.${errorMap['address1']}"/>
    </span>
  </c:if>
</li>
<li>
  <div class="content">
    <dsp:input type="text" bean="${formHandlerComponent}.address2" maxlength="40"
               iclass="mobile_store_textBoxForm" required="false">
      <fmt:message var="streetPlace" key="mobile.street"/>
      <dsp:tagAttribute name="placeholder" value="${streetPlace}"/>
    </dsp:input>
  </div>
</li>
<li>
  <%-- Check for errors in city --%>
  <c:if test="${not empty errorMap['city']}">
    <c:set var="divClassCity" value="mobile_store_error_state"/>
  </c:if>
  <%-- Add bottom border --%>
  <c:if test="${empty errorMap['postalCode']}">
    <c:set var="divClassCityBottom" value="bottom"/>
  </c:if>
  <div class="left ${divClassCity} ${divClassCityBottom}">
    <div class="content">
      <dsp:input type="text" bean="${formHandlerComponent}.city"
                 iclass="mobile_store_textBoxSmall mobile_store_textBoxForm"
                 maxlength="30" required="true">
        <fmt:message var="cityPlace" key="common.city"/>
        <dsp:tagAttribute name="placeholder" value="${cityPlace}"/>
      </dsp:input>
    </div>
    <c:if test="${not empty errorMap['city']}">
  <span class="errorMessage">
    <fmt:message key="mobile.form.validation.${errorMap['city']}"/>
  </span>
    </c:if>
  </div>

  <%-- Check for errors in state --%>
  <c:if test="${not empty errorMap['state']}">
    <c:set var="divClassState" value="mobile_store_error_state"/>
  </c:if>
  <%-- Add bottom border --%>
  <c:if test="${empty errorMap['country']}">
    <c:set var="divClassStateBottom" value="bottom"/>
  </c:if>
  <div class="right ${divClassState} ${divClassStateBottom}">
    <div class="content">
      <dsp:getvalueof var="selectedState" vartype="java.lang.String" bean="${formHandlerComponent}.state"/>
      <dsp:getvalueof var="statePicker" vartype="java.lang.String" value="mobile_store_stateSelect" />
      <dsp:getvalueof var="countryPicker" vartype="java.lang.String" value="mobile_store_countrySelect" />
      <dsp:getvalueof var="countryRestrictionDroplet" vartype="java.lang.String" param="restrictionDroplet"/>
      <dsp:getvalueof var="countryCode" vartype="java.lang.String" bean="${formHandlerComponent}.country"/>
      <dsp:getvalueof var="labelStyle" value="${empty selectedState ? ' default' : ''}"/>
      <%@include file="/mobile/global/util/countryStatePicker.jspf" %>
    </div>
    <c:if test="${not empty errorMap['state']}">
    <span class="errorMessage">
      <fmt:message key="mobile.form.validation.${errorMap['state']}"/>
    </span>
    </c:if>
  </div>
</li>
<li>
  <%-- Check for errors in postalCode --%>
  <c:if test="${not empty errorMap['postalCode']}">
    <c:set var="divClassPostal" value="mobile_store_error_state"/>
  </c:if>
  <%-- Add bottom border --%>
  <c:if test="${empty errorMap['phoneNumber']}">
    <c:set var="divClassPostalBottom" value="bottom"/>
  </c:if>
  <div class="left ${divClassPostal} ${divClassPostalBottom}">
    <div class="content">
      <dsp:input type="text" bean="${formHandlerComponent}.postalCode"
                 iclass="mobile_store_textBoxSmall mobile_store_textBoxForm"
                 maxlength="10" required="true">
        <fmt:message var="zipPlace" key="common.postalZipCode"/>
        <dsp:tagAttribute name="placeholder" value="${zipPlace}"/>
      </dsp:input>
    </div>
    <c:if test="${not empty errorMap['postalCode']}">
  <span class="errorMessage">
    <fmt:message key="mobile.form.validation.${errorMap['postalCode']}"/>
  </span>
    </c:if>
  </div>

  <%-- Check for errors in Country --%>
  <c:if test="${not empty errorMap['country']}">
    <c:set var="divClassCountry" value="mobile_store_error_state"/>
  </c:if>
  <%-- Add bottom border --%>
  <c:if test="${empty errorMap['phoneNumber']}">
    <c:set var="divClassCountryBottom" value="bottom"/>
  </c:if>
  <div class="right ${divClassCountry} ${divClassCountryBottom}">
    <div class="content">
      <dsp:getvalueof var="selectedCountry" vartype="java.lang.String" bean="${formHandlerComponent}.country"/>
      <dsp:getvalueof var="selectStyle" value="${empty selectedCountry ? ' default' : ''}"/>
      <dsp:select required="true" id="mobile_store_countrySelect"
                  bean="${formHandlerComponent}.country" iclass="mobile_store_selectForm${selectStyle}"
                  onchange="selectCountry(event);">
        <%@include file="/mobile/global/util/countryListPicker.jspf" %>
      </dsp:select>
    </div>
    <c:if test="${not empty errorMap['country']}">
    <span class="errorMessage">
      <fmt:message key="mobile.form.validation.${errorMap['country']}"/>
    </span>
    </c:if>
  </div>
</li>
<%-- Check for errors in phoneNumber --%>
<c:if test="${not empty errorMap['phoneNumber']}">
  <c:set var="liClassPhone" value="mobile_store_error_state"/>
</c:if>
<li class="${liClassPhone}">
  <div class="content">
    <dsp:input type="tel" bean="${formHandlerComponent}.phoneNumber"
               iclass="mobile_store_textBoxForm"
               maxlength="15" required="true">
      <fmt:message var="phonePlace" key="common.phoneNumber"/>
      <dsp:tagAttribute name="placeholder" value="${phonePlace}"/>
    </dsp:input>
  </div>
  <c:if test="${not empty errorMap['phoneNumber']}">
    <span class="errorMessage">
      <fmt:message key="mobile.form.validation.${errorMap['phoneNumber']}"/>
    </span>
  </c:if>
</li>
<%-- Make shure that we are in checkout --%>
<c:if test="${formHandlerComponent == '/atg/commerce/order/purchase/ShippingGroupFormHandler.address'}">
  <%-- Check if current user is registered user --%>
  <dsp:importbean bean="/atg/store/droplet/ProfileSecurityStatus"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
  <dsp:droplet name="ProfileSecurityStatus">
    <dsp:oparam name="anonymous">
      <%-- User is anonymous, don't prompt to update his profile --%>
    </dsp:oparam>
    <dsp:oparam name="default">
      <li>
        <div class="content">
          <dsp:input type="checkbox"
                     name="saveShippingAddress"
                     id="atg_store_addressAddSaveAddressInput"
                     checked="true"                     
                     bean="ShippingGroupFormHandler.saveShippingAddress"/>
          <label for="atg_store_addressAddSaveAddressInput" onclick="">
            <fmt:message key="mobile.checkout_updateProfile"/>
          </label>
        </div>
      </li>
    </dsp:oparam>
  </dsp:droplet>
</c:if>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/gadgets/addressAddEdit.jsp#3 $$Change: 692002 $--%>
