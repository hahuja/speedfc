<%--
  Page for editing and adding a new address.

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/myaccount/gadgets/addressAddEdit.jsp renderer of address form (except Nickname field)

  Required parameters:
    addEditMode
      set to 'edit' when a current address is being modified, otherwise it is assumed
      a new address is being added.

  Optional parameters:
    deleteKey
      the key of the address that is being deleted.
    deletable
      flag to mark address item as deletable
--%>
<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/B2CProfileFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/store/droplet/ShippingRestrictionsDroplet"/>

  <fmt:message var="title" key="myaccount_accountAddressEdit.title"/>
  <crs:mobilePageContainer titleString="${title}">
    <jsp:attribute name="modalContent">
      <c:if test="${addEditMode == 'edit' and deletable == 'true'}">
        <div class="mobile_store_moveDialog" id="removeItemDialog">
          <div class="mobile_store_moveItems">
            <ul>
              <li class="remove">
                <fmt:message key="mobile.button.removeText" var="removeText"/>
                <fmt:message key="mobile.checkout_shippingAddressEdit.deleteText" var="deleteText"/>
                <dsp:a bean="/atg/userprofiling/MobileB2CProfileFormHandler.removeAddress" href="addressBook.jsp" iclass="mobile_store_removeLink"
                       value="${deleteKey}" title="${deleteText}">
                  ${removeText}
                </dsp:a>
              </li>
            </ul>
          </div>
        </div>
      </c:if>
      <c:if test="${addEditMode == 'edit' and deletable == 'false'}">
        <%-- Modal popup when address can't be deleted due to gift list redirect for full CRS --%>
		<div id="mobile_store_modalMessageBox">
          <div id="mobile_store_modalInclude">
            <c:set var="successURL" value="/myaccount/addressBook.jsp"/>
            <fmt:message var="topString" key="mobile.myaccount_addresses.redirectTopString"/>
            <fmt:message var="bottomString" key="mobile.myaccount_addresses.redirectBottomString"/>
            <%@include file="/mobile/includes/crsRedirect.jspf" %>
          </div>
		</div>
      </c:if>
    </jsp:attribute>

    <jsp:body>      
      <dsp:getvalueof var="addEditMode" param="addEditMode"/>
      <dsp:getvalueof var="deleteKey" param="deleteKey"/>
      <dsp:getvalueof var="deletable" param="deletable"/>
      <%-- Display subheader --%>
      <c:choose>
        <c:when test="${addEditMode == 'edit'}">
          <fmt:message var="title" key="myaccount_accountAddressEdit.title"/>
        </c:when>
        <c:otherwise>
          <fmt:message var="title" key="myAccount.checkoutDefaults.addAddress"/>
        </c:otherwise>
      </c:choose>

      <fmt:message var="addressBookTitle" key="myaccount_addressBook.title"/>

      <dsp:include page="./gadgets/subheaderAccounts.jsp">
        <dsp:param name="centerText" value="${addressBookTitle}"/>
        <dsp:param name="centerURL" value="/mobile/myaccount/addressBook.jsp"/>
        <dsp:param name="rightText" value="${title}"/>
        <dsp:param name="highlight" value="right"/>
      </dsp:include>

      <%-- Handle form exceptions --%>
      <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="B2CProfileFormHandler.formExceptions"/>
      <jsp:useBean id="errorMap" class="java.util.HashMap"/>
      <c:if test="${not empty formExceptions}">
        <c:forEach var="formException" items="${formExceptions}">
          <dsp:param name="formException" value="${formException}"/>
          <%-- Check the error message code to see what we should do --%>
          <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>

          <c:choose>
            <c:when test="${'errorDuplicateNickname' eq errorCode}">
              <c:set target="${errorMap}" property="nickname" value="inUse"/>
            </c:when>
            <c:when test="${'stateIsIncorrect' eq errorCode}">
              <c:set target="${errorMap}" property="state" value="invalid"/>
              <c:set target="${errorMap}" property="country" value="invalid"/>
            </c:when>
            <c:when test="${'missingRequiredValue' eq errorCode}">
              <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
              <c:if test="${'newNickname' eq propertyName}">
                <c:set var="propertyName" value="nickname"/>
              </c:if>
              <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
            </c:when>
          </c:choose>
        </c:forEach>
      </c:if>

      <dsp:getvalueof id="originatingRequestURL" bean="/OriginatingRequest.requestURI"/>

      <%-- Start Form --%>
      <dsp:form action="${originatingRequestURL}" method="post">
        <%-- Set corresponding success/error URLs --%>
        <c:choose>
          <c:when test="${addEditMode == 'edit'}">
            <%-- Update the Success and Error URLs for updating an address --%>
            <c:url value="accountAddressEdit.jsp" var="errorURL">
              <c:param name="addEditMode" value="edit"/>
              <c:param name="deleteKey" value="${deleteKey}"/>
              <c:param name="deletable" value="${deletable}"/>
            </c:url>

            <dsp:input type="hidden" bean="B2CProfileFormHandler.updateAddressSuccessURL" value="addressBook.jsp"/>
            <dsp:input type="hidden" bean="B2CProfileFormHandler.updateAddressErrorURL" value="${errorURL}"/>
          </c:when>
          <c:otherwise>
            <%-- Update the Success and Error URLs for creating a new address --%>
            <c:url value="accountAddressEdit.jsp" var="errorURL">
              <c:param name="addEditMode" value="add"/>
            </c:url>

            <dsp:input type="hidden" bean="B2CProfileFormHandler.newAddressSuccessURL" value="addressBook.jsp"/>
            <dsp:input type="hidden" bean="B2CProfileFormHandler.newAddressErrorURL" value="${errorURL}"/>
          </c:otherwise>
        </c:choose>

        <%-- Set Owner ID --%>
        <dsp:input bean="B2CProfileFormHandler.editValue.ownerId" beanvalue="Profile.id" type="hidden"/>

        <div class="mobile_store_displayList">
          <ul class="mobile_store_mobileList">
            <%-- Check for errors in "Nick Name" --%>
            <c:if test="${not empty errorMap['nickname']}">
              <c:set var="liClassNick" value="mobile_store_error_state"/>
            </c:if>
            <li class="${liClassNick}">
              <div class="content">
                <%-- Check in "Nick Name" for update or add --%>
                <fmt:message var="nickPlace" key="common.addressNickname"/>
                <c:choose>
                  <c:when test="${addEditMode == 'edit'}">
                    <dsp:input type="hidden" bean="B2CProfileFormHandler.editValue.nickname"/>
                    <dsp:input type="text" bean="B2CProfileFormHandler.editValue.newNickname"
                               iclass="mobile_store_textBoxForm" maxlength="42" required="true">
                      <dsp:tagAttribute name="placeholder" value="${nickPlace}"/>
                    </dsp:input>
                  </c:when>
                  <c:otherwise>
                    <dsp:input type="text" bean="B2CProfileFormHandler.editValue.nickname"
                               iclass="mobile_store_textBoxForm" maxlength="42" required="true">
                      <dsp:tagAttribute name="placeholder" value="${nickPlace}"/>
                    </dsp:input>
                  </c:otherwise>
                </c:choose>
              </div>
              <c:if test="${not empty errorMap['nickname']}">
                <span class="errorMessage">
                  <fmt:message key="mobile.form.validation.${errorMap['nickname']}"/>
                </span>
              </c:if>
            </li>

            <%-- Start inclusion of addressAddEdit for rendering parameters of the form --%>
            <dsp:include page="/mobile/myaccount/gadgets/addressAddEdit.jsp">
              <dsp:param name="formHandlerComponent" value="/atg/userprofiling/B2CProfileFormHandler.editValue"/>
              <dsp:param name="restrictionDroplet" value="/atg/store/droplet/ShippingRestrictionsDroplet"/>
              <dsp:param name="errorMap" value="${errorMap}"/>
            </dsp:include>

            <li>
              <dsp:getvalueof var="defaultAddressId" bean="Profile.shippingAddress.repositoryId"/>
              <dsp:getvalueof var="currentAddressId" bean="B2CProfileFormHandler.editValue.addressId"/>
              <div class="content">
                <div class="checkBox">
                  <dsp:input type="checkbox" name="useShippingAddressAsDefault" id="addresses_useShippingAddressAsDefault"
                             bean="/atg/userprofiling/B2CProfileFormHandler.useShippingAddressAsDefault" checked="${defaultAddressId == currentAddressId}"/>
                  <label for="addresses_useShippingAddressAsDefault" onclick="">
                    <fmt:message key="mobile.myaccount_addresses.default"/>
                  </label>
                </div>
              </div>
            </li>
            <c:if test="${addEditMode == 'edit'}">
              <%-- "Delete" Button --%>
              <li>
                <div class="deleteContainer">
                  <fmt:message key="mobile.checkout_shippingAddressEdit.deleteText" var="deleteText"/>
                  <div id="mobile_store_removeAddress"
                       onclick="${deletable ? 'removeItemDialog($(this).parent())' : 'toggleCantDeleteAddressDialog()'}" title="${deleteText}">
                    ${deleteText}
                  </div>
                </div>
              </li>
            </c:if>
          </ul>
        </div>

        <%-- Show corresponding buttons depending on addEditMode value --%>
        <div class="mobile_store_myaccount_button">
          <c:choose>
            <c:when test="${addEditMode == 'edit'}">
              <%-- "Update" Button --%>
              <fmt:message var="updateText" key="common.button.updateText"/>
              <dsp:input bean="B2CProfileFormHandler.updateAddress" iclass="mainActionBtn profileEdit"
                         id="mobile_store_editAddress" type="submit" value="${updateText}"/>
            </c:when>
            <c:otherwise>
              <%-- "Save" Button --%>
              <fmt:message var="saveText" key="common.button.saveAddressText"/>
              <dsp:input bean="B2CProfileFormHandler.newAddress" iclass="mainActionBtn profileEdit"
                         id="mobile_store_editAddress" type="submit" value="${saveText}"/>
            </c:otherwise>
          </c:choose>
        </div>
      </dsp:form>
      <br/>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/accountAddressEdit.jsp#6 $$Change: 692002 $--%>
