<%--
  This page is used to change the password of the user.

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:importbean bean="/atg/userprofiling/B2CProfileFormHandler"/>

  <fmt:message var="title" key="myaccount_profileMyInfoEditLinks.button.changePasswordText" />
  <dsp:getvalueof var="resultSuccess" param="result"/>
  <crs:mobilePageContainer titleString="${title}" displayModal="${empty resultSuccess ? false : true}">
    <jsp:attribute name="modalContent">
      <%-- Visible by default when resultSuccess is true --%>
      <div id="mobile_store_modalMessageBox" style="${empty resultSuccess ? '' : 'display: block'}">
        <div id="mobile_store_modalInclude">
          <ul class="mobile_store_mobileCRSRedirectTable">
            <li><fmt:message key="emailtemplates_changePassword.title"/></li>
            <li>
              <dsp:a href="profile.jsp" iclass="withArrow">
                <fmt:message var="myaccountTitle" key="myaccount_myAccountMenu.myAccount" />
                <fmt:message key="common.returnTo">
                  <fmt:param value="${myaccountTitle}"/>
                </fmt:message>
              </dsp:a>
            </li>
          </ul>
        </div>
      </div>
    </jsp:attribute>
    <jsp:body>
      <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="B2CProfileFormHandler.formExceptions"/>

      <%-- Handle form exceptions --%>
      <jsp:useBean id="errorMap" class="java.util.HashMap"/>
      <c:if test="${not empty formExceptions}">
        <c:forEach var="formException" items="${formExceptions}">
          <dsp:param name="formException" value="${formException}"/>
          <%-- Check the error message code to see what we should do --%>
          <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
          <c:choose>
            <c:when test="${'missingRequiredValue' eq errorCode}">
              <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
              <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
            </c:when>
            <c:when test="${'passwordsDoNotMatch' eq errorCode}">
              <c:set target="${errorMap}" property="notMatching" value="invalid"/>
            </c:when>
            <c:when test="${'passwordEqualsOldPassword' eq errorCode}">
              <c:set target="${errorMap}" property="allEqual" value="invalid"/>
            </c:when>
            <%-- Old password doesn't match --%>
            <c:when test="${'permissionDefinedPasswordChange' eq errorCode}">
              <c:set target="${errorMap}" property="oldInvalid" value="invalid"/>
            </c:when>
            <%-- Password length too short --%>
            <c:when test="${'atg.droplet.DropletException' eq errorCode}">
              <c:set target="${errorMap}" property="requirements" value="invalid"/>
            </c:when>
          </c:choose>
        </c:forEach>
      </c:if>

      <dsp:include page="./gadgets/subheaderAccounts.jsp">
        <fmt:message key="mobile.myaccount_changePassword.title" var="centerText"/>
        <dsp:param name="rightText" value="${centerText}"/>
        <dsp:param name="highlight" value="right"/>
      </dsp:include>

      <dsp:form action="${originatingRequest.requestURI}" method="post"
                id="atg_store_profilePasswordEditForm" formid="profilepasswordeditform">

        <%-- Prevent prepopulation of input fields --%>
        <dsp:setvalue bean="B2CProfileFormHandler.extractDefaultValuesFromProfile" value="false"/>

        <%--
        <dsp:input bean="B2CProfileFormHandler.changePasswordSuccessURL" type="hidden" value="profile.jsp"/> --%>
        <dsp:getvalueof var="successUrl" bean="/OriginatingRequest.requestURI"/>
        <dsp:input bean="B2CProfileFormHandler.changePasswordSuccessURL" type="hidden" value="${successUrl}?result=true"/>
        <dsp:input bean="B2CProfileFormHandler.changePasswordErrorURL" type="hidden"
                   beanvalue="/OriginatingRequest.requestURI"/>

        <%-- Require that both of the new passwords entered match --%>
        <dsp:input bean="B2CProfileFormHandler.confirmPassword" type="hidden" value="true"/>
    <div class="mobile_store_displayList">
        <ul class="mobile_store_mobileList" role="presentation">
          <%-- Grab commonly used missing error text --%>
          <fmt:message var="missing" key="mobile.form.validation.missing"/>

            <%-- Check to see if the Old Password is missing or incorrect --%>
            <c:if test="${not empty errorMap['oldpassword'] || not empty errorMap['oldInvalid']}">
              <c:set var="liClassOld" value="mobile_store_error_state"/>
            </c:if>
            <li class="${liClassOld}">
              <div>
                <div class="content">
                  <fmt:message var="oldPasswordPlaceholder" key="myaccount_profilePasswordEdit.oldPassword"/>
                  <dsp:input id="mobile_store_oldPassword" maxlength="35"
                             bean="B2CProfileFormHandler.value.oldpassword"
                             type="password" required="true" value=""
                             iclass="mobile_store_textBoxForm">
                    <dsp:tagAttribute name="placeholder" value="${oldPasswordPlaceholder}"/>
                    <label for="mobile_store_oldPassword"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['oldpassword']}">
                  <span class="errorMessage minimalWidth">
                    <p> <%-- Needed for text wrap on long error messages --%>
                      ${missing}
                    </p>
                  </span>
                </c:if>
                <c:if test="${not empty errorMap['oldInvalid']}">
                  <span class="errorMessage minimalWidth">
                    <p>
                      <fmt:message key="mobile.form.validation.retry"/>
                    </p>
                  </span>
                </c:if>
              </div>
            </li>

            <%-- Check for errors in New Password --%>
            <c:if test="${not empty errorMap['password'] || not empty errorMap['allEqual'] ||
                          not empty errorMap['requirements']}">
              <c:set var="liClassNew" value="mobile_store_error_state"/>
            </c:if>
            <li class="${liClassNew}">
              <div>
                <div class="content">
                  <fmt:message var="newPasswordPlaceholder" key="myaccount_profilePasswordEdit.newPassword"/>
                  <dsp:input id="mobile_store_oldPassword" maxlength="35"
                             bean="B2CProfileFormHandler.value.password"
                             type="password" required="true" value=""
                             iclass="mobile_store_textBoxForm">
                    <dsp:tagAttribute name="placeholder" value="${newPasswordPlaceholder}"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['password']}">
                  <span class="errorMessage minimalWidth">
                    <p>
                      ${missing}
                    </p>
                  </span>
                </c:if>
                <c:if test="${not empty errorMap['allEqual']}">
                  <span class="errorMessage normalWidth">
                    <p>
                      <fmt:message key="mobile.form.validation.reusePassword" />
                    </p>
                  </span>
                </c:if>
                <c:if test="${not empty errorMap['requirements']}">
                  <span class="errorMessage normalWidth">
                    <p>
                      <fmt:message key="mobile.form.validation.requirements" />
                    </p>
                  </span>
                </c:if>
              </div>
            </li>

            <%-- Check for errors in Confirm Password --%>
            <c:if test="${not empty errorMap['confirmpassword'] || not empty errorMap['notMatching']}">
              <c:set var="liClassConfirm" value="mobile_store_error_state"/>
            </c:if>
            <li class="${liClassConfirm}">
              <div>
                <div class="content">
                  <fmt:message var="confirmPasswordPlaceholder" key="common.confirmPassword"/>
                  <dsp:input id="mobile_store_oldPassword" maxlength="35"
                             bean="B2CProfileFormHandler.value.confirmpassword"
                             type="password" required="true" value=""
                             iclass="mobile_store_textBoxForm">
                    <dsp:tagAttribute name="placeholder" value="${confirmPasswordPlaceholder}"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['confirmpassword']}">
                  <span class="errorMessage minimalWidth">
                    <p>
                      ${missing}
                    </p>
                  </span>
                </c:if>
                <c:if test="${not empty errorMap['notMatching']}">
                  <span class="errorMessage minimalWidth">
                    <p>
                      <fmt:message key="mobile.form.validation.mismatch" />
                    </p>
                  </span>
                </c:if>
              </div>
            </li>
        </ul>
    </div>
        <%-- Change Password button --%>
        <div class="mobile_store_myaccount_button">
          <fmt:message var="updateButton" key="common.button.saveChanges"/>
          <span class="mobile_store_basicButton">
            <dsp:input bean="B2CProfileFormHandler.changePassword"
             iclass="mainActionBtn profileEdit" type="submit"
             value="${updateButton}" name="atg_store_profilePasswordEditSubmit"/>
          </span>
        </div>
        <br/>
      </dsp:form>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/profilePasswordEdit.jsp#3 $$Change: 692002 $--%>
