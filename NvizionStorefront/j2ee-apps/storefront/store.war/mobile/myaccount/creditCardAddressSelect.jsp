<%--
  This page fragment renders all stored addresses for a registered user

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/myaccount/gadgets/billingAddressesList.jsp list of available addresses to use them as billing one

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Compare"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>

  <fmt:message key="myaccount_myAccountMenu.paymentInfo" var="paymentInfoTitle"/>
  <fmt:message key="mobile.myaccount_payment_subHeader.addCard" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <dsp:include page="./gadgets/subheaderAccounts.jsp" >
        <dsp:param name="centerText" value="${paymentInfoTitle}" />
        <dsp:param name="centerURL" value="/mobile/myaccount/paymentInfo.jsp"/>

        <dsp:param name="rightText" value="${pageTitle}"/>

        <dsp:param name="highlight" value="right" />
      </dsp:include>

      <div class="mobile_store_displayList" id="mobile_store_shippingAddress">
        <dsp:form id="mobile_store_checkoutShippingAddress" formid="mobile_store_checkoutShippingAddress"
          action="${pageContext.request.requestURI}" method="post">

          <div class="mobile_store_chooseShippingAddresses">
            <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.createCardSuccessURL" value="paymentInfo.jsp"/>
            <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.createCardErrorURL" value="newCreditCard.jsp?preFillValues=true"/>
            <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.updateAddressErrorURL" value="creditCardAddressSelect.jsp?preFillValues=true"/>

            <dsp:include page="gadgets/billingAddressesList.jsp" flush="false"/>

            <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.addBillingAddressToCreateNewCreditCard" value="addBillingAddressToCreateNewCreditCard" priority="-10"/>
          </div>
        </dsp:form>
        <script type="text/javascript">
          $(document).ready(function() {
            $("#mobile_store_savedBillingAddresses").delayedSubmit();
          });
        </script>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/creditCardAddressSelect.jsp#3 $$Change: 692002 $--%>
