<%--
  This page renders the Select Default Shipping Address page of the Checkout Defaults

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/global/util/displayAddress.jsp renderer of address info

    Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>

  <fmt:message key="myaccount_address.default" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
    
      <%-- Subheader --%>
      <fmt:message var="defaultShippingAddressText" key="common.defaultShippingAddress"/>
      <fmt:message var="defaultsText" key="mobile.myaccount_checkoutDefaults.defaults"/>
      <dsp:include page="/mobile/myaccount/gadgets/subheaderAccounts.jsp">
        <dsp:param name="centerText" value="${defaultsText}" />
        <dsp:param name="centerURL" value="/mobile/myaccount/profileCheckOutPrefs.jsp" />

        <dsp:param name="rightText" value="${defaultShippingAddressText}" />

        <dsp:param name="highlight" value="right" />
      </dsp:include>
    
    
      <div class="mobile_store_checkoutDefaults">
        <dsp:form id="mobile_store_checkoutDefaultShippingAddress" action="${pageContext.request.requestURI}" method="post" formid="mobile_store_checkoutDefaultShippingAddress">
          <ul id="mobile_store_savedAddresses" class="mobile_store_mobileList">
            <dsp:getvalueof var="secondaryAddresses" vartype="java.util.Map" bean="Profile.secondaryAddresses"/>
            <dsp:getvalueof var="defaultAddress" vartype="java.lang.String" bean="Profile.shippingAddress.repositoryId"/>

            <c:if test="${empty defaultAddress and (fn:length(secondaryAddresses) == 0)}">
              <li class="mobile_store_address">
                <div class="content">
                  <dsp:input
                    id="notSpecified"
                    name="defaultShippingAddress"
                    type="radio"
                    onclick="$('#mobile_store_checkoutDefaultShippingAddress').submit();"
                    bean="MobileB2CProfileFormHandler.value.shippingAddress"
                    value=""
                    checked="${empty defaultAddress}"/>
                  <label for="notSpecified" onclick="">
                    <fmt:message key="common.notSpecified"/>
                  </label>
                </div>
              </li>
            </c:if>

            <c:forEach var="availableShippingAddress" items="${secondaryAddresses}" varStatus="shipping_counter">
              <li class="mobile_store_address">
                <div class="content">
                  <dsp:input
                    id="shipping_address_${shipping_counter.count}"
                    name="defaultShippingAddress"
                    type="radio"
                    onclick="$('#mobile_store_checkoutDefaultShippingAddress').submit();"
                    bean="MobileB2CProfileFormHandler.value.shippingAddress"
                    value="${availableShippingAddress.key}"
                    checked="${defaultAddress == availableShippingAddress.value.repositoryId}"/>
                  <label for="shipping_address_${shipping_counter.count}" onclick="">
                    <dsp:include page="/mobile/global/util/displayAddress.jsp">
                      <dsp:param name="address" value="${availableShippingAddress.value}"/>
                      <dsp:param name="private" value="false"/>
                    </dsp:include>
                  </label>
                </div>
              </li>
            </c:forEach>
          </ul>

          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.updateSuccessURL" value="profileCheckOutPrefs.jsp"/>
          <dsp:input type="hidden" value="" bean="MobileB2CProfileFormHandler.checkoutDefaultShippingAddress" priority="-10"/>

        </dsp:form>

      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/profilePickDefaultShippingAddress.jsp#3 $$Change: 692002 $--%>
