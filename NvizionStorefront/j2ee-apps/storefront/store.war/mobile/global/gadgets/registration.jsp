<%--
  This page renders registration form for new customers

  Page includes:
    None

  Required parameters:
    checkoutLogin
      determines the successURL/errorURL based on where user is registering

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/store/profile/RegistrationFormHandler"/>
  <dsp:importbean bean="/atg/store/droplet/ProfileSecurityStatus"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <dsp:getvalueof var="checkoutLogin" param="checkoutLogin" />
  <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="RegistrationFormHandler.formExceptions"/>
  <dsp:getvalueof var="profileEmail" bean="Profile.email"/>
  <dsp:getvalueof var="handlerEmail" bean="RegistrationFormHandler.value.email"/>

  <%--
    Check to see if the user is unknown, offer option to logout if the user is logged in.
  --%>
  <dsp:droplet name="ProfileSecurityStatus">
    <dsp:oparam name="anonymous">

      <%-- Handle form exceptions --%>
      <jsp:useBean id="errorMap" class="java.util.HashMap"/>
      <c:if test="${not empty formExceptions}">
        <c:forEach var="formException" items="${formExceptions}">
          <dsp:param name="formException" value="${formException}"/>
          <%-- Check the error message code to see what we should do --%>
          <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>

          <c:choose>
            <c:when test="${'invalidEmailAddress' eq errorCode}"><c:set target="${errorMap}" property="email" value="invalid"/></c:when>
            <c:when test="${'userAlreadyExists' eq errorCode}"><c:set target="${errorMap}" property="email" value="inUse"/></c:when>
            <c:when test="${'invalidPasswordLength' eq errorCode}"><c:set target="${errorMap}" property="password" value="invalid"/></c:when>
            <c:when test="${'missingRequiredValue' eq errorCode}">
	          <dsp:getvalueof var="propertyPath" vartype="java.lang.String" param="formException.propertyPath"/>
              <c:set var="field" value="${fn:substringAfter(propertyPath,'/atg/store/profile/RegistrationFormHandler.value.')}"/>
              <c:set target="${errorMap}" property="${field}" value="missing"/>
            </c:when>
	        <%-- Password length too short --%>
	        <c:when test="${'atg.droplet.DropletException' eq errorCode}">
	          <c:set target="${errorMap}" property="password" value="invalid"/>
	        </c:when>
          </c:choose>
        </c:forEach>
      </c:if>

      <div class="mobile_store_displayList" id="mobile_store_newCustomerLogin">

        <dsp:setvalue bean="RegistrationFormHandler.extractDefaultValuesFromProfile" value="false"/>
        <dsp:form id="mobile_store_registration" formid="mobile_store_registerForm" action="${originatingRequest.requestURI}" method="post">

          <c:choose>
            <c:when test="${checkoutLogin == 'true'}">
              <c:set var="successUrl" value="../../checkout/shipping.jsp"/>
            </c:when>
            <c:otherwise>
              <c:set var="successUrl" value="../../myaccount/profile.jsp"/>
            </c:otherwise>
          </c:choose>

          <c:url var="errorUrl" value="${pageContext.request.requestURI}" context="/">
            <c:if test="${checkoutLogin == 'true'}">
              <c:param name="checkoutLogin" value="true"/>
            </c:if>
            <c:param name="registrationErrors" value="true"/>
          </c:url>

          <dsp:input bean="RegistrationFormHandler.createErrorURL" type="hidden" value="${errorUrl}" />
          <dsp:input bean="RegistrationFormHandler.createSuccessURL" type="hidden" value="${successUrl}"/>
          
			<!-- if email is stored and no email entered before -->
			<c:if test="${not empty profileEmail and empty handleEmail}">
				<!-- use this email and "forget" it -->
				<dsp:setvalue bean="RegistrationFormHandler.value.email" value="${profileEmail}" />
				<c:set var="profileEmail" value=""/>
				<dsp:setvalue bean="Profile.email" value="${profileEmail}" />
			</c:if>
			
			<div class="mobile_store_register">

            <span class="mobile_store_myaccount_questionImage">
	          <fmt:message var="privacyTitle" key="mobile.company_privacyAndTerms.pageTitle"/>
              <dsp:a page="/mobile/company/terms.jsp" title="${privacyTitle}" class="icon-help">

              </dsp:a>
            </span>

            <ul class="mobile_store_mobileList">

              <%-- Check for errors in email --%>
              <c:if test="${not empty errorMap['email']}">
                <c:set var="liClassEmail" value="mobile_store_error_state"/>
              </c:if>
              <li class="${liClassEmail}">
                <div class="content">
                  <fmt:message var="emailHint" key="common.email"/>
                  <dsp:input bean="RegistrationFormHandler.value.email"
                             iclass="text" type="email" required="true"
                             id="mobile_store_registerEmailAddress"
                             placeholder="${emailHint}"
                             autocapitalize="off" maxlength="40"/>
                </div>
                <c:if test="${not empty errorMap['email']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['email']}"/>
                  </span>
                </c:if>
              </li>

              <%-- Check for errors in password --%>
              <c:if test="${not empty errorMap['password']}">
                <c:set var="liClassPass" value="mobile_store_error_state"/>
              </c:if>
              <li class="${liClassPass}">
                <div class="content">
                  <fmt:message var="createPasswordHint" key="common.createPassword"/>
                  <dsp:input bean="RegistrationFormHandler.value.password"
                             type="password" required="true" iclass="text"
                             id="mobile_store_registerPassword" value=""
                             maxlength="35">
                    <dsp:tagAttribute name="placeholder" value="${createPasswordHint}"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['password']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['password']}"/>
                  </span>
                </c:if>
              </li>

              <%-- Check for errors in firstName --%>
              <c:if test="${not empty errorMap['firstName']}">
                <c:set var="liClassFirst" value="mobile_store_error_state"/>
              </c:if>
              <li class="${liClassFirst}">
                <div class="content">
                  <fmt:message var="firtNameHint" key="common.firstName"/>
                  <dsp:input bean="RegistrationFormHandler.value.firstName"
                             type="text" required="true" iclass="text"
                             id="mobile_store_registerFirstName" maxlength="40">
                    <dsp:tagAttribute name="placeholder" value="${firtNameHint}"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['firstName']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['firstName']}"/>
                  </span>
                </c:if>
              </li>

              <%-- Check for errors in lastName --%>
              <c:if test="${not empty errorMap['lastName']}">
                <c:set var="liClassLast" value="mobile_store_error_state"/>
              </c:if>
              <li class="${liClassLast}">
                <div class="content">
                  <fmt:message var="lastNameHint" key="common.lastName"/>
                  <dsp:input bean="RegistrationFormHandler.value.lastName"
                             type="text" required="true" iclass="text"
                             id="mobile_store_registerLastName" maxlength="40">
                    <dsp:tagAttribute name="placeholder" value="${lastNameHint}"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['lastName']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['lastName']}"/>
                  </span>
                </c:if>
              </li>
              <li>
                <div class="content">
                  <dsp:input bean="RegistrationFormHandler.value.autoLogin" type="checkbox" id="mobile_store_remember" checked="true"/>
                  <label for="mobile_store_remember" onclick="">
                    <fmt:message key="mobile.checkout_login.remember"/>
                  </label>
                </div>
              </li>
            </ul>
            <div class="mobile_store_formActions">
              <span class="mobile_store_basicButton">
                <fmt:message var="submitText" key="mobile.myaccount_login.button.create"/>
                <dsp:input bean="RegistrationFormHandler.create"
                           id="mobile_store_createMyAccount"
                           type="submit"
                           class="mainActionBtn"
                           alt="${submitText}"
                           value="${submitText}"/>
              </span>
            </div>
          </div>
        </dsp:form>
      </div>
    </dsp:oparam>
  </dsp:droplet>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/global/gadgets/registration.jsp#5 $$Change: 692002 $--%>
