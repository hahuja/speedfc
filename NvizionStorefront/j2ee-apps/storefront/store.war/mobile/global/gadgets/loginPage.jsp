<%--
  This page renders the login form.

  Page includes:
    None

  Required parameters:
    passwordSent
      tells us whether the customer has had a temporary password
      sent to their email
    checkoutLogin
      If true, sets the formhandler, error/success URLs, and proper form
      inputs to the CheckoutProfileFormHandler.  Otherwise,
      B2CProfileFormHandler is used.

  Optional parameters:
    None
--%>

<dsp:page>


  <dsp:importbean bean="/atg/store/profile/SessionBean" var="sessionbean"/>

  <%-- See which formHandler we are using, and set all necessary values --%>
  <dsp:getvalueof var="checkoutLogin" param="checkoutLogin" />

  <c:choose>
    <c:when test="${checkoutLogin == 'true'}">
      <dsp:importbean bean="/atg/store/profile/CheckoutProfileFormHandler" var="formhandler"/>
      <c:set var="handler" value="CheckoutProfileFormHandler"/>
      <c:set var="emailField" value="${handler}.emailAddress"/>
      <dsp:getvalueof var="emailValue" bean="CheckoutProfileFormHandler.emailAddress"/>
      <c:if test="${empty emailValue}">
        <dsp:getvalueof var="emailValue" bean="/atg/userprofiling/Profile.email"/>
      </c:if>
      <c:set var="loginField" value="${handler}.returningCustomer"/>
    </c:when>
    <c:otherwise>
      <dsp:importbean bean="/atg/userprofiling/B2CProfileFormHandler" var="formhandler"/>
      <c:set var="handler" value="B2CProfileFormHandler"/>
      <c:set var="emailField" value="${handler}.value.login"/>
      <dsp:getvalueof var="emailValue" bean="B2CProfileFormHandler.value.login"/>
      <c:set var="loginField" value="${handler}.login"/>
    </c:otherwise>
  </c:choose>
  <c:set var="required" value="true"/>

  <c:if test="${empty emailValue}">
    <dsp:getvalueof var="paramEmailValue" param="email"/>
    <c:if test="${not empty paramEmailValue}">
      <c:set var="emailValue" value="${paramEmailValue}"/>
    </c:if>
  </c:if>

  <dsp:importbean bean="/atg/store/droplet/ProfileSecurityStatus"/>
  <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

  <dsp:getvalueof var="currentLocale" vartype="java.lang.String" bean="/atg/dynamo/servlet/RequestLocale.localeString"/>
  <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="${handler}.formExceptions"/>
  <dsp:getvalueof var="passwordSent" param="passwordSent" />

  <%-- Handle form exceptions --%>
  <jsp:useBean id="errorMap" class="java.util.HashMap"/>
  <c:if test="${not empty formExceptions}">
    <c:forEach var="formException" items="${formExceptions}">
      <dsp:param name="formException" value="${formException}"/>
      <%-- Check the error message code to see what we should do --%>
      <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
      <c:choose>
        <c:when test="${'missingEmail' eq errorCode}">
          <c:set target="${errorMap}" property="email" value="missing"/>
        </c:when>
        <c:when test="${'missingRequiredValue' eq errorCode}">
          <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
          <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
        </c:when>
        <c:when test="${'invalidLogin' eq errorCode || 'invalidEmailAddress' eq errorCode}">
          <c:set target="${errorMap}" property="email" value="invalid"/>
        </c:when>
        <c:when test="${'invalidPassword' eq errorCode}">
          <c:set target="${errorMap}" property="badPassword" value="invalid"/>
        </c:when>
      </c:choose>
    </c:forEach>
  </c:if>

  <%--
    Check Profile's security status. If user is logged in from cookie,
    display default values, i.e. profile's email address in this case, otherwise
    do not populate form handler with profile's values.
  --%>

  <dsp:droplet name="ProfileSecurityStatus">
    <dsp:oparam name="anonymous">
      <dsp:setvalue bean="${handler}.extractDefaultValuesFromProfile"
                    value="false"/>
    </dsp:oparam>
    <dsp:oparam name="autoLoggedIn">
      <dsp:setvalue bean="${handler}.extractDefaultValuesFromProfile"
                    value="true"/>
    </dsp:oparam>
  </dsp:droplet>

  <dsp:form action="${originatingRequest.requestURI}" method="post"
            id="atg_store_registerLoginForm">

    <%-- Set successURL --%>
    <c:choose>
      <c:when test="${checkoutLogin == 'true'}">
        <c:url value="../checkout/shipping.jsp" var="successURL" scope="page">
          <c:param name="locale" value="${currentLocale}"/>
        </c:url>
        <dsp:input bean="CheckoutProfileFormHandler.loginSuccessURL" type="hidden"
                   value="${successURL}"/>
      </c:when>
      <c:otherwise>
        <c:url value="../myaccount/profile.jsp" var="successURL" scope="page">
          <c:param name="locale" value="${currentLocale}"/>
        </c:url>
        <dsp:input bean="B2CProfileFormHandler.loginSuccessURL" type="hidden"
                   value="${successURL}"/>
      </c:otherwise>
    </c:choose>

    <%-- Set errorURL --%>
    <c:url value="${pageContext.request.requestURI}" var="errorURL" context="/">
      <c:if test="${checkoutLogin == 'true'}">
        <c:param name="checkoutLogin" value="${checkoutLogin}"/>
      </c:if>
      <c:param name="loginErrors" value="true"/>
    </c:url>
    <dsp:input type="hidden" bean="${handler}.loginErrorURL" value="${errorURL}"/>

    <ul class="mobile_store_mobileList mobile_store_displayList" role="presentation">
      <c:if test="${not empty errorMap['email'] || not empty errorMap['badPassword']}">
        <li class="mobile_store_error_state errorHeading">
          <span class="errorMessage oneLineError">
            <p class="noMargin"><fmt:message key="mobile.form.validation.invalidLoginCred" /></p>
          </span>
        </li>
      </c:if>
      <c:if test="${param.passwordSent}">
        <li class="mobile_store_error_state errorHeading">
          <span class="errorMessage oneLineError">
            <p class="noMargin"><fmt:message key="mobile.myaccount_login.passwordSent"/></p>
          </span>
        </li>
      </c:if>
        <%-- Check for errors in Email, or if login credentials are wrong --%>
        <c:choose>
          <c:when test="${not empty errorMap['login'] || not empty errorMap['emailAddress']}">
            <c:set var="liClassEmail" value="mobile_store_error_state"/>
          </c:when>
          <c:when test="${not empty errorMap['email'] || not empty errorMap['badPassword'] || param.passwordSent}">
            <c:set var="liClassEmail" value="mobile_store_error_state"/>
            <c:set var="isWrongAccount" value="roundedTopBorder"/>
            <%--<c:set var="inputClassEmail" value='mobile_store_textBoxFormError' />--%>
          </c:when>
          <c:otherwise>
            <%-- Do nothing --%>
          </c:otherwise>
        </c:choose>
        <li class="${liClassEmail} ${isWrongAccount} rightBorder">
          <div class="roundedTop">
            <div class="content">
               <%-- Pre-populate the email field --%>
              <c:set var="populatedEmail" value="${(!empty emailValue) ? emailValue : sessionbean.values['rememberedEmail']}"/>
              <fmt:message var="emailPlaceholder" key="common.loginEmailAddress" />
              <dsp:input bean="${emailField}"
                         iclass="${inputClassEmail} mobile_store_textBoxForm"
                         required="${required}" type="email"
                         id="atg_store_registerLoginEmailAddress"
                         value="${populatedEmail}"
                         placeholder="${emailPlaceholder}"
                         autocapitalize="off"
                         maxlength="40"/>
            </div>
            <c:if test="${not empty errorMap['login'] || not empty errorMap['emailAddress']}">
              <span class="errorMessage">
                <p>
                  <fmt:message key="mobile.form.validation.missing"/>
                </p>
              </span>
            </c:if>
          </div>
        </li>
        <%-- Check for errors in Password, or if login credentials are wrong --%>
        <c:choose>
          <c:when test="${(not empty errorMap['password']) || (passwordSent == 'true')}">
            <c:set var="liClassPass" value="mobile_store_error_state"/>
          </c:when>
          <c:when test="${not empty errorMap['email'] || not empty errorMap['badPassword']}">
            <c:set var="liClassPass" value="mobile_store_error_state"/>
            <%--<c:set var="inputClassPass" value='mobile_store_textBoxFormError' />--%>
          </c:when>
          <c:otherwise>
            <%-- Do nothing --%>
          </c:otherwise>
        </c:choose>
        <li class="${liClassPass} rightBorder">
          <div>
            <div class="content">
              <dsp:input bean="${handler}.value.password"
                         iclass="${inputClassPass} mobile_store_textBoxForm"
                         type="password" required="${required}" value=""
                         id="atg_store_registerLoginPassword"
                         maxlength="35">
                <fmt:message var="password" key="common.loginPassword" />
                <dsp:tagAttribute name="placeholder" value="${password}"/>
              </dsp:input>
            </div>
            <c:choose>
              <c:when test="${passwordSent == 'true'}">
                <span class="errorMessage">
                  <p>
                    <fmt:message key="mobile.passwordReset.paste"/>
                  </p>
                </span>
              </c:when>
              <c:when test="${not empty errorMap['password']}">
                <span class="errorMessage">
                  <p>
                    <fmt:message key="mobile.form.validation.missing"/>
                  </p>
                </span>
              </c:when>
            </c:choose>
          </div>
        </li>
      <c:if test="${not empty errorMap['email'] || not empty errorMap['badPassword']}" >
          <li class="withArrow mobile_store_error_state restorePassLink">
            <dsp:a iclass="mobile_store_forgotPassLogin block" page="/mobile/myaccount/passwordReset.jsp" title="${forgotPasswordTitle}">
              <div>
                <%-- Persist the checkoutLogin parameter --%>
                <dsp:param name="checkoutLogin" value="${checkoutLogin}"/>
                <fmt:message key="mobile.button.passwordResetText"/>
              </div>
            </dsp:a>
          </li>
      </c:if>
      <li>
        <div class="content">
          <div class="checkBox">
              <dsp:input id="mobile_store_autoLogin" type="checkbox" bean="${handler}.value.autoLogin" checked="true"/>
              <label class="mobile_store_myaccount_personalCheckBox" for="mobile_store_autoLogin" onclick="">
                <fmt:message key="mobile.checkout_login.remember"/>
              </label>
          </div>
        </div>
      </li>
    </ul>
    <div class="mobile_store_myaccount_button" >
      <fmt:message key="myaccount_login.submit" var="loginCaption"/>
      <dsp:input bean="${loginField}" id="mobile_store_loginButton" iclass="mainActionBtn" type="submit"
                 value="${loginCaption}"/>
    </div>
  </dsp:form>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/global/gadgets/loginPage.jsp#3 $$Change: 692002 $--%>
