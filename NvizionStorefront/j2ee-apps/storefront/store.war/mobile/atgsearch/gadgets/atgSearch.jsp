<%-- 
  Execute an ATG Search request

  Required parameters:
    None

  Optional parameters:
    q
      search query
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/multisite/Site"/>
  <dsp:importbean bean="/atg/commerce/search/StoreBrowseConstraints"/>

  <%-- this is the form that shows --%>
  <dsp:form method="post" formid="mobile_store_searchForm" id="mobile_store_searchForm">
    <fmt:message var="hintText" key="common.button.searchText"/>

   <%-- If there is already a search query, make sure it is displayed in the search input --%>
   <dsp:getvalueof var="question" param="q"/>

    <dsp:input iclass="mobile_store_searchInput" bean="QueryFormHandler.searchRequest.question"
      type="text" autocomplete="off" value="${question}" placeholder="${hintText}"
      onfocus="searchFocus(true);" onblur="searchFocus(false);"/>
	
	  <%-- Search the current site --%>
	  <dsp:input bean="QueryFormHandler.searchRequest.dynamicTargetSpecifier.siteIdsArray" type="hidden" beanvalue="Site.id"/>

    <%-- for sorting --%>
    <dsp:input bean="QueryFormHandler.searchRequest.docSort" type="hidden" value="relevance"/>
    <dsp:input bean="QueryFormHandler.searchRequest.docSortOrder" type="hidden" value="descending"/>
    <dsp:input bean="QueryFormHandler.searchRequest.multiSearchSession" type="hidden" value="true"/>
    <dsp:input bean="QueryFormHandler.searchRequest.saveRequest" type="hidden" value="true"/>

    <%-- Use the site's default number of results shown, or default to 6 --%>
	  <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="Site.defaultPageSize"/>
	  <c:if test="${empty pageSize}">
	    <c:set var="pageSize" value="6"/>
	  </c:if>
    <dsp:input type="hidden" bean="QueryFormHandler.searchRequest.pageSize" value="${pageSize}"/>

    <dsp:input type="hidden" bean="QueryFormHandler.successURL" value="${contextPrefix}/atgsearch/atgSearchResults.jsp"/>

    <%-- Execute the search --%>
    <dsp:input type="hidden" bean="QueryFormHandler.search" value=""/>

  </dsp:form>
  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/atgsearch/gadgets/atgSearch.jsp#3 $$Change: 692002 $ --%>

