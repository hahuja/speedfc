<%--
Renders a list of facet values

  Required parameters:
    facetHolder
      the current facet holder
    type
      the 'type' of value being selected. Used internally to identify this list.
    id 
      the value for the 'id' attribute of the rendered picker
  
  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/multisite/Site"/>
  <dsp:importbean bean="/atg/commerce/search/refinement/RefinementValueDroplet"/>

  <dsp:getvalueof var="facetHolder" param="facetHolder"/>
  <dsp:getvalueof var="type" param="type"/>
  <dsp:getvalueof var="id" param="id"/>

  <select id="${id}" name="${type}" multiple="multiple" onchange="facetSelect(event);">
    
    <c:choose>
      <c:when test="${facetHolder.facet.refinementElement.property != 'ancestorCategories.$repositoryId' && currentFacetHolder.facet.refinementElement.label ne 'facet.label.Category'}">
        <c:forEach var="item" items="${facetHolder.facetValueNodes}">
          <dsp:droplet name="RefinementValueDroplet">
            <dsp:param name="refinementValue" value="${item.facetValue.value}"/>
            <dsp:param name="refinementId" value="${facetHolder.facet.id}"/>
            <dsp:param name="locale" bean="/atg/userprofiling/Profile.PriceList.locale"/>
            <dsp:oparam name="output">
              <option value="${item.facetValue.value}">
                 <dsp:valueof param="displayValue"/>
              </option>
            </dsp:oparam>
          </dsp:droplet>
        </c:forEach>
      </c:when>
      <c:otherwise>
        <dsp:getvalueof var="rootCategoryId" bean="Site.defaultCatalog.rootNavigationCategory.id"/>
  			<c:forEach var="item" items="${facetHolder.facetValueNodes}">
  				<dsp:droplet name="CategoryLookup">
  				  <dsp:param name="id" value="${item.facetValue.value}"/>
  				  <dsp:oparam name="output">
  				    <dsp:getvalueof var="parentCategoryId" param="element.parentCategory.id"/>
  				    <%-- Only show top-level categories here --%>
  				    <c:if test="${parentCategoryId eq rootCategoryId}">
                <option value="${item.facetValue.value}">
                   <dsp:valueof param="element.displayName"/>
                </option>
  				    </c:if>
  			    </dsp:oparam>
  			  </dsp:droplet>
  		  </c:forEach>
      </c:otherwise>
    </c:choose>
  
  </select>
</dsp:page>