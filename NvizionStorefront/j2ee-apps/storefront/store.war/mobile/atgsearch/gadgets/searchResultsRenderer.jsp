<%--
  Render search results. If this is a refinement or paged request, re-run the search with the modified parameters

  Page includes:
    /mobile/atgsearch/gadgets/noResults.jspf display no result page
    /mobile/atgsearch/gadgets/showMore.jsp display "Show more" entry

  Required parameters:
    token
      the token which identifies this search

Optional parameters:
    query
      the search query to execute
    facetTrail
      the facetTrail to use for this refinement request
    pageNum
      the page number to retrieve for this paged request
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
  <dsp:importbean bean="/atg/dynamo/droplet/For"/>
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  <dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet"/>
	
	<dsp:getvalueof var="facetTrail" param="facetTrail"/>
  <dsp:getvalueof var="pageNum" param="pageNum"/>
  <dsp:getvalueof var="token" param="token"/>
  <dsp:getvalueof var="query" param="query"/>

  <dsp:getvalueof var="isRefinementRequest" value="${!empty facetTrail}"/>
  <dsp:getvalueof var="isPagedRequest" value="${!empty pageNum}"/>
  
  <c:if test="${isRefinementRequest || isPagedRequest}">
    <%-- If it is a refinement or a paged request, re-execute the search to get the new results --%>
    <%@include file="search.jspf"%>

    <dsp:getvalueof var="token" bean="QueryFormHandler.searchResponse.requestChainToken" />
		<dsp:getvalueof bean="QueryFormHandler.searchResponse.groupCount" var="totalCountVar"/>
		
		<%-- Because these results will be returned via AJAX, the updated total count must be in the response --%>
		<input type="hidden" name="count" value="${totalCountVar}"/>
	</c:if>

  <%-- Loop through the search results and render each one --%>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="QueryFormHandler.searchResponse.results"/>
		<dsp:oparam name="outputStart">
		</dsp:oparam>
		<dsp:oparam name="output">
			<dsp:getvalueof var="p_ProdId" param="element.document.properties.$repositoryId" />
			<%-- ATG Search results should only contain product ID, other properties are to be retrieved from the repository --%>
			<dsp:droplet name="ProductLookup">
				<dsp:param name="id" value="${p_ProdId}"/>
				<dsp:oparam name="output">
					<li class="withArrow">
						<%-- Pass off to the same jsp used by other pages for pricing items --%>
						<dsp:getvalueof var="mobileStorePrefix" bean="/atg/store/StoreConfiguration.mobileStorePrefix"/>
						<dsp:include page="${mobileStorePrefix}/browse/gadgets/productListRow.jsp" flush="true">
						 <dsp:param name="product" param="element"/>
						</dsp:include>
						<div class="shadow">
							&nbsp;
						</div>
					</li>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="empty"> <%-- No items found --%>
      <%@include file="noResults.jspf"%>
		</dsp:oparam>
		<dsp:oparam name="outputEnd">
		    <dsp:getvalueof var="numResults" param="count"/>
		   <%-- Add extra padding to the results page if there are < 4 results
		        but not on paged requests --%>
		    <c:if test="${empty pageNum && (numResults < 4)}">
		      <dsp:droplet name="For">
            <dsp:param name="howMany" value="${4 - numResults}"/>
            <dsp:oparam name="output">
              <li></li>
            </dsp:oparam>
		      </dsp:droplet>
		    </c:if>
		    <c:if test="${empty pageNum}">
		      <dsp:getvalueof var="pageNum" value="1"/>
		    </c:if>
		    <%-- Offer to show more results --%>
		    <dsp:include page="showMore.jsp">
		      <dsp:param name="pageNum" value="${pageNum}"/>
		      <dsp:param name="pageSize" bean="QueryFormHandler.searchResponse.pageSize"/>
		      <dsp:param name="token" value="${token}"/>
		    </dsp:include>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/atgsearch/gadgets/searchResultsRenderer.jsp#3 $$Change: 692002 $ --%>