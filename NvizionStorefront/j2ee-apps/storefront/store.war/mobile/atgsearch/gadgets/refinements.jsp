<%--
  Renders the search refinements subheader.

  Page includes:
    /mobile/atgsearch/gadgets/facetsRenderer.jsp renderer of facets values

  Required parameters:
    token
      the token for the executed query
    facetHolders
      the array of available facets

  Optional parameters:
    None
--%>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
	<dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet" />
	<dsp:importbean bean="/atg/store/droplet/FacetDisplayDroplet"/>

  <dsp:getvalueof var="facetHolders" param="facetHolders"/>
	
	<%-- top-level facets, hidden by default --%>
	<div id="mobile_store_refine" class='hidden'>

	  <div id="mobile_store_refine_header">
      <dsp:getvalueof bean="QueryFormHandler.searchResponse.groupCount" var="totalCountVar"/>
      <c:if test="${empty totalCountVar}"><c:set var="totalCountVar" value="0"/></c:if>
      <button id="mobile_store_toggleSearchResults" onclick="toggleRefinements(false);">
         <fmt:message key="mobile.search.results">
           <fmt:param value="${totalCountVar}"/>
         </fmt:message>
      </button>
      <span class="dividerBar left"></span>
      <button id="mobile_store_toggleRefine" onclick="toggleRefinements(true);">
        <%-- Only toggle the facet list if there are actually search results to display --%>
  	      <fmt:message key='mobile.search.refine'/>
  	  </button>
      <span class="dividerBar"></span>
  	</div>
		
		<div id="mobile_store_facets">
      <c:if test="${fn:length(facetHolders) > 0}">
        <ul class="mobile_store_mobileList">
          <%-- Loop through the available top-level facets and render each one --%>
          <c:forEach var="currentFacetHolder" items="${facetHolders}">
            <fmt:message key="${currentFacetHolder.facet.label}" var="facetLabel"/>
            <c:set var="facetId" value="${currentFacetHolder.facet.id}"/>
            <li>
              <label for="mobile_store_facet${facetId}" onclick="">
                <fmt:message key="mobile.search.facets.by">
                  <fmt:param value="${facetLabel}"/>
                </fmt:message>
                <dsp:include page="facetPicker.jsp">
                  <dsp:param name="id" value="mobile_store_facet${facetId}"/>
                  <dsp:param name="type" value="${facetId}"/>
                  <dsp:param name="facetHolder" value="${currentFacetHolder}"/>
                </dsp:include>
              </label>
            </li>
          </c:forEach>
        </ul>
      </c:if>

      <%-- Done/Cancel and "Refine More" buttons --%>
      <div class="mobile_store_buttonContainer">
  	    <dsp:getvalueof var="token" param="token"/>
  	    <input type="hidden" name="token" value="${token}"/>
  	    <button name="cancel" onclick="toggleRefinements(false);" class="mainActionBtn">
  	      <fmt:message key='common.button.cancelText'/>
  	    </button>
  	  </div>
	  </div>
	</div>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/atgsearch/gadgets/refinements.jsp#3 $$Change: 692002 $ --%>