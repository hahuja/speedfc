<%--
  Renders a page with no results. This page should only be reached when a null query has been entered.

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <fmt:message key="search_searchResults.title" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div id="mobile_store_results">
				<ul class="searchResults">
					<li>
					  <h4 class="mobile_store_pageHeader"><fmt:message key="mobile.search.refine.noResults"/></h4>
					</li>
					<li></li>
					<li></li>
			  </ul>
		  </div>
		</jsp:body>
	</crs:mobilePageContainer>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/search/noResults.jsp#1 $$Change: 683854 $--%>