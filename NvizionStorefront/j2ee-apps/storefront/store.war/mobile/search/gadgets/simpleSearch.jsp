<%--
  This page fragment is intended to be embedded on any page that requires a search capability.

  Page includes:
    /mobile/search/noResults.jsp render no result page
    /mobile/search/searchResults.jsp renderer of found products

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>
  <dsp:importbean bean="/atg/multisite/Site"/>

  <dsp:form method="post" id="simpleSearch" formid="simplesearchform">
    <fmt:message var="hintText" key="common.button.searchText"/>

	  <dsp:input type="hidden" bean="ProductSearch.errorURL" value="${contextPrefix}/search/noResults.jsp"/>
    <dsp:input type="hidden" bean="ProductSearch.successURL" value="${contextPrefix}/search/searchResults.jsp"/>
    <dsp:input bean="ProductSearch.currentPage" type="hidden" value="1"/>
    <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="Site.defaultPageSize"/>
	  <c:if test="${empty pageSize}">
	    <c:set var="pageSize" value="6"/>
	  </c:if>
    <dsp:input bean="ProductSearch.resultsPerPage" type="hidden" value="${pageSize}"/>
    <%-- maxResultsPerPage is an unfortunate but necessary lexical redundancy --%>
    <dsp:input bean="ProductSearch.maxResultsPerPage" type="hidden" value="${pageSize}"/> 
    <dsp:input type="hidden" bean="ProductSearch.dummySearchText" value=""/>
	  <input type="hidden" name="searchExecByFormSubmit" value="true">

    <dsp:input iclass="mobile_store_searchInput" bean="ProductSearch.searchInput" type="text"
      autocomplete="off" placeholder="${hintText}" onfocus="searchFocus(true);" onblur="searchFocus(false);"/>
  
  	<dsp:input bean="ProductSearch.search" type="hidden" value="search"/>
  
  </dsp:form>
  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/search/gadgets/simpleSearch.jsp#3 $$Change: 692002 $ --%>
