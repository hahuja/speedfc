<%--
  Renders additional search results

  Page includes:
    /mobile/browse/gadgets/productListRow.jsp renderer of product's row
    /mobile/search/gadgets/showMore.jsp renderer of "More" button

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>+
  <dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>
	<dsp:setvalue bean="ProductSearch.currentPage" paramvalue="page"/>
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" bean="ProductSearch.searchResults"/>
		<dsp:oparam name="output">
			<li>
				<dsp:include page="/mobile/browse/gadgets/productListRow.jsp" flush="true">
					<dsp:param name="product" param="element"/>
				</dsp:include>
			</li>
		</dsp:oparam>	
		<dsp:oparam name="outputEnd">
					<dsp:include page="/mobile/search/gadgets/showMore.jsp" />
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/search/gadgets/moreResults.jsp#3 $$Change: 692002 $--%>