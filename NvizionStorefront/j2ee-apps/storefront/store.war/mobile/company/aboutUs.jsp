<%--
  This page is the company's "About Us" page.
  General information about the company.

Page includes:
    None

  Required Parameters:
    None

  Optional Parameters:
    None
--%>
<dsp:page>
  <fmt:message key="navigation_tertiaryNavigation.aboutUs" var="pageTitle"/>
  <crs:getMessage var="storeName" key="common.storeName"/>
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div class="mobile_store_infoHeader">
        <fmt:message key = "company_aboutUs.ourHistory"/>
      </div>
      <div class="mobile_store_infoContent">
        <fmt:message key="mobile.company_aboutUs.aboutUs"/>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/company/aboutUs.jsp#3 $$Change: 692002 $ --%>