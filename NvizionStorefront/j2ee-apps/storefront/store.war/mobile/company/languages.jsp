<%--
  Displays a list of available languages

  Page includes:
    None

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>

  <%-- Page redirect logic must be outside mobilePageContainer --%>
	<dsp:getvalueof var="localeParam" param="locale"/>

	<%-- If user has already selected a language, go back to the "More" page --%>
	<c:if test="${!empty localeParam}">
	  <c:redirect url="moreInfo.jsp"/>
	</c:if>

  <fmt:message key="navigation_languages.languagesTitle" var="pageTitle"/>

	<crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>

	    <dsp:importbean bean="/atg/dynamo/droplet/ComponentExists" />
		  <dsp:importbean bean="/atg/store/droplet/DisplayLanguagesDroplet" />
		  <dsp:importbean bean="/atg/multisite/Site"/>

			<dsp:droplet name="ComponentExists">
			  <dsp:param name="path" value="/atg/modules/InternationalStore" />
			  <dsp:oparam name="true">
			    <%--
			      DisplayLanguagesDroplet takes a list of language keys, and returns a
			      list of objects associating those keys with a display string
			      representing the language.
      
			      Input Parameters:
			        languages - Available language codes for a particular site, e.g
			                    [en,es]
        
			        countryCode - The country code e.g 'US'
        
			      Open Parameters:
			        output - Serviced when there are no errors
        
			      Output Parameters:
			        currentSelection - The index of the currently selected locale in 
			                           displayLanguages according to the request
        
			        displayLanguages - A list of objects associating the language codes
			                           with display languages           
			    --%>
			    <dsp:droplet name="DisplayLanguagesDroplet">
			      <dsp:param name="languages" bean="Site.languages" />
			      <dsp:param name="countryCode" bean="Site.defaultCountry" />
			      <dsp:oparam name="output">
			        <dsp:getvalueof id="currentSelection" param="currentSelection"/>
			        <dsp:getvalueof var="displayLanguages" param="displayLanguages"/>
        
			        <%-- Check if there are alternate languages available for this Site --%>
			        <c:if test="${not empty displayLanguages}">

			          <%-- Render each alternate languages display name --%>
			          <div class="mobile_store_displayList" id="mobile_store_newBillingAddress">
                  <ul id="mobile_store_languages" class="mobile_store_mobileList">
				          <c:forEach var="language" items="${displayLanguages}" varStatus="languageStatus">
				            <c:set var="isSelected" value="${languageStatus.index == currentSelection ? 'checked' : ''}"/>
            
				            <%-- 
				              If the language is the current language just render text,
				              otherwise render a clickable link
				            --%>
				            <li>
					            <div class="content">
						            <input name="language" id="lang_${languageStatus.index}" type="radio" onclick="chooseLanguage(event);"
			                    value="${language.linkURL}" ${isSelected}/>
			                  <label for="lang_${languageStatus.index}" onclick="">
			                    <c:out value="${language.displayLanguage}"/>
			                  </label>
				              </div>
				            </li>
				          </c:forEach>
			            </ul>
                </div>
			        </c:if>
			      </dsp:oparam>
			    </dsp:droplet>
			  </dsp:oparam>
			</dsp:droplet>
		</jsp:body>
	</crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/company/languages.jsp#3 $$Change: 692002 $ --%>