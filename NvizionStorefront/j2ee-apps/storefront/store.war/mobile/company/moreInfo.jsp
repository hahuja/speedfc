<%--
  This page displays settings and link to additional into of the Merchant (privacy & terms, shipping & returns, etc)

  Page includes:
    /mobile/includes/gadgets/phone.jsp phone link page
    /mobile/includes/gadgets/email.jsp email link page

  Required parameters:
    None

  Optional parameters:
    None
 --%>
<dsp:page>
  <fmt:message key="mobile.moreInfo.pageTitle" var="pageTitle"/>
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      
		  <dsp:importbean bean="/atg/dynamo/droplet/ComponentExists" />
		
      <script type="text/javascript">
        // Only hide the footer on this page
        $(document).ready(function(){ $("#mobile_store_footer").hide();});
      </script>
      <div class="mobile_store_displayList">
      <div id="mobile_store_moreInfoContainer">
        <ul class="mobile_store_mobileList">
          <li>
            <ul class="triple-row">
              <li class="noBorder">
                <div class="content contact">
                  <span class="vAlign"><fmt:message key="navigation_tertiaryNavigation.contactUs"/></span>
                </div>
              </li>
					    <li><div class="content"><dsp:include page="/mobile/includes/gadgets/phone.jsp" /></div></li>
					    <li><div class="content"><dsp:include page="/mobile/includes/gadgets/email.jsp" /></div></li>
            </ul>
          </li>
          <li class="withArrow">
            <div class="content"><dsp:a page="stores.jsp" class="block"><fmt:message key="mobile.company_storeLocations.pageTitle"/></dsp:a></div>
          </li>
          <%-- Only show the "Languages" option if the EStore.International module is present--%>
          <dsp:droplet name="ComponentExists">
					  <dsp:param name="path" value="/atg/modules/InternationalStore" />
					  <dsp:oparam name="true">
         			<li class="withArrow">
			           <div class="content">
				           <dsp:a page="languages.jsp">
				             <fmt:message key="navigation_languages.languagesTitle"/>
				             <dsp:getvalueof var="requestLocale" bean="/OriginatingRequest.requestLocale"/>
				             <span id="mobile_store_currentLanguage"><dsp:valueof value="${requestLocale.capitalizedDisplayLanguage}"/></span>
				           </dsp:a>
                 </div>
			        </li>
			      </dsp:oparam>
			    </dsp:droplet>
          <li class="withArrow">
            <div class="content">
                <dsp:a page="/mobile/company/shipping.jsp" class="block"><fmt:message key="mobile.company_shippingAndReturns.pageTitle"/></dsp:a>
            </div>
          </li>
          <li class="withArrow">
            <div class="content">
                <dsp:a page="terms.jsp" class="block"><fmt:message key="mobile.company_privacyAndTerms.pageTitle"/></dsp:a>
            </div>
          </li>
          <li class="withArrow">
            <div class="content">
	            <dsp:importbean bean="/atg/dynamo/servlet/dafpipeline/MobileDetectionInterceptor"/>
	            <dsp:getvalueof bean="MobileDetectionInterceptor.enableFullSiteParam" var="enableFullSiteParam"/>
	            <dsp:a page="/index.jsp" class="block">
	              <dsp:param name="${enableFullSiteParam}" value="true"/>
	              <fmt:message key="mobile.moreInfo.fullSite"/>
	            </dsp:a>
            </div>
          </li>
          <li class="withArrow">
            <div class="content">
                <dsp:a page="aboutUs.jsp" class="block"><fmt:message key="navigation_tertiaryNavigation.aboutUs"/></dsp:a>
            </div>
          </li>
        </ul>
      </div>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/company/moreInfo.jsp#3 $$Change: 692002 $ --%>
