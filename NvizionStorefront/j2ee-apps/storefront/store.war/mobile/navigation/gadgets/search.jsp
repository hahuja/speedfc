<%--
  Connects necessary pages depending on search type (if ATG Search is installed or no)

  Page includes:
    /mobile/atgsearch/gadgets/atgSearch.jsp atg search
    /mobile/search/gadgets/simpleSearch.jsp simple search

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="atg/store/StoreConfiguration" />
  <dsp:getvalueof var="atgSearchInstalled" bean="StoreConfiguration.atgSearchInstalled" />
  <c:choose>
    <c:when test="${atgSearchInstalled == 'true'}">
      <dsp:include page="/mobile/atgsearch/gadgets/atgSearch.jsp" />
    </c:when>
    <c:otherwise>
      <dsp:include page="/mobile/search/gadgets/simpleSearch.jsp" />
    </c:otherwise>
  </c:choose>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/navigation/gadgets/search.jsp#1 $$Change: 683854 $ --%>
