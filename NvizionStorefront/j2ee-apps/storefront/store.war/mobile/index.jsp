<%--
  Default welcome page

  Page includes:
    /mobile/promo/gadgets/homePromotionalItemRenderer.jsp renderer of product details
    /mobile/global/util/hasRecsInstalled.jsp checks if Recs has been installed
    /mobile/global/util/loggedIn.jsp checks is the user logged in
    /mobile/global/util/currencyCode.jsp renderer of localized price

  Required parameters:
    None

  Optional parameters:
    None
 --%>
<dsp:page>
  <dsp:importbean bean="/atg/dynamo/droplet/multisite/SiteLinkDroplet"/>
  
	<fmt:message key="common.home" var="pageTitle" />
	<crs:mobilePageContainer titleString="${pageTitle}">
		<jsp:body>
		    <dsp:getvalueof var="mobileStorePrefix"
      bean="/atg/store/StoreConfiguration.mobileStorePrefix" />
			<div id="mobile_store_homeTopSlot">
					<div class="mobile_store_shadowTopDown"></div>
                <dsp:include
					page="promo/gadgets/homePromotionalItemRenderer.jsp" />
			</div>
			<div id="mobile_store_homeBottomSlot">
				<div class="mobile_store_shadowTopDown"></div>
				<div id="mobile_store_homeBottomSlotContent"></div>
				<div id="mobile_store_homeBottomSlotProductDetails"></div>
		 </div>
    <dsp:include
				page="${mobileStorePrefix}/global/util/hasRecsInstalled.jsp" />
    <dsp:getvalueof var="recsInstalled" param="recsInstalled" />
    <dsp:include
        page="${mobileStorePrefix}/global/util/loggedIn.jsp" />
    <dsp:getvalueof var="loggedIn" param="loggedIn" />
    <dsp:include page="${mobileStorePrefix}/global/util/currencyCode.jsp" />
    <dsp:getvalueof var="currencyCode" param="currencyCode" />
    <dsp:getvalueof var="currentSiteId" bean="/atg/multisite/Site.id"/>
  <dsp:droplet name="SiteLinkDroplet" siteId="${currentSiteId}" path="/"
               var="siteLink">
    <dsp:oparam name="output">
      <dsp:getvalueof var="siteBaseURL" value="${siteLink.url}"/>
    </dsp:oparam>
  </dsp:droplet>
		  <script type="text/javascript">
		  $(document).bind("ready", loadHomePage("${recsInstalled}","${siteBaseURL}","${loggedIn}","${currencyCode}"));
      </script>
		</jsp:body>
	</crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/index.jsp#3 $$Change: 692002 $ --%>
