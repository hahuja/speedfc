<%--
  Renders main category page

  Required parameters:
   categoryId
     Repository Id of the Category being traversed
      
  Optional parameters:
   facetTrail
     ':' separated list representing the navigation trail
   addFacet
     A facet to be added to the facet trail
   categoryNavIds
     Indicates the categories navigated to get to the current category.
   facetSearch
     Indicates this request was made by clicking a facet link.
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/store/StoreConfiguration" />
  <dsp:importbean bean="/atg/multisite/Site"/>
  
  <dsp:getvalueof id="categoryNavIds" param="categoryNavIds"/>
  <dsp:getvalueof var="atgSearchInstalled" bean="StoreConfiguration.atgSearchInstalled" />

  <%--
    Get the category according to the ID
 
    Input Parameters:
      id - The ID of the category we want to look up
   
    Open Parameters:
      output - Serviced when no errors occur
      error - Serviced when an error was encountered when looking up the category
    
    Output Parameters:
      element - The category whose ID matches the 'id' input parameter  
  --%>
  <dsp:droplet name="CategoryLookup">
    <dsp:param name="id" param="categoryId"/>      
    <dsp:oparam name="output"> 
      <%-- Send 'Category Browsed' event --%>
      <dsp:droplet name="/atg/commerce/catalog/CategoryBrowsed">
        <dsp:param name="eventobject" param="element"/>    
      </dsp:droplet>
            
      <%-- Update last browsed category in profile --%>
      <dsp:include page="gadgets/categoryLastBrowsed.jsp">
        <dsp:param name="lastBrowsedCategory" param="categoryId"/>
      </dsp:include>
      
      <%-- Track the user's navigation to provide the appropriate breadcrumbs --%>
      <dsp:include page="gadgets/breadcrumbsNavigation.jsp">
        <dsp:param name="categoryNavIds" value="${categoryNavIds}"/>
        <dsp:param name="defaultCategory" param="element"/>
      </dsp:include>
      
      <%-- Delegate the the appropriate page depending on whether or not ATG Search is installed --%>
      <c:choose>
        <c:when test="${atgSearchInstalled}" >
          <dsp:include page="/atgsearch/categorySearchResults.jsp">
            <dsp:param name="category" param="element"/>                   
            <dsp:param name="includeRecommendations" value="true"/>
          </dsp:include>
        </c:when>
        <c:otherwise>
          <dsp:include page="/search/categorySearchResults.jsp">
            <dsp:param name="category" param="element"/>                         
            <dsp:param name="includeRecommendations" value="true"/>      
          </dsp:include>
        </c:otherwise>
      </c:choose>
        
    </dsp:oparam>
        
    <%-- Empty Output --%>
    <dsp:oparam name="empty"> 
      <dsp:include page="/browse/gadgets/categoryNotAvailable.jsp">
        <dsp:param name="categoryId" param="categoryId"/>
      </dsp:include>
    </dsp:oparam>
    
    <%-- When an error message is passed back from the bean --%>
    <dsp:oparam name="error">
      <dsp:include page="/browse/gadgets/categoryNotAvailable.jsp">
        <dsp:param name="errorMsg" param="errorMsg"/>
      </dsp:include>
    </dsp:oparam>
    
    <%-- When the wrong site parameter is passed back from the bean --%>
    <dsp:oparam name="wrongSite">
      <dsp:include page="/browse/gadgets/categoryNotAvailable.jsp">
        <dsp:param name="categoryId" param="categoryId"/>
        <dsp:param name="site" bean="Site.name"/>
      </dsp:include>
    </dsp:oparam>
  </dsp:droplet>    
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/category.jsp#1 $$Change: 683854 $ --%>
