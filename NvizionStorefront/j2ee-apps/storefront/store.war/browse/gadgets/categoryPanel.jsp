<%--
  This page renders links to child categories pages for selected category.
  
  Required Parameters:
    None.         

  Optional Parameters:
    None.
--%>
<%--
  This page renders links to child categories pages for selected category.  
--%>
<dsp:page>
  <dsp:importbean bean="/atg/targeting/TargetingFirst"/>
  <dsp:importbean bean="/atg/repository/seo/CatalogItemLink"/>
      
  <dsp:getvalueof var="atgSearchInstalled" bean="/atg/store/StoreConfiguration.atgSearchInstalled"/>
  
  <c:if test="${atgSearchInstalled == 'true'}">
    
    <%--
      TargetingFirst is used to perform a targeting operation with
      the help of its targeter. We pick the first item from the array
      returned by the targeting operation. We use this to retrieve ID
      of the global category refineElement.
       
      Input Parameters:
        howMany
          Indicates the maximum number of items to display 
        targeter
          Specifies the targeter service that will perform the targeting.                          
        elementName
          The name of the targetedProduct
       
      Open Parameters:
        output
          At least 1 target was found.
       
      Output Parameters:
        element
          The result of a target operation.
    --%>
    <dsp:droplet name="TargetingFirst">
      <dsp:param name="howMany" value="1"/>
      <dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/RefinementRepository/GlobalCategoryFacet"/>

      <dsp:oparam name="output">
        <dsp:getvalueof id="refElemRepId" idtype="String" param="element.repositoryId"/>
      </dsp:oparam>
    </dsp:droplet>
  </c:if>                                           

  <div id="atg_store_categories">
    <dsp:getvalueof var="navHistory" vartype="java.util.Collection" scope="page" bean="/atg/commerce/catalog/CatalogNavHistory.navHistory"/>
    <dsp:getvalueof var="childCategories" vartype="java.util.Collection" scope="page" param="category.childCategories"/>
    
    <%-- Link to grand-parent is displayed for non top-level leaf categories --%>
    <c:if test="${fn:length(navHistory) > 2 && empty childCategories}">
      <h3 class="atg_store_categoryParent">
        <dsp:include page="linkToNavItem.jsp">
          <dsp:param name="itemIndex" value="${fn:length(navHistory) - 2}"/>
        </dsp:include>
      </h3>
    </c:if>
    <%-- Link to a parent is always displayed --%>
    <h3 class="atg_store_categoryCurrent${empty childCategories ? 'Leaf' : ''}">
      <dsp:include page="linkToNavItem.jsp">
        <dsp:param name="itemIndex" value="${fn:length(navHistory) - 1}"/>
      </dsp:include>
    </h3>

    <div class="atg_store_facetsGroup_options_catsub">
      <%-- 
        Iterate over the list of subcategories and create link
        for every subcategory.
      --%>
      <ul>
        <c:forEach var="childCategory" items="${childCategories}" varStatus="childCategoriesStatus">
          <dsp:param name="childCategory" value="${childCategory}"/>
          <dsp:getvalueof var="selectedCategoryId" param="category.repositoryId" />
          <dsp:getvalueof var="childCategoryId" param="childCategory.repositoryId"/>
          
          <preview:repositoryItem item="${childCategory}">        
            <%--
              This droplet calculates a default site-aware URL 
              for the child category.
    
              Input parameters:
                item
                  Product catalog item an URL should be calculated for.
    
              Output parameters:
                url
                  URL calculated.
    
              Open parameters:
                output
                  Always rendered.
            --%>
            <dsp:droplet name="CatalogItemLink">
              <dsp:param name="item" param="childCategory"/>
              <dsp:oparam name="output">
                <dsp:getvalueof id="url" idtype="String" param="url"/>
                <%--
                  This droplet determines user's browser type.
              
                  Input parameters:
                    None.
              
                  Output parameters:
                    browserType
                      Specifies a user's browser type.
              
                  Open parameters:
                    output
                      Always rendered.
                --%>
                <dsp:droplet name="/atg/repository/seo/BrowserTyperDroplet">
                  <dsp:oparam name="output">
                    <dsp:getvalueof var="browserType" param="browserType"/>
                    <c:set var="isIndirectUrl" value="${browserType eq 'robot'}"/>
                  </dsp:oparam>
                </dsp:droplet>
              
                <li  onmouseover="this.className='selected';" onmouseout="this.className='';">

                  <%-- Create link for subcategory --%>
                  <dsp:a page="${url}">
                    <c:if test="${not isIndirectUrl}">
                      <%-- Only include addFacet param when ATG Search is installed --%>
                      <c:if test="${atgSearchInstalled == 'true'}">
                        <dsp:param name="addFacet" value="${refElemRepId}:${childCategoryId}"/>
                      </c:if>
                    </c:if>
                    <dsp:valueof param="childCategory.displayName"/>
                  </dsp:a>
                </li>
              </dsp:oparam>
            </dsp:droplet>
          </preview:repositoryItem>
        </c:forEach>
      </ul>
    </div>
  </div>  

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/categoryPanel.jsp#1 $$Change: 683854 $ --%>