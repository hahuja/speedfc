<%--
  This gadget renders all related categories for the product specified.

  Required parameters:
    product
      Specifies a product whose related categories should be displayed.

  Optional parameters:
    None.
--%>

<dsp:page>
  <dsp:getvalueof id="product" param="product"/>

  <dsp:importbean bean="/atg/store/droplet/CatalogItemFilterDroplet"/>
  <dsp:importbean bean="/atg/commerce/catalog/CategoriesForProductGroup"/>

  <%--
    This droplet finds all categories that include product specified from a content group.

    Input parameters:
      product
        Spcifies a product to be processed.

    Output parameters:
      associatedCategories
        Collection of associated categories.

    Open parameters:
      output
        Rendered when associated categories collection is not empty.
  --%>
  <dsp:droplet name="CategoriesForProductGroup">
    <dsp:param name="product" param="product"/>
    <dsp:oparam name="output">
      <%--
        This droplet filters out any categories in other catalogs.

        Input parameters:
          collection
            Specifies a collection of catalog items to be filtered.

        Output parameters:
          filteredCollection
            Filtered collection of catalog items.

        Open parameters:
          output
            Rendered when filtered collection is not empty.
      --%>
      <dsp:droplet name="CatalogItemFilterDroplet">
        <dsp:param name="collection" param="associatedCategories"/>
        <dsp:oparam name="output">
          <dsp:getvalueof var="filteredCollection" vartype="java.lang.Object" param="filteredCollection"/>

          <%-- Do we have something to display? If true, do it. --%>
          <c:if test="${not empty filteredCollection}">
            <dsp:getvalueof id="size" value="${fn:length(filteredCollection)}"/>
            <div id="atg_store_recommendedCategories">
              <h3>
                <fmt:message key="browse_recommendedCategories.recommend"/>
              </h3>
              <ul>
                <c:forEach var="filteredItem" items="${filteredCollection}" varStatus="filteredItemStatus">
                  <dsp:getvalueof id="count" value="${filteredItemStatus.count}"/>
                  <dsp:param name="cat" value="${filteredItem}"/>
                  <c:set var="classString">
                    <crs:listClass count="${count}" size="${size}" selected="false"/>
                  </c:set>
                  <%-- Check to see if category URL is empty. --%>
                  <dsp:getvalueof var="templateUrl" param="cat.template.url"/>
                  <c:choose>
                    <c:when test="${not empty templateUrl}">
                      <%--
                        This droplet builds an URL for the product catalog item specified.

                        Input parameters:
                          item
                            Specifies an item URL should be built for.

                        Output parameters:
                          url
                            URL constructed.

                        Open parameters:
                          output
                            Always rendered.
                      --%>
                      <dsp:droplet name="/atg/repository/seo/CatalogItemLink">
                        <dsp:param name="item" param="cat"/>
                        <dsp:oparam name="output">
                          <fmt:message var="linkTitle" key="browse_recommendedCategories.categoryLinkTitle"/>
                          <li class="<crs:listClass count="${count}" size="${size}" selected="false"/>">
                            <dsp:a href="${param.url}" title="${linkTitle}">
                              <dsp:valueof param="cat.displayName"/>
                            </dsp:a>
                          </li>
                        </dsp:oparam> <%-- End oparam output --%>
                      </dsp:droplet> <%-- End CatalogItemLink droplet --%>
                    </c:when>
                    <c:otherwise>
                      <%-- We can't find category URL, just display its name. --%>
                      <li class="<crs:listClass count="${count}" size="${size}" selected="false"/> disabled">
                        <dsp:valueof param="cat.displayName"/>
                      </li>
                    </c:otherwise>
                  </c:choose>
                </c:forEach><%-- End For Each Category --%>
              </ul>
            </div>
          </c:if>
        </dsp:oparam>
      </dsp:droplet><%-- End filter out categories --%>
    </dsp:oparam>
  </dsp:droplet><%-- End Categories for product group --%>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/recommendedCategories.jsp#1 $$Change: 683854 $--%>
