<%--
  This gadget renders action buttons for a product detail page.
  It can render 'Add to Comparisons' and 'Email a Friend' buttons.

  Required parameters:
    action
      Action to be rendered. May be one of the following:
        compare
        viewComparisons
        emailFriend
    product
      Currently viewed product.
    categoryId
      Currently viewed category.

  Optional parameters:
    None.
--%>

<dsp:page>
  <dsp:importbean bean="/atg/store/droplet/NullPropertiesCheck"/>
  <dsp:importbean bean="/atg/commerce/catalog/comparison/ProductListHandler"/>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>

  <dsp:getvalueof id="product" param="product"/>
  <dsp:getvalueof id="categoryId" param="categoryId"/>
  <dsp:getvalueof id="action" param="action"/>
  <dsp:getvalueof id="productId" idtype="java.lang.String" param="product.repositoryId"/>

  <%-- Select, which action should we display. --%>
  <c:choose>
    <c:when test="${action == 'compare'}">
      <%--
        We should display the Add to Comparisons button.

        This droplet calculates a default site-aware URL for the product catalog item specified.

        Input parameters:
          item
            Product catalog item an URL should be calculated for.

        Output parameters:
          url
            URL calculated.

        Open parameters:
          output
            Always rendered.
      --%>
      <dsp:droplet name="/atg/repository/seo/CatalogItemLink">
        <dsp:param name="item" param="product"/>
        <dsp:oparam name="output">
          <dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
        </dsp:oparam>
      </dsp:droplet>

      <%-- Construct an URL that will be used as a success/error URL. --%>
      <c:url var="url" value="${productUrl}">
        <c:param name="productId"><dsp:valueof param="product.repositoryId"/></c:param>
        <c:param name="categoryId"><dsp:valueof param="categoryId"/></c:param>
        <c:param name="categoryNavIds"><dsp:valueof param="categoryNavIds"/></c:param>
        <c:param name="navAction"><dsp:valueof param="navAction"/></c:param>
        <c:param name="navCount"><dsp:valueof param="navCount"/></c:param>
      </c:url>

      <%-- Button itself. --%>
      <fmt:message var="addToComparisonsText" key="browse_productAction.addToComparisonsSubmit"/>
      <dsp:input type="hidden" bean="ProductListHandler.addProductSuccessURL" value="${url}"/>
      <dsp:input type="hidden" bean="ProductListHandler.addProductErrorURL" value="${url}"/>
      <dsp:input type="hidden" bean="ProductListHandler.productID" paramvalue="product.repositoryId"/>
      <dsp:input type="submit" iclass="atg_store_textButton" bean="ProductListHandler.addProduct" value="${addToComparisonsText}" submitvalue="true" />
    </c:when>
    <c:when test="${action == 'viewComparisons'}">
      <%-- View Comparisons button. --%>
      <fmt:message var="linkTitle" key="browse_productAction.addToComparisonsSubmit"/>
      <dsp:a page="/comparisons/productComparisons.jsp" title="${linkTitle}">        
        <fmt:message  key="browse_productAction.addToComparisonsSubmit"/>
      </dsp:a>
    </c:when>
    <c:when test="${action == 'emailFriend'}">
      <%-- Email a Friend button should be displayed. --%>
      <dsp:getvalueof id="storeEmailAFriendEnabled" idtype="java.lang.Boolean"
                      bean="/atg/multisite/SiteContext.site.emailAFriendEnabled"/>
      <dsp:getvalueof id="productEmailAFriendEnabled" idtype="java.lang.Boolean"
                      param="product.emailAFriendEnabled"/>
      <%-- Display this button only if the current site and product allow to do this. --%>
      <c:if test="${storeEmailAFriendEnabled && productEmailAFriendEnabled}">
        <fmt:message var="linkTitle" key="browse_productAction.emailFriendTitle"/>
        <dsp:a page="/browse/emailAFriend.jsp?productId=${productId}&categoryId=${categoryId}"
               title="${linkTitle}" target="popupLarge">
          <fmt:message key="browse_productAction.emailFriendLink"/>
        </dsp:a>
      </c:if>
    </c:when>
    <c:otherwise>
      <%-- An invalid action is a programming error, not a runtime data error --%>
      <br />INVALID ACTION FOR productAction.jsp: <c:out value="${action}"/><br />
    </c:otherwise>
  </c:choose> 
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/productAction.jsp#1 $$Change: 683854 $ --%>
