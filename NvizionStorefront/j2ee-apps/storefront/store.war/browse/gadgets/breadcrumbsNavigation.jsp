<%--
  Track the user's category navigation to provide the appropriate breadcrumbs.
  
  Required Parameters:
    defaultCategory
      default category for the breadcrumb trail if no categoryNavIds provided         

  Optional Parameters:
    categoryNavIds
      ':' separated list representing the category navigation trail
         
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>

  <dsp:getvalueof var="categoryNavIds" param="categoryNavIds"/>

  <c:choose>
    <c:when test="${!empty categoryNavIds}">
      <c:forEach var="categoryNavId"
                 items="${fn:split(categoryNavIds, ':')}"
                 varStatus="status">

        <c:set var="navAction" value="push"/>

        <c:if test="${status.first}">
          <c:set var="navAction" value="jump"/>
        </c:if>

        <%--
          Get the category according to the ID
         
            Input Parameters:
              id - The ID of the category we want to look up
          
            Open Parameters:
              output - Serviced when no errors occur
          
            Output Parameters:
              element - The category whose ID matches the 'id' input parameter  
        --%>
        <dsp:droplet name="CategoryLookup">
          <dsp:param name="id" value="${categoryNavId}"/>
          <dsp:oparam name="output">
            <dsp:getvalueof var="currentCategory" param="element" vartype="java.lang.Object" scope="request"/>
            <%--
              Add category to the stack of visited locations.  
           
              Input Parameters:
                navAction - navigation action, either 'push' or 'jump'
                item - this is the item to add to the breadcrumb stack
            
              Open Parameters:
                None.
            
              Output Parameters:
                None.
             --%>
            <dsp:droplet name="/atg/commerce/catalog/CatalogNavHistoryCollector">
              <dsp:param name="item" value="${currentCategory}" />
              <dsp:param name="navAction" value="${navAction}" />
            </dsp:droplet>
          </dsp:oparam>
        </dsp:droplet>
      </c:forEach>
    </c:when>
    <c:otherwise>
      <%--
        If categoryNavIds is empty, use default category as
        an item.   
     
        Input Parameters:
          navAction - 'jump' means that we reset history and add defaultCategory as
            the first entry
          item - this is the item to add to the breadcrumb stack
      
        Open Parameters:
          None.
      
        Output Parameters:
          None.
       --%>
      <dsp:droplet name="/atg/commerce/catalog/CatalogNavHistoryCollector">
        <dsp:param name="item" param="defaultCategory" />
        <dsp:param name="navAction" value="jump" />
      </dsp:droplet>
    </c:otherwise>
  </c:choose>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/breadcrumbsNavigation.jsp#1 $$Change: 683854 $--%>
