<%--
  The page displays category featured products.
  
  Required parameters: 
    category
      The category repository item for which featured products are to be displayed.
      
  Optional parameters:
    None
 --%>
<dsp:page>

  <dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
  <dsp:importbean bean="/atg/store/droplet/CatalogItemFilterDroplet" />

  <%--
    This droplet filters out products that located in a wrong catalog.

    Input parameters:
      collection
        Collection of products to be filtered. In this case the list of
        category's related products.

    Output parameters:
      filteredCollection
        Resulting filtered collection.

    Open parameters:
      output
        Always rendered.
  --%>
  <dsp:droplet name="CatalogItemFilterDroplet">
    <dsp:param name="collection" param="category.relatedProducts" />

    <%-- Work with filtered collection --%>
    <dsp:oparam name="output">
      <div id="featured_products">
        <h3>
          <fmt:message  key="browse_featuredProducts.featuredItemTitle" />
        </h3>

        <%-- Display Related Products if you got 'em --%>
        <div id="atg_store_featured_prodList">
          
          <%--
            Calculate the number of available featured products to display. 
            Display not more than 4 featured products.
          --%>
          <dsp:getvalueof var="filteredItems" param="filteredCollection" /> 
          <c:set var="numberOfProducts" value="4" />
          <dsp:getvalueof  var="size" value="${fn:length(filteredItems)}" />
          <c:if test="${size < numberOfProducts}">
            <c:set var="numberOfProducts" value="${size}"/>
          </c:if>

          <ul class="atg_store_product">
            <c:forEach var="product" items="${filteredItems}" begin="0" 
                       end="${numberOfProducts}" varStatus="status">
              <dsp:getvalueof var="index" value="${status.index}"/>
              <dsp:getvalueof var="count" value="${status.count}"/>
              <dsp:getvalueof var="additionalClasses" vartype="java.lang.String" 
                              value="${(count % numberOfProducts) == 1 ? 'prodListBegin' : ((count % numberOfProducts) == 0 ? 'prodListEnd':'')}"/>
                                
              <li class="<crs:listClass count="${count}" size="${size}" selected="false"/>${empty additionalClasses ? '' : ' '}${additionalClasses}">
                <dsp:include page="/global/gadgets/productListRangeRow.jsp">
                  <dsp:param name="categoryId" param="category.repositoryId"/>
                  <dsp:param name="product" value="${product}" />
                  <dsp:param name="categoryNav" value="false" />
                </dsp:include>
              </li> 
            </c:forEach>
          </ul>                   
        </div>
      </div>
    </dsp:oparam>
  </dsp:droplet>
  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/featuredProducts.jsp#1 $$Change: 683854 $--%>
