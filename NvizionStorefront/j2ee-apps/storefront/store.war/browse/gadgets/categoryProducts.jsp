<%-- 
  Displays the products contained within the category whose ID matches the supplied categoryId.
             
  Required Parameters:
    category - Specifies the category for which child products should be displayed
    
  Optional Parameters:
    None
--%>

<dsp:page>

  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <dsp:getvalueof var="category" param="category" />

  <div id="atg_store_catSubProdList">
    <dsp:include page="/global/gadgets/productListRange.jsp">
      <dsp:param name="productArray" param="category.childProducts"/>
      <dsp:param name="productDivName" value="atg_store_prodList"/>
      <dsp:param name="p" value="${param.p}"/>
      <dsp:param name="categoryNavigation" value="${param.navigation}"/>
    </dsp:include>
  </div>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/categoryProducts.jsp#1 $$Change: 683854 $--%>