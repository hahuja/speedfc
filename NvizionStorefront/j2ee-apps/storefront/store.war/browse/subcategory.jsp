<%--
  Layout page for subcategory

  Required parameters:
   categoryId
     Repository Id of the Category being traversed
      
  Optional parameters:
   facetTrail
     ':' separated list representing the navigation trail
   addFacet
     A facet to be added to the facet trail
   categoryNavIds
     Indicates the categorys navigated to get to the current category.
   facetSearch
     Indicates this request was made by clicking a facet link.
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet"/>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  <dsp:importbean bean="/atg/commerce/search/StoreBrowseConstraints"/>
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/targeting/TargetingFirst"/>
  <dsp:importbean bean="/atg/multisite/Site"/>
  <dsp:importbean bean="/atg/store/StoreConfiguration" />
  
  <%-- Used to determine whether or not ATG Search is installed --%>
  <dsp:getvalueof var="atgSearchInstalled" bean="StoreConfiguration.atgSearchInstalled" />        
  <dsp:getvalueof var="categoryId" param="categoryId" />
  <dsp:getvalueof id="categoryNavIds" param="categoryNavIds"/>
  
  <%--
    Get the category according to the ID
 
    Input Parameters:
      id - The ID of the category we want to look up
   
    Open Parameters:
      output - Serviced when no errors occur
      error - Serviced when an error was encountered when looking up the category
    
    Output Parameters:
      element - The category whose ID matches the 'id' input parameter  
  --%>
  <dsp:droplet name="CategoryLookup">
    <dsp:param name="id" param="categoryId"/>
    
    <%-- Droplet output --%>    
    <dsp:oparam name="output">
      <%-- Send 'Category Browsed' event --%>
      <dsp:droplet name="/atg/commerce/catalog/CategoryBrowsed">
        <dsp:param name="eventobject" param="element"/>    
      </dsp:droplet>

      <%-- Update last browsed category in profile --%>
      <dsp:include page="gadgets/categoryLastBrowsed.jsp">
        <dsp:param name="lastBrowsedCategory" param="categoryId"/>
      </dsp:include>

      <%-- Set the categoryObject for individual gadgets to work on --%>
      <dsp:getvalueof var="categoryObject" param="element" vartype="java.lang.Object" scope="request"/>

      <%-- Track the user's navigation to provide the appropriate breadcrumbs --%>
      <dsp:include page="gadgets/breadcrumbsNavigation.jsp">
        <dsp:param name="categoryNavIds" value="${categoryNavIds}"/>
        <dsp:param name="defaultCategory" param="element"/>
      </dsp:include>

      <%-- Check for trailSize parameter. If not found, init to 0. Parameter is used for faceted Navigation --%>
      <c:choose>
        <c:when test="${(! empty param.trailSize) && (fn:length(fn:trim(param.trailSize)) > 0) }">
          <dsp:getvalueof var="trailSizeVar" param="trailSize"/>
        </c:when>
        <c:otherwise>
          <dsp:getvalueof var="trailSizeVar" value="0"/>
        </c:otherwise>
      </c:choose>
     
      <%-- Include categorySearchResults.jsp --%>
      <c:choose>
        <c:when test="${atgSearchInstalled}" >
          <dsp:include page="/atgsearch/categorySearchResults.jsp">
            <dsp:param name="category" value="${categoryObject}" />   
            <dsp:param name="includeRecommendations" value="false"/>        
          </dsp:include>
        </c:when>
        <c:otherwise>
          <dsp:include page="/search/categorySearchResults.jsp">
            <dsp:param name="category" value="${categoryObject}" />     
            <dsp:param name="includeRecommendations" value="false"/>      
          </dsp:include>
        </c:otherwise>
      </c:choose>
    </dsp:oparam>
    
    <%-- Empty Output --%>
    <dsp:oparam name="empty"> 
      <dsp:include page="/browse/gadgets/categoryNotAvailable.jsp">
        <dsp:param name="categoryId" param="categoryId"/>
      </dsp:include>
    </dsp:oparam>
    
    <%-- When an error message is passed back from the bean --%>
    <dsp:oparam name="error">
      <dsp:include page="/browse/gadgets/categoryNotAvailable.jsp">
        <dsp:param name="errorMsg" param="errorMsg"/>
      </dsp:include>
    </dsp:oparam>
    
    <%-- When the wrong site parameter is passed back from the bean --%>
    <dsp:oparam name="wrongSite">
      <dsp:include page="/browse/gadgets/categoryNotAvailable.jsp">
        <dsp:param name="categoryId" param="categoryId"/>
        <dsp:param name="site" bean="Site.name"/>
      </dsp:include>
    </dsp:oparam>  
  </dsp:droplet>  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/subcategory.jsp#2 $$Change: 684979 $--%>
