<%--
  This page is used to display the sort by drop down, pagination links, and product listings.

  Required parameters:
    productArray
      List of the products that will be displayed.
    p
      Current page number as required by pagination functionality.

  Optional parameters:
    viewAll
      Set value to true, if 'view all' is requested.
    isSimpleSearchResults
      A boolean indicating if the current search is respository search, when set to true sort by
      controls are disabled
    mode
      Set to either "icon" or "name" to display either the site icon or site name beside
      the product listings when the current site is not the site the product exists on.
    sort
      How the results should be sorted sorted.
    searchResultsSize
      Total number of products to be displayed
    filter
      A filter to use, if this isnt specified we use the default filter (CatalogItemFilter)
  --%>
<dsp:page>
  <dsp:importbean var="config" bean="/atg/store/StoreConfiguration" />
  <dsp:importbean bean="/atg/store/sort/RangeSortDroplet" />
  <dsp:importbean bean="/atg/store/droplet/CatalogItemFilterDroplet" />
  
  <dsp:getvalueof var="prodDivName" param="productDivName" />
  <dsp:getvalueof var="sort" param="sort" />
  <dsp:getvalueof var="productList" param="productArray"/>
  <dsp:getvalueof var="searchResultsSize" param="searchResultsSize"/>
  <dsp:getvalueof var="viewAll" param="viewAll"/>
  <dsp:getvalueof var="isSimpleSearchResults" param="isSimpleSearchResults"/>
  <dsp:getvalueof var="mode" param="mode"/>
  <dsp:getvalueof var="atgSearchInstalled" bean="StoreConfiguration.atgSearchInstalled" />
  <dsp:getvalueof id="originatingRequestURL" bean="/OriginatingRequest.requestURI" />
  <dsp:getvalueof var="pageSize" param="pageSize"/>
  
  <%--
    CatalogItemFilterDroplet is used to filter a collection of catalog
    items. Here we use this droplet to validate the input collection using 
    the ItemDateValidator.
    
    Input Parameters:
      collection - The unfiltered collection
      
    Open Parameters:
      output - Serviced when no errors occur
    
    Output Parameters:
      filteredCollection - the filtered collection
  --%>
  <dsp:droplet name="CatalogItemFilterDroplet">
    <dsp:param name="collection" param="productArray" />
    <dsp:oparam name="output">
      <dsp:getvalueof var="productList" param="filteredCollection"/>
    </dsp:oparam>  
  </dsp:droplet>
  
  <%-- Default the current page to 1 if it hasnt been set --%>
  <dsp:getvalueof var="p" param="p" />
  <c:if test="${empty p}">
    <c:set var="p" value="1" />
  </c:if>

  <%-- Default the number of products per page to the defaultPageSize property for this Site --%>
  <c:if test="${empty pageSize}">
    <dsp:getvalueof var="pageSize" vartype="java.lang.Object" 
                    bean="/atg/multisite/SiteContext.site.defaultPageSize"/>
  </c:if>
  
  <%-- Determine the total number of items to be displayed --%>
  <dsp:getvalueof var="collectionSize" value="${searchResultsSize}" />
  <c:if test="${empty collectionSize}">
    <c:set var="collectionSize" value="${fn:length(productList)}" />
  </c:if>
  
  <%-- Set the number of products displayed per row to 4 --%>
  <c:set var="rowSize"  value="4" />

  <%-- Range Droplet input parameters --%>
  <c:choose>
    <c:when test="${viewAll eq 'true'}">
      <c:set var="howMany" value="5000" />
      <c:set var="start" value="1" />
    </c:when>
    <c:when test="${(fn:length(productList)) <= pageSize}">
      <c:set var="howMany" value="${pageSize}" />
      <c:set var="start" value="${1}" />
    </c:when>
    <c:otherwise>
      <c:set var="howMany" value="${pageSize}" />
      <c:set var="start" value="${((p - 1) * howMany) + 1}" />
    </c:otherwise>
  </c:choose>

  <%--
    This droplet renders output parameter for a subset of items of its array parameter.
           
    Input parameters:
      array
        An array of items from which to extract the subset of items.
      howMany
        Specifies the number of items to include in subset of items.
      start
        Specifies the starting index (1-based).
            
    Output parameters:
      element
        This parameter is set to the current element of the array each
        time the output parameter is rendered.
                
    Open parameters:
      outputStart
        This parameter is rendered before any output tags if the subset of
        the array being displayed is not empty.
      outputEnd
        This parameter is rendered after all output tags if the subset of
        the array being displayed is not empty.
      output
        This parameter is rendered once for each element in the subset of the
        array that gets displayed.
      empty
        This parameter is rendered if the array itself, or the
        requested subset of the array, contains no elements.
  --%>
  <dsp:droplet name="RangeSortDroplet">
    <dsp:param name="array" value="${productList}"/>
    <dsp:param name="sortSelection" value="${sort}"/>
    <dsp:param name="howMany" value="${howMany}"/>
    <dsp:param name="start" value="${start}"/>

    <%-- Rendered once before the main "output" --%>
    <dsp:oparam name="outputStart">
      
      <%-- If we arent using Repository search display the sorting options --%>
      <c:if test="${isSimpleSearchResults ne true}">
        <dsp:include page="sortDisplay.jsp">
          <dsp:param name="arraySplitSize" value="${pageSize}" />
          <dsp:param name="p" value="${p}" />
        </dsp:include>
      </c:if>
      
      <%--Top Pagination Links--%>
      <dsp:include  page="/global/gadgets/productListRangePagination.jsp">
        <dsp:param name="size" value="${collectionSize}" />
        <dsp:param name="top" value="true" />
        <dsp:param name="p" value="${p}" />
        <dsp:param name="arraySplitSize" value="${pageSize}" />
        <dsp:param name="start" value="${((p - 1) * howMany) + 1}"/>
      </dsp:include>
 
      <div id="<c:out value='${prodDivName}'/>">
      <ul class="atg_store_product" id="atg_store_product">
    </dsp:oparam>
        
    <%-- Product repository item is output --%>
    <dsp:oparam name="output">
      <dsp:setvalue param="product" paramvalue="element"/>
      <dsp:getvalueof var="item" param="product"/>
      <preview:repositoryItem item="${item}">
        <dsp:getvalueof var="count" vartype="java.lang.String" param="count"/>
        <dsp:getvalueof var="size" vartype="java.lang.String" param="size"/>
        <dsp:getvalueof var="additionalClasses" vartype="java.lang.String"
                        value="${(count % rowSize) == 1 ? 'prodListBegin' : ((count % rowSize) == 0 ? 'prodListEnd':'')}"/>

        <li class="<crs:listClass count="${count}" size="${size}" selected="false"/>${empty additionalClasses ? '' : ' '}${additionalClasses}">
          <%-- 
            Category id is empty if we are using search. Use parent category's id in this case.
          --%>  
          <dsp:getvalueof var="catId" param="categoryId"/>
          <c:set var="categoryNav" value="true" />
          <c:if test="${empty catId}">
            <dsp:getvalueof var="catId" param="product.parentCategory.id"/>
            <c:set var="categoryNav" value="false" />
          </c:if>
            
          <%-- Render the Product --%>
          <fmt:message var="siteIndicatorPrefix" key="common.from"/>
          <dsp:include page="/global/gadgets/productListRangeRow.jsp">
            <dsp:param name="categoryId" value="${catId}"/>
            <dsp:param name="product" param="product" />
            <dsp:param name="displaySiteIndicator" value="true"/>
            <dsp:param name="displayCurrentSite" value="false"/>
            <dsp:param name="noSiteIcon" value="false"/>
            <dsp:param name="sitePrefix" value="${siteIndicatorPrefix}"/>
            <dsp:param name="categoryNav" value="${categoryNav}" />
            <dsp:param name="mode" value="${mode}"/>
          </dsp:include>
        </li>
      </preview:repositoryItem>
    </dsp:oparam>

    <%-- Rendered once after all products have been output --%>
    <dsp:oparam name="outputEnd">
      </ul>
      </div>

      <%-- Bottom Pagination Links --%>
      <dsp:include page="/global/gadgets/productListRangePagination.jsp">
        <dsp:param name="p" value="${p}" />
        <dsp:param name="size" value="${collectionSize}" />
        <dsp:param name="top" value="true" />
        <dsp:param name="arraySplitSize" value="${pageSize}" />
        <dsp:param name="start" value="${((p - 1) * howMany) + 1}"/>
      </dsp:include>
       
    </dsp:oparam>
  </dsp:droplet>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/global/gadgets/productListRange.jsp#1 $$Change: 683854 $--%>