<%--  
  Renders page numbers and view all link

  Required Parameters:
    arraySplitSize
      Number of items to be displayed per page.
    start
      Start index of the item to be rendered on this page.
    size
      Total number of items to be displayed
    p
      Current page number.

  Optional Parameters:
    top
      Where to render the pagination links on the page. Set to true for top set of links,
      false for the bottom set.
    viewAll
      Set to true if 'view all' has been requested.
    sort
      How the results should be sorted sorted.
    facetTrail  
      Records the currently supplied faceting on the search results.
    addFacet
      Facet to be added to the facet trail
    categoryId
      The currently browsed category
--%>
<dsp:page>
  <dsp:getvalueof var="size" idtype="java.lang.Integer" param="size"/>
  <dsp:getvalueof var="start" idtype="java.lang.String" param="start"/>
  <dsp:getvalueof var="viewAll" param="viewAll"/>
  <dsp:getvalueof var="top" param="top"/>
  <dsp:getvalueof var="p" param="p"/>
  <dsp:getvalueof var="originatingRequestURL" bean="/OriginatingRequest.requestURI"/>
   
  <%--
    Get URL parameters that will be passed to javascript functions with escaped
    XML specific characters so that to prevent using them in XSS attacks.
  --%>
  <c:set var="arraySplitSize"><dsp:valueof param="arraySplitSize" valueishtml="false"/></c:set>
  <c:set var="question"><dsp:valueof param="q" valueishtml="false"/></c:set>
  <c:set var="sort"><dsp:valueof param="sort" valueishtml="false"/></c:set>
  <c:set var="token"><dsp:valueof param="token" valueishtml="false"/></c:set>
  <c:set var="categoryId"><dsp:valueof param="categoryId" valueishtml="false"/></c:set>
  <c:set var="selectedHowMany"><dsp:valueof param="size" valueishtml="false"/></c:set> 
    
  <%-- ATG Search Specific --%>
  <c:set var="facetTrail"><dsp:valueof param="q_facetTrail" valueishtml="false"/></c:set>
  <c:set var="addFacet"><dsp:valueof param="addFacet" valueishtml="false"/></c:set>
  <c:set var="trail"><dsp:valueof param="trail" valueishtml="false"/></c:set>
  <c:set var="trailSize"><dsp:valueof param="trailSize" valueishtml="false"/></c:set>
  
  <%-- Repository Search Specific --%>
  <c:set var="searchFeatures"><dsp:valueof param="searchFeatures" valueishtml="false"/></c:set>
  <c:set var="searchCategoryId"><dsp:valueof param="searchCategoryId" valueishtml="false"/></c:set>
  <c:set var="sSearchInput"><dsp:valueof param="sSearchInput" valueishtml="false"/></c:set>
  
  <c:if test="${empty start && not empty p}">
    <c:set var="start" value="${(p - 1) * arraySplitSize + 1}"/>
  </c:if>

  <%-- 
    No need to show the pagination when the total number of items
    is less or equal to the defined number of items per page
  --%>
  <c:if test="${size > arraySplitSize}">
    
    <c:if test="${not isSimpleSearchResults}">
      <c:set var="searchCategoryId" value="${categoryId}"/>
      <c:set var="targetElement" value="atg_store_catSubProdList"/>
    </c:if>
    
    <crs:pagination size="${size}" arraySplitSize="${arraySplitSize}" start="${start}" viewAll="${viewAll}" top="${top}">
  
      <jsp:attribute name="pageLinkRenderer"> <%-- RENDER PAGE LINKS --%>   
        <%-- Build links href --%>
        <c:set var="url" value="${originatingRequestURL}"/>
        <c:set var="pageNum" value="${linkText}"/>
        <%@include file="/navigation/gadgets/navLinkHelper.jspf" %>
        
        <%-- 
          For paging we want an extra parameter token which references the search session to
          perform a paging request for, instead of creating a new search session.
        --%>
        <c:if test="${not empty token}">
          <c:url var="url" value="${url}" context="/">
            <c:param name="token" value="${token}"/>
          </c:url>
        </c:if>
         
        <a href="${url}" title="${linkTitle}" class="${linkClass}">
          ${linkText}
        </a>
      </jsp:attribute>
      
      <jsp:attribute name="viewAllLinkRenderer"> <%-- RENDER VIEW ALL LINK --%>
        <%-- Clickable View All Link --%>
        <c:set var="viewAllLink" value="true"/>
        <c:set var="url" value="${originatingRequestURL}"/>
        <c:set var="pageNum" value=""/>
        <%@include file="/navigation/gadgets/navLinkHelper.jspf" %>
        <a href="${url}" title="${linkTitle}" class="${linkClass}">
          ${linkText}
        </a>
      </jsp:attribute>
      
    </crs:pagination>
  </c:if>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/global/gadgets/productListRangePagination.jsp#1 $$Change: 683854 $ --%>