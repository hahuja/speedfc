/**
 *  Product Listing Widget
 *
 *  Created by Jon Sykes, Jan 2011 
 *
 *  Widget that manages the overall product listing view for ajax updates
 *  includes:
 *    - Ajax I/O management
 *    - Creation of productView widgets in the correct location
 *
 */
dojo.provide("atg.store.widget.productListing");


dojo.declare(
  "atg.store.widget.productListing", 
  [dijit._Widget, dijit._Container],
  {
        
    debugOn: true,
    currentlyRequesting: false,
    paginationId: "atg_store_pagination",
    pricePredicate: "was", // this value gets updated with the i18n string
    state: {
      sortSelection: 0
    },
    historyReload: false,
    
    postCreate: function(){
      this.listNode = dojo.byId("atg_store_product");
      this.sortByDropDown = dojo.byId("sortBySelect");
      this.sortByForm = dojo.byId("sortByForm");
      this.addSortByBehavior();
      this.addPagingBehavior();
      this.parseUriParams();
      this.parseUriHash();
      
      console.debug(this);
      
      this.containerNode = dojo.byId("atg_store_product");
      
      // set up anon topics for other UI elements to publish to
      // dojo.publish("/atg/productListing/update", [{ ajaxSearch:"values" }]);
      this.topics = [
        dojo.subscribe("/atg/productListing/update", this, "topicUpdate")
      ]
    },
    
    topicUpdate: function(topicObj){
      this._debug("topic Update: ", topicObj);
      dojo.mixin(this.state, topicObj);
      this.ajaxSubmit();
    },
    
    parseUriParams: function(){
      var query = window.location.search.split('?')[1];
      if(query){
        dojo.mixin(this.state, dojo.queryToObject(query));
      }
    },
    
    parseUriHash: function(){
      var hash = window.location.hash.split('#')[1];
      if(hash){
        dojo.mixin(this.state, dojo.queryToObject(decodeURIComponent(hash)));
        this._debug("state on reload: ", this.state);
        this.productListingStateReload(this.state);
      }
    },
    
    
    addSortByBehavior: function(){
      if(this.sortByDropDown){
        dojo.connect(this.sortByDropDown, "onchange", this, "handleSortBehavior");
      }
    },
    
    handleSortBehavior: function(evt){
      evt.preventDefault();
      // get the selected option node
      var optionNode = dojo.query("option[value=\""+evt.target.value+"\"]", evt.target)[0];
      
      var optionNodePosition = (this._getNodePosition(optionNode)-1);
      
      dojo.mixin(this.state, dojo.queryToObject("sortSelection="+optionNodePosition));
      
      if(this.state.token){
        this.state.token = "";
      }
      
      if(this.state.p){
        this.state.p = "";
      }
      this.ajaxSubmit();
      
    },
    
    addPagingBehavior: function(){

      // test for pagination
      dojo.query(".atg_store_pagination").forEach( function(pagingUI){
        
        dojo.forEach(
          dojo.query("li a", pagingUI),
          function(pageAnchor) {
            dojo.connect(pageAnchor, "onclick", this , function(evt){
              evt.preventDefault();
              
              // Ensure it's not a double click
              if(this.currentlyRequesting){
                this._debug("This link has already been clicked - ignoring");
                return;
              }
              
              if(!dojo.hasClass(pageAnchor, "disabledLink")){
                this._debug("Pagination Link has been clicked");
                
                var linkParams = dojo.queryToObject(pageAnchor.href.split('?')[1]);
                
                // If its a page link make sure view all is empty
                if(linkParams.p){
                  this.state.viewAll = "";
                  this.state.p = linkParams.p;
                }
                
                // If its a view all link make sure p is empty
                if(linkParams.viewAll){
                  this.state.viewAll = linkParams.viewAll;
                  this.state.p = "";
                }
                
                //_this.state.triggerNode = pageAnchor;
                this.ajaxSubmit();
                // update paging ui
                this.updatePagination();
                // set status to requesting
                this.currentlyRequesting = true;
              }else{
                this._debug("Current pagination Link has been clicked");
                evt.preventDefault();
              }
              
            });
            
          }, this);
      }, this);
      
    },
    
    
    /*
     * Hijack a node. The node should be either a <form> or an <a> node.
     * Hookup the submit to use XHR instead of standard browser request, and 
     * process the returned JSON data with the handleRespones() function
     * 
     */
    ajaxSubmit: function(){
      
      var bindParams={
        headers: { "Accept" : "application/json" },
        // expecting mimetype: "application/json",
        handleAs: "json",
        
        load: dojo.hitch(this, "handleJSON"),
        error: dojo.hitch(this, "handleError"),
        timeout: dojo.hitch(this, "handleError")
        
      };

      // stub object
      var content = {};
      
      this._debug("Product Listing State: ", this.state);
      
      // Add in the sorting selection
      if(this.sortByDropDown){
        var sortQuery = this.sortByDropDown[this.state.sortSelection].value;
      }
      
      // Ignore the addFacet parameter
      if(this.state.addFacet){
        this.state.addFacet = null;
      }

      // Dont encode the q parameter as this can cause incorrect handling of spaces in search querys.
      // For example if the user searches for 'red shoe' the url param will be 'red+shoe', if we 
      // encode this it will be 'red%2Bshoe' which is decoded server side as 'red+shoe' not 'red shoe'.      
      if(this.state.q){
        var originalQuery = this.state["q"];
        this.state.q = null;
      }
      
      var targetUrl = contextPath + "/productjson/productData.jsp";
      var queryString = dojo.objectToQuery(this.state);
      
      if(originalQuery){
        queryString += "&q=" + originalQuery;
        this.state.q = originalQuery;
      }
      
      if(sortQuery){
        queryString += "&sort=" + encodeURIComponent(sortQuery);
      }
      
      targetUrl += "?" + queryString;

      // Setup the ajax params
      dojo.mixin(bindParams,{
        url: targetUrl,
        content: content
      });
      
      dojo.xhrGet(bindParams);
      
      this.jsonLoading(true);
      return false;
    },
    
    jsonLoading: function(loading){
      //alert(loading);
      if(loading) {
        dojo.query("div[name='transparentLayer']").addClass("active");
        dojo.query("div[name='ajaxSpinner']").addClass("active");
      } else {
        dojo.query("div[name='transparentLayer']").removeClass("active");
        dojo.query("div[name='ajaxSpinner']").removeClass("active");
      }
    },
    
    
    handleJSON: function(prodObj){
      this._debug("Product Listing JSON handling, Object: ", prodObj);
      
      // clear existing product listings.
      this.clearProducts();
      
      if(!this.historyReload){
        this.addToHistory(unescape(dojo.objectToQuery(this.state)));
      }
      
      // reset this in case the last request was a history related one
      this.historyReload = false;
      // loop through the items in the products array and add product widgets for each
      var itemArray = prodObj.products.items;
      // i18n for the pricePredicate
      this.pricePredicate = prodObj.products.pricePredicate;
      
      // no products in JSON return, check if this is a featured cat landing page
      if(itemArray.length == 0){
        if(dojo.byId("featured_products")){
          // Toggle the views back to featured products
          dojo.byId("featured_products").style.display = "block";
          dojo.byId("atg_store_searchResults").style.display = "none";
        }
        // Toggle recommendations on
        if(dojo.byId("cs-recslot-category")){
          dojo.byId("cs-recslot-category").style.display = "block";
        }
      }else{
        if(dojo.byId("featured_products")){
          dojo.byId("featured_products").style.display = "none";
          dojo.byId("atg_store_searchResults").style.display = "block";
        }
        // Toggle recommendations off
        if(dojo.byId("cs-recslot-category")){
            dojo.byId("cs-recslot-category").style.display = "none";
        }      
      }
      
      dojo.forEach(itemArray, function(item, index, itemArray){
        var classString = getRepositoryItemIdentifier(item.repository, item.itemDescriptor, item.id);
        item.previewClassString = classString;
        this._debug("Adding Product: ", item.name);
        this.addProduct(item);
      }, this);
      // update the UI
      this.jsonLoading(false);
      this.currentlyRequesting = false;
      this.updateSortBy();
      
      this.redrawPagination(prodObj.pagination);
      
      // check to see if facetData came back and trigger facet redraw
      if(prodObj.facets){
        dojo.publish("/atg/facetManager/update", [{ facetObject: prodObj.facets }]);
      }
      
      
      if(prodObj.token){
        this.state.token = prodObj.token;
      }
      
      updatePreview(); 
    },
    
    handleError: function(errObj){
      this.jsonLoading(false);
      this.error("There was an Ajax Response Error", errObj);   
      // FIXME: need to get a definitive list of what can go wrong and handle them here.
    },
    
    /*
     * Clear all items from Product View
     */
    clearProducts: function(){
      // get all child widgets
      var children = this.getChildren();
      // current Children aren't widgets, they are server rendered
      if(children == 0){
        children = dojo.query("LI", this.containerNode);
        for (var i=0; i<children.length; i++){
          this._debug("Clearing Child Node: ",children[i]);
          dojo.destroy(children[i]);
        }
      // These are widget children, handle them with their own
      // destroy functionality
      }else{
        // loop thru them all and destroy them
        for (var i=0; i<children.length; i++){
          this._debug("Clearing Child Widget: ",children[i]);
          children[i].destroy();
        }
      }
    },
    
    
    /**
     * Add a Product to the listing
     */
    addProduct: function(data){

      var productItem = new atg.store.widget.productView({
        pricePredicate: this.pricePredicate,
        data: data
      });
      // add product widget to this parent;
      this.addChild(productItem);
      
    },
    
    /**
     * Update the sortby UI
     */
    updateSortBy: function(){
      if(this.sortByDropDown){
        this._debug("sortSelection: ",this.state.sortSelection);
        dojo.query("option", this.sortByDropDown).forEach.selected = false;
        dojo.query("option", this.sortByDropDown)[this.state.sortSelection].selected = "selected";
        this._debug(dojo.query("option", this.sortByDropDown)[this.state.sortSelection].value);
      }
    },
    
    /**
     * Redraws the pagination using the paging information in the json
     */
    redrawPagination: function(pageData){

      this._debug("Redraw Page Data: ", pageData);
      
      this.pageData = pageData;
      dojo.query(".atg_store_pagination").forEach( function(pagingUI){
        
        dojo.query("UL", pagingUI).forEach( function(pagingUL){
          
          this._debug("Updating this paging element: ", pagingUL);
          
          dojo.empty(pagingUL);
          //alert(pageData);
          if(pageData != null){
            this._debug("Page Data: ", this.pageData);
            if(pageData.pages != null && pageData.pages.length > 0){
              this._debug("Rebuilding Pagination UI for this many pages: ", pageData);
          
              for (var i=0; i<pageData.pages.length; i++){
                dojo.place('<li><a href="?p='+(i+1)+'" title="'+ pageData.goToPageLabel +' '+(i+1)+'">'+ (i+1) +'</a></li>', pagingUL, "last");
              }
              dojo.place('<li><a href="?viewAll=true" title="'+ pageData.viewAllLabel +'">'+ pageData.viewAllLabel +'</a></li>', pagingUL, "last");
            }        
          }
        }, this);
        
      }, this);
      if(pageData != null){
        this.addPagingBehavior();
        this.updatePagination();
      }
      
    },
    
    /**
     * Update the pagination UI
     */
    updatePagination: function(){
      this._debug("updatePagination". data); 

      dojo.query(".atg_store_pagination UL").forEach( function(pagingUI){        
        
        var paginationLinks = dojo.query("li a", pagingUI);
        // clear the disabled class
        paginationLinks.forEach(
          function(aTag){
            aTag.title = aTag.title || aTag.oldTitle;
            dojo.removeClass(aTag, "disabledLink");
          }
        );
        
        
        // add the disabled class to the current state
        if(this.state.viewAll){
          dojo.addClass(paginationLinks[paginationLinks.length-1], "disabledLink");
        }else{
          if(!this.state.p){
            this.state.p = 1;
          }
          dojo.addClass(paginationLinks[(this.state.p-1)], "disabledLink");
          paginationLinks[(this.state.p-1)].oldTitle = paginationLinks[(this.state.p-1)].title;
          paginationLinks[(this.state.p-1)].title = "";
        }
        
      }, this);
      
    },
    
    
    /**
     * History and Bookmark Handling
     */    
    
    historyState: function(state){
      
      this.state = state;
      this.restoreState = function() {
        if (this.state && this.state != '') {
          dijit.byId("atg_store_product").productListingStateReload(this.state);
        } else {
          location.reload();
        }
      };
      // back, forward and changeUrl are needed by dojo
      this.back = this.restoreState;
      this.forward = this.restoreState;
      this.changeUrl = this.state;
    },
    
    
    addToHistory: function(ajaxState) {
      dojo.back.addToHistory(new this.historyState(ajaxState));
    },
    
    productListingStateReload: function(stackState){

      this._debug("stakeState: ", stackState);
      
      if(typeof stackState != "object"){
        dojo.mixin(this.state, dojo.queryToObject(stackState));
      }else{
        dojo.mixin(this.state, stackState);
      }
      
      this.historyReload = true;
      
      this.ajaxSubmit();
      // update paging ui
      this.updatePagination();
      // set status to requesting
      this.currentlyRequesting = true;
      
    },
    
    _getNodePosition: function(node){
      var i = 0;
      do{
        if(node.nodeType == 1){ i++ };
        node = node.previousSibling;
      }while(node);
      
      return i;
    },
    
    destroyRecursive: function(){
      // defining destroyRecursive for back-compat.
      this.destroy();
    },
    
    
    _debug: function(debugTitle, toBeDebugged){
      if(this.debugOn){ 
        var title = debugTitle || "";
        var toBeDebugged = toBeDebugged || "";
        console.debug("----- "+this.id+" -----");     
        console.debug(title, toBeDebugged);     
      }
    },
    
    
    // don't edit below here
    noCommaNeeded:""
});


function HistoryState(state){
  this.state = state;
  this.restoreState = function() {
    if (this.state && this.state != '') {
      dijit.byId("atg_store_productListing").productListingStateReload(this.state);
    } else {
      location.reload();
    }
  };
  // back, forward and changeUrl are needed by dojo
  this.back = this.restoreState;
  this.forward = this.restoreState;
  this.changeUrl = this.state;
}