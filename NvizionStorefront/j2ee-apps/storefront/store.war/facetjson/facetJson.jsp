<%--
  This page returns JSON data containing facets from a search query.
  
  Required parameters:
    facetOptions
      A List of FacetHolders and FacetValues which are used to build the JSON
  
  Optional parameters:
    displayAll
      A List of refinement ids that shouldnt have the more/less pagination applied
    question
      The search query
    categoryId
      The current category being browsed
    resultsCount
      The number of search results.
--%>
<dsp:page>
  
  <dsp:getvalueof var="facetOptions" param="facetOptions"/>
  <dsp:getvalueof var="question" param="question" />
  <dsp:getvalueof var="resultsCount" param="resultsCount" />
  <dsp:getvalueof var="categoryId" param="categoryId" />
  <dsp:getvalueof var="displayAll" param="displayAll" />
  
  <%-- FACET JSON --%>  
  <json:object name="facets">
  
    <%-- Results Count --%>
    <c:if test="${empty categoryId }">
      <c:choose>
        <c:when test="${resultsCount == 1}">
          <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.oneResultFor"/>
        </c:when>
        <c:otherwise>
          <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.resultFor"/>
        </c:otherwise>
      </c:choose>
      <json:property name="resultsCount">
        <c:out value="${resultsCount}"/>
      </json:property>
    </c:if>
  
    <%-- I8N Stuff --%>
    <json:property name="expandFacetLabel">
      <fmt:message key="facet_facetRender.button.moreText"/>
    </json:property>
    
    <json:property name="collapseFacetLabel">
      <fmt:message key="facet_facetRender.button.lessText"/>
    </json:property>
    
    <%-- Facets go here --%>
    <json:array name="facetOptions">
      <c:forEach var="facetListItem" items="${facetOptions}" varStatus="status">
     
        <c:choose>
          <%-- UNSELECTED FACET --%> 
          <c:when test="${facetListItem.class.name == 'atg.repository.search.refinement.FacetHolder'}">
            <dsp:include page="/facetjson/unselectedFacetJson.jsp">
              <dsp:param name="facetHolder" value="${facetListItem}"/>
              <dsp:param name="displayAll" value="${displayAll}"/>
            </dsp:include>
          </c:when>
        
          <%-- CURRENTLY SELECTED FACET (instanceof FacetValue)--%>          
          <c:otherwise>
            <dsp:include page="/facetjson/selectedFacetJson.jsp">
              <dsp:param name="facetValue" value="${facetListItem}"/>
            </dsp:include>
          </c:otherwise>
        
        </c:choose>
      </c:forEach>
    </json:array>
  </json:object>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/facetjson/facetJson.jsp#1 $$Change: 683854 $--%>
