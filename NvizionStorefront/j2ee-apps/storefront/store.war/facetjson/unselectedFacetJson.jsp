<%--
  This page returns JSON data for facets that are currently unselected (FacetHolder objects)
  
  Required parameters:
    facetHolder
      An unselected facet (FacetHolder)
  
  Optional Parameters:
    displayAll
      A List of refinement ids that shouldnt have the more/less pagination applied
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet" />
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  

  <dsp:getvalueof var="facetHolder" param="facetHolder"/>
  <dsp:getvalueof var="displayAll" param="displayAll" />
  <dsp:getvalueof var="facetName" vartype="java.lang.String" value="${facetHolder.facet.label}"/>
  <dsp:getvalueof var="facetId" vartype="java.lang.String" value="${facetHolder.facet.id}"/>
          
  <json:object>
  
    <%-- Facet Name (e.g Price) --%>
    <json:property name="name">
      <fmt:message key="${facetName}"/>
    </json:property>
    
    <%-- Facet Repository Id --%>
    <json:property name="id">
      <dsp:valueof value="${facetId}"/>
    </json:property>
      
    <%-- Is this facet applied? --%>
    <json:property name="selected">${false}</json:property>
    
    <%-- Apply More/Less controls? We assume true by default --%>
    <c:set var="applyMoreLess" value="${true}"/>
    <c:forEach var="item" items="${displayAll}">
      <c:if test="${item eq facetId}">
        <c:set var="applyMoreLess" value="${false}" />
      </c:if>
    </c:forEach>
    
    <c:if test="${not applyMoreLess}">
      <json:property name="displayAllOptions">${true}</json:property>
    </c:if>
             
    <%-- Facet Value options --%>
    <dsp:getvalueof var="facetValueNodes" param="facetHolder.facetValueNodes"/>
    <json:array name="options">
      <c:forEach var="currentFacetValueNode" items="${facetValueNodes}">
        <dsp:param name="currentFacetValueNode" value="${currentFacetValueNode}"/>
        <json:object>
      
          <%-- Facet Value Name (e.g $25-$50) --%>
          <json:property name="facetName">
            <%--
              Used to get human readable strings from a facet.
            
              Input Parameters:
                refinementValue - The facet
              
                refinementId - Facet id
              
                locale - The desired locale
            
              Open Parameters:
                output - Rendered when there are no errors.
              
              Output Parameters:
                displayValue - The facet refinements display value
            --%>
            <dsp:droplet name="/atg/commerce/search/refinement/RefinementValueDroplet">
              <dsp:param name="refinementValue" value="${currentFacetValueNode.facetValue.value}"/>
              <dsp:param name="refinementId" value="${currentFacetValueNode.facetValue.facet.id}"/>
              <dsp:param name="locale" bean="/atg/userprofiling/Profile.PriceList.locale"/>
                
              <dsp:oparam name="output">
                <dsp:valueof param="displayValue" valueishtml="true"/>
              </dsp:oparam>
            </dsp:droplet>
          </json:property>
                
          <%-- FacetTrail String --%>
          <json:property name="urlFacet" escapeXml="false">
            <%--
              Generates a FacetTrail bean based on the input parameters
   
              Input Parameters:
               trail - The facet trail string to be converted to a bean
               refineConfig - the RefineConfig to use
    
              Open Parameters:
                output - Serviced when no errors occur
    
              Output Parameters:
                facetTrail - The generated FacetTrail bean.  
            --%>
            <dsp:droplet name="CommerceFacetTrailDroplet">
              <dsp:param name="trail" bean="FacetSearchTools.facetTrail" />
              <dsp:param name="addFacet" param="currentFacetValueNode.facetValue" />
              <dsp:param name="refineConfig" param="catRC" />
              <dsp:oparam name="output">
                <dsp:getvalueof param="facetTrail.trailString" var="facetTrailString"/>
                <c:out value="${facetTrailString}" escapeXml="false"/>
              </dsp:oparam>
            </dsp:droplet>

          </json:property>
                
          <%-- Product Quantity --%>
          <json:property name="qty">
            <dsp:valueof param="currentFacetValueNode.facetValue.matchingDocsCount"/>
          </json:property>
          
        </json:object>
      </c:forEach>
    </json:array>
  </json:object>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/facetjson/unselectedFacetJson.jsp#1 $$Change: 683854 $--%>
