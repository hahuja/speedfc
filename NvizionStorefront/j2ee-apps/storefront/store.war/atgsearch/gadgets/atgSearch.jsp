<%--
  Included by /navigation/gadgets/search.jsp when ATG Search is installed.
  This file provides a search textbox and a checkbox for each site (when in a multisite
  environment).  Upon submission of the form, the formhandler appends the search query
  and the site IDs of any sites (other than the current) to be included in the search.  These parameters
  are handled on the /atgsearch/searchResults.jsp page, where the search is eventually submitted.
  
  An example success URL resulting from the submission of this form:
  
  /atgsearch/searchResults.jsp?q=chair&sId=storeUS&sId=homeStore
  
  Required Parameters:
    None
    
  Optional Parameters:
    redirectURLOnEmptySearch
      The URL to redirect to on an empty search
--%>
<dsp:page>
  <dsp:importbean bean="/atg/dynamo/droplet/multisite/CartSharingSitesDroplet" />
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
  <dsp:importbean bean="/atg/multisite/Site" var="currentSite"/>
  <dsp:importbean bean="/atg/search/routing/command/search/DynamicTargetSpecifier"/>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  
  <dsp:getvalueof var="errorURL" param="redirectURLOnEmptySearch"/>

  <%-- Search Form --%>
  <dsp:form method="post" formid="searchform" id="searchForm">
    <fmt:message var="hintText" key="common.search.input"/>
    <fmt:message var="submitText" key="search_simpleSearch.submit"/>
  
    <dsp:input bean="QueryFormHandler.successURL" type="hidden"  value="${pageContext.request.contextPath}/atgsearch/searchResults.jsp"/>
    <dsp:input bean="QueryFormHandler.errorURL" type="hidden"  value="${errorURL}"/>
    <dsp:input bean="QueryFormHandler.sortSelection" type="hidden" value=""/>
    <dsp:input bean="QueryFormHandler.searchRequest.multiSearchSession" type="hidden" value="true"/>
    <dsp:input bean="QueryFormHandler.searchRequest.saveRequest" type="hidden" value="true"/>
    <dsp:input bean="QueryFormHandler.searchRequest.pageSize" type="hidden" value="${currentSite.defaultPageSize}"/>
    <dsp:input bean="QueryFormHandler.searchRequest.question" iclass="text atg_store_searchInput" value="${hintText}" type="text" id="atg_store_searchInput" title="${hintText}" autocomplete="off"/>

    <div id="atg_store_searchStoreSelect">
    
    <%--
      Check if the current site has a shared cart, other sites will be editable check boxes. If site
      doesn't have a shareable, then search within the current site context.
    --%>
	<%--
      CartSharingSitesDroplet returns a collection of sites that share the shopping
	  cart shareable (atg.ShoppingCart) with the current site.
	  You may optionally exclude the current site from the result.

      Input Parameters:
        excludeInputSite - Should the returned sites include the current
   
      Open Parameters:
        output - This parameter is rendered once, if a collection of sites
                 is found.
   
      Output Parameters:
        sites - The list of sharing sites.
    --%>
    <dsp:droplet name="CartSharingSitesDroplet" excludeInputSite="true"
                 var="sharingSites">
      <dsp:oparam name="output">
           
        <%-- Loop through the sites --%>
        <dsp:droplet name="ForEach" array="${sharingSites.sites}" var="current">
          
          <%-- Set to search the current site --%>      
          <dsp:oparam name="outputStart">            
            <dsp:input bean="QueryFormHandler.searchRequest.dynamicTargetSpecifier.siteIdsArray" type="hidden" value="${currentSite.id}" priority="10" />
          </dsp:oparam>
          
          <%-- other sites --%>
          <dsp:oparam name="output">
            <dsp:setvalue param="site" value="${current.element}"/>
            <dsp:getvalueof var="siteId" param="site.id"/>
            <div>
              <dsp:input bean="QueryFormHandler.searchRequest.dynamicTargetSpecifier.siteIdsArray" type="checkbox" value="${siteId}" priority="10" id="otherStore" checked="false"/>
              <label for="otherStore">
                <fmt:message key="search.otherStoresLabel">
                  <fmt:param>
                    <dsp:valueof param="site.name"/>
                  </fmt:param>
                </fmt:message>           
              </label>
            </div>    
          </dsp:oparam>
        </dsp:droplet>
      </dsp:oparam>
      
      <dsp:oparam name="empty"> 
        <dsp:input bean="QueryFormHandler.searchRequest.dynamicTargetSpecifier.siteIdsArray" type="hidden" value="${currentSite.id}" priority="10" />
      </dsp:oparam>
    </dsp:droplet>
  </div>
  
  <%-- 
    Inform the results page that we've already executed the search, so it doesn't do it again. The results
    page can be navigated to directly, e.g from a page reload or a bookmark so it will perform a search unless we
    set this parameter.
  --%>
  <dsp:input bean="QueryFormHandler.initialSearch" value="true" type="hidden"/> 
  
  <span class="atg_store_smallButton">      
    <dsp:input type="submit" bean="QueryFormHandler.search" value="submit form" id="atg_store_searchSubmit" title="${submitText}"/>
  </span>
  
  <%-- 
    IE wont submit a form with a single text input to the 
    handle method so if its IE we will use a hidden input 
  --%>
  <!--[if IE]>
    <dsp:input type="hidden" bean="QueryFormHandler.search" value=""/>
  <![endif]-->
 
  </dsp:form>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/atgsearch/gadgets/atgSearch.jsp#1 $$Change: 683854 $ --%>
