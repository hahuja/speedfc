<%-- 
  This page contains all the elements of a search results page.  The passed in parameters
  are not examined by this page, instead, they are inherited by the included pages.  For their usage
  within those pages, view the comments on the included pages.
       
  - atgSearchResultsRenderer displays the actual search results, along with sorting and pagination
    options.
  - facetPanelContainer displays the faceting panel.
  - targetingRandom displays the promotional content.   
         
  Required Parameters:
    (Either)
    q
      The search question.  (Required when doing a query search)
    (Or)
    categoryId
      When doing a category search, this specifies the category under which to search. (Required 
      when doing a category search).

  Optional Parameters:
    p
      The page of search results to render.
    sort
      How the products should be sorted.
    sId
      The ID of a site whose content should also be included in the search.  There may be multiple
      parameters with this name, which indicates multiple sites should be searched.
    facetTrail
      Records the currently supplied faceting on the search results.
    addFacet
      Specifies a new facet which should be applied, and added to the facet trail.
    pageSize
      The number of results to display per page
    trailSize
      The trail size (number of facets traversed) upto this point
--%>
<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1" %>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  
  <dsp:getvalueof var="trailSize" param="trailSize" />
  <dsp:getvalueof var="facetTrail" param="q_facetTrail" />
  <dsp:getvalueof var="addFacet" param="addFacet" />
  <dsp:getvalueof var="query" param = "q" />
  <dsp:getvalueof var="token" param="token"/>
  <dsp:getvalueof var="sort" param="sort"/>
  <dsp:getvalueof var="pageSize" param="pageSize"/>
     
  <%-- 
    NB.  The following section of code MUST be prior to any code that may result in the response
    being committed, for example, dsp:include tags.  Therefore it must be before the 
    pageContainer tag, and it must use an include directive to include the search.jsp file.  The
    reason this is important is to allow server-side redirects to work.  A response can only be
    set to send a redirect when the response is uncommitted. If you have configured a redirect
    to occur when someone searches for a particular term, for example, then the
    BaseSearchFormHandler needs to have an uncommitted response so it can redirect correctly.
  --%>   
  <dsp:getvalueof var="searchExec" bean="QueryFormHandler.initialSearch"/> 
  <c:if test="${searchExec ne true}">
    <%-- 
      Execute the search if it is has not yet been executed in
      this request e.g on page refresh or load from Bookmark. 
    --%>
    <%@include file="/atgsearch/search.jsp"%>
  </c:if> 
  
  <crs:pageContainer divId="atg_store_facetGlossaryIntro" contentClass="category"
                     index="false" follow="false"
                     bodyClass="category atg_store_searchResults atg_store_leftCol">                            
    <jsp:body>   
      <div id="atg_store_contentHeader">
        <h2 class="title">
          <fmt:message key="search_searchResults.title"/>
        </h2>
      </div>
      
      <div class="atg_store_main">
        <div id="ajaxContainer" >
          <div divId="ajaxRefreshableContent">
            <%-- Render the search results. --%>
            <dsp:include page="/atgsearch/atgSearchResultsRenderer.jsp">
              <dsp:param name="token" value="${token}"/>
            </dsp:include>
          </div>
          
          <div name="transparentLayer" id="transparentLayer"></div>
          
          <div name="ajaxSpinner" id="ajaxSpinner"></div>
        </div>
      </div>
      
      <div class="aside">
        <%-- Render faceting--%>
        <dsp:include page="/facet/gadgets/facetPanelContainer.jsp">
          <dsp:param name="facetTrail" value="${facetTrail}" />
          <dsp:param name="trailSize" value="${trailSize}" />
          <dsp:param name="facetSearchResponse" param="refinement" />
          <dsp:param name="addFacet" value="${addFacet}" />
        </dsp:include>
    
        <%-- Render promotions--%>
        <dsp:include page="/global/gadgets/targetingRandom.jsp">
          <dsp:param name="targeter" bean="/atg/registry/Slots/CategoryPromotionContent1"/>
          <dsp:param name="renderer" value="/promo/gadgets/promotionalContentTemplateRenderer.jsp"/>
          <dsp:param name="elementName" value="promotionalContent"/>
        </dsp:include>
        <dsp:include page="/global/gadgets/targetingRandom.jsp">
          <dsp:param name="targeter" bean="/atg/registry/Slots/CategoryPromotionContent2"/>
          <dsp:param name="renderer" value="/promo/gadgets/promotionalContentTemplateRenderer.jsp"/>
          <dsp:param name="elementName" value="promotionalContent"/>
        </dsp:include>
      </div>
      
      <%-- Browser back button suport --%>
      <script type="text/javascript">
        dojo.require("dojo.back");
        dojo.back.init();
        dojo.back.setInitialState(new HistoryState(""));       
      </script>
      
    </jsp:body>
  </crs:pageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/atgsearch/searchResults.jsp#2 $$Change: 684979 $ --%>
