<%--
  Initalizes the search form depending on whether ATG Search is installed. If 
  ATG Search is not installed Repository Search will be used.
  
  Required Parameters:
    None
  
  Optional Parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/StoreConfiguration" var="storeConfiguration" />
  <dsp:getvalueof var="originatingRequestURL" bean="/OriginatingRequest.requestURI"/>
  
  <dsp:getvalueof var="pageNum" param="p"/>
  <dsp:getvalueof var="viewAll" param="viewAll"/>
 
  <%-- Build the redirect URL when a blank search occurs --%>
  <c:set var="url" value="${originatingRequestURL}"/>
  <%@include file="/navigation/gadgets/navLinkHelper.jspf" %>
  
  <c:if test="${viewAll eq true}">
    <c:url var="url" value="${url}" context="/">
      <c:param name="viewAll" value="${true}"/>
    </c:url>
  </c:if>
  
  <%-- We also want to keep the currently selected page --%>

  <div id="atg_store_search">
    <c:choose>
      <%-- If ATG Search is installed use ATG Search --%>
      <c:when test="${storeConfiguration.atgSearchInstalled == 'true'}">
        <dsp:include page="/atgsearch/gadgets/atgSearch.jsp">
          <dsp:param name="redirectURLOnEmptySearch" value="${url}"/>
        </dsp:include>
      </c:when>
      <%-- If ATG Search is not installed use Repository Search --%>
      <c:otherwise>
        <dsp:include page="/search/gadgets/simpleSearch.jsp">
          <dsp:param name="redirectURLOnEmptySearch" value="${url}"/>
        </dsp:include>
      </c:otherwise>
    </c:choose>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/navigation/gadgets/search.jsp#1 $$Change: 683854 $ --%>
