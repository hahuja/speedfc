<%--
  This JSP will use the CategoryLookup droplet to get products within a category, if ATG Search is
  installed query the search engine to retrieve the facets. It will then Invoke the range droplet
  to get the correct product subset for the current page, do sorting and create a json from the
  results.
  
  Required Parameters:
    categoryId
      When doing a category search, this specifies the category under which to search.

  Optional Parameters:
    p
      The page of search results to render.
    sort
      How the results should be sorted sorted.
    q_facetTrail
      Records the currently supplied faceting on the search results.
    pageSize
      The number of results to display per page
    viewAll
      Indicates all results should be displayed on the page
    isSimpleSearch
      Indicates Repository Search and no facet data should be returned.
    paginationRequest
      Indicates a pagination request which means only product data should be returned in the JSON
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/commerce/multisite/SiteIdForCatalogItem"/>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/store/sort/RangeSortDroplet" />
  
  <dsp:getvalueof var="categoryId" param="categoryId" />
  <dsp:getvalueof var="facetTrail" param="q_facetTrail" />
  <dsp:getvalueof var="viewAll" param="viewAll"/>
  <dsp:getvalueof var="p" param="p"/>
  <dsp:getvalueof var="sort" param="sort"/>
  <dsp:getvalueof var="pageSize" param="pageSize"/>
  <dsp:getvalueof var="isSimpleSearch" param="isSimpleSearch"/>
  <dsp:getvalueof var="paginationRequest" param="paginationRequest"/>
  
  <%-- If search is installed query the search engine for facets --%>
  <c:if test="${isSimpleSearch ne 'true' && paginationRequest ne 'true'}">
    <%@include file="/atgsearch/search.jsp"%>    
  </c:if>
  
  <c:if test="${empty pageSize}">
    <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="/atg/multisite/SiteContext.site.defaultPageSize"/>
  </c:if>
  
  <c:if test="${empty p}">
    <c:set var="p" value="1" />
  </c:if>
  
  <json:object>   
      
    <%-- FACET JSON DATA --%>
    <c:if test="${isSimpleSearch ne 'true' && paginationRequest ne 'true'}">
      <%@include file="/facetjson/facetData.jsp" %>
    </c:if>
    
    <%--
      Get the category repository object according to the id
   
      Input Parameters:
        id - The ID of the category we want to look up
    
      Open Parameters:
        output - Serviced when no errors occur
        error - Serviced when an error was encountered when looking up the category
    
      Output Parameters:
        element - The category whose ID matches the 'id' input parameter  
    --%>
    <dsp:droplet name="CategoryLookup">
      <dsp:param name="id" value="${categoryId}"/>
      <dsp:oparam name="output">     
        <dsp:getvalueof var="productList" param="element.childProducts"/>
        
        <%--
          PAGINATION & SORT BY
          When a pagination request is made nothing else will 
          change so we only need to retrieve the product listings
        --%> 
        <c:if test="${paginationRequest ne 'true'}">
          <dsp:include page="controlsJson.jsp">
            <dsp:param name="size" value="${(fn:length(productList))}"/>
            <dsp:param name="pageSize" value="${pageSize}"/>
          </dsp:include>
        </c:if> 
        
        <%-- Setup Range droplet parameters --%>
        <c:choose>
          <c:when test="${viewAll eq 'true'}">
            <c:set var="howMany" value="5000" />
            <c:set var="start" value="1" />
          </c:when>
          <c:when test="${(fn:length(productList)) <= pageSize}">
            <c:set var="howMany" value="${pageSize}" />
            <c:set var="start" value="${1}" />
          </c:when>
          <c:otherwise>
            <c:set var="howMany" value="${pageSize}" />
            <c:set var="start" value="${((p - 1) * howMany) + 1}" />
          </c:otherwise>
        </c:choose>
      
        <%-- PRODUCT JSON DATA --%>
        <json:object name="products">
        
          <%-- I8N Stuff --%>
          <json:property name="pricePredicate">
            <fmt:message key="common.price.old"/>
          </json:property>
          
          <%-- Products --%>
          <json:array name="items">
              
            <%--
              This droplet renders output parameter for a subset of items of its array parameter.
            
              Input parameters:
                array
                  An array of items from which to extract the subset of items.
                howMany
                  Specifies the number of items to include in subset of items.
                start
                  Specifies the starting index (1-based).
            
              Output parameters:
                element
                  This parameter is set to the current element of the array each
                  time the output parameter is rendered.
                
              Open parameters:
                outputStart
                  This parameter is rendered before any output tags if the subset of
                  the array being displayed is not empty.
                outputEnd
                  This parameter is rendered after all output tags if the subset of
                  the array being displayed is not empty.
                output
                  This parameter is rendered once for each element in the subset of the
                  array that gets displayed.
                empty
                  This parameter is rendered if the array itself, or the
                  requested subset of the array, contains no elements.
            --%>
            <dsp:droplet name="RangeSortDroplet">
              <dsp:param name="array" value="${productList}"/>
              <dsp:param name="sortSelection" value="${sort}"/>
              <dsp:param name="howMany" value="${howMany}"/>
              <dsp:param name="start" value="${start}"/>
        
              <dsp:oparam name="output">
                <dsp:setvalue param="product" paramvalue="element"/>
                <dsp:getvalueof var="productId" param="product.id" />

                <json:object>             
                   <%-- PRODUCT REPOSITORY NAME --%>
                   <json:property name="repository">
                     <dsp:getvalueof var="repositoryName" scope="page" param="product.repository.repositoryName"/>
                     <c:out value="${repositoryName}" escapeXml="false"/>
                   </json:property>
			  
                  <%-- PRODUCT ITEM DESCRIPTOR --%>
                  <json:property name="itemDescriptor">
                    <dsp:getvalueof var="itemDescriptor" scope="page" param="product.itemDescriptor.itemDescriptorName"/>
                    <c:out value="${itemDescriptor}" escapeXml="false"/>
                  </json:property>
			  
                  <%-- PRODUCT ID --%>
                  <json:property name="id">  
                    <dsp:getvalueof var="prodId" param="product.id" />
                    <c:out value="${prodId}" escapeXml="false"/>
                  </json:property>
				
                  <%-- PRODUCT DISPLAY NAME --%>
                  <json:property name="name">
                    <dsp:getvalueof var="displayName" scope="page" param="product.displayName"/>
                    <c:out value="${displayName}" escapeXml="false"/>
                  </json:property>
                            
                  <%-- PRODUCT URL --%>
                  <%--
                    The SiteIdForItemDroplet will return the most appropriate site id for a given 
                    repository item. In this instance we pass in the product repository item and
                    obtain its siteId if there is no specified siteId and we have a product.
                
                    Input Parameters:
                      item - The repository item used to obtain the site id
                  
                    Open Parameters:
                      output - Rendered if there are no errors
                  
                    Output Parameters:
                      siteId - The most appropriate siteId for the 'item' repository item
                  --%>
                  <dsp:droplet name="SiteIdForCatalogItem">
                    <dsp:param name="item" param="product"/>
                    <dsp:oparam name="output">
                      <dsp:getvalueof var="siteId" param="siteId"/>
                    </dsp:oparam>
                  </dsp:droplet>
              
                  <json:property name="url" escapeXml="false">
                    <dsp:include page="/global/gadgets/crossSiteLinkGenerator.jsp">
                      <dsp:param name="product" param="product"/>
                      <dsp:param name="siteId" param="siteId"/>
                      <dsp:param name="queryParams" value="productId=${productId}"/>
                    </dsp:include>
                    <dsp:getvalueof var="siteLinkUrl" param="siteLinkUrl"/>
                    <c:out value="${siteLinkUrl}"/>
                  </json:property>
              
                  <%-- PRODUCT IMAGE --%>
                  <json:property name="imageUrl">
                    <dsp:getvalueof param="product.mediumImage.url" var="imageUrl"/>
                              
                    <%-- Render the image URL, with a default if still empty --%>
                    <c:out value="${imageUrl}" default="/nrsdocroot/content/images/products/medium/MissingProduct_medium.jpg" escapeXml="false"/>         
                  </json:property>
              
                  <%-- PRODUCT PRICE --%>
                  <dsp:getvalueof var="childSKUs" param="product.childSKUs" />
                  <json:object name="price">
                    <c:choose>
                      <%-- One Sku belonging to this product --%>
                      <c:when test="${fn:length(childSKUs) == 1}">
                        <dsp:include page="/productjson/singleSkuPriceJson.jsp">
                          <dsp:param name="product" param="product"/>
                          <dsp:param name="sku" param="product.childSKUs[0]"/>
                        </dsp:include>
                      </c:when>
                      <c:otherwise>
                        <dsp:include page="/productjson/priceRangeJson.jsp">
                          <dsp:param name="product" param="product"/>
                        </dsp:include>   
                      </c:otherwise>
                    </c:choose>
                  </json:object>
                
                </json:object>
              </dsp:oparam>
            </dsp:droplet>
          
          </json:array>
        </json:object>
      
      </dsp:oparam>
    </dsp:droplet>
  </json:object>
      
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/productjson/categorySearchJson.jsp#1 $$Change: 683854 $--%>
