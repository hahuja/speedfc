<%--
  This JSP will use the ProductSearch droplet perform a repository search, invoke the range droplet
  to get the correct products for the current page, do sorting and create a json from the results.
  
  Required Parameters:
    q
      The search question. 

  Optional Parameters:
    p
      The page of search results to render.
    pageSize
      The number of results to display per page
    viewAll
      Indicates all results should be displayed on the page
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Range"/>
  <dsp:importbean bean="/atg/commerce/multisite/SiteIdForCatalogItem"/>
  <dsp:importbean bean="/atg/multisite/Site"/>
  <dsp:importbean bean="/atg/dynamo/droplet/multisite/GetSiteDroplet" />
  
  <dsp:getvalueof var="query" param="q" />
  <dsp:getvalueof var="viewAll" param="viewAll"/>
  <dsp:getvalueof var="p" param="p"/>
  <dsp:getvalueof var="pageSize" param="pageSize"/>
  
  <%-- configure and execute the search --%>
  <c:choose>
    <c:when test="${viewAll eq 'true'}">
      <dsp:setvalue bean="ProductSearch.resultsPerPage" value="-1"/>
      <dsp:setvalue bean="ProductSearch.currentPage" value="-1"/>
    </c:when>
    <c:otherwise>
      <%-- Use the pageSize param, if it's empty use the site property, if it's still empty, default to 12 --%>
      <c:if test="${empty pageSize}">
        <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="Site.defaultPageSize"/>

        <c:if test="${empty pageSize}">
          <c:set var="pageSize" value="12"/>
        </c:if>
      </c:if>

      <dsp:setvalue bean="ProductSearch.resultsPerPage" value="${pageSize}"/>
      <dsp:setvalue bean="ProductSearch.currentPage" value="${empty p ? '1' : p}"/>
    </c:otherwise>
  </c:choose>
  
  <dsp:setvalue bean="ProductSearch.dummySearchText" value="Search" />
  <dsp:setvalue bean="ProductSearch.searchInput" value="${query}" />
  <dsp:setvalue bean="ProductSearch.search" value="" />
  
  <%-- Get the results and create the JSON--%>
  <dsp:getvalueof var="productArray" bean="ProductSearch.searchResults"/>
  
  <%-- 
    Set inputs for the Range droplet which gives us the correct
    subset of results depending on the current page number and pageSize 
  --%>
  <c:choose>
    <c:when test="${viewAll eq 'true'}">
      <c:set var="howMany" value="5000" />
      <c:set var="start" value="1" />
    </c:when>
    <c:when test="${(fn:length(productArray)) <= pageSize}">
      <c:set var="howMany" value="${pageSize}" />
      <c:set var="start" value="${1}" />
    </c:when>
    <c:otherwise>
      <c:set var="howMany" value="${pageSize}" />
      <c:set var="start" value="${((p - 1) * howMany) + 1}" />
    </c:otherwise>
  </c:choose>
  
  <json:object> 
  
    <%-- PAGINATION JSON DATA --%>
    <dsp:getvalueof bean="ProductSearch.resultSetSize" var="size"/>
          
    <dsp:include page="controlsJson.jsp">
      <dsp:param name="size" value="${size}"/>
      <dsp:param name="pageSize" value="${pageSize}"/>
      <dsp:param name="returnSortJSON" value="${false}"/>
    </dsp:include>
    
    <%-- PRODUCT JSON DATA --%>
    <json:object name="products">
    
      <%-- I8N Stuff --%>
      <json:property name="pricePredicate">
        <fmt:message key="common.price.old"/>
      </json:property>
      
      <%-- Products --%>
      <json:array name="items">
        
        <%--
          This droplet renders output parameter for a subset of items of its array parameter.
            
          Input parameters:
            array
              An array of items from which to extract the subset of items.
            howMany
              Specifies the number of items to include in subset of items.
            start
              Specifies the starting index (1-based).
            
          Output parameters:
            element
              This parameter is set to the current element of the array each
              time the output parameter is rendered.
                
          Open parameters:
            outputStart
              This parameter is rendered before any output tags if the subset of
              the array being displayed is not empty.
            outputEnd
              This parameter is rendered after all output tags if the subset of
              the array being displayed is not empty.
            output
              This parameter is rendered once for each element in the subset of the
              array that gets displayed.
            empty
              This parameter is rendered if the array itself, or the
              requested subset of the array, contains no elements.
          --%>
        <dsp:droplet name="Range">
          <dsp:param name="array" value="${productArray}"/>
          <dsp:param name="howMany" value="${howMany}"/>
          <dsp:param name="start" value="${start}"/>
        
          <dsp:oparam name="output">
            <dsp:setvalue param="product" paramvalue="element"/>
            <dsp:getvalueof var="productId" param="product.id" />

            <json:object>             
               <%-- PRODUCT REPOSITORY NAME --%>
              <json:property name="repository">
                <dsp:getvalueof var="repositoryName" scope="page" param="product.repository.repositoryName"/>
                <c:out value="${repositoryName}" escapeXml="false"/>
              </json:property>
			  
              <%-- PRODUCT ITEM DESCRIPTOR --%>
              <json:property name="itemDescriptor">
                <dsp:getvalueof var="itemDescriptor" scope="page" param="product.itemDescriptor.itemDescriptorName"/>
                <c:out value="${itemDescriptor}" escapeXml="false"/>
              </json:property>
			  
              <%-- PRODUCT ID --%>
              <json:property name="id">  
                <dsp:getvalueof var="prodId" param="product.id" />
                <c:out value="${prodId}" escapeXml="false"/>
              </json:property>

              <%-- PRODUCT DISPLAY NAME --%>
              <json:property name="name">
                <dsp:getvalueof var="displayName" scope="page" param="product.displayName"/>
                <c:out value="${displayName}" escapeXml="false"/>
              </json:property>
                            
              <%-- PRODUCT URL --%>
              <%--
                The SiteIdForItemDroplet will return the most appropriate site id for a given 
                repository item. In this instance we pass in the product repository item and
                obtain its siteId if there is no specified siteId and we have a product.
                
                Input Parameters:
                  item - The repository item used to obtain the site id
                 
                Open Parameters:
                  output - Rendered if there are no errors
                  
                Output Parameters:
                  siteId - The most appropriate siteId for the 'item' repository item
              --%>
              <dsp:droplet name="SiteIdForCatalogItem">
                <dsp:param name="item" param="product"/>
                <dsp:oparam name="output">
                  <dsp:getvalueof var="siteId" param="siteId"/>
                </dsp:oparam>
              </dsp:droplet>
              
              <json:property name="url" escapeXml="false">
                <dsp:include page="/global/gadgets/crossSiteLinkGenerator.jsp">
                  <dsp:param name="product" param="product"/>
                  <dsp:param name="siteId" param="siteId"/>
                  <dsp:param name="queryParams" value="productId=${productId}"/>
                </dsp:include>
                <dsp:getvalueof var="siteLinkUrl" param="siteLinkUrl"/>
                <c:out value="${siteLinkUrl}"/>
              </json:property>
              
              <%-- PRODUCT IMAGE --%>
              <json:property name="imageUrl">
                <dsp:getvalueof param="product.mediumImage.url" var="imageUrl"/>
                              
                <%-- Render the image URL, with a default if still empty --%>
                <c:out value="${imageUrl}" default="/nrsdocroot/content/images/products/medium/MissingProduct_medium.jpg" escapeXml="false"/>         
              </json:property>
              
              <%-- PRODUCT PRICE --%>
              <dsp:getvalueof var="childSKUs" param="product.childSKUs" />
              <json:object name="price">
                <c:choose>
                  <%-- One Sku belonging to this product --%>
                  <c:when test="${fn:length(childSKUs) == 1}">
                    <dsp:include page="/productjson/singleSkuPriceJson.jsp">
                      <dsp:param name="product" param="product"/>
                      <dsp:param name="sku" param="product.childSKUs[0]"/>
                    </dsp:include>
                  </c:when>
                  <c:otherwise>
                    <dsp:include page="/productjson/priceRangeJson.jsp">
                      <dsp:param name="product" param="product"/>
                    </dsp:include>   
                  </c:otherwise>
                </c:choose>
              </json:object>
              
              <%-- PRODUCT SITE --%>
              <dsp:getvalueof var="currentSiteId" bean="Site.id"/>
              <c:if test="${siteId ne  currentSiteId}">
                <%-- 
                  GetSiteDroplet is used to get a Site object for a given siteId. In this instance
                  its used to retrieve the site details (name, icon) for the site with this site id.
      
                  Input Parameters:
                    siteId - A site id
        
                  Open Parameters:
                    output - Rendered once if the site is found
      
                  Output Parameters:
                    site - The Site object representing siteId
                --%>
                <dsp:droplet name="GetSiteDroplet">
                  <dsp:param name="siteId" value="${siteId}"/>
                  <dsp:oparam name="output">
                    <dsp:getvalueof param="site.name" var="siteName"/>
                  </dsp:oparam>
                </dsp:droplet>
              
                <json:property name="site">
                  <c:out value="${siteName}"/>
                </json:property>
              </c:if>
                
            </json:object>
          </dsp:oparam>
        </dsp:droplet>
          
      </json:array>
    </json:object>
  </json:object>
      
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/productjson/searchJson.jsp#1 $$Change: 683854 $--%>
