<%--
  Constructs the sort by and pagination control JSON.
  
  Required Parameters:
    size 
      The total number of results
  
  Optional Parameters:
    q
      The search question.  (Required when doing a query search)
    categoryId
      When doing a category search, this specifies the category under which to search. (Required 
      when doing a category search).
    q_facetTrail
      Records the currently supplied faceting on the search results.
    pageSize
      The number of results to display per page
    viewAll
      Indicates all results should be displayed on the page
    token
      The current search engine token
    pagesAvailable
      The total number of pages available to navigate 
    returnSortJSON
      Defaults to true, if set to false the sort JSON is not returned 
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/multisite/Site"/>
  
  <dsp:getvalueof var="query" param = "q" />
  <dsp:getvalueof var="sort" param = "sort" />
  <dsp:getvalueof var="sortSelection" param = "sortSelection" />
  <dsp:getvalueof var="pageSize" param = "pageSize" />
  <dsp:getvalueof var="facetTrail" param="q_facetTrail" />
  <dsp:getvalueof var="categoryId" param="categoryId" />
  <dsp:getvalueof var="viewAll" param="viewAll" />
  
  <dsp:getvalueof var="returnSortJSON" param="returnSortJSON" />
  <c:if test="${empty returnSortJSON}">
    <c:set var="returnSortJSON" value="${true}"/>
  </c:if>

  <dsp:getvalueof var="pageSize" param="pageSize"/>
  <c:if test="${empty pageSize || pageSize eq 0}">
    <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="Site.defaultPageSize"/>
  </c:if>
  
  <dsp:getvalueof var="size" param="size"/>
  
  <%-- Determine the total number of pages available if it hasn't been supplied --%>
  <dsp:getvalueof var="pagesAvailable" param="pagesAvailable"/>
  <c:if test="${empty pagesAvailable }"/>
  <c:choose>
    <c:when test="${size mod pageSize gt 0}">
      <c:set var="pagesAvailable" value="${(size div pageSize) + 1}"/>
    </c:when>
    <c:otherwise>
      <c:set var="pagesAvailable" value="${(size div pageSize)}"/>
    </c:otherwise>
  </c:choose>
  
  <%-- 
    PAGINATION JSON
    Only render the pagination controls if the number
    of results is greater than the results per page
  --%>
  <c:if test="${size gt pageSize}">           
    <json:object name="pagination">
      
      <%-- I8N Stuff --%>
      <json:property name="viewAllLabel">
        <fmt:message key="common.button.viewAllText"/>
      </json:property>
      
      <json:property name="goToPageLabel">
        <fmt:message key="common.paginationTitleJSON"/>
      </json:property>
      
      <%-- Values --%>
      <json:property name="viewAll" value="${viewAll }"/>      
      <json:array name="pages">
        <c:forEach var="i" begin="1" end="${pagesAvailable}" step="1" varStatus ="status">
          <c:set var="start" value="${((i - 1) * pageSize) + 1}" />
          <json:object>
            <json:property name="linkText">${i}</json:property>
            <json:property name="startValue">${start}</json:property>
          </json:object>                
        </c:forEach>
      </json:array>
    </json:object>
  </c:if>
    
  <%-- SORT BY JSON DATA --%>  
  <c:if test="${returnSortJSON == 'false'}">
    <json:object name="sortBy">
      <%-- I8N Stuff --%>
      <json:property name="topPicksLabel">
        <fmt:message key="common.topPicks"/>
      </json:property>
    
      <json:property name="nameAZLabel">
        <fmt:message key="sort.nameAZ"/>
      </json:property>
    
      <json:property name="nameZALabel">
        <fmt:message key="sort.nameZA"/>
      </json:property>
    
      <json:property name="priceLHLabel">
        <fmt:message key="sort.priceLH"/>
      </json:property>
    
      <json:property name="priceHLLabel">
        <fmt:message key="sort.priceHL"/>
      </json:property>
     
      <%-- Values --%>
      <json:property name="sort" value="${sort}"/>
      <json:property name="sortSelection" value="${sortSelection}"/>
    </json:object>
  </c:if>
  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/productjson/controlsJson.jsp#1 $$Change: 683854 $--%>
