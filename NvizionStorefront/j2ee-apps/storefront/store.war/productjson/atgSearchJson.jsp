<%--
  This JSP will query the search engine for the facets & products then create a json using the
  results.
  
  Required Parameters:
    (Either)
    q
      The search question.  (Required when doing a query search)
    (Or)
    categoryId
      When doing a category search, this specifies the category under which to search. (Required 
      when doing a category search).

  Optional Parameters:
    p
      The page of search results to render.
    sort
      How the results are sorted.
    facetTrail
      Records the currently supplied faceting on the search results.
    addFacet
      Specifies a new facet which should be applied, and added to the facet trail.
    pageSize
      The number of results to display per page
    trailSize
      The trail size (number of facets traversed) upto this point
    viewAll
      Indicates all results should be displayed on the page
    paginationRequest
      Indicates we are here due to a pagination request. We dont need to return Facet, Sort-by 
      or Pagination data in the JSON.
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
  <dsp:importbean bean="/atg/commerce/multisite/SiteIdForCatalogItem"/>
  <dsp:importbean bean="/atg/dynamo/droplet/multisite/GetSiteDroplet" />
  <dsp:importbean bean="/atg/multisite/Site"/>
  <dsp:importbean bean="/atg/search/droplet/GetClickThroughId"/>

  <dsp:getvalueof var="paginationRequest" param="paginationRequest"/>  
    
  <%-- Perform the search --%>
  <%@include file="/atgsearch/search.jsp" %>
  
  <%-- Get the product results --%>
  <dsp:getvalueof bean="QueryFormHandler.searchResponse.results" var="productListings"/>
  <dsp:getvalueof bean="QueryFormHandler.searchResponse.groupCount" var="size"/>

  <json:object>    
    
    <%-- Updated Search Token --%>    
    <json:property name="token" value="${token }"/>
    
    <c:if test="${paginationRequest ne 'true'}">
      <%-- FACET JSON DATA --%>
      <%@include file="/facetjson/facetData.jsp" %>
    
      <%-- 
        PAGINATION & SORT BY JSON
      --%>
      <dsp:getvalueof bean="QueryFormHandler.pagesAvailable" var="pagesAvailable"/>
      
      <dsp:include page="controlsJson.jsp">
        <dsp:param name="pagesAvailable" value="${pagesAvailable}"/>
        <dsp:param name="size" value="${size}"/>
        <dsp:param name="pageSize" value="${pageSize}"/>
      </dsp:include>
    </c:if>
        
    <%-- PRODUCT JSON DATA --%>
    <json:object name="products">
        
      <%-- I8N Stuff --%>
      <json:property name="pricePredicate">
        <fmt:message key="common.price.old"/>
      </json:property>
      
      <%-- Products --%>
      <json:array name="items">
      
        <c:forEach var="element" items="${productListings}" varStatus="loopStatus">
          <dsp:param name="element" value="${element}"/>
          <dsp:getvalueof var="productId" param="element.document.properties.$repositoryId" />
          
          <%--
            Generates unique search click identifier for each search result. The generated search click
            identifier will be appended to product links on search results page as URL parameter and will
            notify reporting service. This parameter tells reporting service that user navigated to the product
            page from the search results and which search query returned the product.
            
            Input Parameters:
              result
                Search result to generate search click identifier for.
                
            Open Parameters:
              output
                Always serviced.
                
            Output Parameters:
              searchClickId
                Generated search click identifier. 
            
           --%>
          <dsp:droplet name="GetClickThroughId">
            <dsp:param name="result" param="element"/>
            <dsp:oparam name="output">

              <json:object>
                <%--
                  Get the product according to the ID returned from the ATG Search results
               
                    Input Parameters:
                      id - The ID of the product we want to look up
                      filterBySite - The site to filter by, or false if no filter should be applied
                      filterByCatalog - The catalog to filter by, or false if no filter should be applied
                  
                    Open Parameters:
                      output - Serviced when no errors occur
                      error - Serviced when an error was encountered when looking up the product
                      empty - Serviced when no product is found
                
                    Output Parameters:
                      element - The product whose ID matches the 'id' input parameter  
                --%>
                <dsp:droplet name="ProductLookup">
                  <dsp:param bean="/OriginatingRequest.requestLocale.locale" name="repositoryKey"/>
                  <dsp:param name="id" value="${productId}"/>
                  <dsp:param name="filterBySite" value="false"/>
                  <dsp:param name="filterByCatalog" value="false"/>
                
                  <dsp:oparam name="output">
                    <dsp:setvalue param="product" paramvalue="element"/>
                  
                    <%-- PRODUCT REPOSITORY NAME --%>
                    <json:property name="repository">
                      <dsp:getvalueof var="repositoryName" scope="page" param="product.repository.repositoryName"/>
                      <c:out value="${repositoryName}" escapeXml="false"/>
                    </json:property>
            
                    <%-- PRODUCT ITEM DESCRIPTOR --%>
                    <json:property name="itemDescriptor">
                      <dsp:getvalueof var="itemDescriptor" scope="page" param="product.itemDescriptor.itemDescriptorName"/>
                      <c:out value="${itemDescriptor}" escapeXml="false"/>
                    </json:property>
            
                    <%-- PRODUCT ID --%>
                    <json:property name="id">  
                      <dsp:getvalueof var="prodId" param="product.id" />
                      <c:out value="${prodId}" escapeXml="false"/>
                    </json:property>        
            
                    <%-- PRODUCT DISPLAY NAME --%>
                    <json:property name="name">
                      <dsp:getvalueof var="displayName" scope="page" param="product.displayName"/>
                      <c:out value="${displayName}" escapeXml="false"/>
                    </json:property>
                                
                    <%-- PRODUCT URL --%>
                    <%--
                      The SiteIdForItemDroplet will return the most appropriate site id for a given 
                      repository item. In this instance we pass in the product repository item and
                      obtain its siteId if there is no specified siteId and we have a product.
                    
                      Input Parameters:
                        item - The repository item used to obtain the site id
                      
                      Open Parameters:
                        output - Rendered if there are no errors
                      
                      Output Parameters:
                        siteId - The most appropriate siteId for the 'item' repository item
                    --%>
                    <dsp:droplet name="SiteIdForCatalogItem">
                      <dsp:param name="item" param="product"/>
                      <dsp:oparam name="output">
                        <dsp:getvalueof var="siteId" param="siteId"/>
                      </dsp:oparam>
                    </dsp:droplet>
                  
                    <json:property name="url" escapeXml="false">
                      <%--
                        Get the generated search click identifier for the product search result and add it as product URL parameter so that
                        reporting service can identify when user gets to product page from search results.
                      --%>
                      <dsp:getvalueof var="searchClickId" param="searchClickId"/>
                      <dsp:include page="/global/gadgets/crossSiteLinkGenerator.jsp">
                        <dsp:param name="product" param="product"/>
                        <dsp:param name="siteId" value="${siteId}"/>
                        <dsp:param name="queryParams" value="productId=${productId}&searchClickId=${searchClickId}"/>
                      </dsp:include>
                      <dsp:getvalueof var="siteLinkUrl" param="siteLinkUrl"/>
                      <c:out value="${siteLinkUrl}"/>
                    </json:property>
                  
                    <%-- PRODUCT IMAGE --%>
                    <json:property name="imageUrl">
                      <dsp:getvalueof param="product.mediumImage.url" var="imageUrl"/>
                                
                      <%-- Render the image URL, with a default if still empty --%>
                      <c:out value="${imageUrl}" default="/nrsdocroot/content/images/products/medium/MissingProduct_medium.jpg" escapeXml="false"/>        
                    </json:property>
                  
                    <%-- PRODUCT PRICE --%>
                    <dsp:getvalueof var="childSKUs" param="product.childSKUs" />
                    <json:object name="price">
                      <c:choose>
                        <%-- One Sku belonging to this product --%>
                        <c:when test="${fn:length(childSKUs) == 1}">
                          <dsp:include page="/productjson/singleSkuPriceJson.jsp">
                            <dsp:param name="product" param="product"/>
                            <dsp:param name="sku" param="product.childSKUs[0]"/>
                          </dsp:include>
                        </c:when>
                        <c:otherwise>
                          <dsp:include page="/productjson/priceRangeJson.jsp">
                            <dsp:param name="product" param="product"/>
                          </dsp:include>   
                        </c:otherwise>
                      </c:choose>
                    </json:object>
                    
                    <%-- PRODUCT SITE --%>
                    <dsp:getvalueof var="currentSiteId" bean="Site.id"/>
                    <c:if test="${siteId ne  currentSiteId}">
                      <json:property name="site">
                        <%-- 
                          GetSiteDroplet is used to get a Site object for a given siteId. In this instance
                          its used to retrieve the site details (name, icon) for the site with this site id.
          
                          Input Parameters:
                            siteId - A site id
            
                          Open Parameters:
                            output - Rendered once if the site is found
            
                          Output Parameters:
                            site - The Site object representing siteId
                        --%>
                        <dsp:droplet name="GetSiteDroplet">
                          <dsp:param name="siteId" value="${siteId}"/>
                          <dsp:oparam name="output">
                            <dsp:valueof param="site.name"/>
                          </dsp:oparam>
                        </dsp:droplet>
                      </json:property>
                    </c:if>
                  
                  </dsp:oparam>
                </dsp:droplet>
                
              </json:object>
            </dsp:oparam>
          </dsp:droplet>
        </c:forEach>
      
      </json:array>
    </json:object>
  </json:object>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/productjson/atgSearchJson.jsp#1 $$Change: 683854 $--%>
