<%--
  This JSP determines if we are using ATG Search, Repository Search or Category Search and delegates
  the JSON creation responsibility to the correct JSP.
  
  Required Parameters:
    (Either)
    q
      The search question.  (Required when doing a query search)
    (Or)
    categoryId
      When doing a category search, specifies the category under which to search. (Required when 
      doing a category search).
  
  Optional Parameters:
    paginationRequest
      Indicates we are here due to a pagination request. We dont need to return Facet, Sort-by 
      or Pagination data in the JSON.
    facetTrail
      The current facetTrail string
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/StoreConfiguration"/>
  <dsp:importbean bean="/atg/targeting/TargetingFirst"/>

  <dsp:getvalueof var="atgSearchInstalled" bean="StoreConfiguration.atgSearchInstalled" />
  <dsp:getvalueof var="categoryId" param="categoryId"/>
  <dsp:getvalueof var="query" param="q"/>
  <dsp:getvalueof var="facetTrail" param="q_facetTrail"/>
      
  <%-- Indicates a pagination request, we only need to return product data in the json --%>
  <dsp:getvalueof var="paginationRequest" param="paginationRequest"/>
  <c:if test="${empty paginationRequest}">
    <c:set var="paginationRequest" value="${false}"/>
  </c:if>
    
  <%-- Determine how to handle this request for JSON --%>
  <c:choose>
  
    <%-- Query Search --%>
    <c:when test="${!empty query}">
      <c:choose>
        <%-- ATG Search --%>
        <c:when test="${atgSearchInstalled eq 'true'}">
          <dsp:include page="atgSearchJson.jsp">
            <dsp:param name="paginationRequest" value="${paginationRequest}"/>
          </dsp:include> 
        </c:when>
        <%-- Repository Search --%>
        <c:otherwise>
          <dsp:include page="searchJson.jsp"/>
        </c:otherwise>
      </c:choose>
    </c:when>
        
    <%-- Category Search --%>
    <c:when test="${!empty categoryId}">
    
      <c:if test="${atgSearchInstalled}">
        <%-- 
          When the categoryId is set we need to look at the facetTrail to determine if we are
          navigating a facet, in which case we want to return products from the search engine.
          If we arent navigating a facet or are navigating the default facet return products
          from the CatalogLookup droplet
          
          Input Parameters:
            targeter - Specifies the targeter service that will perform
                       the targeting
                             
            howMany - the maximum number of target items to display
                  
          Open Parameters:
            empty - No targets were found
                
            output - At least 1 target was found
                  
          Output Parameters:
            element - the result of a target operation
        --%>
        <dsp:droplet name="TargetingFirst">
          <dsp:param name="howMany" value="1" />
          <dsp:param name="targeter"
                     bean="/atg/registry/RepositoryTargeters/RefinementRepository/GlobalCategoryFacet" />

          <dsp:oparam name="output">
            <dsp:getvalueof var="globalRefinementId" param="element.repositoryId" />
            <c:set var="defaultFacet" value="${globalRefinementId}:${categoryId}"/>
            
            <c:if test="${defaultFacet eq facetTrail }">
              <c:set var="navigatingDefaultFacet" value="true"/>
            </c:if>
            
          </dsp:oparam>
        </dsp:droplet>
      </c:if> 
    
      <c:choose>
        <%-- Retrieve products from CategoryLookup --%> 
        <c:when test="${empty facetTrail || navigatingDefaultFacet eq 'true'}">
          <dsp:include page="categorySearchJson.jsp">
            <dsp:param name="isSimpleSearch" value="${!atgSearchInstalled}"/>
            <dsp:param name="paginationRequest" value="${paginationRequest}"/>
          </dsp:include>
        </c:when>
        <%-- Retrieve products from ATG Search, we are navigating a facet --%> 
        <c:otherwise>
          <dsp:include page="atgSearchJson.jsp">
            <dsp:param name="paginationRequest" value="${paginationRequest}"/>
          </dsp:include>
        </c:otherwise>
      </c:choose> 
    </c:when>
    
    <%-- Error --%>
    <c:otherwise>
      <json:object>
        <json:property name="error"></json:property>
      </json:object>
    </c:otherwise>
    
  </c:choose>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/productjson/productData.jsp#1 $$Change: 683854 $--%>
