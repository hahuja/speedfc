<%-- 
  This gadget renders the template URL of a promotionalContent item.
  
  Required Parameters:
    promotionalContent
      The promotionalContent repository item
    
  Optional Parameters:
    None  
--%>

<dsp:page> 
  <dsp:getvalueof var="pageurl" idtype="java.lang.String" 
                  param="promotionalContent.template.url"/>
  <dsp:include page="${pageurl}">
    <dsp:param name="promotionalContent" param="promotionalContent"/>
  </dsp:include>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/promo/gadgets/promotionalContentTemplateRenderer.jsp#1 $$Change: 683854 $--%>
