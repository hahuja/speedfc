<%--
  This tag generates a URL for a 'Continue Shopping' link and stores this in the 'continueShoppingURL' page scope variable.
  The resulting 'Continue Shopping' link will be to the users last browsed category, or to '/index.jsp' if this doesn't exist
  or is not valid for the users current context.

  The JSP including this tag can then use 'continueShoppingURL' to display the 'Continue Shopping' link.

  Required attributes:
    None.
 
  Optional attributes:
    None.

  Example:
    <crs:continueShopping>
      <c:set var="continueShoppingLink" value="/index.jsp"/>

      <c:if test="${not empty continueShoppingURL}">
        <c:set var="continueShoppingLink" value="${continueShoppingURL}"/>
      </c:if>

      <dsp:a href="${continueShoppingLink}">
        <fmt:message key="common.button.continueShoppingText"/>
      </dsp:a>
    </crs:continueShopping>
--%>

<%@include file="/includes/taglibs.jspf"%>
<%@include file="/includes/context.jspf"%>

<%-- 'continueShoppingURL' page scope variable to store the URL --%>
<%@ variable name-given="continueShoppingURL" variable-class="java.lang.String" %>

<dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>

<%-- Obtain the current request context root path --%>
<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>

<%-- Obtain the users last browsed category --%>
<dsp:getvalueof var="categoryLastBrowsed" bean="Profile.categoryLastBrowsed"/>

<%--
  Check if the user has a last browsed category and generate the link based on this or otherwise
  set the continue shiopping link to '/index.jsp'.
--%>
<c:choose>
  <c:when test="${not empty categoryLastBrowsed}">
    <%--
      Check if the last category browsed belongs to the user's current Profile catalog and if the
      item belongs to the current site. 
      
      Input parameters:
        id
          The repository id for the catalog.

      Output parameters:
        output
          If the category belongs to the user's current Profile catalog.
        wrongCatalog 
          If the category does not belong to the user's current catalog.
        noCatalog 
          If the current user does not have a catalog.
        wrongSite  
          If the category is not found on the current site.
        error
          If an error occurred during lookup.
        empty
          If the category is not found.
    --%>
    <dsp:droplet name="CategoryLookup">
      <dsp:param name="id" bean="Profile.categoryLastBrowsed"/>

      <dsp:oparam name="output">
        <%--  --%>
        <dsp:getvalueof var="templateURL" vartype="java.lang.String" param="element.template.url"/>

        <c:choose>
          <c:when test="${not empty templateURL}">
            <dsp:getvalueof var="categoryId" vartype="java.lang.String" param="element.repositoryId"/>
            <c:url var="continueShoppingURL" context="${contextroot}" value="${templateURL}">
              <c:param name="categoryId" value="${categoryId}"/>
            </c:url>
          </c:when>
          <c:otherwise>
             <%-- No Id in profile for last category browsed. --%>
             <c:set var="continueShoppingURL" value="${contextroot}/index.jsp"/>
          </c:otherwise>
        </c:choose><%-- End is empty check on category --%>
      </dsp:oparam>

      <dsp:oparam name="wrongCatalog">
        <%-- Category not found in this catalog lookup. --%>
        <c:url var="continueShoppingURL" context="${contextroot}" value="/index.jsp"/>
      </dsp:oparam>

      <dsp:oparam name="noCatalog">
        <%-- No catalog found in this catalog lookup. --%>
        <c:url var="continueShoppingURL" context="${contextroot}" value="/index.jsp"/>
      </dsp:oparam>

      <dsp:oparam name="wrongSite">
        <%-- Category not found in current site catalog. --%>
        <c:url var="continueShoppingURL" context="${contextroot}" value="/index.jsp"/>
      </dsp:oparam>

      <dsp:oparam name="error">
        <%-- An error has occurred during lookup. --%>
        <c:url var="continueShoppingURL" context="${contextroot}" value="/index.jsp"/>
      </dsp:oparam>

      <dsp:oparam name="empty">
        <%-- Category not found with lookup. --%>
        <c:url var="continueShoppingURL" context="${contextroot}" value="/index.jsp"/>
      </dsp:oparam>
    </dsp:droplet>
  </c:when>
  <c:otherwise>
    <%-- No Id in profile for last category browsed --%>
    <c:url var="continueShoppingURL" context="${contextroot}" value="/index.jsp"/>
  </c:otherwise>
</c:choose>

<jsp:doBody/>

<%-- @version $Id$$Change$--%>
