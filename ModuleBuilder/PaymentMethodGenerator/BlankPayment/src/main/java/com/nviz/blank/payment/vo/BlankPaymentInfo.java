package com.nviz.blank.payment.vo;

public class BlankPaymentInfo {

	private double amountAppliedToOrder;

	public double getAmountAppliedToOrder() {
		return amountAppliedToOrder;
	}

	public void setAmountAppliedToOrder(double amountAppliedToOrder) {
		this.amountAppliedToOrder = amountAppliedToOrder;
	}
}
