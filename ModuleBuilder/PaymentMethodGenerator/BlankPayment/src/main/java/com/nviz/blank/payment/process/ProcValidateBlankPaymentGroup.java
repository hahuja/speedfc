package com.nviz.blank.payment.process;

import atg.commerce.order.PaymentGroup;
import atg.commerce.order.processor.ValidatePaymentGroupPipelineArgs;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.nviz.blank.payment.name.BlankPaymentGroup;

public class ProcValidateBlankPaymentGroup extends GenericService implements
		PipelineProcessor {

	private static int SUCCESS_CODE = 1;
	private static int[] RETURN_CODES = { SUCCESS_CODE };

	@Override
	public int[] getRetCodes() {
		return RETURN_CODES;
	}

	@Override
	public int runProcess(Object pParam, PipelineResult pResult)
			throws Exception {
		ValidatePaymentGroupPipelineArgs args;
		// Dynamo guarantees that the pipeline parameter object
		// passed to a payment group validation processor will be
		// of type ValidatePaymentGroupPipelineArgs.
		args = (ValidatePaymentGroupPipelineArgs) pParam;
		PaymentGroup paymentGroup = args.getPaymentGroup();
		// Now try casting the payment group to the type we expect
		// and validating the fields. If the payment group is of
		// the wrong type, or if anything else goes wrong, add an
		// error to the pipeline result so the order manager will
		// abort the checkout process.
		try {
			BlankPaymentGroup points = (BlankPaymentGroup) paymentGroup;
		} catch (ClassCastException cce) {
			pResult.addError("ClassNotRecognized",
					"Expected a Blank payment group, but got "
							+ paymentGroup.getClass().getName() + " instead.");
		}
		return SUCCESS_CODE;
	}

}
