package com.nviz.blank.payment.name;

import atg.commerce.order.ChangedProperties;
import atg.commerce.order.PaymentGroup;

public interface BlankPaymentGroup extends PaymentGroup, ChangedProperties {

}
