package com.nviz.blank.payment.process;

import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.nviz.blank.payment.name.BlankPaymentGroup;
import com.nviz.blank.payment.vo.BlankPaymentInfo;

public class ProcCreateBlankPaymentInfo extends GenericService implements
		PipelineProcessor {
	public static final int SUCCESS = 1;

	private String blankPaymentInfoClass;

	protected BlankPaymentInfo getBlankPaymentInfo() throws Exception {
		if (isLoggingDebug())
			logDebug("Making a new instance of type: "
					+ this.getBlankPaymentInfoClass());
		BlankPaymentInfo blankPaymentInfo = (BlankPaymentInfo) Class.forName(
				this.getBlankPaymentInfoClass()).newInstance();
		return blankPaymentInfo;
	}
	
	public int runProcess(Object pParam, PipelineResult pResult)
			throws Exception {
		PaymentManagerPipelineArgs params = (PaymentManagerPipelineArgs) pParam;
		Order order = params.getOrder();
		BlankPaymentGroup blankPaymentGroup = (BlankPaymentGroup) params.getPaymentGroup();
		double amount = params.getAmount();
		// create and populate store points info class
		BlankPaymentInfo blankPaymentInfo = this.getBlankPaymentInfo();
		if (isLoggingDebug())
			logDebug("Putting BlankPaymentInfo object into pipeline: "
					+ blankPaymentInfo.toString());
		params.setPaymentInfo(blankPaymentInfo);
		return SUCCESS;
	}

	// ----------------------------------------------------------------------
	/**
	 * Return the possible return values for this processor. This processor
	 * always returns a success code.
	 **/
	public int[] getRetCodes() {
		int retCodes[] = { SUCCESS };
		return retCodes;
	}

	public String getBlankPaymentInfoClass() {
		return blankPaymentInfoClass;
	}

	public void setBlankPaymentInfoClass(String blankPaymentInfoClass) {
		this.blankPaymentInfoClass = blankPaymentInfoClass;
	}
}
