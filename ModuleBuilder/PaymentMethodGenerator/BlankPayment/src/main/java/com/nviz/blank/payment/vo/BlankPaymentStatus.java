package com.nviz.blank.payment.vo;

import atg.payment.PaymentStatusImpl;

public class BlankPaymentStatus extends PaymentStatusImpl {

	private String statusCode = null;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
}
