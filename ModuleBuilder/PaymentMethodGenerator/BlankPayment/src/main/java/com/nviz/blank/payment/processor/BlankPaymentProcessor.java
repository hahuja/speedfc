package com.nviz.blank.payment.processor;

import com.nviz.blank.payment.vo.BlankPaymentInfo;
import com.nviz.blank.payment.vo.BlankPaymentStatus;

public interface BlankPaymentProcessor {
	
	public BlankPaymentStatus authorize(BlankPaymentInfo pStorePointsInfo);
	public BlankPaymentStatus debit(BlankPaymentInfo pStorePointsInfo, BlankPaymentStatus pStatus);
	public BlankPaymentStatus credit(BlankPaymentInfo pStorePointsInfo, BlankPaymentStatus pStatus);
	public BlankPaymentStatus credit(BlankPaymentInfo pStorePointsInfo);
}
