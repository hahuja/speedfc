package com.nviz.blank.payment.name;

import com.nviz.blank.payment.constant.BlankPaymentConstants;

import atg.commerce.order.PaymentGroupImpl;

public class BlankPaymentGroupImpl extends PaymentGroupImpl implements
		BlankPaymentGroup {

	
	public String getUserId() {
		return (String) getPropertyValue(BlankPaymentConstants.USER_ID_NAME);
	}

	public void setUserId(String pUserId) {
		setPropertyValue(BlankPaymentConstants.USER_ID_NAME, pUserId);
	}
	
	public double getAmountAppliedToOrder(){
		String amount = "0";
		if(getPropertyValue(BlankPaymentConstants.AMOUNT_FIELD_NAME) != null){
			amount = (String) getPropertyValue(BlankPaymentConstants.AMOUNT_FIELD_NAME);
		}
		return Double.parseDouble(amount);
	}
	
	public void setAmountAppliedToOrder(double amountAppliedToOrder){
		setPropertyValue(BlankPaymentConstants.AMOUNT_FIELD_NAME, amountAppliedToOrder);
	}
}
