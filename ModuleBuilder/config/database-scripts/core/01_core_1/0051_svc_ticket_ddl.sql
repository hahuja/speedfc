



create table svct_research_act (
	activity_id	varchar2(40)	not null,
	ksession_id	varchar2(40)	not null
,constraint svct_researchact_p primary key (activity_id)
,constraint svct_researchact_f foreign key (activity_id) references tkt_activity (activity_id));

-- Problem category

create table svct_prob_cat (
	problem_cat_id	varchar2(40)	not null,
	name	varchar2(254)	not null,
	logical_org_id	varchar2(40)	null
,constraint svct_prbct_p primary key (problem_cat_id)
,constraint svct_prbct_og_f foreign key (logical_org_id) references dlo_logical_org (logical_org_id));

create index svct_prbct_og_ix on svct_prob_cat (logical_org_id);



