


--     ************************** /atg/svc/shared/ServiceSharedRepository ******************************************  
-- 
-- 
--     It is a good idea to dedicate different tablespaces to different categories of tables. Mostly the categorization is on the basis of growth and volatility of data. The application tables can be grouped into the following categories:  
--     1) STATIC: Those that are kind of static in size like pricelists, catalogs etc. These undergo a change periodically.   
--     2) GROWTH: Those that grow at a faster rate with the passage of time and size of the business. They are mostly appended. Transaction tables, like orders is an example.  
--     3) UPDATE: Those that have a lot of insert and update activities.   
--     4) TEMPORARY: Those that have insert, update and deletes like messaging table (dms tables are an example of this)  
--     The type of table space type for each table is noted in the "Table Space Type" comment  
-- 
-- 
--     Table: svc_topic, Repository Item: Topic    Table Space Type: STATIC    A tree of topics used to classify solutions.  

create table svc_topic (
	topic_id	varchar2(40)	not null,
	parent_id	varchar2(40)	null,
	name	varchar2(200)	null,
	display_name	varchar2(200)	null,
	description	varchar2(1000)	null,
	is_primary	number(1)	not null
,constraint svc_topic_p primary key (topic_id)
,constraint svc_topic_fk1 foreign key (parent_id) references svc_topic (topic_id));

create index svc_topic1_ix on svc_topic (parent_id);
create index svc_topic_ix1 on svc_topic (name);
--     Table: svc_topicchild_seq, Repository Item: Topic.children    Table Space Type: STATIC    An ordered list of child topics  

create table svc_topicchild_seq (
	parent_topic_id	varchar2(40)	not null,
	child_topic_id	varchar2(40)	not null,
	sequence_num	number(10)	not null
,constraint svc_topicchild_p primary key (parent_topic_id,sequence_num)
,constraint svc_topicchild_fk1 foreign key (parent_topic_id) references svc_topic (topic_id));

--     Table: svc_topiclabel, Repository Item: TopicLabel    Table Space Type: STATIC    Marks the root of a view of the topic tree designated by the topic label  

create table svc_topiclabel (
	label_id	varchar2(80)	not null,
	name	varchar2(200)	not null
,constraint svc_topiclabel_p primary key (label_id)
,constraint svc_topic_un unique (name));

--     Table: svc_topic_label, Repository Item: TopicLabel.topicId    Table Space Type: STATIC    The topic defined for the topic label  

create table svc_topic_label (
	label_id	varchar2(40)	not null,
	topic_id	varchar2(40)	not null
,constraint svc_topic_label_p primary key (label_id)
,constraint svctopiclabel_fk1 foreign key (topic_id) references svc_topic (topic_id));

create index svctopiclabel1_ix on svc_topic_label (topic_id);
--     Table: svc_topicusecount, Repository Item: Topic.useCount    Table Space Type: UPDATE    *** Deprecated - at least for 2006.3  ***  

create table svc_topicusecount (
	topic_id	varchar2(40)	not null,
	solnclass_id	varchar2(40)	not null,
	usecount	number(10)	not null
,constraint svc_topusecnt_p primary key (topic_id,solnclass_id)
,constraint svc_topicuse_fk1 foreign key (topic_id) references svc_topic (topic_id));

--     Table: svc_topic_macro, Repository Item: TopicMacro    Table Space Type: STATIC  

create table svc_topic_macro (
	macro_id	varchar2(40)	not null,
	macro_name	varchar2(80)	not null,
	learn_id	varchar2(40)	null,
	def	varchar2(1666)	not null
,constraint svc_topicmacro_p primary key (macro_id));

--     Table: svc_topicmacro_seq, Repository Item: Topic.macros    Table Space Type: STATIC  

create table svc_topicmacro_seq (
	topic_id	varchar2(40)	not null,
	macro_id	varchar2(40)	not null,
	sequence_num	number(10)	not null
,constraint svc_topicmacros_p primary key (topic_id,sequence_num)
,constraint svc_topicmac_fk1 foreign key (topic_id) references svc_topic (topic_id)
,constraint svc_topicmac_fk2 foreign key (macro_id) references svc_topic_macro (macro_id));

create index svc_topicmac2_ix on svc_topicmacro_seq (macro_id);
--     Table: svc_global_macro, Repository Item: GlobalMacro    Table Space Type: STATIC  

create table svc_global_macro (
	macro_id	varchar2(40)	not null,
	macro_name	varchar2(80)	not null,
	learn_id	varchar2(40)	null,
	def	varchar2(1666)	not null
,constraint svc_globalmacro_p primary key (macro_id));

--     Table: svc_topic_pattern, Repository Item: TopicPattern    Table Space Type: STATIC  

create table svc_topic_pattern (
	pattern_id	varchar2(40)	not null,
	learn_id	varchar2(40)	null,
	pattern	varchar2(1000)	not null,
	weight	number(10)	not null,
	language	varchar2(16)	null,
	groups	varchar2(200)	null,
	is_enabled	number(1)	not null
,constraint svc_topicpattern_p primary key (pattern_id));

--     Table: svc_topic_pat_seq, Repository Item: Topic.patterns    Table Space Type: STATIC  

create table svc_topic_pat_seq (
	topic_id	varchar2(40)	not null,
	pattern_id	varchar2(40)	not null,
	sequence_num	number(10)	not null
,constraint svc_topicpats_p primary key (topic_id,sequence_num)
,constraint svc_topicpats_fk1 foreign key (topic_id) references svc_topic (topic_id)
,constraint svc_topicpats_fk2 foreign key (pattern_id) references svc_topic_pattern (pattern_id));

create index svc_topicpats2_ix on svc_topic_pat_seq (pattern_id);
--     Table: svc_ksession, Repository Item: KnowledgeSession    Table Space Type: GROWTH  
--      Tracks user activity related to a problem solving session for a given problem.     NOTE: This table is analogous to the pt_search_session table, but is meant to track problem history     for application purposes only.  Events are logged separately for reporting purposes.     A problem can span multiple login sessions. Every time someone works on a problem we record     a new knowledge session. The session ends when they work on a new problem, logout or click 'start over'.  

create table svc_ksession (
	session_id	varchar2(40)	not null,
	session_start	timestamp	not null,
	session_end	timestamp	null,
	user_id	varchar2(40)	null,
	ticket_id	varchar2(40)	null
,constraint svc_ksession_p primary key (session_id));

create index svc_ksession_ix1 on svc_ksession (user_id);
--     Table: svc_viewed_answer, Repository Item: ViewedAnswer    Table Space Type: GROWTH  
--      Tracks answers viewed during a knowledge session.     NOTE: This table was part of the pt_feedback table in ES 4.0, but is meant to track problem history     for application purposes only.  Events are logged separately for reporting purposes.  

create table svc_viewed_answer (
	answer_id	varchar2(40)	not null,
	view_date	timestamp	not null,
	type	number(10)	not null,
	excerpt	varchar2(4000)	null,
	title	varchar2(2048)	null,
	context_id	varchar2(1000)	null,
	server_group_id	varchar2(40)	null,
	url	varchar2(400)	null,
	solution_id	varchar2(40)	null,
	soln_version	varchar2(40)	null
,constraint svc_view_answer_p primary key (answer_id));

create index svc_viewanswer_ix1 on svc_viewed_answer (url);
create index svc_viewanswer_ix2 on svc_viewed_answer (solution_id);
--     Table: svc_rec_answer:, Repository Item: RecommendedAnswer    Table Space Type: GROWTH    Tracks use of documents or solutiosn as recommended answers to a problem  

create table svc_rec_answer (
	rec_answer_id	varchar2(40)	not null,
	date_created	date	not null,
	date_modified	date	not null,
	created_by	varchar2(50)	not null,
	modified_by	varchar2(254)	not null,
	ticket_id	varchar2(40)	null,
	link_state	number(10)	not null,
	doc_type	number(10)	not null,
	excerpt	varchar2(4000)	null,
	title	varchar2(2048)	null,
	context_id	varchar2(1000)	null,
	server_group_id	varchar2(40)	null,
	url	varchar2(400)	null,
	solution_id	varchar2(40)	null,
	soln_version	varchar2(40)	null
,constraint svc_rec_answer_p primary key (rec_answer_id));

create index svc_recanswer_ix1 on svc_rec_answer (url);
create index svc_recanswer_ix2 on svc_rec_answer (solution_id);
create index svc_recanswer_ix3 on svc_rec_answer (ticket_id);
--     Table: svc_sess_view_ans, Repository Item: KnowledgeSession.viewedAnswers    Table Space Type: GROWTH    Tracks answers viewed for a problem solving session for a given problem.  

create table svc_sess_view_ans (
	session_id	varchar2(40)	not null,
	sequence_num	number(10)	not null,
	answer_id	varchar2(40)	not null
,constraint svc_sess_vw_ans_p primary key (session_id,sequence_num)
,constraint svc_sessans_fk1 foreign key (session_id) references svc_ksession (session_id)
,constraint svc_sessans_fk2 foreign key (answer_id) references svc_viewed_answer (answer_id));

create index svc_sessans1_ix on svc_sess_view_ans (answer_id);
--     Table: svc_session_link, Repository Item: KnowledgeSession.linkedDocuments    Table Space Type: GROWTH    Tracks which recommended answers were suggested (i.e. linked) during a knowledge session.  

create table svc_session_link (
	session_id	varchar2(40)	not null,
	rec_answer_id	varchar2(40)	not null
,constraint svc_sess_link_p primary key (session_id,rec_answer_id)
,constraint svc_sesslink_fk1 foreign key (session_id) references svc_ksession (session_id)
,constraint svc_sesslink_fk2 foreign key (rec_answer_id) references svc_rec_answer (rec_answer_id));

create index svc_sesslink_ix2 on svc_session_link (rec_answer_id);
--     Table: svc_session_reject, Repository Item: KnowledgeSession.rejectedDocuments    Table Space Type: GROWTH    Tracks which recommended answers were suggested (i.e. linked) during a knowledge session.  

create table svc_session_reject (
	session_id	varchar2(40)	not null,
	rec_answer_id	varchar2(40)	not null
,constraint svc_sess_reject_p primary key (session_id,rec_answer_id)
,constraint svc_sessreject_fk1 foreign key (session_id) references svc_ksession (session_id)
,constraint svc_sessreject_fk2 foreign key (rec_answer_id) references svc_rec_answer (rec_answer_id));

create index svc_sessreject_ix2 on svc_session_reject (rec_answer_id);
--     Table: svc_query, Repository Item: KnowledgeQuery    Table Space Type: GROWTH    Tracks queries performed either during a knowledge session or as favorite searches.    NOTE: This table is analogous to the pt_query table in ES 4.0, but is meant to track problem history    for application purposes only.  Events are logged separately for reporting purposes.  

create table svc_query (
	query_id	varchar2(40)	not null,
	query_date	timestamp	not null,
	query_type	number(10)	not null,
	browse_topic_id	varchar2(40)	null,
	search_req_type	number(10)	null
,constraint svc_query_p primary key (query_id));

--     Table: svc_query_pred, Repository Item: KnowledgeQueryPredicate    Table Space Type: GROWTH  

create table svc_query_pred (
	predicate_id	varchar2(40)	not null,
	query_id	varchar2(40)	not null,
	field_name	varchar2(255)	null,
	pred_type	number(10)	not null,
	value_type	number(10)	not null,
	case_insensitive	number(10)	null,
	op_type	number(10)	not null,
	boolean_value	number(10)	null,
	num_upper_value	number(6)	null,
	num_lower_value	number(6)	null,
	date_upper_value	date	null,
	date_lower_value	date	null
,constraint svc_querypred_p primary key (predicate_id)
,constraint svc_qrypred_fk1 foreign key (query_id) references svc_query (query_id));

create index svc_qrypred_idx1 on svc_query_pred (query_id);
--     Table: svc_search_text, Repository Item: KnowledgeQuery.searchTextValues    Table Space Type: GROWTH  

create table svc_search_text (
	query_id	varchar2(40)	not null,
	sequence_num	number(10)	not null,
	search_text	varchar2(1666)	not null
,constraint svc_searchtext_p primary key (query_id,sequence_num)
,constraint svc_srchtext_fk1 foreign key (query_id) references svc_query (query_id));

--     Table: svc_pred_text, Repository Item: KnowledgeQueryPredicate.textValues    Table Space Type: GROWTH  

create table svc_pred_text (
	predicate_id	varchar2(40)	not null,
	sequence_num	number(10)	not null,
	text_value	varchar2(1666)	not null
,constraint svc_predvseqt_p primary key (predicate_id,sequence_num)
,constraint svc_predtext_fk1 foreign key (predicate_id) references svc_query_pred (predicate_id));

--     Table: svc_session_query, Repository Item: KnowledgeSession.queries    Table Space Type: GROWTH  

create table svc_session_query (
	session_id	varchar2(40)	not null,
	sequence_num	number(10)	not null,
	query_id	varchar2(40)	not null
,constraint svc_prob_query_p primary key (session_id,sequence_num)
,constraint svc_sessquery_fk1 foreign key (session_id) references svc_ksession (session_id)
,constraint svc_sessquery_fk2 foreign key (query_id) references svc_query (query_id));

create index svc_sessquery1_ix on svc_session_query (query_id);
--     Table: svc_fav_query, Repository Item: FavoriteQuery    Table Space Type: UPDATE  

create table svc_fav_query (
	query_id	varchar2(40)	not null,
	user_id	varchar2(40)	not null,
	name	varchar2(40)	not null,
	fav_type	varchar2(40)	not null,
	description	varchar2(200)	null
,constraint svc_favquery_p primary key (query_id)
,constraint svc_favquery_fk1 foreign key (query_id) references svc_query (query_id));

create index svc_fav_query_ix1 on svc_fav_query (user_id,fav_type);
--     Table: svc_fav_query_org, Repository Item: FavoriteQuery.organizationIds    Table Space Type: STATIC    The groups this favorite query is "pushed" to  

create table svc_fav_query_org (
	query_id	varchar2(40)	not null,
	org_id	varchar2(40)	not null
,constraint svc_favqueryorg_p primary key (query_id,org_id)
,constraint svc_favqryorg_fk1 foreign key (query_id) references svc_query (query_id));

--     Table: svc_window_attrb, Repository Item: WindowAttribute    Table Space Type: UPDATE  

create table svc_window_attrb (
	window_attrb_id	varchar2(80)	not null,
	session_id	varchar2(100)	null,
	window_id	varchar2(80)	null,
	map_key	varchar2(256)	not null,
	serialized_data	blob	null
,constraint svc_wndattrb_p primary key (window_attrb_id));

create index svcwndatt1_ix on svc_window_attrb (session_id);
create index svcwndatt2_ix on svc_window_attrb (window_id);
create index svcwndatt3_ix on svc_window_attrb (map_key);
--     Table: svc_sstatus_tdefn, Repository Item: SolutionStatusTaskDefn    Table Space Type: STATIC  

create table svc_sstatus_tdefn (
	id	varchar2(40)	not null,
	status_id	varchar2(40)	not null,
	workflow_name	varchar2(255)	not null,
	task_id	varchar2(40)	not null
,constraint svcsolnsttsk_p primary key (id)
,constraint svc_solnsttsk_su unique (task_id,workflow_name,status_id));

--     Table: svc_soln_solnstatus, Repository Item: SolutionSolutionStatus    Table Space Type: GROWTH    The status of a solution - tracked per solution version  

create table svc_soln_sstatus (
	id	varchar2(40)	not null,
	soln_id	varchar2(40)	not null,
	soln_version	number(10)	not null,
	status_id	varchar2(40)	not null,
	deployed	number(1)	not null,
	last_modify_date	date	not null
,constraint svcssolnstatus_p primary key (id)
,constraint svc_ssolnstatus_su unique (soln_id,soln_version));

--     Table: svc_solnrelevance, Repository Item: SolutionRelevance    Table Space Type: GROWTH  

create table svc_solnrelevance (
	id	varchar2(40)	not null,
	soln_id	varchar2(40)	not null,
	soln_class_id	varchar2(40)	not null,
	usecount	number(10)	not null,
	hotusecount	number(10)	not null,
	viewcount	number(10)	not null,
	isqoaa	number(1)	not null,
	last_use_date	timestamp	not null,
	last_view_date	timestamp	not null,
	score_hot	number(26,7)	null,
	score_top	number(26,7)	null,
	score_qoaa	number(26,7)	null
,constraint svc_solnrel_p primary key (id)
,constraint svc_solnrel_su unique (soln_id));

create index svc_solnrel_sci on svc_solnrelevance (soln_class_id);
create index svc_solnrel_uci on svc_solnrelevance (usecount);
create index svc_solnrel_hui on svc_solnrelevance (hotusecount);
create index svc_solnrel_vci on svc_solnrelevance (viewcount);
create index svc_solnrel_lui on svc_solnrelevance (last_use_date);
create index svc_solnrel_lvi on svc_solnrelevance (last_view_date);
create index svc_solnlst_hscri on svc_solnrelevance (score_hot);
create index svc_solnlst_tscri on svc_solnrelevance (score_top);
create index svc_solnlst_qscri on svc_solnrelevance (score_qoaa);
--     Table: svc_user_favorites, Repository Item: Favorites    Table Space Type: UPDATE  

create table svc_user_favorites (
	favorites_id	varchar2(40)	not null,
	user_id	varchar2(40)	not null,
	soln_id	varchar2(40)	null,
	title	varchar2(255)	not null,
	context_id	varchar2(1000)	null,
	group_id	varchar2(40)	null,
	doc_url	varchar2(400)	null,
	doc_soln_type	varchar2(40)	not null
,constraint svc_favorites_p primary key (favorites_id));

create index svc_user_favs_ix1 on svc_user_favorites (user_id);
--     Table: svc_recommend_read, Repository Item: RecommendedReading    Table Space Type: UPDATE  

create table svc_recommend_read (
	recommend_id	varchar2(40)	not null,
	user_id	varchar2(40)	not null,
	soln_id	varchar2(40)	null,
	title	varchar2(255)	not null,
	context_id	varchar2(1000)	null,
	group_id	varchar2(40)	null,
	doc_url	varchar2(400)	null,
	doc_soln_type	varchar2(40)	not null
,constraint svc_recommend_p primary key (recommend_id));

create index svc_recommend_ix1 on svc_recommend_read (user_id);
--     Table: svc_usr_srch, Repository Item:     Table Space Type: GROWTH    Tracks saved search.    *** DEPRECATED ***  

create table svc_usr_srch (
	srch_id	varchar2(40)	not null,
	user_id	varchar2(40)	not null,
	srch_text	varchar2(500)	not null
,constraint svc_usrsrch_p primary key (srch_id));

--     Table: svc_soln_redirect, Repository Item: SolutionRedirect     Table Space Type: GROWTH    Defines a forwarding reference for a solution that was deleted because of a merge.  

create table svc_soln_redirect (
	id	varchar2(40)	not null,
	deleted_soln_id	varchar2(40)	not null,
	deleted_soln_disp_id	varchar2(40)	not null,
	forward_soln_id	varchar2(40)	not null
,constraint svc_solredirect_p primary key (id)
,constraint svc_solredir_ix1 unique (deleted_soln_id)
,constraint svc_solredir_ix2 unique (deleted_soln_disp_id));

create index svc_solredir_ix3 on svc_soln_redirect (forward_soln_id);
--     Table: svc_link_status, Repository Item: LinkStatus     Defines a link validation status.  

create table svc_link_status (
	status_id	varchar2(40)	not null,
	val	number(10)	not null
,constraint svc_link_status_id primary key (status_id));

--     Table: svc_link_type, Repository Item: LinkType    Defines a link validation status.  

create table svc_link_type (
	type_id	varchar2(40)	not null,
	val	number(10)	not null
,constraint svc_link_type_id primary key (type_id));

--     Table: svc_link, Repository Item: Link     Defines a broken link.  

create table svc_link (
	id	varchar2(40)	not null,
	solution_id	varchar2(40)	not null,
	soln_status_id	varchar2(40)	null,
	soln_owning_group_id	varchar2(40)	null,
	url	varchar2(254)	not null,
	status_id	varchar2(40)	not null,
	attempts_number	number(10)	not null,
	scan_time	timestamp	null,
	externally_visible	number(1)	not null,
	link_type	varchar2(40)	null,
	link_text	varchar2(120)	null
,constraint svc_link_id primary key (id)
,constraint svc_link_fk1 foreign key (status_id) references svc_link_status (status_id)
,constraint svc_link_fk2 foreign key (link_type) references svc_link_type (type_id));

create index svc_link_ix4 on svc_link (status_id);
create index svc_link_ix5 on svc_link (link_type);
create index svc_link_ix1 on svc_link (solution_id);
create index svc_link_ix2 on svc_link (soln_status_id);
create index svc_link_ix3 on svc_link (soln_owning_group_id);
--     Table: svc_link_validation_stat, Repository Item: LinkValidationStatisticsRecord	Table Space Type: GROWTH    A table of records which decribes link validation service statistics  

create table svc_link_validation_rep (
	report_id	varchar2(40)	not null,
	completion_time	timestamp	not null
,constraint svc_link_v_rep_id primary key (report_id));




