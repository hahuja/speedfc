


-- Message Store tables
-- Message table

create table tkt_ads_messages (
	msg_id	varchar2(40)	not null,
	channel_id	number(10)	not null,
	trans_comp_path	varchar2(254)	null,
	encoding	varchar2(40)	null,
	subject	varchar2(512)	null,
	text_body	clob	null,
	html_body	clob	null,
	xml_body	clob	null
,constraint tkt_ads_messages_p primary key (msg_id)
,constraint tkt_ads_messages_f foreign key (msg_id) references tkt_ads_act_data (act_data_id));

create index tkt_ads_msg1_x on tkt_ads_messages (channel_id);
-- Raw Message table

create table tkt_ads_raw_msgs (
	raw_msg_id	varchar2(40)	not null,
	channel_id	number(10)	not null,
	trans_comp_path	varchar2(254)	null,
	content	blob	null,
	status	number(10)	not null,
	creation_time	timestamp	not null,
	retry_count	number(10)	not null,
	type	number(10)	not null
,constraint tkt_ads_raw_msgs_p primary key (raw_msg_id));

-- Inbound Message table

create table tkt_ads_in_msgs (
	msg_id	varchar2(40)	not null,
	classification	clob	null,
	raw_msg_id	varchar2(40)	not null
,constraint tkt_ads_in_msgs_p primary key (msg_id)
,constraint tkt_ads_m_inmsgs_f foreign key (msg_id) references tkt_ads_act_data (act_data_id)
,constraint tkt_ads_r_inmsgs_f foreign key (raw_msg_id) references tkt_ads_raw_msgs (raw_msg_id));

create index tkt_ads_inrmsg1_ix on tkt_ads_in_msgs (raw_msg_id);
-- Outbound Message table

create table tkt_ads_out_msgs (
	msg_id	varchar2(40)	not null,
	send_format	number(10)	not null,
	response_type	number(10)	not null,
	last_modified_time	timestamp	not null,
	status	number(10)	not null,
	reason_text	varchar2(254)	null,
	claim_owner	varchar2(254)	null,
	claim_date	timestamp	null
,constraint tkt_ads_out_msgs_p primary key (msg_id)
,constraint tkt_ads_outmsgs_f foreign key (msg_id) references tkt_ads_act_data (act_data_id));

create index tkt_ads_out_m1_x on tkt_ads_out_msgs (status);
create index tkt_ads_out_m3_x on tkt_ads_out_msgs (claim_date);
-- Message Attachment table

create table tkt_ads_msg_atts (
	att_id	varchar2(40)	not null,
	content_id	varchar2(40)	null,
	content	blob	null,
	filename	varchar2(254)	null,
	type	number(10)	not null,
	content_id_hdr	varchar2(40)	null,
	att_size	number(10)	null
,constraint tkt_ads_msg_atts_p primary key (att_id));

-- List of Message Attachments

create table tkt_ads_msgattlist (
	msg_id	varchar2(40)	not null,
	idx	number(10)	not null,
	att_id	varchar2(40)	not null
,constraint tkt_ads_attchl_p primary key (msg_id,idx)
,constraint tkt_ads_attchl_m_f foreign key (msg_id) references tkt_ads_act_data (act_data_id)
,constraint tkt_ads_attchl_a_f foreign key (att_id) references tkt_ads_msg_atts (att_id));

create index tkt_ads_atl_at1_ix on tkt_ads_msgattlist (att_id);
-- Message Address table

create table tkt_ads_msg_addrs (
	addr_id	varchar2(40)	not null,
	type	varchar2(40)	not null,
	personal_name	varchar2(254)	null,
	addr_value	varchar2(254)	null
,constraint tkt_ads_msg_adds_p primary key (addr_id));


create table tkt_ads_msgaddlist (
	msg_id	varchar2(40)	not null,
	idx	number(10)	not null,
	addr_id	varchar2(40)	not null
,constraint tkt_ads_addrl_p primary key (msg_id,idx)
,constraint tkt_ads_addrl_m_f foreign key (msg_id) references tkt_ads_act_data (act_data_id)
,constraint tkt_ads_addrl_a_f foreign key (addr_id) references tkt_ads_msg_addrs (addr_id));

create index tkt_ads_adl_ad1_ix on tkt_ads_msgaddlist (addr_id);
-- Message Headers table

create table tkt_ads_msg_hdrs (
	msg_id	varchar2(40)	not null,
	name	varchar2(40)	not null,
	hdr_value	varchar2(254)	null
,constraint tkt_msghdrs_a_f foreign key (msg_id) references tkt_ads_act_data (act_data_id));

create index tkt_msghdrs_a1_ix on tkt_ads_msg_hdrs (msg_id);
-- Message Properties table

create table tkt_ads_msg_props (
	msg_id	varchar2(40)	not null,
	name	varchar2(40)	not null,
	prop_value	varchar2(254)	null
,constraint tkt_msgprops_a_f foreign key (msg_id) references tkt_ads_act_data (act_data_id));

create index tkt_msgprops_a1_ix on tkt_ads_msg_props (msg_id);
-- Raw SMTP Message table

create table tkt_ads_smtp_msgs (
	raw_msg_id	varchar2(40)	not null,
	rcpt_to	varchar2(254)	null,
	ip_address	varchar2(254)	null,
	from_address	varchar2(254)	null
,constraint tkt_ads_smtp_msg_p primary key (raw_msg_id)
,constraint tkt_ads_smtp_msg_f foreign key (raw_msg_id) references tkt_ads_raw_msgs (raw_msg_id));

-- Raw POP3 Message table

create table tkt_ads_pop3_msgs (
	raw_msg_id	varchar2(40)	not null,
	account_name	varchar2(254)	null,
	ip_address	varchar2(254)	null
,constraint tkt_ads_pop3_msg_p primary key (raw_msg_id)
,constraint tkt_ads_pop3_msg_f foreign key (raw_msg_id) references tkt_ads_raw_msgs (raw_msg_id));

-- Raw SMS Message table

create table tkt_ads_sms_msgs (
	raw_msg_id	varchar2(40)	not null,
	toolkit	varchar2(254)	null,
	smsc	varchar2(254)	null,
	source_address	varchar2(40)	not null,
	source_ton	number(10)	null,
	source_npi	number(10)	null,
	dest_address	varchar2(40)	not null,
	destination_ton	number(10)	null,
	destination_npi	number(10)	null
,constraint tkt_ads_sms_msgs_p primary key (raw_msg_id)
,constraint tkt_ads_sms_msgs_f foreign key (raw_msg_id) references tkt_ads_raw_msgs (raw_msg_id));

-- Raw MMS Message table

create table tkt_ads_mms_msgs (
	raw_msg_id	varchar2(40)	not null,
	toolkit	varchar2(254)	null,
	mmsc	varchar2(254)	null
,constraint tkt_ads_mms_msgs_p primary key (raw_msg_id)
,constraint tkt_ads_mms_msgs_f foreign key (raw_msg_id) references tkt_ads_raw_msgs (raw_msg_id));




