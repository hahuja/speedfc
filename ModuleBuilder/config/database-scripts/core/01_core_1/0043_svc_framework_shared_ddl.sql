


--     ************************** /atg/svc/shared/ServiceSharedRepository ******************************************  
-- 
-- 
--     Table: svc_recent_tkts, Repository Item: RecentTickets    Table Space Type: UPDATE  

create table svc_recent_tkts (
	recent_ticket_id	varchar2(40)	not null,
	user_id	varchar2(40)	not null,
	ticket_id	varchar2(40)	not null,
	ticket_access_type	varchar2(40)	not null
,constraint svc_rcnt_tkts_p primary key (recent_ticket_id));




