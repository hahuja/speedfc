


--  @version $ $

create table ssvc_logging (
	id	varchar2(40)	not null,
	profile_id	varchar2(40)	not null,
	organization_id	varchar2(40)	null,
	session_id	varchar2(100)	null,
	parent_session_id	varchar2(100)	null,
	ip_address	varchar2(39)	null,
	event_date	timestamp	null,
	env_name	varchar2(80)	null,
	site_id	varchar2(40)	null,
	site_name	varchar2(80)	null,
	version	number(10)	not null,
	audit_type	number(10)	null
,constraint ssvc_logging_p primary key (id));


create table ssvc_update_prof (
	id	varchar2(40)	not null,
	updated_item_id	varchar2(40)	null
,constraint ssvc_upd_prof_p primary key (id)
,constraint ssvc_upd_prof_f foreign key (id) references ssvc_logging (id));


create table ssvc_prof_props (
	id	varchar2(40)	not null,
	property_name	varchar2(100)	null,
	update_type	number(10)	not null,
	old_value	varchar2(255)	null,
	new_value	varchar2(255)	null,
	version	number(10)	not null,
	profile_update_id	varchar2(40)	null
,constraint ssvcprofupd_p primary key (id));


create table ssvc_session_end (
	id	varchar2(40)	not null,
	session_create	timestamp	not null,
	duration	number(19)	not null
,constraint ssvc_sess_end_p primary key (id)
,constraint ssvc_sess_end_f foreign key (id) references ssvc_logging (id));


create table ssvc_rate_ans (
	id	varchar2(40)	not null,
	doc_id	varchar2(255)	null,
	doc_type	varchar2(40)	null,
	ans_text	varchar2(255)	null,
	contact_support	number(10)	null
,constraint ssvc_rate_ans_p primary key (id)
,constraint ssvc_rate_ans_f foreign key (id) references ssvc_logging (id));


create table ssvc_rate_event (
	id	varchar2(40)	not null,
	rating_comment	varchar2(255)	null,
	rating_name	varchar2(255)	null,
	query_id	varchar2(40)	null,
	email	varchar2(255)	null
,constraint ssvc_rate_event_p primary key (id)
,constraint ssvc_rate_evt_f foreign key (id) references ssvc_logging (id));


create table ssvc_view_ans (
	id	varchar2(40)	not null,
	ans_id	varchar2(40)	null
,constraint ssvc_view_ans_p primary key (id)
,constraint ssvc_view_ans_f foreign key (id) references ssvc_logging (id));


create table ssvc_ticket (
	id	varchar2(40)	not null,
	ticket_id	varchar2(40)	null
,constraint ssvc_ticket_p primary key (id)
,constraint ssvc_ticket_f foreign key (id) references ssvc_logging (id));




