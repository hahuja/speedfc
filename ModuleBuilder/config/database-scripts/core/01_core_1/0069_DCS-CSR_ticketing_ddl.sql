


--  @version $Id: //application/DCS-CSR/version/10.1/src/sql/DCS-CSR_ticketing_ddl.xml#1 $$Change: 683854 $

create table csrt_grant_appease (
	id	varchar2(40)	not null,
	appeasement_id	varchar2(40)	null,
	amount	number(19,7)	null
,constraint csrt_grnt_apps_p primary key (id));


create table csrt_price_overrde (
	id	varchar2(40)	not null,
	order_id	varchar2(40)	null,
	component_id	varchar2(40)	null,
	component_type	varchar2(40)	null,
	old_price	number(19,7)	null,
	new_price	number(19,7)	null
,constraint csrt_prc_ovrrd_p primary key (id));


create table csrt_order_event (
	id	varchar2(40)	not null,
	order_id	varchar2(40)	null,
	amount	number(19,7)	null
,constraint csrt_ord_ev_p primary key (id));


create table csrt_return_order (
	id	varchar2(40)	not null,
	ret_req_id	varchar2(40)	null,
	repl_order_id	varchar2(40)	null
,constraint csrt_ret_ord_p primary key (id));


create table csrt_recv_rtrn_itm (
	id	varchar2(40)	not null,
	item_id	varchar2(40)	null,
	quantity	number(19,0)	null
,constraint csrt_rcv_rt_it_p primary key (id));


create table csrt_claim_item (
	id	varchar2(40)	not null,
	claimable_id	varchar2(40)	null,
	claimable_type	varchar2(40)	null
,constraint csrt_claim_item_p primary key (id));


create table csrt_ci_event (
	id	varchar2(40)	not null,
	item_id	varchar2(40)	null,
	old_quantity	number(19,0)	null,
	new_quantity	number(19,0)	null
,constraint csrt_comm_item_p primary key (id));


create table csrt_pg_event (
	id	varchar2(40)	not null,
	pay_group_id	varchar2(40)	null,
	update_type	number(1)	not null
,constraint csrt_pay_grp_p primary key (id));


create table csrt_split_sg (
	id	varchar2(40)	not null,
	src_ship_group_id	varchar2(40)	null,
	dest_ship_group_id	varchar2(40)	null,
	commerce_item_id	varchar2(40)	null,
	quantity	number(10)	not null
,constraint csrt_splt_sg_p primary key (id));


create table csrt_split_cc (
	id	varchar2(40)	not null,
	src_cost_ctr_id	varchar2(40)	null,
	dest_cost_ctr_id	varchar2(40)	null,
	commerce_ident_id	varchar2(40)	null,
	quantity	number(10)	not null
,constraint csrt_splt_cc_p primary key (id));


create table csrt_sg_event (
	id	varchar2(40)	not null,
	ship_group_id	varchar2(40)	null,
	update_type	number(10)	not null
,constraint csrt_sg_evnt_p primary key (id));


create table csrt_order_comment (
	id	varchar2(40)	not null,
	comment_id	varchar2(40)	null
,constraint csrt_ord_cmnt_p primary key (id));


create table csrt_update_org (
	id	varchar2(40)	not null,
	updated_item_id	varchar2(40)	null
,constraint csrt_upd_org_p primary key (id));


create table csrt_orders (
	order_id	varchar2(40)	not null,
	ticket_id	varchar2(40)	not null
,constraint csrt_orders_p primary key (order_id,ticket_id));


create table csrt_oma_event (
	id	varchar2(40)	not null,
	man_adj_id	varchar2(40)	null,
	adjustment_type	integer	not null,
	update_type	number(1)	not null,
	reason	integer	not null
,constraint csrt_oma_event_p primary key (id));


create table csrt_schd_event (
	id	varchar2(40)	not null,
	sch_order_id	varchar2(40)	null,
	update_type	number(1)	not null
,constraint csrt_schd_event_p primary key (id));


create table csrt_appr_event (
	id	varchar2(40)	not null,
	approval_id	varchar2(40)	null,
	update_type	number(1)	not null
,constraint csrt_appr_event_p primary key (id));


create table csrt_order_appr_event (
	id	varchar2(40)	not null,
	order_id	varchar2(40)	not null
,constraint csrtordapprevt_p primary key (id)
,constraint csrtordapprevt_f foreign key (id) references csrt_appr_event (id));


create table csrt_grt_prom_event (
	id	varchar2(40)	not null,
	promo_id	varchar2(40)	null
,constraint csrt_grprm_ev_p primary key (id));


create table csrt_ign_prom_event (
	id	varchar2(40)	not null,
	promo_id	varchar2(40)	null,
	order_id	varchar2(40)	null
,constraint csrt_igprm_ev_p primary key (id));


create table csrt_gl_event (
	id	varchar2(40)	not null,
	giftlist_id	varchar2(40)	null,
	event_name	varchar2(64)	null
,constraint csrt_gl_event_p primary key (id));


create table csrt_gi_event (
	id	varchar2(40)	not null,
	catalog_ref_id	varchar2(40)	null,
	old_quantity	number(19,0)	null,
	new_quantity	number(19,0)	null
,constraint csrt_gi_event_p primary key (id));




