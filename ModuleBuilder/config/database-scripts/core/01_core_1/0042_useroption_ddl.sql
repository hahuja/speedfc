


-- /atg/svc/option/UserOptionRepository
--     It is a good idea to dedicate different tablespaces to different categories of tables. Mostly the categorization is on the basis of growth and volatility of data. The application tables can be grouped into the following categories:  
--     1) STATIC: Those that are kind of static in size like pricelists, catalogs etc. These undergo a change periodically.  
--     2) GROWTH: Those that grow at a faster rate with the passage of time and size of the business. They are mostly appended. Transaction tables, like orders is an example.  
--     3) UPDATE: Those that have a lot of insert and update activities.  
--     4) TEMPORARY: Those that have insert, update and deletes like messaging table (dms tables are an example of this)  
--     The type of table space type for each table is noted in the "Table Space Type" comment  
--     Table: svc_user_opt, Repository Item: UserOption    Table Space Type: GROWTH    These are defined by the users of the system.  Their default can be a site option or default option  

create table svc_user_opt (
	user_id	varchar2(40)	not null,
	option_id	varchar2(40)	not null,
	option_name	varchar2(80)	not null,
	option_value	varchar2(1666)	null,
	option_default	varchar2(1666)	null,
	option_right	varchar2(80)	null,
	option_data_type	number(10)	null,
	is_multival	number(1)	null
,constraint svcuseropt_p primary key (option_id)
,constraint svcuseropt1_un unique (user_id,option_name));

--     Table: svc_user_opt, Repository Item: UserOption, Property: info    Table Space Type: GROWTH    Additional data for a user option  

create table svc_useropt_info (
	option_id	varchar2(40)	not null,
	info_key	varchar2(40)	not null,
	info_value	varchar2(255)	not null
,constraint useroptinfo_p primary key (option_id,info_key)
,constraint useroptinfo_fk1 foreign key (option_id) references svc_user_opt (option_id));

--     Table: svc_user_opt, Repository Item: UserOption, Property: allowedValues    Table Space Type: GROWTH    Allowed values for options that are edited by an enumeration  

create table svc_useropt_val (
	option_id	varchar2(40)	not null,
	option_value	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint useroptval_p primary key (option_id,sequence_num)
,constraint useroptval_fk1 foreign key (option_id) references svc_user_opt (option_id));




