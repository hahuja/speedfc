


--     @version $Id: //application/service/version/10.1/common-pub/src/sql/svc_process_data_ddl.xml#1 $$Change: 683854 $  
--     Service Process Data.  

create table svc_process_data (
	asset_version	number(19)	not null,
	id	varchar2(40)	not null,
	wkf_type	varchar2(40)	not null
,constraint svc_proc_data_pk primary key (id,asset_version));




