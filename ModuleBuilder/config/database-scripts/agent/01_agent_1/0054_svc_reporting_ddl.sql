



create table svcr_sol_event (
	id	varchar2(40)	not null,
	solution_id	varchar2(40)	not null,
	project_id	varchar2(40)	null,
	owning_group_id	varchar2(40)	not null,
	organization_id	varchar2(40)	not null,
	status_id	varchar2(40)	not null,
	old_status_id	varchar2(40)	null,
	use_count	number(10)	not null,
	duration	number(19)	null,
	summarized	number(1)	default 0 null
,constraint svcr_sol_ev_p primary key (id));




