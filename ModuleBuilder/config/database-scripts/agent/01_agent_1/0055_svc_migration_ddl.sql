


--  @version $Id: //application/service/version/10.1/migration/src/sql/svc_migration_ddl.xml#1 $$Change: 683854 $
-- This file contains create table statements, which will configure your database for use with the service migration schema.
-- Tracks status of each migration step, per eServer knowledge base

create table svcm_step (
	id	varchar2(40)	not null,
	status	number(10)	not null,
	item_type	number(10)	not null,
	type	number(10)	not null,
	kb	varchar2(40)	not null,
	process_id	varchar2(40)	null
,constraint svcm_step_p primary key (id));

-- Tracks status of a batched migration step, per eServer knowledge baselast_id is the item id of the last item processed in the previous batch.

create table svcm_batch_step (
	id	varchar2(40)	not null,
	last_id	varchar2(40)	null,
	items_loaded	number(10)	null
,constraint svcm_batch_step_p primary key (id));

-- eServer group id to Service organization mapping

create table svcm_group (
	id	varchar2(40)	not null,
	eserver_id	number(10)	not null,
	org_id	varchar2(40)	not null,
	kb	varchar2(40)	not null
,constraint svcm_org_p primary key (id)
,constraint svcm_group_ix1 unique (eserver_id,kb));

-- eServer user id to Service user mapping

create table svcm_user (
	id	varchar2(40)	not null,
	eserver_id	number(10)	not null,
	user_id	varchar2(40)	not null,
	kb	varchar2(40)	not null
,constraint svcm_user_p primary key (id)
,constraint svcm_user_ix1 unique (eserver_id,kb));

-- Profile ids for looking up favorite solutions

create table svcm_user_favs (
	id	varchar2(40)	not null,
	profile_id	varchar2(40)	not null,
	kb	varchar2(40)	not null
,constraint svcm_userfavs_p primary key (id)
,constraint svcm__ix1 unique (profile_id,kb));

-- Favorite solution ids for users

create table svcm_userfavs_m (
	id	varchar2(40)	not null,
	solution_id	varchar2(80)	not null,
	fav_order	varchar2(40)	not null
,constraint svcuserfavsm_p primary key (id,fav_order)
,constraint userfavs_fk1 foreign key (id) references svcm_user_favs (id));

-- Security segments for statements from eServer

create table svcm_stmt_security (
	id	varchar2(40)	not null,
	sm_id	number(10)	not null,
	segment	varchar2(255)	not null,
	type	number(10)	not null,
	kb	varchar2(40)	not null
,constraint svcm_stmtsec_p primary key (id)
,constraint svcm_stmtsec_ix1 unique (sm_id,type,kb));

-- Security segments for named ACLs from eServer

create table svcm_named_acl (
	id	varchar2(40)	not null,
	acl_id	number(10)	not null,
	type	number(10)	not null,
	ace_type	number(10)	not null,
	segment	varchar2(255)	not null,
	kb	varchar2(40)	not null
,constraint svcm_namacl_p primary key (id)
,constraint svcm_namacl_ix1 unique (acl_id,ace_type,type,kb));

create index svcm_namacl_ix2 on svcm_named_acl (acl_id,kb);
create index svcm_namacl_ix3 on svcm_named_acl (acl_id,type,kb);
create index svcm_namacl_ix4 on svcm_named_acl (acl_id,ace_type,kb);
-- Eserver solution ids mapped to Service solution ids

create table svcm_fav_solutions (
	id	varchar2(40)	not null,
	solution_id	varchar2(80)	not null,
	new_id	varchar2(40)	null,
	kb	varchar2(40)	not null
,constraint svcm_favsols_p primary key (id)
,constraint svcm_fav_sol_ix1 unique (solution_id,kb));

-- eServer property tag to SolutionField ids

create table svcm_property (
	id	varchar2(40)	not null,
	prop_tag	varchar2(40)	not null,
	field_id	varchar2(40)	not null,
	kb	varchar2(40)	not null
,constraint svcm_property_p primary key (id)
,constraint svcm_property_ix1 unique (prop_tag,kb));

-- Eserver solution class ids mapped to Service solution class ids

create table svcm_soln_class (
	id	varchar2(40)	not null,
	class_id	number(10)	not null,
	new_id	varchar2(40)	null,
	kb	varchar2(40)	not null
,constraint svcm_solnclass_p primary key (id)
,constraint svcm_solnclass_ix1 unique (class_id,kb));

-- Multi table for properties on a solution class

create table svcm_classprops (
	id	varchar2(40)	not null,
	prop_tag	varchar2(40)	not null
,constraint svcm_classprops_p primary key (id,prop_tag)
,constraint solnclss_fk1 foreign key (id) references svcm_soln_class (id));

-- eServer enum ids to org ids for owning groups

create table svcm_og_val_map (
	id	varchar2(40)	not null,
	enum_id	number(10)	not null,
	org_id	varchar2(40)	not null,
	kb	varchar2(40)	not null
,constraint svcm_ogvaluemap_p primary key (id)
,constraint svcm_og_val_ix1 unique (org_id,kb));

-- File attachments that have been downloaded

create table svcm_attachments (
	id	varchar2(40)	not null,
	file_name	varchar2(255)	not null,
	url	varchar2(1024)	not null,
	new_file_name	varchar2(255)	not null,
	is_image	number(1)	not null,
	kb	varchar2(40)	not null,
	is_deploy	number(1)	null
,constraint svcm_attach_p primary key (id));

create index svcm_attach_ix1 on svcm_attachments (file_name);



