


--     @version $Id: //application/service/version/10.1/common-pub/src/sql/svc_internal_user_ddl.xml#1 $$Change: 683854 $  
--     Service StatusAccessRight  

create table svc_status_right (
	id	varchar2(40)	not null,
	status_id	varchar2(40)	not null,
	baseright_id	varchar2(40)	not null
,constraint svc_sts_right_pk primary key (id)
,constraint svc_sts_right_fk foreign key (id) references dpi_access_right (access_right_id));




