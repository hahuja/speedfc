


--  @version $Id: //application/DCS-CSR/version/10.1/src/sql/DCS-CSR_logging_ddl.xml#1 $$Change: 683854 $

create table csr_grant_appease (
	id	varchar2(40)	not null,
	appeasement_id	varchar2(40)	null,
	amount	number(19,7)	null
,constraint csr_grnt_apps_p primary key (id));


create table csr_price_override (
	id	varchar2(40)	not null,
	order_id	varchar2(40)	null,
	component_id	varchar2(40)	null,
	component_type	varchar2(40)	null,
	old_price	number(19,7)	null,
	new_price	number(19,7)	null
,constraint csr_prc_ovrrd_p primary key (id));


create table csr_order_event (
	id	varchar2(40)	not null,
	order_id	varchar2(40)	null,
	amount	number(19,7)	null
,constraint csr_ord_ev_p primary key (id));


create table csr_return_order (
	id	varchar2(40)	not null,
	ret_req_id	varchar2(40)	null,
	repl_order_id	varchar2(40)	null
,constraint csr_ret_ord_p primary key (id));


create table csr_recv_rtrn_item (
	id	varchar2(40)	not null,
	item_id	varchar2(40)	null,
	quantity	number(19,0)	null
,constraint csr_rcv_rt_it_p primary key (id));


create table csr_claim_item (
	id	varchar2(40)	not null,
	claimable_id	varchar2(40)	null,
	claimable_type	varchar2(40)	null
,constraint csr_claim_item_p primary key (id));


create table csr_ci_event (
	id	varchar2(40)	not null,
	item_id	varchar2(40)	null,
	old_quantity	number(19,0)	null,
	new_quantity	number(19,0)	null
,constraint csr_comm_item_p primary key (id));


create table csr_pg_event (
	id	varchar2(40)	not null,
	pay_group_id	varchar2(40)	null,
	update_type	number(1)	not null
,constraint csr_pay_grp_p primary key (id));


create table csr_split_sg (
	id	varchar2(40)	not null,
	src_ship_group_id	varchar2(40)	null,
	dest_ship_group_id	varchar2(40)	null,
	commerce_item_id	varchar2(40)	null,
	quantity	number(10)	not null
,constraint csr_splt_sg_p primary key (id));


create table csr_split_cc (
	id	varchar2(40)	not null,
	src_cost_ctr_id	varchar2(40)	null,
	dest_cost_ctr_id	varchar2(40)	null,
	commerce_ident_id	varchar2(40)	null,
	quantity	number(10)	not null
,constraint csr_splt_cc_p primary key (id));


create table csr_sg_event (
	id	varchar2(40)	not null,
	ship_group_id	varchar2(40)	null,
	update_type	number(10)	not null
,constraint csr_sg_evnt_p primary key (id));


create table csr_upd_props (
	id	varchar2(40)	not null,
	audit_id	varchar2(40)	null,
	property_name	varchar2(40)	null,
	old_value	varchar2(255)	null,
	new_value	varchar2(255)	null,
	version	number(10)	not null
,constraint csr_upd_prof_p primary key (id));


create table csr_order_comment (
	id	varchar2(40)	not null,
	comment_id	varchar2(40)	null
,constraint csr_ord_cmnt_p primary key (id));


create table csr_view_card (
	id	varchar2(40)	not null,
	cc_number	varchar2(20)	null
,constraint csr_view_card_p primary key (id));


create table csr_oma_event (
	id	varchar2(40)	not null,
	man_adj_id	varchar2(40)	null,
	adjustment_type	integer	not null,
	update_type	number(1)	not null,
	reason	integer	null
,constraint csr_oma_event_p primary key (id));


create table csr_schd_event (
	id	varchar2(40)	not null,
	sch_order_id	varchar2(40)	null,
	update_type	number(1)	not null
,constraint csr_schd_event_p primary key (id));


create table csr_appr_event (
	id	varchar2(40)	not null,
	approval_id	varchar2(40)	not null,
	update_type	number(10)	not null
,constraint csr_appr_event_p primary key (id)
,constraint csrapprevent_f foreign key (id) references agent_audit (id));


create table csr_order_appr_event (
	id	varchar2(40)	not null,
	order_id	varchar2(40)	not null
,constraint csrordapprevt_p primary key (id)
,constraint csrordapprevt_f foreign key (id) references csr_appr_event (id));


create table csr_grt_prom_event (
	id	varchar2(40)	not null,
	promo_id	varchar2(40)	null
,constraint csr_grprm_ev_p primary key (id));


create table csr_ign_prom_event (
	id	varchar2(40)	not null,
	promo_id	varchar2(40)	null,
	order_id	varchar2(40)	null
,constraint csr_igprm_ev_p primary key (id));


create table csr_gl_event (
	id	varchar2(40)	not null,
	giftlist_id	varchar2(40)	null,
	event_name	varchar2(64)	null
,constraint csr_gl_event_p primary key (id));


create table csr_gi_event (
	id	varchar2(40)	not null,
	catalog_ref_id	varchar2(40)	null,
	old_quantity	number(19,0)	null,
	new_quantity	number(19,0)	null
,constraint csr_gi_event_p primary key (id));




