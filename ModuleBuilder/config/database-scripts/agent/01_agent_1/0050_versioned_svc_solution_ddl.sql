


-- /atg/svc/ServiceRepository
--     It is a good idea to dedicate different tablespaces to different categories of tables. Mostly the categorization is on the basis of growth and volatility of data. The application tables can be grouped into the following categories:  
--   1) STATIC: Those that are kind of static in size like pricelists, catalogs etc. These undergo a change periodically.
--   2) GROWTH: Those that grow at a faster rate with the passage of time and size of the business. They are mostly appended. Transaction tables, like orders is an example.
--   3) UPDATE: Those that have a lot of insert and update activities.
--   4) TEMPORARY: Those that have insert, update and deletes like messaging table (dms tables are an example of this)
--   The type of table space type for each table is noted in the "Table Space Type" comment
--     Table: svc_org_value, Repository Item: OrganizationValue    Table Space Type: STATIC    Wraps an organization for use in owning group, internal audience and custom organization properites.  

create table svc_org_value (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	org_value_id	varchar2(40)	not null,
	org_name	varchar2(255)	not null,
	org_id	varchar2(40)	not null,
	org_value_type	number(10)	not null
,constraint svcorgvalue_p primary key (org_value_id,asset_version));

create index svc_org_value_wsx on svc_org_value (workspace_id);
create index svc_org_value_cix on svc_org_value (checkin_date);
--     Table: svc_orgval_intaud, Repository Item: OrganizationValue.internalAudience    Table Space Type: STATIC    internal audience (segments) that are related to an organization value.  

create table svc_orgval_intaud (
	asset_version	number(19)	not null,
	org_value_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint svcorgvalintaud_p primary key (org_value_id,sequence_num,asset_version));

--     Table: svc_fld_defn, Repository Item: FieldDefinition    Table Space Type: STATIC    Defines default behavior for a field.  A field may appear in multiple solution/problem classes and can be given solution/problem class specific behavior via svc_scls_fld_defn or svc_pcls_fld_defn  

create table svc_fld_defn (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	fld_defn_id	varchar2(40)	not null,
	display_name	varchar2(255)	not null,
	fld_type	number(10)	not null,
	fld_name	varchar2(255)	not null,
	is_required	number(1)	not null,
	is_multival	number(1)	not null,
	is_default	number(1)	null,
	fld_purpose	number(10)	not null,
	fld_indexing	number(10)	not null,
	fld_usage	number(10)	not null,
	fld_editor	number(10)	not null
,constraint svc_fld_defn_p primary key (fld_defn_id,asset_version));

create index svc_fld_defn_un1 on svc_fld_defn (fld_name);
create index svc_fld_defn_wsx on svc_fld_defn (workspace_id);
create index svc_fld_defn_cix on svc_fld_defn (checkin_date);
--     Table: svc_fldtype_data, Repository Item: FieldDefinitionTypeMetadata    Table Space Type: STATIC    Default values for field definition types such as should they be multivalued, content vs. property, etc.  

create table svc_fldtype_data (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	fldtype_data_id	varchar2(40)	not null,
	fld_type	varchar2(40)	not null,
	is_required	number(1)	not null,
	is_multival	number(1)	not null,
	is_editmultival	number(1)	not null,
	fld_purpose	varchar2(40)	not null,
	is_editpurpose	number(1)	not null,
	fld_indexing	varchar2(40)	not null,
	is_editindexing	number(1)	not null
,constraint svcfldtypedata_p primary key (fldtype_data_id,asset_version));

create index svcfldtypedata_un1 on svc_fldtype_data (fld_type);
create index svc_fldtype_da_wsx on svc_fldtype_data (workspace_id);
create index svc_fldtype_da_cix on svc_fldtype_data (checkin_date);
--     Table: svc_default_val, Repository Item: DefaultFieldValue    Table Space Type: STATIC    svc_default_val: represents a default for a field definition value.  

create table svc_default_val (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	value_id	varchar2(40)	not null,
	fld_defn_id	varchar2(40)	not null,
	display_order	number(10)	not null,
	bool_val	number(1)	null,
	date_val	date	null,
	number_val	number(19,6)	null,
	string_val	clob	null
,constraint svcdefaultval_p primary key (value_id,asset_version));

create index svcdefaultval_ix1 on svc_default_val (fld_defn_id);
create index svc_default_va_wsx on svc_default_val (workspace_id);
create index svc_default_va_cix on svc_default_val (checkin_date);
--     Table: svc_list_value, Repository Item: EnumeratedListValue    Table Space Type: STATIC    svc_list_value - defines the range of values for a FieldDefinition that is a List type.  

create table svc_list_value (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	value_id	varchar2(40)	not null,
	name	varchar2(255)	not null,
	display_name	varchar2(255)	not null
,constraint svc_list_value_p primary key (value_id,asset_version));

create index svclistvalue_ix1 on svc_list_value (name);
create index svc_list_value_wsx on svc_list_value (workspace_id);
create index svc_list_value_cix on svc_list_value (checkin_date);
--     Table: svc_flddefn_lval, Repository Item: ListFieldDefinition.listValues    Table Space Type: STATIC    Linking table for field definition to list value.  

create table svc_flddefn_lval (
	asset_version	number(19)	not null,
	value_id	varchar2(40)	not null,
	fld_defn_id	varchar2(40)	not null,
	display_order	number(10)	not null
,constraint svc_flddefn_lval_p primary key (value_id,fld_defn_id,asset_version));

create index svc_flddeflval_ix2 on svc_flddefn_lval (fld_defn_id);
--     Table: svc_flddefn_bool, Repository Item: BooleanFieldDefinition.falseLabel    Table Space Type: STATIC    Defines a label for a boolean value for a boolean field definition  

create table svc_flddefn_bool (
	asset_version	number(19)	not null,
	label_id	varchar2(40)	not null,
	true_label	varchar2(255)	not null,
	false_label	varchar2(255)	not null
,constraint svcflddefnbool_p primary key (label_id,asset_version));

--     Table: svc_flddefn_intaud, Repository Item: FieldDefinition.internalAudience    Table Space Type: STATIC    svc_flddefn_intaud: internal audience (segments) that are related to a field definition.  

create table svc_flddefn_intaud (
	asset_version	number(19)	not null,
	fld_defn_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint flddefnintaud_p primary key (fld_defn_id,sequence_num,asset_version));

--     Table: svc_flddefn_intmod, Repository Item: FieldDefinition.internalModify    Table Space Type: STATIC    svc_flddefn_intmod: internal modify (segments) that are related to a field definition.  

create table svc_flddefn_intmod (
	asset_version	number(19)	not null,
	fld_defn_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint flddefnintmod_p primary key (fld_defn_id,sequence_num,asset_version));

--     Table: svc_flddefn_extaud, Repository Item: FieldDefinition.externalAudience    Table Space Type: STATIC    svc_flddefn_extaud: external audience (segments) that are related to a field definition.  

create table svc_flddefn_extaud (
	asset_version	number(19)	not null,
	fld_defn_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint flddefnextaud_p primary key (fld_defn_id,sequence_num,asset_version));

--     Table: svc_flddefn_seg, Repository Item: UserFieldDefinition    Table Space Type: STATIC    svc_flddefn_seg: user field definition segment filter  

create table svc_flddefn_seg (
	asset_version	number(19)	not null,
	fld_defn_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint svcflddefnseg_p primary key (fld_defn_id,sequence_num,asset_version));

--     Table: svc_flddefn_list, Repository Item: ListFieldDefinition.sortOrder    Table Space Type: STATIC    Defines the sort order of the list values.  Auxiliary table that could be used to store other properties    about the list field definition.  

create table svc_flddefn_list (
	asset_version	number(19)	not null,
	fld_defn_id	varchar2(40)	not null,
	sort_order	number(10)	null
,constraint svcflddefnlist_p primary key (fld_defn_id,asset_version));

--     Table: svc_lval_intaud, Repository Item: EnumeratedListValue.internalAudience    Table Space Type: STATIC    svc_list_intaud: internal audience (segments) that are related to a list value.  

create table svc_lval_intaud (
	asset_version	number(19)	not null,
	value_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint svclvalintaud_p primary key (value_id,sequence_num,asset_version));

--     Table: svc_soln_class, Repository Item: SolutionClass    Table Space Type: STATIC    A solution class defines the fields in a given solution.  Every solution must have exactly one solution class.  

create table svc_soln_class (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	class_id	varchar2(40)	not null,
	class_name	varchar2(255)	not null,
	class_desc	varchar2(255)	null,
	workflow_name	varchar2(100)	not null
,constraint svc_soln_class_p primary key (class_id,asset_version));

create index svc_soln_class_wsx on svc_soln_class (workspace_id);
create index svc_soln_class_cix on svc_soln_class (checkin_date);
--     Table: svc_soln_class, Repository Item: SolutionClass.internalAudience    Table Space Type: STATIC    Solution class internal audience segments.  

create table svc_scls_intaud (
	asset_version	number(19)	not null,
	class_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint svcsclsintaud_p primary key (class_id,sequence_num,asset_version));

--     Table: svc_scls_fld_defn, Repository Item: SolutionClassFieldDefinition    Table Space Type: STATIC    Defines field behavior for a given solution class. Overrides global field definition  

create table svc_scls_fld_defn (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	fld_defn_class_id	varchar2(40)	not null,
	fld_defn_id	varchar2(40)	not null,
	display_name	varchar2(255)	not null,
	is_required	number(1)	null,
	is_title	number(1)	not null,
	indexing_value	varchar2(40)	null,
	srch_weight	number(10)	null
,constraint svcsclfldefn_p primary key (fld_defn_class_id,asset_version));

create index svcsclfldefn1_ix on svc_scls_fld_defn (fld_defn_id);
create index svc_scls_fld_d_wsx on svc_scls_fld_defn (workspace_id);
create index svc_scls_fld_d_cix on svc_scls_fld_defn (checkin_date);
--     Table: svc_scls_fld_defns, Repository Item: SolutionClass.fields    Table Space Type: STATIC    Defines the relationship between a SolutionClass and SolutionClassFieldDefintion field behavior.  

create table svc_scls_fld_defns (
	asset_version	number(19)	not null,
	fld_defn_class_id	varchar2(40)	not null,
	class_id	varchar2(40)	not null,
	fld_order	number(10)	not null
,constraint svcsclfldefns_p primary key (fld_defn_class_id,class_id,asset_version));

create index svcsclfldefns2_ix on svc_scls_fld_defns (class_id);
--     Table: svc_soln_status, Repository Item: SolutionStatus    Table Space Type: STATIC    Enumerated list of solution states.  The status controls solution view and modify security  

create table svc_soln_status (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	status_id	varchar2(40)	not null,
	display_name	varchar2(255)	not null
,constraint svc_solnstatus_p primary key (status_id,asset_version));

create index svc_soln_statu_wsx on svc_soln_status (workspace_id);
create index svc_soln_statu_cix on svc_soln_status (checkin_date);
--     Table: svc_soln, Repository Item: Solution    Table Space Type: GROWTH  

create table svc_soln (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	soln_id	varchar2(40)	not null,
	legacy_id	varchar2(85)	null,
	raw_title	varchar2(2048)	null,
	class_id	varchar2(40)	not null,
	date_created	timestamp	not null,
	date_modified	timestamp	null,
	author	varchar2(254)	not null,
	modified_by	varchar2(254)	null,
	owning_group_id	varchar2(40)	null,
	language	varchar2(40)	not null,
	is_bestbet	number(1)	not null,
	recognition	varchar2(40)	null,
	status_id	varchar2(40)	null,
	externally_visible	number(1)	null,
	field_data	clob	null
,constraint svc_soln_p primary key (soln_id,asset_version));

create index svcsolnlegid_ix on svc_soln (legacy_id);
create index svc_soln1_ix on svc_soln (class_id);
create index svc_soln_wsx on svc_soln (workspace_id);
create index svc_soln_cix on svc_soln (checkin_date);
--     Table: svc_soln_topic, Repository Item: Solution.topicIds    Table Space Type: GROWTH    svc_soln_topic: relates a solution to zero or more topics.  

create table svc_soln_topic (
	asset_version	number(19)	not null,
	soln_id	varchar2(40)	not null,
	topic_id	varchar2(40)	not null
,constraint svc_soln_topic_p primary key (soln_id,topic_id,asset_version));

--     Table: svc_soln_int_aud, Repository Item: Solution.internalAudience    Table Space Type: GROWTH    svc_soln_int_aud: defines the internal audience OrganizationValue that can see the solution.  

create table svc_soln_int_aud (
	asset_version	number(19)	not null,
	soln_int_aud_id	number(10)	not null,
	soln_id	varchar2(40)	not null,
	org_value_id	varchar2(40)	not null
,constraint svc_soln_int_aud_p primary key (soln_id,soln_int_aud_id,asset_version));

create index svcsolnintaud2_ix on svc_soln_int_aud (org_value_id);
--     Table: svc_soln_int_mod, Repository Item: Solution.internalModify    Table Space Type: GROWTH    Defines the internal audience OrganizationValues that can modify the solution.    *** DEPRECATED ***  

create table svc_soln_int_mod (
	asset_version	number(19)	not null,
	soln_int_mod_id	varchar2(40)	not null,
	soln_id	varchar2(40)	not null,
	org_value_id	varchar2(40)	not null
,constraint svc_soln_int_mod_p primary key (soln_id,soln_int_mod_id,asset_version));

create index svcsolnintmod2_ix on svc_soln_int_mod (org_value_id);
--     Table: svc_soln_segment, Repository Item: Solution.audience    Table Space Type: GROWTH    Defines the external audience segments that may view the solution  

create table svc_soln_segment (
	asset_version	number(19)	not null,
	soln_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint svc_soln_seg_p primary key (soln_id,sequence_num,asset_version));

--     Table: svc_qoaa, Repository Item: QOAA    Table Space Type: STATIC    Questions Others Are Asking (a.k.a. Best Bets)  

create table svc_qoaa (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	id	varchar2(40)	not null,
	soln_id	varchar2(40)	not null,
	title	varchar2(1000)	null
,constraint svc_solnqoaa_p primary key (id,asset_version));

create index svc_solnqoaa_su on svc_qoaa (soln_id);
create index svc_qoaa_wsx on svc_qoaa (workspace_id);
create index svc_qoaa_cix on svc_qoaa (checkin_date);
--     Table: svc_tf_param, Repository Item: TransactionalFragmentParameter    Table Space Type: STATIC    Defines a transactional fragment parameter to be filled in by the author on the field  

create table svc_tf_param (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	tf_param_id	varchar2(40)	not null,
	param_name	varchar2(40)	not null,
	param_label	varchar2(255)	not null,
	default_value	varchar2(1666)	null,
	fld_type	number(10)	not null,
	is_hidden	number(1)	null
,constraint svc_tf_param_p primary key (tf_param_id,asset_version));

create index svc_tf_param_un1 on svc_tf_param (param_name);
create index svc_tf_param_wsx on svc_tf_param (workspace_id);
create index svc_tf_param_cix on svc_tf_param (checkin_date);
--     Table: svc_tfparam_lval, Repository Item: TransactionalFragmentParameterListValue    Table Space Type: STATIC    Transaction Fragment list parameter value  

create table svc_tfparam_lval (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	tfparam_lval_id	varchar2(40)	not null,
	display_name	varchar2(255)	not null,
	param_value	varchar2(255)	not null
,constraint svctfprmlval_p primary key (tfparam_lval_id,asset_version));

create index svc_tfparam_lv_wsx on svc_tfparam_lval (workspace_id);
create index svc_tfparam_lv_cix on svc_tfparam_lval (checkin_date);
--     Table: svc_tfplval_lnk, Repository Item: TransactionalFragmentFieldValue.parameters    Table Space Type: STATIC    Linking table between a transactional fragment parameter and transactional fragment parameter list value  

create table svc_tfplval_lnk (
	asset_version	number(19)	not null,
	tf_param_id	varchar2(40)	not null,
	tfparam_lval_id	varchar2(40)	not null,
	list_order	number(10)	not null
,constraint svctfplvallnkp primary key (tf_param_id,tfparam_lval_id,asset_version));

create index svctfplvallnk_ix1 on svc_tfplval_lnk (tfparam_lval_id);
--     Table: svc_mktg_items, Repository Item: MarketingItem    Table Space Type: STATIC    Marketing Items table for self-service marketing panel  

create table svc_mktg_items (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1)	not null,
	version_deleted	number(1)	not null,
	version_editable	number(1)	not null,
	pred_version	number(19)	null,
	checkin_date	date	null,
	id	varchar2(40)	not null,
	name	varchar2(100)	not null,
	description	varchar2(1000)	null,
	image_url	varchar2(1000)	null,
	link_url	varchar2(1000)	null,
	startdate	date	not null,
	enddate	date	not null,
	link_target	varchar2(255)	null
,constraint svc_mktg_items_p primary key (id,asset_version));

create index svc_mktg_items_wsx on svc_mktg_items (workspace_id);
create index svc_mktg_items_cix on svc_mktg_items (checkin_date);
--     Table: svc_mktg_segments, Repository Item: MarketingItem.segments    Table Space Type: STATIC    Table for holding (marketing) segments property for the user profile which     is a multi-value persistent property.  

create table svc_mktg_segments (
	asset_version	number(19)	not null,
	id	varchar2(40)	not null,
	segment_name	varchar2(255)	not null
,constraint svc_sgmt_p primary key (id,segment_name,asset_version));




