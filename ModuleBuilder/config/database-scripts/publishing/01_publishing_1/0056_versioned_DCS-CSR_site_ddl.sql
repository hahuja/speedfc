


--  @version $Id: //application/DCS-CSR/version/10.1/Management/src/sql/versioned_DCS-CSR_site_ddl.xml#1 $$Change: 683854 $
--  CSC Site Configuration          

create table csr_site_config (
	asset_version	number(19)	not null,
	id	varchar2(40)	not null,
	dis_priority	number(10)	null
,constraint csr_site_config_p primary key (ID,asset_version));




