


-- /atg/svc/userprofiling/ServiceSegmentRepository
--     It is a good idea to dedicate different tablespaces to different categories of tables. Mostly the categorization is on the basis of growth and volatility of data. The application tables can be grouped into the following categories:  
--     1) STATIC: Those that are kind of static in size like pricelists, catalogs etc. These undergo a change periodically.  
--     2) GROWTH: Those that grow at a faster rate with the passage of time and size of the business. They are mostly appended. Transaction tables, like orders is an example.  
--     3) UPDATE: Those that have a lot of insert and update activities.  
--     4) TEMPORARY: Those that have insert, update and deletes like messaging table (dms tables are an example of this)  
--     The type of table space type for each table is noted in the "Table Space Type" comment  
--     Table: svc_segment, Repository Item: Segment    Table Space Type: STATIC    Wraps an segment (profile group) for use with Service.  

create table svc_segment (
	segment_id	varchar2(40)	not null,
	segment_name	varchar2(255)	not null,
	is_internal	number(1)	not null,
	is_field	number(1)	not null,
	is_value	number(1)	not null
,constraint svc_segment_p primary key (segment_id));

--     Table: svc_seg_intaud, Repository Item: Segment.internalAudience    Table Space Type: STATIC    Yes, it is true we secure segments with segments  

create table svc_seg_intaud (
	segment_id	varchar2(40)	not null,
	nucleus_name	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint svcsegintaud_p primary key (segment_id,sequence_num));




