


-- /atg/svc/option/OptionRepository
--     It is a good idea to dedicate different tablespaces to different categories of tables. Mostly the categorization is on the basis of growth and volatility of data. The application tables can be grouped into the following categories:  
--     1) STATIC: Those that are kind of static in size like pricelists, catalogs etc. These undergo a change periodically.  
--     2) GROWTH: Those that grow at a faster rate with the passage of time and size of the business. They are mostly appended. Transaction tables, like orders is an example.  
--     3) UPDATE: Those that have a lot of insert and update activities.  
--     4) TEMPORARY: Those that have insert, update and deletes like messaging table (dms tables are an example of this)  
--     The type of table space type for each table is noted in the "Table Space Type" comment  
--     Table: svc_global_opt, Repository Item: GlobalOption    Table Space Type: STATIC    Global option setting for all self service sites.  

create table svc_global_opt (
	option_id	varchar2(40)	not null,
	option_name	varchar2(80)	not null,
	option_value	clob	null,
	option_default	clob	null,
	option_right	varchar2(80)	null,
	option_data_type	number(10)	null,
	is_multival	number(1)	null
,constraint svc_globalopt_p primary key (option_id));

--     Table: svc_globalopt_info, Repository Item: GlobalOption.info    Table Space Type: STATIC    Additional data for a site option  

create table svc_globalopt_info (
	option_id	varchar2(40)	not null,
	info_key	varchar2(40)	not null,
	info_value	varchar2(255)	not null
,constraint globaloptinfo_p primary key (option_id,info_key));

--     Table: svc_globalopt_val, Repository Item: GlobalOption.allowedValues    Table Space Type: STATIC    Allowed values for options that are edited by an enumeration  

create table svc_globalopt_val (
	option_id	varchar2(40)	not null,
	option_value	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint globaloptval_p primary key (option_id,sequence_num));

--     Table: svc_ss_site_opt, Repository Item: SelfServiceSiteOption    Table Space Type: STATIC    Options per self service site  

create table svc_ss_site_opt (
	site_id	varchar2(255)	not null,
	option_id	varchar2(40)	not null,
	option_name	varchar2(80)	not null,
	option_value	clob	null,
	option_default	clob	null,
	option_right	varchar2(80)	null,
	option_data_type	number(10)	null,
	is_multival	number(1)	null
,constraint svc_ss_siteopt_p primary key (option_id));

create index svc_ss_siteopt_un on svc_ss_site_opt (site_id,option_name);
--     Table: svc_ss_siteopt_info, Repository Item: SelfServiceSiteOption.info    Table Space Type: STATIC    Additional data for a self service site option  

create table svc_ss_siteopt_info (
	option_id	varchar2(40)	not null,
	info_key	varchar2(40)	not null,
	info_value	varchar2(255)	not null
,constraint ss_siteoptinfo_p primary key (option_id,info_key));

--     Table: svc_ss_siteopt_val, Repository Item: SelfServiceSiteOption.allowedValues    Table Space Type: STATIC    Allowed values for options that are edited by an enumeration  

create table svc_ss_siteopt_val (
	option_id	varchar2(40)	not null,
	option_value	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint ss_siteoptval_p primary key (option_id,sequence_num));

--     Table: svc_seg_pri, Repository Item: SiteSegmentPriority    Table Space Type: STATIC    Defines the priority of segments for use with SiteSegmentOption when user is a member of multiple segment option settings defined within a site.  

create table svc_site_seg_pri (
	priority_id	varchar2(40)	not null,
	site_id	varchar2(40)	not null,
	segment_name	varchar2(255)	not null,
	segment_priority	number(10)	not null
,constraint svc_seg_pri_p primary key (priority_id));

create index svc_seg_pri_un1 on svc_site_seg_pri (segment_name,site_id);
--     Table: svc_site_seg_opt, Repository Item: SiteSegmentOption    Table Space Type: STATIC    Options per segment per self service site  

create table svc_site_seg_opt (
	option_id	varchar2(40)	not null,
	site_seg_pri_id	varchar2(40)	not null,
	option_name	varchar2(80)	not null,
	option_value	clob	null,
	option_default	clob	null,
	option_right	varchar2(80)	null,
	option_data_type	number(10)	null,
	is_multival	number(1)	null
,constraint svc_site_seg_opt_p primary key (option_id));

create index svc_siteseg_opt_un on svc_site_seg_opt (option_name,site_seg_pri_id);
--     Table: svc_sitesegopt_info, Repository Item: SiteSegmentOption.info    Table Space Type: STATIC    Additional data for a site segment option  

create table svc_sitesegopt_info (
	option_id	varchar2(40)	not null,
	info_key	varchar2(40)	not null,
	info_value	varchar2(255)	not null
,constraint sitesegoptinfo_p primary key (option_id,info_key));

--     Table: svc_ss_siteopt_val, Repository Item: SiteSegmentOption.allowedValues    Table Space Type: STATIC    Allowed values for options that are edited by an enumeration  

create table svc_sitesegopt_val (
	option_id	varchar2(40)	not null,
	option_value	varchar2(255)	not null,
	sequence_num	number(10)	not null
,constraint sitesegoptval_p primary key (option_id,sequence_num));




